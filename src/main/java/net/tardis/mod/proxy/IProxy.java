package net.tardis.mod.proxy;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.tardis.mod.misc.GuiContext;

public interface IProxy {
	
	default void init() {}

	void openGUI(int id, GuiContext cont);

    void playMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol, boolean repeat);
    
    World getClientWorld();
    
    PlayerEntity getClientPlayer();
    
    public void forceThirdPerson();

    /**
     * Stops sound that's playing
     * @param event - Sound Event to stop
     * @param cate - Category to stop it in
     */
    default void shutTheFuckUp(SoundEvent event, SoundCategory cate) {}

}
