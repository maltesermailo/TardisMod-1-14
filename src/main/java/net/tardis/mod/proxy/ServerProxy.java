package net.tardis.mod.proxy;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.tardis.mod.misc.GuiContext;

public class ServerProxy implements IProxy {
	
	@Override
	public void openGUI(int guiId, GuiContext cont) {}
	
    @Override
	public void playMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol, boolean repeat) {
	}

	@Override
	public World getClientWorld() {
		throw new IllegalStateException("Only run this on the client!");
	}

	@Override
	public PlayerEntity getClientPlayer() {
		// TODO Auto-generated method stub
		throw new IllegalStateException("Only run this on the client!");
	}

	@Override
	public void forceThirdPerson() {
		// TODO Auto-generated method stub
		
	}

}
