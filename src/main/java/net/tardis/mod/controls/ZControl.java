package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class ZControl extends BaseControl{
	
	public ZControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if (this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		if(getConsole() instanceof CoralConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);

		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			console.setDestination(console.getDestinationDimension(), console.getDestination()
				.add(0, 0, player.isSneaking() ? -console.getCoordIncr() : console.getCoordIncr()));
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-6 / 16.0, 10 / 16.0, 6 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.4059090875328474, 0.625, 0.3479865154450874);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.22546609302967927, 0.5, -0.7);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.7824804304370165, 0.5625, 0.5757301742949457);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.0842288148454582, 0.4375, 0.14702252220259904);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.4806918248077612, 0.71875, 0.4727898888202058);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.7536695293401995, 0.5625, 0.295);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.9560655342761755, 0.49, 0.8645582692692724);
		
		
		return new Vec3d(-8.85 / 16.0, 9.85 / 16.0, -3.35 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
