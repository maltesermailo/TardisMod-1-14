package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;

public interface IControl extends INBTSerializable<CompoundNBT>, IRegisterable<IControl>{

	EntitySize getSize();
	
	/*
	 * Returns true if whatever the point of this control worked
	 */
	boolean onRightClicked(ConsoleTile console, PlayerEntity player);
	/*
	 * Offset from the middle of the console block
	 */
	Vec3d getPos();
	default void onHit(ConsoleTile console, PlayerEntity player) {
		this.onRightClicked(console, player);
	}
	
	void setConsole(ConsoleTile console);
	ConsoleTile getConsole();

	SoundEvent getFailSound(ConsoleTile console);

	SoundEvent getSuccessSound(ConsoleTile console);

	TranslationTextComponent getDisplayName();
	
	int getAnimationTicks();
	void setAnimationTicks(int ticks);
	int getMaxAnimationTicks();
	float getAnimationProgress();
	
	void onPacketUpdate();
	
	void markDirty();
	void clean();
	boolean isDirty();
	
	boolean isGlowing();
	void setGlow(boolean glow);

}
