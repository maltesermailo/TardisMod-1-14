package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class RealWorldFlightControl extends BaseControl {

	public RealWorldFlightControl(ConsoleTile console) {
		super(console);
	}
	
	/*
	 * True if brake is off and TARDIS can fly
	 */
	private boolean isRealWorldFlight = false;

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile || this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console == null || !console.hasWorld())
			return false;
		if(!console.getWorld().isRemote) {
			this.isRealWorldFlight = !this.isRealWorldFlight;
			this.markDirty();
			
			String realWorldFlightStatus = new TranslationTextComponent(this.isRealWorldFlight ? 
					"message.tardis.control.real_world_flight_enabled" : "message.tardis.control.real_world_flight_disabled").getFormattedText();
			
			player.sendStatusMessage(new StringTextComponent(realWorldFlightStatus), true);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vec3d(-0.26235428418574, 0.5, -0.8356870917823487);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.7540996966019162, 0.375, -0.11433737548021605);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.6830937732580076, 0.53125, 0.3992647269693531);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.6931968059961732, 0.5, -0.30326668755810876);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.6116690047969111, 0.4375, 0.677963662404454);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.6503619239419756, 0.65625, 0.5581014002800839);
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(-0.18449380685298444, 0.4375, -0.9292019958460777);
		return new Vec3d(-0.7441079011068746, 0.53125, -0.42926772282866826);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}
	
	public boolean isRealWorldFlight() {
		return isRealWorldFlight;
	}
	
	public void setRealWorldFlight(boolean isRealWorldFlight) {
		this.isRealWorldFlight = isRealWorldFlight;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("realWorldFlight", this.isRealWorldFlight);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.isRealWorldFlight = nbt.getBoolean("realWorldFlight");
	}

}
