package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class CommunicatorControl extends BaseControl implements ITickable{
	
	//private static String TRANS = "status.tardis.communicator.located";

	public CommunicatorControl(ConsoleTile console) {
		super(console);
		if(this.getConsole() != null)
			this.getConsole().registerTicker(this);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.3F, 0.3F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile || this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.375F, 0.375F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);

		return EntitySize.flexible(0.4F, 0.4F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console.getWorld().isRemote && !console.getDistressSignals().isEmpty())
			Tardis.proxy.openGUI(Constants.Gui.COMMUNICATOR, null);
		if(!console.getWorld().isRemote)
			this.startAnimation();
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(6 / 16.0, 12 / 16.0, -4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.5577870538580397, 0.33124999701976776, -0.6428223124704939);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.70, 0.375, -0.1);
		}
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.23559568057373126, 0.59375, 0.713925883398866);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(-0.0908606167517797, 0.5937, -0.8449081117709261);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.9384990837486501, 0.5625, 0.7009343144566027);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(-0.725851740394144, 0.4375, -0.5289371401696679);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(-0.7855808358915296, 0.53125, -0.4541628113712102);
		
		return new Vec3d(0, 8 / 16.0, -6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return console instanceof NemoConsoleTile ? SoundEvents.BLOCK_BELL_USE : TSounds.COMMUNICATOR_STEAM;
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public void tick(ConsoleTile console) {
		
		if(console == null || console.getWorld() == null)
			return;
		
		if(!console.getWorld().isRemote && !console.getDistressSignals().isEmpty() && console.getWorld().getGameTime() % 100 == 0) {
			console.getWorld().playSound(null, console.getPos(), TSounds.COMMUNICATOR_BEEP, SoundCategory.BLOCKS, 1F, 1F);
			console.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
				if(!sys.canBeUsed())
					console.getDistressSignals().clear();
			});
		}
	}

}
