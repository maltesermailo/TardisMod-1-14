package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class IncModControl extends BaseControl{
	
	public static final String USE_TRANSLATION = "control.use." + Tardis.MODID + ".coord_inc";
	public static int[] COORD_MODS = new int[] {1, 10, 100, 1000};
	public int index;
	
	public IncModControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(4 / 16.0F,  4 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof XionConsoleTile)
		    return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		return EntitySize.flexible(3 / 16.0F, 2 / 16F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-13 / 16.0, 6 / 16.0F, 7 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.6071484912786207, 0.39374999701976776, 0.3642372909987306);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.5, 0.5625, 0.3175);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.3493324857597736, 0.59375, -0.21821192994449046);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.23779465119274457, 0.40625, 0.9133857377499579);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.7843703199635816, 0.625, 0.3790835380316553);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.5197218881192642, 0.46875, 0.9129873919696583);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.9851766344648096, 0.28125, 0.5559581802805318);
		
		return new Vec3d(-11 / 16.0, 7 / 16.0, -11 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			if(player.isSneaking()) {
				if(index - 1 < 0)
					index = COORD_MODS.length - 1;
				else index -= 1;
				console.coordIncr = COORD_MODS[index];
			}
			else {
				if(index + 1 >= COORD_MODS.length)
					index = 0;
				else index += 1;
				console.coordIncr = COORD_MODS[index];
				console.updateClient();
			}
			this.startAnimation();
			player.sendStatusMessage(new TranslationTextComponent(USE_TRANSLATION, console.coordIncr), true);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_TWO;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.index = tag.getInt("index");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("index", index);
		return tag;
	}

}
