package net.tardis.mod.controls;

import java.util.HashMap;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.tileentities.ConsoleTile;

public class ControlRegistry {
	
	private static HashMap<ResourceLocation, ControlEntry<?>> REGISTRY = new HashMap<ResourceLocation, ControlEntry<?>>();
	
	public static ControlEntry<ThrottleControl> THROTTLE;
	public static ControlEntry<HandbrakeControl> DEMAT;
	public static ControlEntry<RandomiserControl> RANDOM;
	public static ControlEntry<DimensionControl> DIMENSION;
	public static ControlEntry<FacingControl> FACING;
	public static ControlEntry<XControl> X;
	public static ControlEntry<YControl> Y;
	public static ControlEntry<ZControl> Z;
	public static ControlEntry<IncModControl> INC_MOD;
	public static ControlEntry<LandingTypeControl> LAND_TYPE;
	public static ControlEntry<RefuelerControl> REFUELER;
	public static ControlEntry<FastReturnControl> FAST_RETURN;
	public static ControlEntry<TelepathicControl> TELEPATHIC;
	public static ControlEntry<StabilizerControl> STABILIZERS;
	public static ControlEntry<SonicPortControl> SONIC_PORT;
	public static ControlEntry<CommunicatorControl> COMMUNICATOR;
	public static ControlEntry<DoorControl> DOOR;
	public static ControlEntry<MonitorControl> MONITOR;
	public static ControlEntry<RealWorldFlightControl> REAL_WORLD_FLIGHT;
	
	public static void register() {
		THROTTLE = register(new ResourceLocation(Tardis.MODID, "throttle"), new ControlEntry<ThrottleControl>(ThrottleControl::new));
		DEMAT = register(new ResourceLocation(Tardis.MODID, "demat"), new ControlEntry<HandbrakeControl>(HandbrakeControl::new));
		RANDOM = register(new ResourceLocation(Tardis.MODID, "randomizer"), new ControlEntry<RandomiserControl>(RandomiserControl::new));
		DIMENSION = register(new ResourceLocation(Tardis.MODID, "dimension"), new ControlEntry<DimensionControl>(DimensionControl::new));
		FACING = register(new ResourceLocation(Tardis.MODID, "facing"), ControlEntry.create(FacingControl::new));
		X = register(new ResourceLocation(Tardis.MODID, "x"), ControlEntry.create(XControl::new));
		Y = register(new ResourceLocation(Tardis.MODID, "y"), ControlEntry.create(YControl::new));
		Z = register(new ResourceLocation(Tardis.MODID, "z"), ControlEntry.create(ZControl::new));
		INC_MOD = register(new ResourceLocation(Tardis.MODID, "inc_mod"), ControlEntry.create(IncModControl::new));
		LAND_TYPE = register(new ResourceLocation(Tardis.MODID, "land_type"), ControlEntry.create(LandingTypeControl::new));
		REFUELER = register(new ResourceLocation(Tardis.MODID,"refueler"), ControlEntry.create(RefuelerControl::new));
		FAST_RETURN = register(new ResourceLocation(Tardis.MODID, "fast_return"), ControlEntry.create(FastReturnControl::new));
		TELEPATHIC = register(new ResourceLocation(Tardis.MODID, "telepathic"), ControlEntry.create(TelepathicControl::new));
		STABILIZERS = register(new ResourceLocation(Tardis.MODID, "stabilizers"), ControlEntry.create(StabilizerControl::new));
		SONIC_PORT = register(new ResourceLocation(Tardis.MODID, "sonic_port"), ControlEntry.create(SonicPortControl::new));
		COMMUNICATOR = register(new ResourceLocation(Tardis.MODID, "communicator"), ControlEntry.create(CommunicatorControl::new));
		DOOR = register(new ResourceLocation(Tardis.MODID, "door"), ControlEntry.create(DoorControl::new));
		MONITOR = register(new ResourceLocation(Tardis.MODID, "monitor"), ControlEntry.create(MonitorControl::new));
		REAL_WORLD_FLIGHT = register(new ResourceLocation(Tardis.MODID, "real_world_flight"), ControlEntry.create(RealWorldFlightControl::new));
	}
	
	public static <T extends IControl> ControlEntry<T> register(ResourceLocation name, ControlEntry<T> control) {
		REGISTRY.put(name, control.setRegistryName(name));
		return control;
	}
	
	public static <T extends IControl> ControlEntry<T> register(String name, ControlEntry<T> control) {
		ResourceLocation key = new ResourceLocation(Tardis.MODID, name);
		REGISTRY.put(key, control.setRegistryName(key));
		return control;
	}
	
	public static ControlEntry<?> getControl(ResourceLocation name){
		return REGISTRY.getOrDefault(name, null);
	}
	
	public static class ControlEntry<T extends IControl>{
		
		ResourceLocation registryName;
		IControlFactory<T> fact;
		
		public ControlEntry(IControlFactory<T> fact) {
			this.fact = fact;
		}
		
		ControlEntry<T> setRegistryName(ResourceLocation name){
			this.registryName = name;
			return this;
		}
		
		public static <T extends IControl> ControlEntry<T> create(IControlFactory<T> fact) {
			return new ControlEntry<T>(fact);
		}
		
		public ResourceLocation getRegistryName() {
			return this.registryName;
		}
		
		public T spawn(ConsoleTile console) {
			T control = fact.create(console);
			control.setRegistryName(registryName);
			return control;
		}
	}

    public interface IControlFactory<T extends IControl> {
		
		T create(ConsoleTile console);
	}

}
