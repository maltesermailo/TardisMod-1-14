package net.tardis.mod.misc;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class TexVariant {

	private ResourceLocation tex;
	private TranslationTextComponent translation;
	
	public TexVariant(ResourceLocation loc, String name) {
		this.tex = loc;
		this.translation = new TranslationTextComponent("texvar.tardis." + name);
	}
	
	public TexVariant(String loc, String name) {
		this(new ResourceLocation(Tardis.MODID, "textures/exteriors/" + loc + ".png"), name);
	}
	
	public ResourceLocation getTexture() {
		return this.tex;
	}
	
	public TranslationTextComponent getTranlation() {
		return this.translation;
	}
}
