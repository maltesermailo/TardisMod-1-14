package net.tardis.mod.misc;

import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class TardisNatureBasic extends TardisNature {

	/**
	 * Last Time this TARDIS had a player in it
	 */
	private long lastTimePlayer = 0l;
	
	@Override
	public int modFlight(ConsoleTile console, int mood) {
		//If mood is less than Estatic, increase mood in flight
		if(mood < EnumHappyState.ESTATIC.getTreshold() * 1.5) //Unstabilized Flight doubles the mood
			return mood + (console.getControl(StabilizerControl.class).isStabilized() ? 1 : 2);
		return mood;
	}

	@Override
	public int modLanded(ConsoleTile console, int mood) {
		if(!console.getWorld().isRemote) {
			ServerWorld world = (ServerWorld)console.getWorld();
			if(!world.getPlayers().isEmpty()) {
				this.lastTimePlayer = world.getGameTime();
			}
		}
		return mood;
	}

	@Override
	public int modEmptyTick(ConsoleTile console, int mood) {
		if(!console.getWorld().isRemote) {
			if(console.getWorld().getGameTime() - this.lastTimePlayer > 24000) {
				//Approach APATHETIC
				if(mood > EnumHappyState.APATHETIC.getTreshold())
					return mood - 1;
				return mood + 1;
			}
			return mood;
		}
		return mood;
	}

}
