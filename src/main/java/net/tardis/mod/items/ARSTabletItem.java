package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Gui;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.registries.TardisRegistries;

public class ARSTabletItem extends Item {

	public ARSTabletItem() {
		super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		boolean corridorBlock = context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn;
		PlayerEntity player = context.getPlayer();
		if(context.getHand() == player.getActiveHand()) {
			
			if(corridorBlock && !context.getWorld().isRemote) {
				//If in TARDIS
				if(context.getWorld().getDimension().getType().getModType() == TDimensions.TARDIS) {
					//If console is in world
					TardisHelper.getConsoleInWorld(context.getWorld()).ifPresent(tile -> {
						//If is an admin
						if(tile.canDoAdminFunction(player)) {
							//Spawn the structure, must be scheduled
							context.getWorld().getServer().enqueue(new TickDelayedTask(1, () -> {
								ARSPiece piece = getSelectedPiece(context.getItem());
								player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
								    if (cap.getCountdown() > 0) {//Stop players from creating new rooms while trying to delete a room
								    	player.sendStatusMessage(new TranslationTextComponent("ars.message.room.delete.concurrent_mod.denied"),true);
								    }
									else {
										if(piece != null) {
											piece.spawn((ServerWorld)context.getWorld(), context.getPos(), player, context.getFace().getOpposite());
											context.getWorld().playSound(null, context.getPos(), SoundEvents.BLOCK_BEACON_ACTIVATE, SoundCategory.BLOCKS, 1F, 1F);
											if (!player.abilities.isCreativeMode)
												player.getCooldownTracker().setCooldown(TItems.ARS_TABLET, TConfig.COMMON.arsCreateRoomCooldown.get() * 20);
										}
										else {
											player.sendStatusMessage(new TranslationTextComponent("ars.message.structure_null"),true);
										}
									}
								});
							}));
						}
						else player.sendStatusMessage(Constants.Translations.NOT_ADMIN, true);
					});
				}
			}
			
			
		}
		
		//If kill
		if(context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_kill && context.getWorld().isRemote) {
			Tardis.proxy.openGUI(Gui.ARS_TABLET_KILL, new BlockPosGuiContext(context.getPos()));
			return ActionResultType.SUCCESS;
		}
		return context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn ? ActionResultType.SUCCESS : super.onItemUse(context);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(worldIn.isRemote)
			Tardis.proxy.openGUI(Constants.Gui.ARS_TABLET, null);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Nullable
	public static ARSPiece getSelectedPiece(ItemStack stack) {
		CompoundNBT tag = stack.getOrCreateTag();
		if(tag.contains("piece"))
			return TardisRegistries.ARS_PIECES.getValue(new ResourceLocation(tag.getString("piece")));
		return null;
	}
	
	public static void setPiece(ItemStack stack, ARSPiece piece) {
		stack.getOrCreateTag().putString("piece", piece.getRegistryName().toString());
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.piece",getSelectedPiece(stack) == null ? "None" : getSelectedPiece(stack).getTranslation().getFormattedText()));
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.spawn"));
			tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.remove"));
		}
	}
	
	

}
