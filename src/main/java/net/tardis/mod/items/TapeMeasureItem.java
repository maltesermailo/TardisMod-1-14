package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.properties.Prop;

public class TapeMeasureItem extends Item {

	public TapeMeasureItem() {
		super(Prop.Items.ONE.get().group(null));
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		CompoundNBT tag = stack.getTag();
		tooltip.add(new StringTextComponent("DEV Tool for adding new interiors"));
		tooltip.add(new TranslationTextComponent("item.info.shift"));
		if(tag != null && !tag.isEmpty()) {
			if(tag.contains("first")) {
				BlockPos pos = BlockPos.fromLong(tag.getLong("first"));
				tooltip.add(new StringTextComponent("First: " + pos.getX() + ", " + pos.getY() + ", " + pos.getZ()));
			}
			
			if(tag.contains("second")) {
				BlockPos pos = BlockPos.fromLong(tag.getLong("second"));
				tooltip.add(new StringTextComponent("Second: " + pos.getX() + ", " + pos.getY() + ", " + pos.getZ()));
			}
			
			if(tag.contains("first") && tag.contains("second")) {
				BlockPos first = BlockPos.fromLong(tag.getLong("first"));
				BlockPos second = BlockPos.fromLong(tag.getLong("second"));
				
				int x = first.getX() > second.getX() ? first.getX() - second.getX() : second.getX() - first.getX();
				int y = first.getY() > second.getY() ? first.getY() - second.getY() : second.getY() - first.getY();
				int z = first.getZ() > second.getZ() ? first.getZ() - second.getZ() : second.getZ() - first.getZ();
				
				tooltip.add(new StringTextComponent("Diff: " + x + ", " + y + ", " + z));
			}
		}
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.tape_measure.howto"));
			tooltip.add(new TranslationTextComponent("tooltip.tape_measure.howto_2"));
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);

	}

	@Override
	public ActionResultType onItemUseFirst(ItemStack stack, ItemUseContext context) {
		if(context.getPlayer().isSneaking()) {
			context.getItem().setTag(new CompoundNBT());
			return ActionResultType.SUCCESS;
		}
		
		CompoundNBT nbt = context.getItem().getOrCreateTag();
		
		if(nbt.contains("first"))
			nbt.putLong("second", context.getPos().up().toLong());
		else nbt.putLong("first", context.getPos().up().toLong());
		
		context.getItem().setTag(nbt);
		return ActionResultType.SUCCESS;
	}

}
