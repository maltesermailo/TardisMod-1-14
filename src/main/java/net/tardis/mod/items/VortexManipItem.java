package net.tardis.mod.items;



import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.itemgroups.TItemGroups;

public class VortexManipItem extends BaseItem{
	
	public VortexManipItem(){
		super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
		
		this.addPropertyOverride(new ResourceLocation("open"), (stack, worldIn, entityIn) -> {
			
			IVortexCap cap = stack.getCapability(Capabilities.VORTEX_MANIP).orElse(null);
			if(cap == null)
				return 0F;
			return cap.getOpen() ? 1F : 0F;
		});
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn){
		ItemStack stack = player.getHeldItem(handIn);
		if (stack != ItemStack.EMPTY) {
			if(worldIn.isRemote)
				if (!player.getCooldownTracker().hasCooldown(this)) {
					Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
					stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> cap.setOpen(true));
				}
		}
		return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return false;
	}
	
	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
		if (!worldIn.isRemote) {
			if (entityIn instanceof PlayerEntity) {
				stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> cap.tick((PlayerEntity)entityIn));
			}
			if (worldIn.getGameTime() % 20 == 0) {
				syncCapability(stack); //sync capabilities each second instead of every tick
			}
		}
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
			if (Screen.hasShiftDown()) {
				tooltip.add(new TranslationTextComponent("tooltip.vm.total_charge", cap.getClientTotalCurrentCharge()));
				tooltip.add(new TranslationTextComponent("tooltip.vm.battery_count", cap.getBatteries()));
			}
		});
		
	}
	
	@Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) { //sync capability to client
        CompoundNBT tag = stack.getOrCreateTag();
        stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(handler -> tag.put("cap_sync", handler.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(handler -> handler.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }
    
	public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }
	
}
