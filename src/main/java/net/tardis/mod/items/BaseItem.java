package net.tardis.mod.items;

import net.minecraft.item.Item;
import net.tardis.mod.itemgroups.TItemGroups;

public class BaseItem extends Item {

	public BaseItem() {
		super(new Properties().group(TItemGroups.MAIN));
	}
	
	public BaseItem(Properties prop) {
		super(prop);
	}

}
