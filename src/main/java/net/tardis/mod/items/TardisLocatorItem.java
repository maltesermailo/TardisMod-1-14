package net.tardis.mod.items;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.UsernameCache;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;

public class TardisLocatorItem extends Item {

	public TardisLocatorItem(Properties properties) {
		super(properties);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		if(!context.getWorld().isRemote) {
			UUID id = readOwnerID(context.getItem());
			if(id == null) {
				writeOwnerID(context.getItem(), context.getPlayer().getUniqueID());
				id = context.getPlayer().getUniqueID();
			}
			
			TardisHelper.getConsole(context.getWorld().getServer(), context.getPlayer().getUniqueID()).ifPresent(tile -> {
				context.getPlayer().sendStatusMessage(new TranslationTextComponent("status.tardis.item.locator", Helper.formatBlockPos(tile.getLocation()), Helper.formatDimName(tile.getDimension())), false);
			});
		}
		return super.onItemUse(context);
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		UUID id = readOwnerID(stack);
		if(id != null) {
			tooltip.add(new TranslationTextComponent("tooltip.tardis.locator", UsernameCache.containsUUID(id) ? UsernameCache.getLastKnownUsername(id) : id.toString()));
		}
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.tardis.locator.use"));
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	public static void writeOwnerID(ItemStack stack, UUID loc) {
		CompoundNBT tag = stack.getOrCreateTag();
		tag.putString("owner", loc.toString());
	}
	
	@Nullable	
	public static UUID readOwnerID(ItemStack stack) {
		if(stack.hasTag() && stack.getTag().contains("owner")) {
			return UUID.fromString(stack.getOrCreateTag().getString("owner"));
		}
		return null;
	}

}
