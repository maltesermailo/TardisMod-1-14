package net.tardis.mod.tileentities.machines;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class TransductionBarrierTile extends TileEntity{

	private String landingCode = "";
	
	public TransductionBarrierTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TransductionBarrierTile() {
		this(TTiles.TRANSDUCTION_BARRIER);
	}
	
	public void setCode(String code) {
		this.landingCode = code;
		this.markDirty();
	}
	
	public String getCode() {
		return this.landingCode;
	}
	
	public boolean canLand(ConsoleTile tile) {
		return tile.getLandingCode().toLowerCase().contentEquals(landingCode.toLowerCase());
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.landingCode = compound.getString("landing_code");
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putString("landing_code", this.landingCode);
		return super.write(compound);
	}

}
