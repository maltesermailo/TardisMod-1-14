package net.tardis.mod.tileentities;

import java.util.Random;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;

public class BrokenExteriorTile extends TileEntity implements ITickableTileEntity{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	private DimensionType consoleDim = null;
	protected final Random rand = new Random();
	
	private boolean hasPlayed = false;
	
	public BrokenExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public BrokenExteriorTile() {
		super(TTiles.BROKEN_TARDIS);
	}

	public void setConsoleDimension(DimensionType type) {
		this.consoleDim = type;
	}
	
	public DimensionType getConsoleDimension() {
		return this.consoleDim;
	}
	
	public void increaseLoyalty(int amt) {
		if(!world.isRemote && consoleDim != null) {
			ServerWorld ws = world.getServer().getWorld(consoleDim);
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ConsoleTile console = (ConsoleTile)te;
					console.getEmotionHandler().setLoyalty(console.getEmotionHandler().getLoyalty() + amt);
					console.getEmotionHandler().setMood(console.getEmotionHandler().getMood() + ((int) amt / 4));
				}
			}
		}
	}
	
	public int getLoyalty() {
		if(!world.isRemote && this.consoleDim != null) {
			ServerWorld ws = world.getServer().getWorld(consoleDim);
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					return ((ConsoleTile)te).getEmotionHandler().getLoyalty();
				}
			}
		}
		return -1;
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if(compound.contains("console"))
			this.consoleDim = DimensionType.byName(new ResourceLocation(compound.getString("console")));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.consoleDim != null)
			compound.putString("console", this.consoleDim.getRegistryName().toString());
		return super.write(compound);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	@Override
	public void tick() {
		if(world.isRemote && !this.hasPlayed) {
			PlayerEntity player = Tardis.proxy.getClientPlayer();
			//TODO: Finish music playing
		}
	}


}
