package net.tardis.mod.tileentities;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.schematics.Schematic;

public class ShipComputerTile extends TileEntity implements IItemHandlerModifiable{

	private Schematic schematic;
	private ItemStackHandler handler = new ItemStackHandler(27);
	
	public ShipComputerTile() {
		super(TTiles.SHIP_COMPUTER);
	}
	
	public Schematic getSchematic() {
		return this.schematic;
	}
	
	public void setSchematic(Schematic sche) {
		this.schematic = sche;
		this.markDirty();
	}
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		handler.deserializeNBT(compound.getCompound("inv"));
		if(compound.contains("schematic"))
			this.schematic = TardisRegistries.SCHEMATICS.getValue(new ResourceLocation(compound.getString("schematic")));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.schematic != null)
			compound.putString("schematic", this.schematic.getRegistryName().toString());
		compound.put("inv", handler.serializeNBT());
		return super.write(compound);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.write(new CompoundNBT()));
	}
	
	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}

	@Override
	public int getSlots() {
		return handler.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		this.markDirty();
		return handler.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		this.markDirty();
		return handler.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		this.markDirty();
		return handler.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return handler.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return true;
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		handler.setStackInSlot(slot, stack);
		this.markDirty();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
	}
	

}
