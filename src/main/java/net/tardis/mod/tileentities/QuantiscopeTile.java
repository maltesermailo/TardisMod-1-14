package net.tardis.mod.tileentities;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.containers.QuantiscopeSonicContainer;
import net.tardis.mod.containers.QuantiscopeWeldContainer;
import net.tardis.mod.recipe.Recipes;
import net.tardis.mod.recipe.WeldRecipe;
import net.tardis.mod.sounds.TSounds;

public class QuantiscopeTile extends TileEntity implements IItemHandlerModifiable, ITickableTileEntity{

	private ItemStackHandler handler = new ItemStackHandler(7);
	private EnumMode mode = EnumMode.SONIC;
	private WeldRecipe weldRecipe;
	private int progress = 0;
	
	public QuantiscopeTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public QuantiscopeTile() {
		super(TTiles.QUANTISCOPE);
	}
	
	@Override
	public void tick() {
		if(mode == EnumMode.WELD) {
			if(this.weldRecipe == null) {
				this.weldRecipe = this.getRecipe();
			}
			else if(this.shouldWeld()) {
				
				//If first progress tick, update client
				if(this.progress == 0)
					if(!world.isRemote)
						this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
				
				++progress;
				if(progress >= this.mode.workTime) {
					finish();
				}
				if (world.getGameTime() % 20 == 0) {
					if(world.isRemote)
						world.addParticle(ParticleTypes.LAVA, this.getPos().getX() + 0.5, this.getPos().getY(), this.getPos().getZ() + 0.5, 0, 0, 0);
					if (!world.isRemote) {
						world.playSound(null, this.getPos(), TSounds.ELECTRIC_SPARK, SoundCategory.BLOCKS, 0.5F, 1.5F);
					}
				}
				
			}
			else {
				this.weldRecipe = null;
				this.progress = 0;
				if(!world.isRemote)
					this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
			}
				
		}
	}
	
	private boolean shouldWeld() {
		if(this.weldRecipe != null) {
			return this.doesMatrixMatchRecipe(this.weldRecipe) && this.insertItem(6, new ItemStack(this.weldRecipe.getOutput()), true) == ItemStack.EMPTY;
		}
		return false;
	}
	
	private void finish() {
		
		if(this.weldRecipe.isRepair()) {
			ItemStack result = this.getStackInSlot(5).copy();
			result.setDamage(0);
			this.setStackInSlot(6, result);
		}
		else {
			this.insertItem(6, new ItemStack(this.weldRecipe.getOutput()), false);
			this.markDirty();
		}
		
		for(int i = 0; i < 6; ++i)
			this.getStackInSlot(i).shrink(1);
		this.progress = 0;
		this.weldRecipe = null;
		if(!world.isRemote)
			this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
	}
	
	private boolean doesMatrixMatchRecipe(WeldRecipe recipe) {
		ItemStack repair = this.getStackInSlot(5);
		return recipe.matches(repair, this.getStackInSlot(0), this.getStackInSlot(1), this.getStackInSlot(2), this.getStackInSlot(3), this.getStackInSlot(4));
	}
	
	@Nullable
	private WeldRecipe getRecipe() {
		for(WeldRecipe rec : Recipes.WELD_RECIPE) {
			if(this.doesMatrixMatchRecipe(rec))
				return rec;
		}
		return null;
	}
	
	public float getProgress() {
		return this.progress / 200.0F;
	}
	
	public void setMode(EnumMode mode) {
		this.mode = mode;
		this.markDirty();
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.mode = EnumMode.values()[compound.getInt("mode")];
		this.handler.deserializeNBT(compound.getCompound("item_handler"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("mode", this.mode.ordinal());
		compound.put("item_handler", this.handler.serializeNBT());
		return super.write(compound);
	}
	
	public Container createContainer(int id, PlayerInventory inv) {
		return mode.fact.create(id, inv, this);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	public static enum EnumMode{
		SONIC(QuantiscopeSonicContainer::new),
		WELD(QuantiscopeWeldContainer::new);
		
		IFactory<Container> fact;
		int workTime = 200;
		
		EnumMode(IFactory<Container> fact){
			this.fact = fact;
		}
		
		EnumMode(IFactory<Container> fact, int workTime){
			this.fact = fact;
			this.workTime = workTime;
		}
	}
	
	public static interface IFactory<T extends Container>{
		T create(int id, PlayerInventory inv, QuantiscopeTile tile);
	}

	@Override
	public int getSlots() {
		return this.handler.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return this.handler.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		this.markDirty();
		return this.handler.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		this.markDirty();
		return this.handler.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return this.handler.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return true;
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		this.handler.setStackInSlot(slot, stack);
		this.markDirty();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
	}

}
