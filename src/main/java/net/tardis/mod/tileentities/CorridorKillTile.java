package net.tardis.mod.tileentities;

import javax.annotation.Nullable;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;

public class CorridorKillTile extends TileEntity implements ITickableTileEntity{

	private BlockPos start;
	private BlockPos end;
	private BlockPos restorePos;
	private Direction dir;
	
	private int ticksTillDeath = 0;
	
	public CorridorKillTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}

	public CorridorKillTile() {
		super(TTiles.CORRIDOR_KILL);
		this.dir = Direction.NORTH; //Give default values to fields so we can still save structures with the void blocks
		this.start = new BlockPos(0,0,0);
		this.end = new BlockPos(0,0,0);
		this.restorePos = new BlockPos(0,0,0);
	}

	public void setStart(BlockPos pos) {
		this.start = pos;
		this.markDirty();
	}
	
	public void setEnd(BlockPos pos) {
		this.end = pos;
		this.markDirty();
	}
	
	public void setRestorePos(BlockPos pos, Direction dir) {
		this.restorePos = pos;
		this.dir = dir;
		this.markDirty();
	}
	
	private void deleteRoom() {
		if(!world.isRemote) {
			world.getServer().enqueue(new TickDelayedTask(1, () -> {
				for(BlockPos pos : BlockPos.getAllInBoxMutable(start, end)) {
					world.setBlockState(pos, Blocks.AIR.getDefaultState());
				}
				for(int x = -1; x <= 1; ++x) {
					for(int y = -1; y < 3; ++y) {
						if(dir == Direction.SOUTH || dir == Direction.NORTH)
							world.setBlockState(this.restorePos.add(x, y, 0), TBlocks.tech_wall_dark_blank.getDefaultState());
						else world.setBlockState(this.restorePos.add(0, y, x), TBlocks.tech_wall_dark_blank.getDefaultState());
					}
				}
				world.setBlockState(restorePos, TBlocks.corridor_spawn.getDefaultState());
				world.playSound(null, restorePos, SoundEvents.BLOCK_BEACON_DEACTIVATE, SoundCategory.BLOCKS, 1F, 1F);
				
				//Save players by teleporting them to the console
				for(ServerPlayerEntity player : world.getEntitiesWithinAABB(ServerPlayerEntity.class, new AxisAlignedBB(this.start, this.end))) {
					player.connection.setPlayerLocation(0, 129, 2, player.rotationYaw, player.rotationPitch);
					player.sendStatusMessage(new TranslationTextComponent("ars.message.room.deleted"), false);
					player.world.playSound(null, player.getPosition(), SoundEvents.ENTITY_SHULKER_TELEPORT, SoundCategory.PLAYERS, 1F, 1F);
				}
				
			}));
		}
	}
	
	public void startDeletion(@Nullable PlayerEntity player) {
	    //Set countdown to 3 seconds if in creative or 10 in survival. Add 20 ticks more to because 0 'counts' as a second
		this.ticksTillDeath = player.abilities.isCreativeMode ? 80 : 220;
		if(player != null && !world.isRemote) {
			player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
				cap.startCountdown(this.ticksTillDeath);
				cap.update();
			});
		}
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.dir = Direction.values()[compound.getInt("direction")];
		this.start = BlockPos.fromLong(compound.getLong("start"));
		this.end = BlockPos.fromLong(compound.getLong("end"));
		this.restorePos = BlockPos.fromLong(compound.getLong("restore"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("direction", this.dir.ordinal());
		compound.putLong("start", this.start.toLong());
		compound.putLong("end", this.end.toLong());
		compound.putLong("restore",this.restorePos.toLong());
		return super.write(compound);
	}

	@Override
	public void tick() {
		if(this.ticksTillDeath > 0) {
			--this.ticksTillDeath;
			if(this.ticksTillDeath == 0)
				this.deleteRoom();
			if(this.ticksTillDeath % 20 == 0)
				world.playSound(null, this.getPos(), SoundEvents.ENTITY_ARROW_HIT_PLAYER, SoundCategory.BLOCKS, 1F, 1); //This is the famous "ding" sound you hear when you hit another player with arrows
		}
	}
}
