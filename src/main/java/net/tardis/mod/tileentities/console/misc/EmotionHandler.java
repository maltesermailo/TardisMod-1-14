package net.tardis.mod.tileentities.console.misc;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.misc.TardisNature;
import net.tardis.mod.misc.TardisNatureBasic;
import net.tardis.mod.tileentities.ConsoleTile;

public class EmotionHandler implements ITickable, INBTSerializable<CompoundNBT>{
	
	private static final ResourceLocation SAVE_KEY = new ResourceLocation(Tardis.MODID, "emotion");
	private TardisNature nature = new TardisNatureBasic();
	private TraitTag[] traits = new TraitTag[5];
	private ConsoleTile tile;
	/*
	 * A value between -100 and 100
	 * 100 a Tardis has all the perks (Transmat when low health, snap to open door, etc)
	 */
	private int loyalty = -100;
	private int mood = EnumHappyState.CONTENT.threshhold;
	
	public EmotionHandler(ConsoleTile console) {
		console.registerDataHandler(SAVE_KEY, this);
		console.registerTicker(this);
		this.tile = console;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.loyalty = tag.getInt("loyalty");
		this.mood = tag.getInt("mood");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("loyalty", this.loyalty);
		tag.putInt("mood", this.mood);
		return tag;
	}

	@Override
	public void tick(ConsoleTile console) {
		if(console.getWorld().getGameTime() % 80 == 0) {
			int prevMood = mood;
			if(console.isInFlight()) {
				this.mood = this.getNature().modFlight(console, this.mood);
			}
			else this.mood = this.getNature().modLanded(console, this.mood);
			
			if(console.getWorld().getPlayers().isEmpty() && Helper.isOwnerOn(console.getWorld(), console.getWorld().getDimension().getType()))
				this.mood = this.getNature().modEmptyTick(console, this.mood);
			
			//Particle Spawning
			if(prevMood > this.mood) {
				console.getWorld().addParticle(ParticleTypes.ANGRY_VILLAGER,
						console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
			}
			else if(prevMood < this.mood) {
				console.getWorld().addParticle(ParticleTypes.HEART,
						console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
			}
			
		}
	}
	
	public TardisNature getNature() {
		return this.nature;
	}
	
	public void setNature(TardisNature nature) {
		this.nature = nature;
	}
	
	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}
	
	public int getLoyalty() {
		return loyalty;
	}
	
	public int getMood() {
		return this.mood;
	}
	
	public void setMood(int mood) {
		this.mood = mood;
	}
	
	public void addMood(int mood) {
		this.mood += mood;
	}
	
	public void addLoyalty(int loyalty) {
		this.loyalty += loyalty;
	}
	
	public TraitTag[] getTraits() {
		return this.traits;
	}
	
	public boolean addLoyaltyIfOwner(int loyalty, @Nullable PlayerEntity player) {
		if(player != null && player.getUniqueID().equals(tile.getOwner())) {
			this.addLoyalty(loyalty);
			return true;
		}
		return false;
	}

	public static enum EnumHappyState{
		ESTATIC(100),
		HAPPY(90),
		CONTENT(50),
		APATHETIC(0),
		DISCONTENT(-20),
		SAD(-100);
		
		int threshhold = 0;
		
		EnumHappyState(int threshold){
			this.threshhold = threshold;
		}
		
		public int getTreshold() {
			return this.threshhold;
		}
	}
}
