package net.tardis.mod.tileentities.console.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ExteriorPropertyManager implements INBTSerializable<CompoundNBT>{

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "exterior_manager");
	protected ConsoleTile console;
	
	private ResourceLocation exteriorAnimation = new ResourceLocation(Tardis.MODID, "classic");
	private int variantIndex = 0;
	
	public ExteriorPropertyManager(ConsoleTile tile) {
		this.console = tile;
		tile.registerDataHandler(NAME, this);
	}
	
	public void setExteriorAnimation(ResourceLocation loc) {
		this.exteriorAnimation = loc;
		if(console != null)
			console.getExterior().getExterior(console)
				.setExteriorAnimation(TardisRegistries.EXTERIOR_ANIMATIONS.getValue(loc));
	}
	
	public ResourceLocation getExteriorAnimation() {
		return this.exteriorAnimation;
	}

	public int getExteriorVariant() {
		return this.variantIndex;
	}
	
	public void setExteriorVariant(int i) {
		if(console.getExterior().getVariants() != null) {
			if(console.getExterior().getVariants().length > i) {
				this.variantIndex = i;
				
				//set door too
				if(!console.getWorld().isRemote) {
					ExteriorTile tile = console.getExterior().getExterior(console);
					if(tile != null) {
						tile.setVariant(i);
					}
				}
				return;
			}
		}
		this.variantIndex = 0;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("exterior_animation", this.exteriorAnimation.toString());
		tag.putInt("variant", this.variantIndex);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		
		if(nbt.isEmpty())
			return;
		
		if(nbt.contains("exterior_animation"))
			this.exteriorAnimation = new ResourceLocation(nbt.getString("exterior_animation"));
		this.variantIndex = nbt.getInt("variant");
	}
}
