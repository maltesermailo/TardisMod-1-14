package net.tardis.mod.tileentities;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;

public class ToyotaSpinnyTile extends TileEntity implements ITickableTileEntity{

	private ConsoleTile tile;
	
	public float rotation = 0;
	public float prevRotation = 0;
	
	private AxisAlignedBB renderBox;
	
	public ToyotaSpinnyTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ToyotaSpinnyTile() {
		super(TTiles.TOYOTA_SPIN);
	}

	@Override
	public void tick() {
		
		if(tile == null || tile.isRemoved())
			TardisHelper.getConsoleInWorld(world).ifPresent(con -> this.tile = con);
		
		this.prevRotation = this.rotation;
		
		if(tile != null && tile.isInFlight()) {
			this.playFlightStartSound(tile);
			this.playFlyLoop(tile, 41);
			this.playLandSound(tile);
			this.rotation = (this.rotation + 1) % 360.0F;
		}
		
		if(this.renderBox == null)
			this.renderBox = new AxisAlignedBB(this.getPos()).grow(5);
		
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return renderBox == null ? new AxisAlignedBB(this.getPos()) : renderBox;
	}
	
	public void playFlightStartSound(ConsoleTile console) {
		if(!console.getWorld().isRemote) {
			if (console.flightTicks == console.getMaxFlightTime() - 1) {
				console.getWorld().playSound(null, console.getPos(), TSounds.ROTOR_START, SoundCategory.BLOCKS, 0.175F, 1F);
			}
		}
	}
	
	public void playFlyLoop(ConsoleTile console, int loopTime) {
		if (!console.getWorld().isRemote()) {
			boolean hasTakeOffSoundPlayed = console.flightTicks <= console.getMaxFlightTime() - 80; 
			boolean isTimeBeforeLand = console.flightTicks > 70;
			if(console.flightTicks % loopTime == 0 && (hasTakeOffSoundPlayed && isTimeBeforeLand)) {
				console.getWorld().playSound(null, this.tile.getPos(), TSounds.ROTOR_TICK, SoundCategory.BLOCKS, 0.175F, 1F);
			}
		}
	}
	
	public void playLandSound(ConsoleTile console) {
		if(!console.getWorld().isRemote && console.getSoundScheme().getLandTime() == console.flightTicks + 130) {
			console.getWorld().playSound(null, this.tile.getPos(), TSounds.ROTOR_END, SoundCategory.BLOCKS, 0.175F, 1F);
		}	
	}

}
