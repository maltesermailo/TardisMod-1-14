package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.tileentities.ConsoleTile;

public class AntiGravProtocol extends Protocol {

	public static final String TRANS = "status.tardis.anti_gravs.";
	public static final TranslationTextComponent TRANS_MON = new TranslationTextComponent("protocol.tardis.antigrav");

	@Override
	public void call(World world, ConsoleTile console) {
		if(!world.isRemote) {
			console.setAntiGrav(!console.getAntiGrav());
			for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(console.getPos()).grow(16))) {
				player.sendStatusMessage(new TranslationTextComponent(TRANS + console.getAntiGrav()), true);
			}
		}
	}

	@Override
	public String getDisplayName() {
		return TRANS_MON.getFormattedText();
	}

	@Override
	public String getSubmenu() {
		return "exterior_prop";
	}
}
