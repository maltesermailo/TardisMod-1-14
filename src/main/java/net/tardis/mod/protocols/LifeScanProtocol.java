package net.tardis.mod.protocols;

import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class LifeScanProtocol extends Protocol {

	private static AxisAlignedBB BOX = new AxisAlignedBB(-20, -20, -20, 20, 20, 20);
	
	@Override
	public void call(World world, ConsoleTile console) {
		if(!world.isRemote) {
			IntWrapper wrapPlayer = new IntWrapper();
			IntWrapper wrapMob = new IntWrapper();
			((ServerWorld)world).getEntities().forEach((entity) -> {
				if(entity instanceof ServerPlayerEntity) {
					wrapPlayer.addInt();
				}
				if (entity instanceof MobEntity) {
					wrapMob.addInt();
				}
			});
			for(PlayerEntity entity : world.getEntitiesWithinAABB(PlayerEntity.class, BOX.offset(console.getPos()))) {
				entity.sendStatusMessage(new TranslationTextComponent("message.tardis.scanner", wrapPlayer.val, wrapMob.val), true);
			}
		}
		else Tardis.proxy.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getDisplayName() {
		return "Scan for life signs";
	}
	
	@Override
	public String getSubmenu() {
		return "security";
	}



	public static class IntWrapper{
		
		private int val;
		
		public IntWrapper() {}
		
		public IntWrapper(int val) {
			this.val = val;
		}
		
		public void addInt() {
			++val;
		}
		
		public void subInt() {
			--this.val;
		}
		
		public int getInt() {
			return this.val;
		}
	}

}
