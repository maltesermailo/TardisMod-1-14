package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisInputMessage {

	public boolean isJumping;
	
	public TardisInputMessage(boolean jump) {
		this.isJumping = jump;
	}
	
	public static void encode(TardisInputMessage mes, PacketBuffer buf) {
		buf.writeBoolean(mes.isJumping);
	}
	
	public static TardisInputMessage decode(PacketBuffer buf) {
		boolean isJumping = buf.readBoolean();
		
		return new TardisInputMessage(isJumping);
	}
	
	public static void handle(TardisInputMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			if(context.get().getSender().getRidingEntity() != null && context.get().getSender().getRidingEntity() instanceof TardisEntity) {
				TardisEntity entity = (TardisEntity)context.get().getSender().getRidingEntity();
				
				entity.setJumping(mes.isJumping);
			}
		});
		context.get().setPacketHandled(true);
	}
	
}
