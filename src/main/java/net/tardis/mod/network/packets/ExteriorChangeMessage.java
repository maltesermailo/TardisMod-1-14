package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.subsystem.ChameleonSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorChangeMessage {
	
	ResourceLocation exterior;
	
	public ExteriorChangeMessage(ResourceLocation ext) {
		this.exterior = ext;
	}
	
	public static void encode(ExteriorChangeMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.exterior);
	}
	
	public static ExteriorChangeMessage decode(PacketBuffer buf) {
		return new ExteriorChangeMessage(buf.readResourceLocation());
	}
	
	public static void handle(ExteriorChangeMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().getServerWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				console.getSubsystem(ChameleonSubsystem.class).ifPresent(sys -> {
					IExterior ext = ExteriorRegistry.getExterior(mes.exterior);
					if(console.getExteriors().contains(ext)) {
						if(sys.canBeUsed()) {
							sys.damage(context.get().getSender(), 1);
							
							console.getExterior().remove(console);
							console.setExterior(ext);
							console.getExterior().place(console, console.getDimension(), console.getLocation());
						}
						else context.get().getSender().sendStatusMessage(new TranslationTextComponent(Constants.Translations.NO_COMPONENT, "Chameleon Circuit"), true);
					}
				});
			}
		});
		context.get().setPacketHandled(true);
	}

}
