package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.packets.console.ConsoleData;
import net.tardis.mod.network.packets.console.LandType;

public class ConsoleUpdateMessage {
	
	private DimensionType console;
	private DataTypes type;
	private ConsoleData data;
	
	public ConsoleUpdateMessage(DataTypes type, ConsoleData data, DimensionType tile) {
		this.type = type;
		this.data = data;
		this.console = tile;
	}
	
	public static void encode(ConsoleUpdateMessage mes, PacketBuffer buff) {
		buff.writeInt(mes.type.ordinal());
		buff.writeResourceLocation(DimensionType.getKey(mes.console));
		
		mes.data.serialize(buff);
	}
	
	public static ConsoleUpdateMessage decode(PacketBuffer buff) {
		DataTypes type = DataTypes.values()[buff.readInt()];
		DimensionType dim = DimensionType.byName(buff.readResourceLocation());
		
		ConsoleData data = type.data.get();
		data.deserialize(buff);
		
		return new ConsoleUpdateMessage(type, data, dim);
	}
	
	public static void handle(ConsoleUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsole(cont.get().getSender().getServer(), mes.console).ifPresent(tile -> {
				mes.data.applyToConsole(tile, cont);
			});
		});
		cont.get().setPacketHandled(true);
	}
	
	public static enum DataTypes{
		LAND_CODE(() -> new LandType(""));
		
		Supplier<ConsoleData> data;
		
		DataTypes(Supplier<ConsoleData> data){
			this.data = data;
		}
		
	}
	

}
