package net.tardis.mod.network.packets;

import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;

/**
 * Created by 50ap5ud5
 * on 9 Jul 2020 @ 9:14:31 pm
 */
public class VMDistressMessage {

	private UUID id;

	public VMDistressMessage(UUID uuid) {
		this.id = uuid;
	}

	public static void encode(VMDistressMessage mes, PacketBuffer buf) {
		buf.writeUniqueId(mes.id);
	}

	public static VMDistressMessage decode(PacketBuffer buf) {
		return new VMDistressMessage(buf.readUniqueId());
	}

	public static void handle(VMDistressMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ServerPlayerEntity player = context.get().getSender();
			ServerWorld world = player.getServerWorld();
			if (TardisHelper.hasTARDIS(player.getServer(), mes.id)) {
				TardisHelper.getConsole(player.getServer(), mes.id).ifPresent(tile -> {
					tile.addDistressSignal(new SpaceTimeCoord(world.getDimension().getType(), player.getPosition()));
				});
             	player.getCooldownTracker().setCooldown(TItems.VORTEX_MANIP, TConfig.COMMON.vmCooldownTime.get() * 20);
				PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.vm.distress_sent", ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUUID(mes.id).getName().getFormattedText()), false);
			}
			else {
				PlayerHelper.sendMessageToPlayer(player,Constants.Translations.NO_TARDIS_FOUND, false);
			}

		});
		context.get().setPacketHandled(true);
	}
}
