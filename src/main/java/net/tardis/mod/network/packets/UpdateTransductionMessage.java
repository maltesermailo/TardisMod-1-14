package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.network.packets.console.LandType;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;

public class UpdateTransductionMessage {
	
	BlockPos pos;
	String code;
	
	public UpdateTransductionMessage(BlockPos pos, String code) {
		this.pos = pos;
		this.code = code;
	}
	
	public static void encode(UpdateTransductionMessage mes, PacketBuffer buff) {
		buff.writeBlockPos(mes.pos);
		buff.writeInt(mes.code.length());
		buff.writeString(mes.code, mes.code.length());
	}
	
	public static UpdateTransductionMessage decode(PacketBuffer buff) {
		BlockPos pos = buff.readBlockPos();
		int max = buff.readInt();
		return new UpdateTransductionMessage(pos, buff.readString(max));
	}
	
	public static void handle(UpdateTransductionMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			World world = cont.get().getSender().world;
			TileEntity te = world.getTileEntity(mes.pos);
			if(te instanceof TransductionBarrierTile) {
				((TransductionBarrierTile)te).setCode(mes.code);
				cont.get().getSender().sendStatusMessage(new TranslationTextComponent(LandType.TRANSLATION_KEY, mes.code), false);
			}
		});
		cont.get().setPacketHandled(true);
	}

}
