package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import org.apache.logging.log4j.Level;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.CorridorKillTile;

public class ARSDeleteMessage {
	
	BlockPos pos;
	
	public ARSDeleteMessage(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(ARSDeleteMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static ARSDeleteMessage decode(PacketBuffer buf) {
		return new ARSDeleteMessage(buf.readBlockPos());
	}
	
	public static void handle(ARSDeleteMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().world).ifPresent(tile -> {
				
				if(tile.canDoAdminFunction(context.get().getSender())) {
					TileEntity te = context.get().getSender().world.getTileEntity(mes.pos);
					if(te instanceof CorridorKillTile) {
						((CorridorKillTile)te).startDeletion(context.get().getSender());
						if (!context.get().getSender().abilities.isCreativeMode)
						     context.get().getSender().getCooldownTracker().setCooldown(TItems.ARS_TABLET, TConfig.COMMON.arsDeleteRoomCooldown.get() * 20);
					}
					else Tardis.LOGGER.log(Level.ERROR, "Invalid packet! Tile at " + mes.pos + " is not a corridor delete block!");
				}
				else Tardis.LOGGER.log(Level.DEBUG, "Player with UUID " + context.get().getSender().getUniqueID() + " tryed to delete corridor, but is not allowed!");
				
			});
		});
		context.get().setPacketHandled(true);
	}

}
