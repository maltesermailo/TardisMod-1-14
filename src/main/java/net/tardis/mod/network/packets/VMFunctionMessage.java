package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.misc.vm.IVortexMFunction;
import net.tardis.mod.misc.vm.VortexMFunctions;


public class VMFunctionMessage {
	
	public int function;
	public int subFunction;
	public boolean isSubFunction;
	public boolean isServer;
	
	public VMFunctionMessage(int function, int subFunction, boolean isSubFunction, boolean isServer) {
		this.function = function;
		this.subFunction = subFunction;
		this.isSubFunction = isSubFunction;
		this.isServer = isServer;
	}
	
	public static VMFunctionMessage decode(PacketBuffer buf) {
		return new VMFunctionMessage(buf.readInt(),buf.readInt(),buf.readBoolean(),buf.readBoolean());
	}
	
	public static void encode(VMFunctionMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.function);
		buf.writeInt(mes.subFunction);
		buf.writeBoolean(mes.isSubFunction);
		buf.writeBoolean(mes.isServer);
	}

	public static void handle(VMFunctionMessage mes, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerWorld world = ctx.get().getSender().getServerWorld();
			IVortexMFunction func = VortexMFunctions.getVMFunction(mes.function,mes.subFunction,mes.isSubFunction);
			if (mes.isServer && func != null)
				func.sendActionToServer(world, ctx.get().getSender());
		});
	}
}
