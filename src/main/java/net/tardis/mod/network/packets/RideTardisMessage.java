package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class RideTardisMessage {
	
	public static void encode(RideTardisMessage mes, PacketBuffer buf) {
		
	}
	
	public static RideTardisMessage decode(PacketBuffer buf) {
		return new RideTardisMessage();
	}
	
	public static void handle(RideTardisMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			if(context.get().getSender().getRidingEntity() != null) {
				if(context.get().getSender().getRidingEntity() instanceof TardisEntity) {
					TardisEntity entity = (TardisEntity)context.get().getSender().getRidingEntity();
					if(entity.getConsole() != null) {
						entity.getConsole().stopRide(false);
					}
				}
			} else {
				TileEntity te = context.get().getSender().getServerWorld().getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ConsoleTile console = (ConsoleTile)te;
					
					if(console.isRealWorldFlight()) {
						console.ride(context.get().getSender());
					} else {
						context.get().getSender().sendMessage(new StringTextComponent(new TranslationTextComponent("message.tardis.control.real_world_flight_not_possible").getFormattedText()));
					}
				}
			}
		});
		context.get().setPacketHandled(true);
	}
	
}
