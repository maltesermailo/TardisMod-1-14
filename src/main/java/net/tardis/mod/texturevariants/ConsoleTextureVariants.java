package net.tardis.mod.texturevariants;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.TexVariant;

public class ConsoleTextureVariants {

	public static TexVariant[] NEMO = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_ivory.png"), "console.nemo.ivory"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_wood.png"), "console.nemo.wood")
	};
	
	public static TexVariant[] STEAM = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam_ironclad.png"), "console.steam.ironclad")
	};
	
	public static TexVariant[] TOYOTA = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota_blue.png"), "console.toyota.blue"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota_red.png"), "console.toyota.red")
	};
	
	public static final TexVariant[] GALVANIC = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_joker.png"), "console.galvanic.joker"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_white.png"), "console.galvanic.white"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_gold.png"), "console.galvanic.gold")
	};

	public static final TexVariant[] XION = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_glass.png"), "console.xion.glass"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_sapphire.png"), "console.xion.sapphire"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_ruby.png"), "console.xion.ruby")
	};
	
	public static final TexVariant[] CORAL = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral_blue.png"), "console.coral.blue"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral_white.png"), "console.coral.white"),
	};
}
