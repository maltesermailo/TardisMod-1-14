package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

public class FlightEvent implements IRegisterable<FlightEvent> {

    public static FlightEvent SCRAP;
    
    public static IFlightCrash DIMENSION = cons -> {
    	List<DimensionType> types = Helper.getAllValidDimensionTypes();
    	cons.setDestination(types.get(ConsoleTile.rand.nextInt(types.size() - 1)), cons.getDestination());
    };
    

    private List<Class<? extends IControl>> controlsToHit = new ArrayList<Class<? extends IControl>>();
    private ResourceLocation name;
    private TranslationTextComponent translation;
    private IFlightCrash crash = cons -> cons.setDestination(cons.getDestinationDimension(), cons.randomizeCoords(cons.getDestination(), 30));

    @SafeVarargs
    public FlightEvent(String name, IFlightCrash crash, Class<? extends IControl>... controls) {
        this.translation = new TranslationTextComponent("flight.tardis." + name);
        this.controlsToHit = Lists.newArrayList(controls);
        this.crash = crash;
    }
    
    @SafeVarargs
    public FlightEvent(String name, Class<? extends IControl>... controls) {
        this.translation = new TranslationTextComponent("flight.tardis." + name);
        this.controlsToHit = Lists.newArrayList(controls);
    }

    public static void registerAll() {
    	TardisRegistries.registerRegisters(() -> {
	        TardisRegistries.FLIGHT_EVENT.register("scrap", SCRAP = new FlightEvent("scrap", FacingControl.class, RandomiserControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("time_wind", new FlightEvent("time_wind", ThrottleControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("x", new FlightEvent("x", cons -> cons.setDestination(cons.getDestinationDimension(), cons.getDestination().add(20, 0, 0)), XControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("y", new FlightEvent("y", YControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("z", new FlightEvent("z", ZControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("refueler", new FlightEvent("refueler", cons -> cons.setArtron(cons.getArtron() - ConsoleTile.rand.nextInt(10)), RefuelerControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("door", new FlightEvent("door", cons -> {
	        	cons.getDoor().ifPresent(door -> door.setOpenState(EnumDoorState.BOTH));
	        }, DoorControl.class));
	        TardisRegistries.FLIGHT_EVENT.register("dimension", new FlightEvent("dimension", DIMENSION, DimensionControl.class));
    	});
    }
    
    public static FlightEvent getRandomEvent(Random rand) {
    	Collection<FlightEvent> events = TardisRegistries.FLIGHT_EVENT.getRegistry().values();
    	int index = rand.nextInt(events.size() - 1);
    	
    	int i = 0;
    	for(FlightEvent eve : events) {
    		if(index == i)
    			return eve;
    		++i;
    	}
    	return events.stream().findFirst().get();
    }

    @Override
    public ResourceLocation getRegistryName() {
        return this.name;
    }

    @Override
    public FlightEvent setRegistryName(ResourceLocation regName) {
        this.name = regName;
        return this;
    }

    public List<Class<? extends IControl>> getControls() {
        return this.controlsToHit;
    }

    public TranslationTextComponent getTranslation() {
        return this.translation;
    }
    
    @Nullable
    public IFlightCrash getCrashAction() {
    	return this.crash;
    }
    
    @FunctionalInterface
    public static interface IFlightCrash{
    	void onCrash(ConsoleTile tile);
    }
}
