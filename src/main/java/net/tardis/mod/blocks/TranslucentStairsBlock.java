package net.tardis.mod.blocks;


import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.StairsBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.INeedItem;

public class TranslucentStairsBlock extends StairsBlock implements INeedItem {


    private BlockItem BLOCKITEM = new BlockItem(this, new Item.Properties().group(TItemGroups.FUTURE)) {
    };


	public TranslucentStairsBlock(Properties prop, Supplier<BlockState> state, SoundType sound, float hardness, float resistance) {

        super(state, prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.TRANSLUCENT;
    }

    @Override
    public Item getItem() {
        return BLOCKITEM;
    }


}
