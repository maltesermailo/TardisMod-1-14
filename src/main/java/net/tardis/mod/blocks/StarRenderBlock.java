package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.tileentities.StarRenderTile;

public class StarRenderBlock extends TileBlock {

	public StarRenderBlock(Properties prop) {
		super(prop);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		
			((StarRenderTile)worldIn.getTileEntity(pos)).setDimensionToRender(TDimensions.MOON_TYPE);
		
		return true;
	}

}
