package net.tardis.mod.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants.BlockRotateMode;
import net.tardis.mod.helper.Helper;

/**
 * Created by 50ap5ud5 on 16 Jul 2020 @ 6:32:43 pm
 * @implNote Allows adjacent blocks in a 1 block radius to rotate 90 degrees when the main block is right clicked
 * @implSpec Allows for rotating monitors
 * 
 */
public class RotatingBlock extends Block{

	public RotatingBlock(Properties prop, SoundType sound, float hardness, float resistance) {
		super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
	}

	@Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn,
			BlockRayTraceResult hit) {
		if (!worldIn.isRemote) {
			List<BlockPos> allPos = new ArrayList<BlockPos>();
			allPos.add(pos.north());
			allPos.add(pos.south());
			allPos.add(pos.east());
			allPos.add(pos.west());
			allPos.forEach(blockPos -> {
				BlockState bState = worldIn.getBlockState(blockPos);
				System.out.println(blockPos);
				System.out.println(bState);
				Helper.rotateBlock(worldIn, blockPos, bState, player.isSneaking() ? BlockRotateMode.CCW : BlockRotateMode.CW);
			});
		}
		return true;
	}
	
	

	@Override
	public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
			ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
	
	

}
