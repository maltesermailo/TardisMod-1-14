package net.tardis.mod.recipe;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.compat.jei.AlembicRecipe;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.RecipeSyncMessage;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class Recipes {
	
	public static IRecipeType<AlembicRecipe> ALEMBIC = new IRecipeType<AlembicRecipe>() {};
	public static List<WeldRecipe> WELD_RECIPE = new ArrayList<WeldRecipe>();
	
	@SubscribeEvent
	public static void register(PlayerEvent.PlayerLoggedInEvent event) {
		if(event.getPlayer() instanceof ServerPlayerEntity)
			Network.sendTo(new RecipeSyncMessage(new ArrayList<>(WELD_RECIPE)), (ServerPlayerEntity)event.getPlayer());
	}

}
