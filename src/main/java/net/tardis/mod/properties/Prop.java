package net.tardis.mod.properties;

import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.Item;
import net.minecraftforge.common.ToolType;
import net.tardis.mod.itemgroups.TItemGroups;

public class Prop {

	public static class Blocks{
		
		public static final Supplier<Block.Properties> BASIC_TECH = () -> Block.Properties.create(Material.IRON)
				.hardnessAndResistance(5F)
				.harvestLevel(0)
				.harvestTool(ToolType.PICKAXE);
		
		public static final Supplier<Block.Properties> BASIC_WOOD = () -> Block.Properties.create(Material.WOOD)
				.hardnessAndResistance(2.0F, 3.0F)
				.sound(SoundType.WOOD)
				.harvestLevel(0)
				.harvestTool(ToolType.AXE);
		
		public static final Supplier<Block.Properties> BASIC_CRYSTAL = () -> Block.Properties.create(Material.GLASS)
				.hardnessAndResistance(1F)
				.harvestLevel(0)
				.harvestTool(ToolType.PICKAXE);

		public static final Supplier<Block.Properties> BASIC_SAND = () -> Block.Properties.create(Material.SAND, MaterialColor.SAND).hardnessAndResistance(0.5F).sound(SoundType.SAND).harvestTool(ToolType.SHOVEL);

		public static final Supplier<Block.Properties> BASIC_STONE = () -> Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F);

		public static final Supplier<Block.Properties> BASIC_GLASS = () -> Block.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS);
				
	}

	public static class Items{
		
		public static final Supplier<Item.Properties> ONE = () -> new Item.Properties()
				.group(TItemGroups.MAIN)
				.maxStackSize(1);
		
		public static final Supplier<Item.Properties> SIXTEEN = () -> new Item.Properties()
				.group(TItemGroups.MAIN)
				.maxStackSize(16);
		
		public static final Supplier<Item.Properties> SIXTY_FOUR = () -> new Item.Properties()
				.group(TItemGroups.MAIN)
				.maxStackSize(64);
	}
}
