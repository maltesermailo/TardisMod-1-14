package net.tardis.mod.dimensions;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.world.gen.GenerationSettings;

public class SpaceGenerationSettings extends GenerationSettings{

	@Override
	public int getVillageDistance() {
		return 0;
	}

	@Override
	public int getVillageSeparation() {
		return 0;
	}

	@Override
	public int getOceanMonumentSpacing() {
		return 0;
	}

	@Override
	public int getOceanMonumentSeparation() {
		return 0;
	}

	@Override
	public int getStrongholdDistance() {
		return 0;
	}

	@Override
	public int getStrongholdCount() {
		return 0;
	}

	@Override
	public int getStrongholdSpread() {
		return 0;
	}

	@Override
	public int getBiomeFeatureDistance() {
		return 0;
	}

	@Override
	public int getBiomeFeatureSeparation() {
		return 0;
	}

	@Override
	public int getShipwreckDistance() {
		return 0;
	}

	@Override
	public int getShipwreckSeparation() {
		return 0;
	}

	@Override
	public int getOceanRuinDistance() {
		return 0;
	}

	@Override
	public int getOceanRuinSeparation() {
		return 0;
	}

	@Override
	public int getEndCityDistance() {
		return 0;
	}

	@Override
	public int getEndCitySeparation() {
		return 0;
	}

	@Override
	public int getMansionDistance() {
		return 0;
	}

	@Override
	public int getMansionSeparation() {
		return 0;
	}

	@Override
	public BlockState getDefaultBlock() {
		return Blocks.AIR.getDefaultState();
	}

	@Override
	public BlockState getDefaultFluid() {
		return Blocks.AIR.getDefaultState();
	}

	@Override
	public void setDefaultBlock(BlockState p_214969_1_) {}

	@Override
	public void setDefaultFluid(BlockState p_214970_1_) {}

	@Override
	public int getBedrockRoofHeight() {
		return 0;
	}

	@Override
	public int getBedrockFloorHeight() {
		return 0;
	}

}
