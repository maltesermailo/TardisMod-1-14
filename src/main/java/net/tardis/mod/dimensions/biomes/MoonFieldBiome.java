package net.tardis.mod.dimensions.biomes;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.tardis.mod.blocks.TBlocks;

public class MoonFieldBiome extends Biome {

	public MoonFieldBiome() {
		super(new Builder()
				.category(Category.PLAINS)
				.downfall(0)
				.temperature(0)
				.precipitation(RainType.NONE)
				.surfaceBuilder(SurfaceBuilder.DEFAULT, new SurfaceBuilderConfig(TBlocks.regolith_sand.getDefaultState(), TBlocks.regolith_packed.getDefaultState(), TBlocks.regolith_sand.getDefaultState()))
				.depth(0.125F)
				.scale(0.05F)
				.waterColor(4159204)
				.waterFogColor(329011)
				.parent(null));
	}

	@Override
	public void decorate(Decoration stage, ChunkGenerator<? extends GenerationSettings> chunkGenerator, IWorld worldIn, long seed, SharedSeedRandom random, BlockPos pos) {
		// TODO Auto-generated method stub
		super.decorate(stage, chunkGenerator, worldIn, seed, random, pos);
	}

	@Override
	public void buildSurface(Random random, IChunk chunkIn, int x, int z, int startHeight, double noise,
			BlockState defaultBlock, BlockState defaultFluid, int seaLevel, long seed) {
		// TODO Auto-generated method stub
		super.buildSurface(random, chunkIn, x, z, startHeight, noise, defaultBlock, defaultFluid, seaLevel, seed);
	}

}
