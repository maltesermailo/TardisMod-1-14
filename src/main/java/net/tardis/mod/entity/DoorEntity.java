package net.tardis.mod.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SEntityVelocityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.api.space.cap.IOxygenSealer;
import net.tardis.api.space.cap.SpaceCapabilities;
import net.tardis.api.space.dimension.IDimProperties;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class DoorEntity extends Entity{

	public static final DataParameter<Integer> STATE = EntityDataManager.createKey(DoorEntity.class, DataSerializers.VARINT);
	public static final DataParameter<Boolean> SUCC = EntityDataManager.createKey(DoorEntity.class, DataSerializers.BOOLEAN);
	public IDoorType doorType = EnumDoorType.STEAM;
	private List<UUID> teleportImmune = new ArrayList<>();
	private boolean isLocked = false;
	private float health = 10;
	
	public DoorEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public DoorEntity(World worldIn) {
		this(TEntities.DOOR, worldIn);
	}

	@Override
	protected void registerData() {
		this.dataManager.register(STATE, EnumDoorState.CLOSED.ordinal());
		this.dataManager.register(SUCC, false);
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(STATE, compound.getInt("door_state"));
		this.getDataManager().set(SUCC, compound.getBoolean("succ"));
		this.isLocked = compound.getBoolean("locked");
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putInt("door_state", this.dataManager.get(STATE));
		compound.putBoolean("locked", this.isLocked);
		compound.putBoolean("succ", this.getDataManager().get(SUCC));
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	public void setOpenState(EnumDoorState open) {
		this.dataManager.set(STATE, open.ordinal());
		if(open != EnumDoorState.CLOSED)
			this.setLocked(false);
	}

	public EnumDoorState getOpenState() {
		return EnumDoorState.values()[this.dataManager.get(STATE)];
	}
	
	public void setSucking(boolean succ) {
		this.getDataManager().set(SUCC, succ);
	}
	
	public boolean getSucking() {
		return this.getDataManager().get(SUCC);
	}
	
	public void openOther() {
		if(!world.isRemote) {
			ConsoleTile console = this.getConsole();
			if(console != null) {
				ExteriorTile ext = console.getExterior().getExterior(console);
				if(ext != null) {
					ext.setDoorState(this.getOpenState());
					if(this.getOpenState() != EnumDoorState.CLOSED) {
						ext.setLocked(false);
					}
				}
			}
		}
	}
	
	@Override
	public void onKillCommand() {
		return;
	}
	
	@Override
	public ItemStack getPickedResult(RayTraceResult target) {
		return new ItemStack(TItems.INT_DOOR);
	}

	@Override
	public void tick() {
		super.tick();
		
		ConsoleTile console = this.getConsole();
		if(console == null)
			return;
		
		if(!world.isRemote) {
			//Teleport entities
			
			if(this.getOpenState() != EnumDoorState.CLOSED && this.ticksExisted > 5) {
				List<Entity> entities = world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox().grow(0.1));
				this.teleportEntity(entities);
				List<UUID> list = new ArrayList<UUID>();
				for(Entity e : entities) {
					if(this.teleportImmune.contains(e.getUniqueID()))
						list.add(e.getUniqueID());
				}
				this.teleportImmune = list;
			}
			
			//Suck if in a space dim
			if(world.getGameTime() % 100 == 0)
				this.setSucking();
		}
		this.doorType = console.getExterior().getDoorType();
		
		if((console.isInFlight() || this.getDataManager().get(SUCC)) && this.getOpenState() != EnumDoorState.CLOSED) {
			this.suckIntoVoid();
		}
		
		
	}
	
	public void addEntityToTeleportedList(UUID id) {
		this.teleportImmune.add(id);
	}

	private void teleportEntity(List<Entity> entity) {
		ConsoleTile console = this.getConsole();
		if(console == null)
			return;
		
		world.getServer().enqueue(new TickDelayedTask(1, () -> {
			
			for(Entity e : entity) {
				if(!(e instanceof DoorEntity)) {
					
					if(console.isInFlight() && !this.getConsole().isRealWorldFlight()) {
						
						if(e instanceof PlayerEntity && console.getOwner().equals(e.getUniqueID())) {
							if(console.getEmotionHandler().getLoyalty() > 50) {
								this.setOpenState(EnumDoorState.CLOSED);
								continue;
							}
							else if(console.getEmotionHandler().getLoyalty() > 25) {
								console.initLand();
							}
						}
						
						BlockPos diff = console.getDestination().subtract(console.getLocation());
						double scale = console.flightTicks / console.getMaxFlightTime();
						DimensionType type = scale > 0.5 ? console.getDestinationDimension() : console.getDimension();
						
						SpaceTimeCoord target = new SpaceTimeCoord(type, world.getServer().getWorld(type).getHeight(Type.WORLD_SURFACE, console.getLocation().add(new BlockPos(diff.getX() * scale, diff.getY() * scale, diff.getZ() * scale))));
						
						//The big Succ
						e.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
							cap.setDestination(target);
							Helper.teleportEntities(e, world.getServer().getWorld(TDimensions.VORTEX_TYPE), 0, 128, 0, e.rotationYaw, e.rotationPitch);
						});
						
						//Suckout non- players too
						if(!e.getCapability(Capabilities.PLAYER_DATA).isPresent()) {
							Helper.teleportEntities(e, world.getServer().getWorld(DimensionType.byName(target.getDimType())), target.getPos().getX() + 0.5, target.getPos().getY() + 1, target.getPos().getZ() + 0.5, e.rotationYaw, e.rotationPitch);
						}
						
						return;
					}
					
					if(this.teleportImmune.contains(e.getUniqueID()))
						continue;
					
					ExteriorTile ext = console.getExterior().getExterior(console);
					if(ext != null)
						ext.addTeleportedEntity(e.getUniqueID());
					
					Vec3d oldMotion = e.getMotion();
					
					ServerWorld newWorld = world.getServer().getWorld(console.getDimension());
					
					Direction dir = console.getExteriorDirection();
					
					float diff = (e.rotationYaw % 360.0F) - this.rotationYaw % 360.0F;
					
					float realFacing = Helper.getAngleFromFacing(dir.getOpposite()) + diff;
					
					BlockPos pos = console.getLocation().offset(dir);
					e.rotationYaw = Helper.getAngleFromFacing(dir.getOpposite());
					
					Helper.teleportEntities(e, newWorld, pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, realFacing, e.rotationPitch);
					
					if(e instanceof PlayerEntity && console.getEntity() != null && !console.isRealWorldFlight()) {
						e.startRiding(console.getEntity());
					}
					
					Vec3d setMot = oldMotion.rotateYaw(-(float)Math.toRadians(realFacing));
					
					world.getServer().enqueue(new TickDelayedTask(2, () -> {
						Entity ent = newWorld.getEntityByUuid(e.getUniqueID());
						if(ent != null)
							ent.setMotion(setMot);
						if(ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity)ent).connection.sendPacket(new SEntityVelocityPacket(ent));
						}
					}));
					
				}
			}
			
		}));
	}
	
	public void suckIntoVoid() {
		for(LivingEntity e : world.getEntitiesWithinAABB(LivingEntity.class, this.getBoundingBox().grow(40))) {
			if(e.canEntityBeSeen(this)) {
				e.stopRiding();
				if (e.isSleeping())
					e.wakeUp();
				Vec3d motion = this.getPositionVec().subtract(e.getPositionVec()).normalize().scale(0.1);
				e.setMotion(e.getMotion().add(motion));
				e.isAirBorne = true;
				e.onGround = false;
			}
		}
	}
	
	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		if(!world.isRemote) {
			ExteriorTile ext = this.getConsole().getExterior().getExterior(this.getConsole());
			if(player.isSneaking()) {
				if(this.isLocked) {
					this.isLocked = false;
					world.playSound(null, this.getPosition(), TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 1F, 1F);
					this.playSoundAtExterior(ext, TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 1F);
                    if (ext != null)
                        ext.setLocked(false);
					player.sendStatusMessage(ExteriorBlock.UNLOCKED, true);
					return true;
				}
				else {
					this.isLocked = true;
					this.setOpenState(EnumDoorState.CLOSED);
					this.openOther();
					if(ext != null) {
						ext.setLocked(isLocked);
					}
					world.playSound(null, this.getPosition(), TSounds.DOOR_LOCK, SoundCategory.BLOCKS, 1F, 1F);
					this.playSoundAtExterior(ext, TSounds.DOOR_LOCK, SoundCategory.BLOCKS, 1F);
					player.sendStatusMessage(ExteriorBlock.LOCKED, true);
					return true;
				}
			}
			
			if(this.isLocked) {
				player.sendStatusMessage(ExteriorBlock.LOCKED, true);
				return true;
			}
			
			EnumDoorState[] valid = this.doorType.getValidStates();
			int index = this.getOpenState().ordinal() + 1;
			if(index >= valid.length)
				index = 0;
			this.setOpenState(valid[index]);
			this.openOther();
			if (this.getOpenState() == EnumDoorState.CLOSED) {
				world.playSound(null, player.getPosition(), TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 1F, 1F);
				this.playSoundAtExterior(ext, TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 1F);
			}
			else {
				world.playSound(null, player.getPosition(), TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 1F, 1F);
				this.playSoundAtExterior(ext, TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 1F);
			}
			
			
		}
		return true;
	}
	
	public void setSucking() {
		if(!world.isRemote) {
			ServerWorld ws = world.getServer().getWorld(this.getConsole().getDimension());
			if(ws.getDimension() instanceof IDimProperties) {
				//If would get sucked out
				boolean suck = !((IDimProperties)ws.getDimension()).hasAir();
				
				//Account for Oxygen sealers
				
				ExteriorTile ext = this.getConsole().getExterior().getExterior(this.getConsole());
				
				//If the exterior doesn't exist, it can't be in space
				if(ext == null)
					return;
				
				Direction dir = ext.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
				
				for(TileEntity te : ws.loadedTileEntityList) {
					IOxygenSealer oxy = te.getCapability(SpaceCapabilities.OXYGEN_SEALER).orElse(null);
					if(oxy != null) {
						if(oxy.getSealedPositions().contains(ext.getPos().offset(dir))) {
							suck = false;
							break;
						}
					}
				}
				
				//Actually set
				if(this.getDataManager().get(SUCC) != suck)
					this.getDataManager().set(SUCC, suck);
				
			}
			else if(this.getDataManager().get(SUCC))
				this.getDataManager().set(SUCC, false);
		}
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}
	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if(!world.isRemote) {
			if (source.getTrueSource() instanceof PlayerEntity) {
				PlayerEntity playerIn = (PlayerEntity) source.getTrueSource();
				if (playerIn.abilities.isCreativeMode) {
					this.health = 0;
				}
				else{
					this.health -= amount;
				}
			}
			if(this.health <= 0) {
				ItemEntity entity = new ItemEntity(world, posX, posY, posZ, this.getPickedResult(null));
				world.addEntity(entity);
				remove();
			}
			this.world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
		}
		return true;
	}
	
	@Nullable
	public ConsoleTile getConsole() {
		TileEntity te = this.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile)
			return (ConsoleTile) te;
		return null;
	}
	
	public void setLocked(boolean locked) {
		this.isLocked = locked;
	}
	
	public boolean isLocked() {
		return this.isLocked;
	}
	
	public void playSoundAtExterior(@Nullable ExteriorTile tile, SoundEvent event, SoundCategory cat, float vol) {
		if(tile != null && tile.hasWorld())
			tile.getWorld().playSound(null, tile.getPos(), event, cat, vol, 1F);
	}
	
	public void updateOther() {
		ConsoleTile tile = this.getConsole();
		if(tile != null) {
			ExteriorTile ext = tile.getExterior().getExterior(tile);
			if(ext != null) {
				ext.setDoorState(this.getOpenState());
				ext.setLocked(this.isLocked);
				ext.updateClient();
			}
		}
	}
	
}
