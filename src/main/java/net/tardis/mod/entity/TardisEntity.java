package net.tardis.mod.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.horse.HorseEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SEntityVelocityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.controls.RealWorldFlightControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.entity.ai.FollowIntoTardisGoal;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TardisEntity extends Entity{

	public static final DataParameter<String> EXTERIOR = EntityDataManager.createKey(TardisEntity.class, DataSerializers.STRING);
	public static final DataParameter<CompoundNBT> EXTERIOR_DATA = EntityDataManager.createKey(TardisEntity.class, DataSerializers.COMPOUND_NBT);
	private ConsoleTile console;
	private int sneakTicks = 0;
    private IExterior exterior;
    private DimensionType interiorDimension;
    
    private boolean hasLanded = false;
    private boolean canDismount = false;
    private boolean jumping = false;
    
    public float renderYaw = 0;
	
    //Readonly
    private ExteriorTile tile;
    
	public TardisEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public TardisEntity(World worldIn) {
		this(TEntities.TARDIS, worldIn);
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(EXTERIOR, ExteriorRegistry.TRUNK.getRegistryName().toString());
		this.getDataManager().register(EXTERIOR_DATA, new CompoundNBT());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(EXTERIOR, compound.getString("exterior"));
		
		if(compound.contains("exterior_data")) {
			CompoundNBT tag = compound.getCompound("exterior_data");
			this.dataManager.set(EXTERIOR_DATA, tag);
			this.readExteriorFromData(tag);
		}
		
		if(!world.isRemote && compound.contains("interior_dimension")) {
			TardisHelper.getConsole(world.getServer(), DimensionType.byName(new ResourceLocation(compound.getString("interior_dimension"))))
				.ifPresent(console -> this.console = console);
		}
	}
	
	public void readExteriorFromData(CompoundNBT tag) {
		TileEntityType<?> type = ForgeRegistries.TILE_ENTITIES.getValue(new ResourceLocation(tag.getString("id")));
		if(type != null) {
			this.tile = (ExteriorTile) type.create();
			this.tile.setWorld(world);
			tile.deserializeNBT(tag);
		}
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putString("exterior", this.getDataManager().get(EXTERIOR));
		
		if(tile != null)
			compound.put("exterior_data", tile.serializeNBT());
		
		if(this.console != null)
			compound.putString("interior_dimension", DimensionType.getKey(this.console.getWorld().dimension.getType()).toString());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		if(world.isRemote) {
			if(isBeingRidden()) {
				Tardis.proxy.forceThirdPerson();
			}
		}
		
		if(isBeingRidden()) {
			if(this.getControllingPassenger() instanceof PlayerEntity) {
				this.renderYaw += 5;
				
				if(!world.isRemote) {
					this.setMotion(Vec3d.ZERO);
					
					PlayerEntity entity = (PlayerEntity) this.getControllingPassenger();
					
					this.prevRotationPitch = this.rotationPitch;
					this.prevRotationYaw = this.rotationYaw;
					
					this.rotationPitch = entity.rotationPitch;
					this.rotationYaw = entity.rotationYaw;
					
					float speed = this.getConsole().getControl(ThrottleControl.class).getAmount();
					
					Vec3d lookVec = Helper.getVectorForRotation(0, this.rotationYaw).scale(speed);
					
					if(entity.moveForward > 0) {
						this.setMotion(this.getMotion().add(lookVec.x, 0, lookVec.z));
					} else if (entity.moveForward < 0) {
						this.setMotion(this.getMotion().add(-lookVec.x, 0, -lookVec.z));
					}
					
					if(entity.moveStrafing > 0) {
						Vec3d vec = Helper.getVectorForRotation(0, this.rotationYaw - 90F).scale(speed);
						
						this.setMotion(this.getMotion().add(vec.x, 0, vec.z));
					} else if (entity.moveStrafing < 0) {
						Vec3d vec = Helper.getVectorForRotation(0, this.rotationYaw + 90F).scale(speed);
						
						this.setMotion(this.getMotion().add(vec.x, 0, vec.z));
					}
					
					if(this.isJumping()) {
						this.setMotion(this.getMotion().add(0, 1 * speed, 0));
						this.setJumping(false);
					}
					
					if(entity.isSneaking()) {
						this.setMotion(this.getMotion().add(0, -1 * speed, 0));
					}
					
					//Update Tardis Screen
					this.getConsole().setLocation(this.dimension, this.getPosition());
					this.getConsole().updateFlightTime();
				}
			}
		}
		
		this.move(MoverType.SELF, this.getMotion());
		if(!this.hasNoGravity() && this.getMotion().y > -3)
			this.setMotion(this.getMotion().add(0, -0.08, 0));
		
		if(!world.isRemote && !console.isRealWorldFlight()) {
			
			if(this.getConsole() != null && world.getGameTime() % 20 == 0) {
				console.setLocation(world.getDimension().getType(), this.getPosition());
			}
			
			//Damage
			if(this.getPassengers().isEmpty()) {
				boolean hasWackeed = false;
				for(Entity ent : world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox())) {
					ent.attackEntityFrom(DamageSource.ANVIL, 5);
				}
				if(hasWackeed)
					world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
			}
			
			if(this.onGround || (world.getBlockState(this.getPosition().down())).isSolid())
				remove();
				
			if(posY < 0) {
				this.setPosition(posX, 2, posZ);
				this.setMotion(Vec3d.ZERO);
				if(console != null) {
					console.setAntiGrav(true);
					this.remove();
				}
			}
		}
	}
	
	public boolean isJumping() {
		return jumping;
	}
	
	public void setJumping(boolean jumping) {
		this.jumping = jumping;
	}
	
	@Override
	public void remove() {
		super.remove();
		if(!this.hasLanded)
			land();
	}

	@Override
	public void updateRidden() {
		super.updateRidden();

        if (this.onGround) {
        	
            if (this.getRidingEntity().isSneaking())
                ++sneakTicks;
            else this.sneakTicks = 0;
            if (sneakTicks > 30)
                console.stopRide(false);
        }
        else this.sneakTicks = 0;
	}
	
	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		if(!player.world.isRemote) {
			player.world.getServer().enqueue(new TickDelayedTask(1, () -> {
				double x = 0, y = TardisHelper.TARDIS_POS.getY(), z = 0;
				ConsoleTile console = null;
				ServerWorld ws = this.world.getServer().getWorld(this.getConsole().getInteriorDimension());
				
				//Get Console
				if(ws != null) {
					TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
					if(te instanceof ConsoleTile)
						console = (ConsoleTile)te;
				}
				
				//If an interior door exists, put the player near it
				DoorEntity door = console != null ? console.getDoor().orElse(null) : null;
				
				float rotationYaw = door != null ? door.rotationYaw : player.rotationYaw;
				
				System.out.println("TEst" + player);
				
				if(door != null)
					door.addEntityToTeleportedList(player.getUniqueID());
				
				Helper.teleportEntities(player, ws, x, y, z, rotationYaw, player.rotationPitch);
				
				//Motion
				
				Vec3d oldMotion = player.getMotion();
				Vec3d setMot = oldMotion.rotateYaw(-(float)Math.toRadians(door.rotationYaw));
				
				world.getServer().enqueue(new TickDelayedTask(1, () -> {
					Entity ent = ws.getEntityByUuid(player.getUniqueID());
					if(ent != null)
						ent.setMotion(setMot);
					if(ent instanceof ServerPlayerEntity) {
						((ServerPlayerEntity)ent).connection.sendPacket(new SEntityVelocityPacket(ent));
					}
				}));
			}));
		}
		return false;
	}
	
	public void setCanDismount(boolean canDismount) {
		this.canDismount = canDismount;
	}
	
	public boolean canDismount() {
		return canDismount;
	}
	
	@Override
	public boolean canBeRiddenInWater(Entity rider) {
		return true;
	}

	public void land() {
        if (!world.isRemote && console != null && !hasLanded && !console.isRealWorldFlight()) {
        	console.setLocation(world.getDimension().getType(), this.getPosition());
			console.getExterior().place(console, world.getDimension().getType(), this.getPosition());
			console.setEntity(null);
        } else if ((!world.isRemote && console != null && console.isRealWorldFlight() && this.getControllingPassenger() != null)) {
        	console.stopRide(true);
        }
        
		hasLanded = true;
	}
	
	public void setHasLanded(boolean hasLanded) {
		this.hasLanded = hasLanded;
	}
	
	@Override
	public Entity getControllingPassenger() {
		if(this.getPassengers().size() > 0) {
			return this.getPassengers().get(0);
		}
		
		return null;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}
	
	@Override
	public boolean canPassengerSteer() {
		return true;
	}
	
	@Override
	protected boolean canBeRidden(Entity entityIn) {
		// Prevents players from accidentally riding the TARDIS
		return true;
	}

    public IExterior getExterior() {
        if (exterior == null) {
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
        }
        return this.exterior;
    }
    
    @Nullable
    public ExteriorTile getExteriorTile() {
    	if(this.tile == null)
    		this.readExteriorFromData(this.getDataManager().get(EXTERIOR_DATA));
    	return this.tile;
    }
    
    public void setExteriorTile(ExteriorTile tile) {
    	this.tile = tile;
    	this.dataManager.set(EXTERIOR_DATA, tile.serializeNBT());
    }
    
    public ConsoleTile getConsole() {
    	return console;
    }
    
    public DimensionType getInteriorDimension() {
		return interiorDimension;
	}
    
    public void setInteriorDimension(DimensionType interiorDimension) {
		this.interiorDimension = interiorDimension;
	}

    @Override
	public Entity changeDimension(DimensionType destination) {
		// TODO Auto-generated method stub
		return super.changeDimension(destination);
	}

	public void setConsole(ConsoleTile tile) {
        this.console = tile;
        this.exterior = tile.getExterior();
        if (!world.isRemote)
        	this.getDataManager().set(EXTERIOR, this.exterior.getRegistryName().toString());
    }

}
