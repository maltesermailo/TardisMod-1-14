package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.controls.ControlRegistry.ControlEntry;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateControlMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class ControlEntity extends Entity{

	private static final DataParameter<String> CONTROL = EntityDataManager.createKey(ControlEntity.class, DataSerializers.STRING);
	private IControl controlObject;
	private ConsoleTile console;
	private boolean shouldTick = false;
	private boolean firstTick = true;
	
	private int timeTillUpdate = 30;
	
	public ControlEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		
	}

	public ControlEntity(World world) {
		this(TEntities.CONTROL, world);
	}

	/*
	 * Must be done before spawning, unless you're an idiot
	 */
	public void setControl(IControl control) {
		this.controlObject = control;
		if(!world.isRemote)
			this.dataManager.set(CONTROL, control.getRegistryName().toString());
		this.updateControlValues();
	}
	
	public IControl getControl() {
		if(this.controlObject == null) {
			ControlEntry<?> entry = ControlRegistry.getControl(new ResourceLocation(this.getDataManager().get(CONTROL)));
			if(entry != null)
				return (this.controlObject = entry.spawn(this.getConsole()));
		}
		return this.controlObject;
	}
	
	public void updateControlValues() {
		this.recalculateSize();
		this.shouldTick = this.getControl() instanceof ITickable;
	}
	
	@Override
	protected void registerData() {
		this.getDataManager().register(CONTROL, this.controlObject != null ? controlObject.getRegistryName().toString() : "");
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(CONTROL, compound.getString("control"));
		this.getControl().deserializeNBT(compound.getCompound("control_data"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putString("control", this.getControl().getRegistryName().toString());
		compound.put("control_data", this.controlObject.serializeNBT());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		if(!this.world.isRemote && this.getControl() == null)
			remove();
		
		if(this.shouldTick)
			((ITickable) this.getControl()).tick(this.getConsole());
		
		if(this.ticksExisted % 20 == 0) {
			if(!world.isRemote)
				this.getDataManager().set(CONTROL, this.getControl().getRegistryName().toString());
			this.updateControlValues();
		}
		
		if(this.getControl() != null && this.getControl().getAnimationTicks() > 0)
			this.getControl().setAnimationTicks(this.getControl().getAnimationTicks() - 1);
		
		if(this.ticksExisted == 5) {
			if(!world.isRemote)
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		}
		
		if(timeTillUpdate > 0) {
			--timeTillUpdate;
			if(this.timeTillUpdate == 0 && !world.isRemote)
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		}
		
		if(!world.isRemote && this.ticksExisted > 20 && this.getControl() != null && this.getControl().isDirty()) {
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
			this.getControl().clean();
		}
		
		if(this.firstTick) {
			this.timeTillUpdate = 20;
			this.firstTick = false;
		}
	}

	@Override
	public EntitySize getSize(Pose poseIn) {
		return this.getControl() == null ? super.getSize(poseIn) : this.getControl().getSize();
	}

	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		if(!this.isControlVaild())
			return false;

		//Flight Events
		if (this.getConsole().getAvoidanceControls().contains(this.getControl().getClass())) {
			this.getConsole().getAvoidanceControls().remove(this.getControl().getClass());
			this.setAnimationTicks(this.getControl().getMaxAnimationTicks());
			this.world.playSound(null, this.getPosition(), this.getControl().getSuccessSound(getConsole()), SoundCategory.PLAYERS, 1F, 1F);
			return true;
		}
		
		boolean worked = this.getControl().onRightClicked(this.getConsole(), player);
		world.playSound(null, getPosition(), worked ? this.getControl().getSuccessSound(this.getConsole()) : this.getControl().getFailSound(this.getConsole()), SoundCategory.BLOCKS, 1, 1);
		if(!world.isRemote)
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		this.getConsole().setPilot(player);
		
		return true;
	}

	@Override
	public boolean hitByEntity(Entity entityIn) {
		if(!this.isControlVaild())
			return false;

		//Flight Events
		if (this.getConsole().getAvoidanceControls().contains(this.getControl().getClass())) {
			this.getConsole().getAvoidanceControls().remove(this.getControl().getClass());
			this.world.playSound(null, this.getPosition(), this.getControl().getSuccessSound(getConsole()), SoundCategory.PLAYERS, 1F, 1F);
			this.setAnimationTicks(20);
			return true;
		}
		
		if(entityIn instanceof PlayerEntity) {
			this.getControl().onHit(this.getConsole(), (PlayerEntity)entityIn);
			if(world instanceof ServerWorld)
				world.playSound(null, getPosition(), this.getControl().getSuccessSound(this.getConsole()), SoundCategory.BLOCKS, 1, 1);
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		}
		return super.hitByEntity(entityIn);
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean canBeAttackedWithItem() {
		return true;
	}

	@Override
	public ITextComponent getDisplayName() {
		return controlObject == null ? new StringTextComponent("None") : this.getControl().getDisplayName();
	}
	
	private ConsoleTile getConsole() {
		return this.console;
	}
	
	public void setConsole(ConsoleTile console) {
		this.console = console;
		if(this.getControl() != null)
			this.getControl().setConsole(console);
	}
	
	public void setAnimationTicks(int ticks) {
		this.getControl().setAnimationTicks(ticks);
		if (!world.isRemote)
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
	}
	
	@Override
	public void onAddedToWorld() {
		super.onAddedToWorld();
		timeTillUpdate = 30;
	}
	
	public boolean isControlVaild(){
		if(this.getControl() == null)
			return false;
		return this.getControl().getConsole() != null && this.getControl().getConsole().hasWorld();
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public int getBrightnessForRender() {
		//Seems this is full- bright... what an odd number
		return 15728880;
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}
}
