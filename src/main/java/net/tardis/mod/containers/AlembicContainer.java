package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.tardis.mod.containers.slot.FilteredSlot;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicContainer extends Container{

	AlembicTile tile;
	
	public AlembicContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.ALEMBIC, id);
		this.init(tile = (AlembicTile)inv.player.world.getTileEntity(buf.readBlockPos()), inv);
	}
	
	public AlembicContainer(int id, PlayerInventory inv, AlembicTile tile) {
		super(TContainers.ALEMBIC, id);
		this.init(this.tile = tile, inv);
	}
	
	public void init(AlembicTile tile, PlayerInventory inv) {
		
		this.addSlot(new Slot(tile, 0, 19, 1));//Holds water
		this.addSlot(new Slot(tile, 1, 19, 67));//Can take, no place
		this.addSlot(new FilteredSlot(tile, 2, 48, 30, item -> item.getItem() == TItems.CINNABAR));
		this.addSlot(new Slot(tile, 3, 48, 73));//Furnace Fuel slot
		this.addSlot(new FilteredSlot(tile, 4, 135, 2, item -> item.getItem() == Items.GLASS_BOTTLE));
		this.addSlot(new Slot(tile, 5, 135, 66));//Can take, no place
		for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            this.addSlot(new Slot(inv, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + 4));
	         }
	      }

	      for(int i1 = 0; i1 < 9; ++i1) {
	         this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 161 + 4));
	      }
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	public AlembicTile getAlembic() {
		return tile;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		Slot slot = this.getSlot(index);
		if(slot.inventory instanceof PlayerInventory) {
			//Empty bottles
			if(slot.getStack().getItem() == Items.GLASS_BOTTLE) {
				ItemStack stack = this.tile.insertItem(4, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			//Fuel
			if(slot.getStack().getBurnTime() > 0) {
				ItemStack stack = this.tile.insertItem(3, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			//Water
			if(slot.getStack().getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).orElse(null) != null) {
				ItemStack stack = tile.insertItem(0, slot.getStack().copy(), false);
				slot.putStack(ItemStack.EMPTY);
				return stack;
			}
			//Cinnabar
			if(slot.getStack().getItem().isIn(TardisItemTags.CINNABAR)) {
				ItemStack stack = tile.insertItem(2, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
		}
		else {
			ItemStack stack = slot.getStack().copy();
			if(playerIn.addItemStackToInventory(stack)) {
				slot.putStack(ItemStack.EMPTY);
				return stack;
			}
		}
		return ItemStack.EMPTY;
	}


}
