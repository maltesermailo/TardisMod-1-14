package net.tardis.mod.tags;
import net.minecraft.block.Block;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class TardisBlockTags {

	public static final Tag<Block> ARS = new BlockTags.Wrapper(new ResourceLocation(Tardis.MODID, "ars"));
	public static final Tag<Block> BLOCKED = new BlockTags.Wrapper(new ResourceLocation(Tardis.MODID, "blocked"));;
}
