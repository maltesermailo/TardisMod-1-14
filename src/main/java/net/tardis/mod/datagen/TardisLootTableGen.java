package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.blocks.TBlocks;

public class TardisLootTableGen implements IDataProvider{

	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator generator;
	
	public TardisLootTableGen(DataGenerator gen) {
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		Path path = this.generator.getOutputFolder();
		
		this.generateSelfTable(TBlocks.moon_base_glass, cache, path);
		this.generateSelfTable(TBlocks.regolith_packed, cache, path);
		this.generateSelfTable(TBlocks.regolith_sand, cache, path);
		this.generateSelfTable(TBlocks.airlock, cache, path);
		this.generateSelfTables(cache, path, 
				TBlocks.alabaster_bookshelf,
				TBlocks.ebony_bookshelf);
		
	}
	
	public void generateSelfTables(DirectoryCache cache, Path base, Block... blocks) throws IOException {
		for(Block b : blocks)
			this.generateSelfTable(b, cache, base);
	}
	
	public static Path getPath(Path path, ResourceLocation rl) {
		return path.resolve("data/" + rl.getNamespace() + "/loot_tables/" + rl.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "Loot Tables";
	}
	
	public void generateSelfTable(Block block, DirectoryCache cache, Path base) throws IOException {
		//IDataProvider.save(GSON, cache, this.createBlockDropSelf(block), getPath(base, block.getRegistryName()));
		this.generateTable(cache, getPath(base, block.getRegistryName()), () -> this.createBlockDropSelf(block));
	}
	
	public void generateTable(DirectoryCache cache, Path path, Supplier<JsonElement> element) throws IOException {
		IDataProvider.save(GSON, cache, element.get(), path);
	}
	
	/**
	 * 
	 * @param Block - Block to generate loot table for
	 * @param drop - Item to drop, always
	 * @return
	 */
	public JsonElement createBlockDropGuarenteed(Block block, ResourceLocation drop) {
		JsonObject root = new JsonObject();
		root.add("type", new JsonPrimitive("minecraft:block"));
		
		JsonArray pool = new JsonArray();
		
		JsonObject first = new JsonObject();
		
		first.add("name", new JsonPrimitive(block.getRegistryName().toString()));
		first.add("rolls", new JsonPrimitive(1));
		
		JsonArray entries = new JsonArray();
		
		JsonObject entry = new JsonObject();
		
		entry.add("type", new JsonPrimitive("minecraft:item"));
		entry.add("name", new JsonPrimitive(drop.toString()));
		
		entries.add(entry);
		
		first.add("entries", entries);
		
		pool.add(first);
		
		root.add("pools", pool);
		
		return root;
	}
	
	public JsonElement createBlockDropSelf(Block block) {
		return this.createBlockDropGuarenteed(block, block.getRegistryName());
	}

}
