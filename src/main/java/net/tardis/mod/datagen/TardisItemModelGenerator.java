package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;

public class TardisItemModelGenerator implements IDataProvider {

	public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	DataGenerator generator;
	
	public TardisItemModelGenerator(DataGenerator generator){
		this.generator = generator;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		Path base = generator.getOutputFolder();
		
		generateSpriteItem(getPath(base, TItems.SPACE_HELM), cache, "space_helmet");
		generateSpriteItem(getPath(base, TItems.SPACE_CHEST), cache, "space_chest");
		generateSpriteItem(getPath(base, TItems.SPACE_LEGS), cache, "space_legs");
		generateSpriteItem(getPath(base, TItems.SPACE_BOOTS), cache, "space_boots");
		
		for(Block b : ForgeRegistries.BLOCKS) {
			if(b.getRegistryName().getNamespace().equals(Tardis.MODID)) {
				IDataProvider.save(GSON, cache, this.createBlockModel(b), getPath(base, b.asItem()));
			}
		}
		
	}
	
	public static Path getPath(Path base, Item item) {
		ResourceLocation key = item.getRegistryName();
		return base.resolve("assets/" + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
	}
	
	public void generateSpriteItem(Path path, DirectoryCache cache, String... textures) throws IOException {
		JsonObject doc = new JsonObject();
		doc.add("parent", new JsonPrimitive("item/generated"));
		
		JsonObject tex = new JsonObject();
		int index = 0;
		for(String s : textures) {
			tex.add("layer" + index, new JsonPrimitive("tardis:item/" + s));
		}
		doc.add("textures", tex);
		
		IDataProvider.save(GSON, cache, doc, path);
		
	}
	
	public JsonObject createBlockModel(Block block) {
		ResourceLocation key = block.getRegistryName();
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));
		return root;
	}

	@Override
	public String getName() {
		return "TARDIS Item Model";
	}

}
