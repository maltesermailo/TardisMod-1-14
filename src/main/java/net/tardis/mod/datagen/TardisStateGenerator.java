package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.blocks.TBlocks;

public class TardisStateGenerator implements IDataProvider{

	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator generator;
	
	public TardisStateGenerator(DataGenerator gen) {
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		Path base = generator.getOutputFolder();
		
		IDataProvider.save(GSON, cache, this.createFullCube("block/alabaster_bookshelf"), getPath(base, TBlocks.alabaster_bookshelf.getRegistryName()));
		IDataProvider.save(GSON, cache, this.createFullCube("block/ebony_bookshelf"), getPath(base, TBlocks.ebony_bookshelf.getRegistryName()));
		
	}

	@Override
	public String getName() {
		return "Tardis Block Model Generator";
	}

	public static Path getPath(Path base, ResourceLocation file) {
		return base.resolve("assets/" + file.getNamespace() + "/blockstates/" + file.getPath() + ".json");
	}
	
	
	private JsonObject createFullCube(String model) {
		JsonObject root = new JsonObject();
		
			JsonObject variants = new JsonObject();
			
				JsonObject empty = new JsonObject();
				empty.add("model", new JsonPrimitive("tardis:" + model));
				variants.add("", empty);
			
			root.add("variants", variants);
		
		return root;
	}
}
