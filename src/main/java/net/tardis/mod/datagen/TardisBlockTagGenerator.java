package net.tardis.mod.datagen;

import net.minecraft.block.Block;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.StairsBlock;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.Tag;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.blocks.RoundelBlock;
import net.tardis.mod.tags.TardisBlockTags;

import java.util.Comparator;
import java.util.function.Predicate;

public class TardisBlockTagGenerator extends BlockTagsProvider{

	public TardisBlockTagGenerator(DataGenerator generatorIn) {
		super(generatorIn);
	}

	@Override
	protected void registerTags() {

		Predicate<Block> isTardis = block -> Tardis.MODID.equals(block.getRegistryName().getNamespace());
        Tag<Block>[] immuneArray = new Tag[]{BlockTags.DRAGON_IMMUNE, BlockTags.WITHER_IMMUNE};

		for(Block b : ForgeRegistries.BLOCKS) {

			/* Slab Blocks */
			if(b instanceof SlabBlock && isTardis.test(b)){
				getBuilder(BlockTags.SLABS).add(b);
			}

			/* Stair Blocks */
			if(b instanceof StairsBlock && isTardis.test(b)){
				getBuilder(BlockTags.STAIRS).add(b);
			}

            if(b instanceof ConsoleBlock || b instanceof ExteriorBlock) {
                for (Tag<Block> blockTag : immuneArray) {
                    getBuilder(blockTag).add(b);
                }
            }


			/* ARS Blocks */
			if(b instanceof IARS || b instanceof RoundelBlock) {
				this.getBuilder(TardisBlockTags.ARS).add(b);
			}
		}
	}

}
