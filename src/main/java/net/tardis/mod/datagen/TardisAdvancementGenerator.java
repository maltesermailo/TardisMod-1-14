package net.tardis.mod.datagen;


import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.criterion.InventoryChangeTrigger;
import net.minecraft.advancements.criterion.LocationPredicate;
import net.minecraft.advancements.criterion.PositionTrigger;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.items.TItems;

public class TardisAdvancementGenerator implements IDataProvider {
	
   private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();
   private static List<Advancement> advancements = new ArrayList<>(); 
   private final DataGenerator generator;

   public TardisAdvancementGenerator(DataGenerator generatorIn) {
      this.generator = generatorIn;
   }

   /**
    * Performs this provider's action.
    */
   public void act(DirectoryCache cache) throws IOException {
      Path path = this.generator.getOutputFolder();
      
      this.createAdvancement(
    		  "moon",
    		  "moon",
    		  new ItemStack(TItems.SPACE_HELM),
    		  PositionTrigger.Instance.forLocation(LocationPredicate.forBiome(TDimensions.MOON_FIELD)));
      
      this.createAdvancement(
    		  "space_suit",
    		  "space_suit",
    		  new ItemStack(TItems.SPACE_HELM),
    		  InventoryChangeTrigger.Instance.forItems(() -> TItems.SPACE_BOOTS, () -> TItems.SPACE_CHEST, () -> TItems.SPACE_HELM, () -> TItems.SPACE_LEGS));
      
      for(Advancement adv : advancements) {
    	  IDataProvider.save(GSON, cache, adv.copy().serialize(), getPath(path, adv));
      }

   }

   private static Path getPath(Path pathIn, Advancement advancementIn) {
      return pathIn.resolve("data/" + advancementIn.getId().getNamespace() + "/advancements/" + advancementIn.getId().getPath() + ".json");
   }

   /**
    * Gets a name for this provider, to use in logging.
    */
   public String getName() {
      return "Advancements";
   }
   
   public Advancement create(String name, String title, ItemStack display, ICriterionInstance... inst) {
	  Advancement.Builder adv = Advancement.Builder.builder()
		  .withDisplay(
				  display,
				  new TranslationTextComponent("advancements.tardis.title." + title),
				  new TranslationTextComponent("advancements.tardis.desc." + title),
				  null,
				  FrameType.TASK,
				  true,
				  true,
				  false);
	  int i = 0;
	  for(ICriterionInstance in : inst) {
		  adv = adv.withCriterion(i + "", in);
		  i++;
	  }
	  return adv.build(new ResourceLocation(Tardis.MODID, name));
   }
   
   public void createAdvancement(String name, String title, ItemStack display, ICriterionInstance inst) {
	   advancements.add(this.create(name, title, display, inst));
   }
}