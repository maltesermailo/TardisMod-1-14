package net.tardis.mod.client.animation;

import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorAnimation implements IExteriorAnimation {
	
	protected ExteriorTile exterior;
	private ExteriorAnimationEntry<?> type;
	
	public ExteriorAnimationEntry<?> getType(){
		return type;
	}

    public ExteriorAnimation(ExteriorAnimationEntry<?> entry, ExteriorTile tile) {
		this.type = entry;
		this.exterior = tile;
	}
	
	public static void registerAll() {
		TardisRegistries.registerRegisters(() -> {
			TardisRegistries.EXTERIOR_ANIMATIONS.register("classic", new ExteriorAnimationEntry<>("anim.tardis.exterior.classic", ClassicExteriorAnimation::new));
			TardisRegistries.EXTERIOR_ANIMATIONS.register("new_who", new ExteriorAnimationEntry<>("anim.tardis.exterior.new_who", NewWhoExteriorAnimation::new));
		});
	}
	
}