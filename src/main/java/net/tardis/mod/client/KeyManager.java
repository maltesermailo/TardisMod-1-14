package net.tardis.mod.client;

import org.lwjgl.glfw.GLFW;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class KeyManager {
	
	public static KeyBinding KEY_STOP_RIDE = new KeyBinding("TARDIS Stop Ride", GLFW.GLFW_KEY_F, "TARDIS Mod");
	
	public static void init() {
		ClientRegistry.registerKeyBinding(KEY_STOP_RIDE);
	}

}
