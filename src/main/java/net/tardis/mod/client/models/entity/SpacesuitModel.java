package net.tardis.mod.client.models.entity;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.player.PlayerEntity;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SpacesuitModel extends BipedModel<PlayerEntity> {
	
	public static final SpacesuitModel INSTANCE = new SpacesuitModel();
	
	private final RendererModel ring;
	private final RendererModel belt;
	private final RendererModel tank;
	private final RendererModel colar;
	private final RendererModel rightsholder;
	private final RendererModel patch;
	private final RendererModel leftsholder;

	public SpacesuitModel() {
		textureWidth = 128;
		textureHeight = 128;

		bipedHead = new RendererModel(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 0, -5.0F, -9.0F, -5.0F, 10, 8, 10, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 50, 78, 0.5F, -6.25F, 4.25F, 1, 3, 2, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 44, 78, -1.5F, -6.25F, 4.25F, 1, 3, 2, 0.0F, false));

		ring = new RendererModel(this);
		ring.setRotationPoint(0.5F, -0.5F, 0.25F);
		bipedHead.addChild(ring);
		setRotationAngle(ring, 0.2618F, 0.0F, 0.0F);
		ring.cubeList.add(new ModelBox(ring, 0, 18, -6.0F, -2.5F, -5.0F, 11, 1, 11, 0.0F, false));

		bipedBody = new RendererModel(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedBody.cubeList.add(new ModelBox(bipedBody, 44, 18, -5.0F, 0.0F, -2.5F, 10, 12, 5, 0.0F, false));
		bipedBody.cubeList.add(new ModelBox(bipedBody, 42, 69, -2.5F, 1.5F, -3.5F, 5, 5, 1, 0.0F, false));

		belt = new RendererModel(this);
		belt.setRotationPoint(0.0F, 24.0F, 0.0F);
		bipedBody.addChild(belt);
		belt.cubeList.add(new ModelBox(belt, 10, 78, -5.5F, -15.5F, -3.0F, 3, 4, 2, 0.0F, false));
		belt.cubeList.add(new ModelBox(belt, 0, 78, 2.5F, -15.5F, -3.0F, 3, 4, 2, 0.0F, false));
		belt.cubeList.add(new ModelBox(belt, 64, 69, 2.5F, -15.5F, 1.0F, 3, 4, 2, 0.0F, false));
		belt.cubeList.add(new ModelBox(belt, 54, 69, -5.25F, -15.5F, 1.0F, 3, 4, 2, 0.0F, false));

		tank = new RendererModel(this);
		tank.setRotationPoint(0.5F, 22.0F, 0.25F);
		bipedBody.addChild(tank);
		tank.cubeList.add(new ModelBox(tank, 24, 69, -3.0F, -20.604F, 1.1838F, 5, 5, 4, 0.0F, false));

		colar = new RendererModel(this);
		colar.setRotationPoint(0.5F, -0.5F, 0.25F);
		bipedBody.addChild(colar);
		setRotationAngle(colar, 0.2618F, 0.0F, 0.0F);
		colar.cubeList.add(new ModelBox(colar, 40, 0, -6.0F, -1.5341F, -4.2588F, 11, 3, 11, 0.0F, false));
		colar.cubeList.add(new ModelBox(colar, 20, 52, -4.0F, -0.3093F, 0.4483F, 7, 4, 6, 0.0F, false));

		bipedLeftArm = new RendererModel(this);
		bipedLeftArm.setRotationPoint(5.0F, 2.0F, 0.0F);
		bipedLeftArm.cubeList.add(new ModelBox(bipedLeftArm, 28, 78, 0.0F, 2.0F, 2.0F, 3, 4, 1, 0.15F, false));
		bipedLeftArm.cubeList.add(new ModelBox(bipedLeftArm, 20, 35, -1.0F, -2.0F, -2.5F, 5, 12, 5, 0.15F, false));

		rightsholder = new RendererModel(this);
		rightsholder.setRotationPoint(1.2813F, -0.5F, 0.0F);
		bipedLeftArm.addChild(rightsholder);
		setRotationAngle(rightsholder, 0.0F, 0.0F, 0.3491F);
		rightsholder.cubeList.add(new ModelBox(rightsholder, 46, 52, -2.5554F, -2.5639F, -3.0F, 6, 2, 6, 0.0F, false));

		patch = new RendererModel(this);
		patch.setRotationPoint(4.5F, 3.0F, 0.0F);
		bipedLeftArm.addChild(patch);
		setRotationAngle(patch, -0.7854F, 0.0F, 0.0F);
		patch.cubeList.add(new ModelBox(patch, 20, 78, -1.25F, -1.7071F, -1.6464F, 1, 3, 3, 0.0F, false));

		bipedRightArm = new RendererModel(this);
		bipedRightArm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		bipedRightArm.cubeList.add(new ModelBox(bipedRightArm, 36, 78, -3.0F, 2.0F, 2.0F, 3, 4, 1, 0.15F, false));
		bipedRightArm.cubeList.add(new ModelBox(bipedRightArm, 0, 35, -4.0F, -2.0F, -2.5F, 5, 12, 5, 0.15F, false));

		leftsholder = new RendererModel(this);
		leftsholder.setRotationPoint(-1.2813F, -0.5F, 0.0F);
		bipedRightArm.addChild(leftsholder);
		setRotationAngle(leftsholder, 0.0F, 0.0F, -0.3491F);
		leftsholder.cubeList.add(new ModelBox(leftsholder, 0, 69, -3.4446F, -2.5639F, -3.0F, 6, 2, 6, 0.0F, false));

		bipedLeftLeg = new RendererModel(this);
		bipedLeftLeg.setRotationPoint(1.9F, 12.0F, 0.0F);
		bipedLeftLeg.cubeList.add(new ModelBox(bipedLeftLeg, 0, 52, -1.9375F, 0.0F, -2.5F, 5, 12, 5, 0.2F, false));

		bipedRightLeg = new RendererModel(this);
		bipedRightLeg.setRotationPoint(-1.9F, 12.0F, 0.0F);
		bipedRightLeg.cubeList.add(new ModelBox(bipedRightLeg, 40, 35, -3.125F, 0.0F, -2.5F, 5, 12, 5, 0.2F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}