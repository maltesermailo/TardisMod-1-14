package net.tardis.mod.client.models.exteriors;
//Made with Blockbench

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IBotiModel;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ClockExteriorModel extends ExteriorModel implements IBotiModel{
	private final RendererModel glow_clockface;
	private final RendererModel boti;
	private final RendererModel door_rotate_y;
	private final RendererModel pendulum_rotate_z;
	private final RendererModel disk;
	private final RendererModel pendulum_backwall;
	private final RendererModel pully;
	private final RendererModel pully2;
	private final RendererModel door_frame;
	private final RendererModel door_latch;
	private final RendererModel grandfather_clock;
	private final RendererModel top;
	private final RendererModel posts;
	private final RendererModel foot1;
	private final RendererModel foot2;
	private final RendererModel foot3;
	private final RendererModel foot4;
	private final RendererModel torso;
	private final RendererModel clockface;
	private final RendererModel numbers;
	private final RendererModel twelveToSix;
	private final RendererModel fiveTo11;
	private final RendererModel fourTo10;
	private final RendererModel threeToNine;
	private final RendererModel twoToEight;
	private final RendererModel oneToSeven;
	private final RendererModel hand_rotate_z;
	private final RendererModel side_details;
	private final RendererModel side_details2;
	private final RendererModel back_details;
	private final RendererModel base;
	private final RendererModel hitbox;

	public ClockExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		glow_clockface = new RendererModel(this);
		glow_clockface.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow_clockface.cubeList.add(new ModelBox(glow_clockface, 230, 180, -20.0F, -167.0F, 0.0F, 40, 48, 6, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 412, 194, -20.0F, -121.0F, 4.0F, 40, 100, 4, 0.0F, false));

		door_rotate_y = new RendererModel(this);
		door_rotate_y.setRotationPoint(20.0F, -4.0F, -5.0F);

		pendulum_rotate_z = new RendererModel(this);
		pendulum_rotate_z.setRotationPoint(-20.0F, -88.0F, 5.0F);
		door_rotate_y.addChild(pendulum_rotate_z);
		pendulum_rotate_z.cubeList.add(new ModelBox(pendulum_rotate_z, 194, 185, -2.0F, 7.0F, 0.0F, 4, 56, 1, 0.0F, false));

		disk = new RendererModel(this);
		disk.setRotationPoint(0.0F, 69.0F, 0.0F);
		setRotationAngle(disk, 0.0F, 0.0F, -0.7854F);
		pendulum_rotate_z.addChild(disk);
		disk.cubeList.add(new ModelBox(disk, 11, 184, -6.0F, -6.0F, -1.0F, 12, 12, 2, 0.0F, false));

		pendulum_backwall = new RendererModel(this);
		pendulum_backwall.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(pendulum_backwall);
		pendulum_backwall.cubeList.add(new ModelBox(pendulum_backwall, 75, 190, -16.0F, -109.0F, 2.0F, 32, 84, 0, 0.0F, false));

		pully = new RendererModel(this);
		pully.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully);
		pully.cubeList.add(new ModelBox(pully, 198, 195, -24.0F, -81.0F, 0.0F, 2, 12, 1, 0.0F, false));
		pully.cubeList.add(new ModelBox(pully, 19, 192, -25.0F, -69.0F, -1.0F, 4, 12, 2, 0.0F, false));

		pully2 = new RendererModel(this);
		pully2.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully2);
		pully2.cubeList.add(new ModelBox(pully2, 198, 196, -10.0F, -81.0F, 0.0F, 2, 24, 1, 0.0F, false));
		pully2.cubeList.add(new ModelBox(pully2, 17, 188, -11.0F, -57.0F, -1.0F, 4, 12, 4, 0.0F, false));

		door_frame = new RendererModel(this);
		door_frame.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_frame);
		door_frame.cubeList.add(new ModelBox(door_frame, 149, 90, -16.0F, -105.0F, -4.0F, 4, 72, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, 8.0F, -161.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -161.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -20.0F, -157.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, 12.0F, -157.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -165.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -121.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -113.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 107, 164, -16.0F, -29.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 101, 36, -20.0F, -153.0F, -5.0F, 4, 128, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 120, 36, 16.0F, -153.0F, -5.0F, 4, 128, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 89, 77, -16.0F, -117.0F, -4.0F, 32, 4, 0, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -117.0F, 1.0F, 32, 4, 1, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 121, 103, -16.0F, -109.0F, -4.0F, 32, 4, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 137, 84, 12.0F, -105.0F, -4.0F, 4, 72, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 111, 164, -16.0F, -33.0F, -4.0F, 32, 4, 3, 0.0F, false));

		door_latch = new RendererModel(this);
		door_latch.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_latch);
		door_latch.cubeList.add(new ModelBox(door_latch, 11, 183, -19.0F, -97.0F, -7.0F, 2, 8, 12, 0.0F, false));

		grandfather_clock = new RendererModel(this);
		grandfather_clock.setRotationPoint(0.0F, 24.0F, 0.0F);

		top = new RendererModel(this);
		top.setRotationPoint(0.0F, 0.0F, 0.0F);
		grandfather_clock.addChild(top);
		top.cubeList.add(new ModelBox(top, 80, 71, 16.0F, -165.0F, -8.0F, 12, 8, 32, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 33, 60, -28.0F, -165.0F, -8.0F, 12, 8, 32, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 34, 49, -32.0F, -169.0F, -9.0F, 64, 4, 34, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 33, 60, -33.0F, -171.0F, -10.0F, 66, 4, 36, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 33, 60, -31.0F, -173.0F, -8.0F, 62, 4, 32, 0.0F, false));

		posts = new RendererModel(this);
		posts.setRotationPoint(0.0F, 0.0F, 0.0F);
		grandfather_clock.addChild(posts);

		foot1 = new RendererModel(this);
		foot1.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot1);
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 20.0F, -5.0F, -9.0F, 8, 4, 8, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 21.0F, -2.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 21.0F, -7.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 111, 36, 22.0F, -158.0F, -7.0F, 4, 134, 4, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 21.0F, -31.0F, -8.0F, 6, 6, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 21.0F, -33.6F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 16, 159, 21.0F, -36.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 129, 46, 21.0F, -154.2F, -8.0F, 6, 6, 4, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 96, 73, 21.0F, -157.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 45, 71, 21.0F, -147.4F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 106, 87, 21.0F, -118.2F, -8.0F, 6, 6, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 105, 92, 21.0F, -121.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot1.cubeList.add(new ModelBox(foot1, 108, 100, 21.0F, -111.4F, -8.0F, 6, 2, 6, 0.0F, false));

		foot2 = new RendererModel(this);
		foot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot2);
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 20.0F, -5.0F, 17.0F, 8, 4, 8, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 21.0F, -2.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 21.0F, -7.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 100, 37, 22.0F, -158.0F, 19.0F, 4, 134, 4, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 21.0F, -31.0F, 18.0F, 6, 6, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 21.0F, -33.6F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 16, 159, 21.0F, -36.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 101, 89, 21.0F, -154.2F, 18.0F, 6, 6, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 170, 37, 21.0F, -157.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 116, 72, 21.0F, -147.4F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 109, 79, 21.0F, -118.2F, 18.0F, 6, 6, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 116, 87, 21.0F, -121.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 95, 79, 21.0F, -111.4F, 18.0F, 6, 2, 6, 0.0F, false));

		foot3 = new RendererModel(this);
		foot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot3);
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -28.0F, -5.0F, 17.0F, 8, 4, 8, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -27.0F, -2.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -27.0F, -7.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 29, 39, -26.0F, -158.0F, 19.0F, 4, 134, 4, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -27.0F, -31.0F, 18.0F, 6, 6, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -27.0F, -33.6F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 16, 159, -27.0F, -36.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 110, 74, -27.0F, -154.2F, 18.0F, 6, 6, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 113, 85, -27.0F, -157.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 112, 75, -27.0F, -147.4F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 101, 87, -27.0F, -118.2F, 18.0F, 6, 6, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 102, 83, -27.0F, -121.0F, 18.0F, 6, 2, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 108, 99, -27.0F, -111.4F, 18.0F, 6, 2, 6, 0.0F, false));

		foot4 = new RendererModel(this);
		foot4.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot4);
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -28.0F, -5.0F, -9.0F, 8, 4, 8, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -27.0F, -2.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -27.0F, -7.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 130, 37, -26.0F, -158.0F, -7.0F, 4, 134, 4, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -27.0F, -31.0F, -8.0F, 6, 6, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -27.0F, -33.6F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 16, 159, -27.0F, -36.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 59, 30, -27.0F, -154.2F, -8.0F, 6, 6, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 93, 64, -27.0F, -157.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 47, 74, -27.0F, -147.4F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 126, 97, -27.0F, -118.2F, -8.0F, 6, 6, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 116, 98, -27.0F, -121.0F, -8.0F, 6, 2, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 101, 95, -27.0F, -111.4F, -8.0F, 6, 2, 6, 0.0F, false));

		torso = new RendererModel(this);
		torso.setRotationPoint(0.0F, 0.0F, 0.0F);
		grandfather_clock.addChild(torso);
		torso.cubeList.add(new ModelBox(torso, 90, 15, -20.0F, -165.0F, 20.0F, 40, 144, 2, 0.0F, false));
		torso.cubeList.add(new ModelBox(torso, 96, 14, -24.0F, -157.0F, -4.0F, 4, 136, 26, 0.0F, false));
		torso.cubeList.add(new ModelBox(torso, 80, 10, 20.0F, -157.0F, -4.0F, 4, 136, 26, 0.0F, false));
		torso.cubeList.add(new ModelBox(torso, 355, 83, -20.0F, -25.0F, 8.0F, 40, 4, 2, 0.0F, false));

		clockface = new RendererModel(this);
		clockface.setRotationPoint(0.0F, 0.0F, -12.0F);
		torso.addChild(clockface);

		numbers = new RendererModel(this);
		numbers.setRotationPoint(0.0F, 0.0F, 0.5F);
		clockface.addChild(numbers);

		twelveToSix = new RendererModel(this);
		twelveToSix.setRotationPoint(0.0F, 0.0F, 0.0F);
		numbers.addChild(twelveToSix);
		twelveToSix.cubeList.add(new ModelBox(twelveToSix, 195, 206, -1.0F, -152.0F, 11.0F, 2, 4, 4, 0.0F, false));
		twelveToSix.cubeList.add(new ModelBox(twelveToSix, 195, 206, -1.0F, -128.0F, 11.0F, 2, 4, 4, 0.0F, false));

		fiveTo11 = new RendererModel(this);
		fiveTo11.setRotationPoint(0.0F, -138.0F, 13.0F);
		setRotationAngle(fiveTo11, 0.0F, 0.0F, 2.618F);
		numbers.addChild(fiveTo11);
		fiveTo11.cubeList.add(new ModelBox(fiveTo11, 195, 206, -1.0F, -14.0F, -2.0F, 2, 4, 4, 0.0F, false));
		fiveTo11.cubeList.add(new ModelBox(fiveTo11, 195, 206, -1.0F, 10.0F, -2.0F, 2, 4, 4, 0.0F, false));

		fourTo10 = new RendererModel(this);
		fourTo10.setRotationPoint(0.0F, -138.0F, 13.0F);
		setRotationAngle(fourTo10, 0.0F, 0.0F, 2.0944F);
		numbers.addChild(fourTo10);
		fourTo10.cubeList.add(new ModelBox(fourTo10, 195, 206, -1.0F, -14.0F, -2.0F, 2, 4, 4, 0.0F, false));
		fourTo10.cubeList.add(new ModelBox(fourTo10, 195, 206, -1.0F, 10.0F, -2.0F, 2, 4, 4, 0.0F, false));

		threeToNine = new RendererModel(this);
		threeToNine.setRotationPoint(0.0F, -138.0F, 13.0F);
		setRotationAngle(threeToNine, 0.0F, 0.0F, 1.5708F);
		numbers.addChild(threeToNine);
		threeToNine.cubeList.add(new ModelBox(threeToNine, 195, 206, -1.0F, -14.0F, -2.0F, 2, 4, 4, 0.0F, false));
		threeToNine.cubeList.add(new ModelBox(threeToNine, 195, 206, -1.0F, 10.0F, -2.0F, 2, 4, 4, 0.0F, false));

		twoToEight = new RendererModel(this);
		twoToEight.setRotationPoint(0.0F, -138.0F, 13.0F);
		setRotationAngle(twoToEight, 0.0F, 0.0F, 1.0472F);
		numbers.addChild(twoToEight);
		twoToEight.cubeList.add(new ModelBox(twoToEight, 195, 206, -1.0F, -14.0F, -2.0F, 2, 4, 4, 0.0F, false));
		twoToEight.cubeList.add(new ModelBox(twoToEight, 195, 206, -1.0F, 10.0F, -2.0F, 2, 4, 4, 0.0F, false));

		oneToSeven = new RendererModel(this);
		oneToSeven.setRotationPoint(0.0F, -138.0F, 13.0F);
		setRotationAngle(oneToSeven, 0.0F, 0.0F, 0.5236F);
		numbers.addChild(oneToSeven);
		oneToSeven.cubeList.add(new ModelBox(oneToSeven, 195, 206, -1.0F, -14.0F, -2.0F, 2, 4, 4, 0.0F, false));
		oneToSeven.cubeList.add(new ModelBox(oneToSeven, 195, 206, -1.0F, 10.0F, -2.0F, 2, 4, 4, 0.0F, false));

		hand_rotate_z = new RendererModel(this);
		hand_rotate_z.setRotationPoint(0.0F, -138.0F, 12.0F);
		clockface.addChild(hand_rotate_z);
		hand_rotate_z.cubeList.add(new ModelBox(hand_rotate_z, 24, 188, -1.0F, -12.0F, -1.5F, 2, 12, 1, 0.0F, false));
		hand_rotate_z.cubeList.add(new ModelBox(hand_rotate_z, 25, 200, -1.0F, -2.0F, -0.5F, 2, 2, 1, 0.0F, false));

		side_details = new RendererModel(this);
		side_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details);
		side_details.cubeList.add(new ModelBox(side_details, 92, 77, 21.0F, -109.0F, 1.0F, 4, 76, 14, 0.0F, false));
		side_details.cubeList.add(new ModelBox(side_details, 102, 12, 21.0F, -146.0F, 1.0F, 4, 24, 14, 0.0F, false));
		side_details.cubeList.add(new ModelBox(side_details, 88, 71, 21.0F, -153.0F, -3.0F, 4, 4, 22, 0.0F, false));
		side_details.cubeList.add(new ModelBox(side_details, 97, 75, 21.0F, -117.0F, -3.0F, 4, 4, 22, 0.0F, false));
		side_details.cubeList.add(new ModelBox(side_details, 94, 145, 21.0F, -29.0F, -3.0F, 4, 4, 22, 0.0F, false));

		side_details2 = new RendererModel(this);
		side_details2.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details2);
		side_details2.cubeList.add(new ModelBox(side_details2, 131, 80, -25.0F, -109.0F, 1.0F, 4, 76, 14, 0.0F, false));
		side_details2.cubeList.add(new ModelBox(side_details2, 123, 66, -25.0F, -146.0F, 1.0F, 4, 24, 14, 0.0F, false));
		side_details2.cubeList.add(new ModelBox(side_details2, 109, 76, -25.0F, -153.0F, -3.0F, 4, 4, 22, 0.0F, false));
		side_details2.cubeList.add(new ModelBox(side_details2, 105, 81, -26.0F, -117.0F, -3.0F, 5, 4, 22, 0.0F, false));
		side_details2.cubeList.add(new ModelBox(side_details2, 100, 134, -25.0F, -29.0F, -3.0F, 4, 4, 22, 0.0F, false));

		back_details = new RendererModel(this);
		back_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(back_details);
		back_details.cubeList.add(new ModelBox(back_details, 87, 79, -18.0F, -109.0F, 15.0F, 36, 76, 8, 0.0F, false));
		back_details.cubeList.add(new ModelBox(back_details, 60, 61, -18.0F, -146.0F, 19.0F, 36, 24, 4, 0.0F, false));
		back_details.cubeList.add(new ModelBox(back_details, 68, 65, -21.0F, -153.0F, 19.0F, 44, 4, 4, 0.0F, false));
		back_details.cubeList.add(new ModelBox(back_details, 67, 94, -25.0F, -117.0F, 19.0F, 48, 4, 4, 0.0F, false));
		back_details.cubeList.add(new ModelBox(back_details, 71, 161, -21.0F, -29.0F, 19.0F, 44, 4, 4, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		grandfather_clock.addChild(base);
		base.cubeList.add(new ModelBox(base, 31, 129, -32.0F, -20.0F, -9.0F, 64, 8, 34, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 31, 129, -32.0F, -9.0F, -9.0F, 64, 2, 34, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 31, 129, -31.0F, -24.0F, -7.0F, 62, 16, 30, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 31, 129, -32.0F, -25.0F, -9.0F, 64, 2, 34, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 31, 129, -23.0F, -120.0F, -1.0F, 46, 2, 18, 0.0F, false));

		hitbox = new RendererModel(this);
		hitbox.setRotationPoint(0.0F, 24.0F, 0.0F);
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -32.0F, -28.0F, -12.0F, 64, 28, 40, 0.0F, false));
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -28.0F, -168.0F, -8.0F, 56, 140, 32, 0.0F, false));
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -34.0F, -176.0F, -12.0F, 68, 12, 40, 0.0F, false));
	}

	public void render(float f5) {
		glow_clockface.render(f5);
		door_rotate_y.render(f5);
		grandfather_clock.render(f5);
	}
	public void setRotationAngle(RendererModel model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile exterior) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 0.125, 0);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		if(exterior.getOpen() != EnumDoorState.CLOSED)
			door_rotate_y.rotateAngleY = -(float)Math.toRadians(exterior.getOpen() == EnumDoorState.ONE ? 45 : 80);
		else door_rotate_y.rotateAngleY = 0;
		
		door_rotate_y.render(0.0625F);
		grandfather_clock.render(0.0625F);
		this.boti.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1F, this.glow_clockface);
		
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	public void renderBoti(float scale) {
		GlStateManager.pushMatrix();
		GlStateManager.disableTexture();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		boti.render(0.0625F);
		GlStateManager.enableTexture();
		GlStateManager.popMatrix();
	}

	@Override
	public void renderOverwrite(BotiContext context) {
		GlStateManager.pushMatrix();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		BotiContextExterior cont = (BotiContextExterior)context;
		if(cont.getConsole().getOpen() != EnumDoorState.CLOSED)
			door_rotate_y.rotateAngleY = -(float)Math.toRadians(cont.getConsole().getOpen() == EnumDoorState.ONE ? 45 : 80);
		else door_rotate_y.rotateAngleY = 0;
		GlStateManager.popMatrix();
	}

	@Override
	public void renderBoti(float scale, boolean raw) {
		// TODO Auto-generated method stub
		
	}
}