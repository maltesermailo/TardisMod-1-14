package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;

@SuppressWarnings("deprecation")
public class HartnelConsoleModel extends Model {
	private final RendererModel Panelstuff;
	private final RendererModel NorthEastPanel;
	private final RendererModel Rotation5;
	private final RendererModel handbrake_rotate_x;
	private final RendererModel throttle_rotate_x;
	private final RendererModel stabilizer_translate_y;
	private final RendererModel multiplier_translate_x;
	private final RendererModel xbutton_translate_y;
	private final RendererModel ybutton_translate_y;
	private final RendererModel zbutton_translate_y;
	private final RendererModel MiscButton27;
	private final RendererModel MiscButton28;
	private final RendererModel Switch4;
	private final RendererModel Switchslope;
	private final RendererModel Switch7;
	private final RendererModel Switchslope2;
	private final RendererModel Switch5;
	private final RendererModel Switchslope3;
	private final RendererModel Switch6;
	private final RendererModel Switchslope4;
	private final RendererModel Leverslopes;
	private final RendererModel NorthPanel;
	private final RendererModel Door_rotate_x;
	private final RendererModel Misclever2;
	private final RendererModel Misclever;
	private final RendererModel MiscButton10;
	private final RendererModel MiscButton9;
	private final RendererModel MiscButton8;
	private final RendererModel MiscButton7;
	private final RendererModel MiscButton6;
	private final RendererModel MiscButton5;
	private final RendererModel SwitchSlopes;
	private final RendererModel NorthWestPanel;
	private final RendererModel Rotation;
	private final RendererModel MiscButton12;
	private final RendererModel MiscButton13;
	private final RendererModel MiscButton14;
	private final RendererModel landtype_translate_x;
	private final RendererModel MiscLever3;
	private final RendererModel Button2;
	private final RendererModel Button3;
	private final RendererModel Button;
	private final RendererModel Dials2;
	private final RendererModel telepathiccircuit;
	private final RendererModel Buttonangle;
	private final RendererModel SouthWestPanel;
	private final RendererModel Rotation2;
	private final RendererModel Landingselection;
	private final RendererModel random;
	private final RendererModel MiscButton18;
	private final RendererModel MiscButton17;
	private final RendererModel MiscButton16;
	private final RendererModel fastreturn_translate_x;
	private final RendererModel MiscSwitch2;
	private final RendererModel MiscSwitch;
	private final RendererModel Dials;
	private final RendererModel SmallDials;
	private final RendererModel SouthPanel;
	private final RendererModel Rotation3;
	private final RendererModel Switch1;
	private final RendererModel Switch8;
	private final RendererModel Switch2;
	private final RendererModel refuel_rotate_x;
	private final RendererModel facingcontrol_rotate_y;
	private final RendererModel TwistLever3;
	private final RendererModel TwistLever4;
	private final RendererModel communicator;
	private final RendererModel dimensioncontrol;
	private final RendererModel Miscbutton2;
	private final RendererModel SouthEastPanel;
	private final RendererModel Rotation4;
	private final RendererModel MiscButton23;
	private final RendererModel MiscButton22;
	private final RendererModel MiscButton21;
	private final RendererModel Sonicport;
	private final RendererModel TwistLever5;
	private final RendererModel TwistLever6;
	private final RendererModel Miscbutton3;
	private final RendererModel Miscbutton4;
	private final RendererModel Dimension;
	private final RendererModel Baseconsole;
	private final RendererModel Toppanels;
	private final RendererModel Borders;
	private final RendererModel Edgeborders;
	private final RendererModel bone34;
	private final RendererModel bone35;
	private final RendererModel bone36;
	private final RendererModel bone37;
	private final RendererModel bone38;
	private final RendererModel Border;
	private final RendererModel bone55;
	private final RendererModel bone56;
	private final RendererModel bone65;
	private final RendererModel bone58;
	private final RendererModel bone57;
	private final RendererModel bone60;
	private final RendererModel bone59;
	private final RendererModel bone62;
	private final RendererModel bone63;
	private final RendererModel bone64;
	private final RendererModel bone61;
	private final RendererModel Underconsole;
	private final RendererModel Underpanel;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel bone69;
	private final RendererModel bone70;
	private final RendererModel bone71;
	private final RendererModel bone72;
	private final RendererModel bone73;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel bone76;
	private final RendererModel Underborder;
	private final RendererModel bone27;
	private final RendererModel bone22;
	private final RendererModel bone28;
	private final RendererModel bone23;
	private final RendererModel bone29;
	private final RendererModel bone24;
	private final RendererModel bone30;
	private final RendererModel bone25;
	private final RendererModel bone31;
	private final RendererModel bone26;
	private final RendererModel bone33;
	private final RendererModel bone32;
	private final RendererModel Rotorbit;
	private final RendererModel Edges;
	private final RendererModel Circleinner;
	private final RendererModel bone78;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel bone81;
	private final RendererModel bone82;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel bone85;
	private final RendererModel Centre5;
	private final RendererModel bone50;
	private final RendererModel bone51;
	private final RendererModel bone52;
	private final RendererModel bone53;
	private final RendererModel bone54;
	private final RendererModel Rotor_translate_x_rotate_y;
	private final RendererModel bone110;
	private final RendererModel bone112;
	private final RendererModel bone114;
	private final RendererModel bone116;
	private final RendererModel bone118;
	private final RendererModel bone120;
	private final RendererModel Panels;
	private final RendererModel bone44;
	private final RendererModel bone89;
	private final RendererModel bone39;
	private final RendererModel bone45;
	private final RendererModel bone40;
	private final RendererModel bone46;
	private final RendererModel bone41;
	private final RendererModel bone47;
	private final RendererModel bone42;
	private final RendererModel bone48;
	private final RendererModel bone43;
	private final RendererModel bone49;
	private final RendererModel Base;
	private final RendererModel Centre3;
	private final RendererModel bone;
	private final RendererModel bone12;
	private final RendererModel bone17;
	private final RendererModel bone13;
	private final RendererModel bone18;
	private final RendererModel bone14;
	private final RendererModel bone19;
	private final RendererModel bone15;
	private final RendererModel bone20;
	private final RendererModel bone16;
	private final RendererModel bone21;
	private final RendererModel Centre6;
	private final RendererModel bone111;
	private final RendererModel bone113;
	private final RendererModel bone115;
	private final RendererModel bone117;
	private final RendererModel bone119;
	private final RendererModel Centre2;
	private final RendererModel bone7;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel bone11;
	private final RendererModel Centre;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone6;

	public HartnelConsoleModel() {
		textureWidth = 128;
		textureHeight = 128;

		Panelstuff = new RendererModel(this);
		Panelstuff.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(Panelstuff, 0.0F, 3.1416F, 0.0F);

		NorthEastPanel = new RendererModel(this);
		NorthEastPanel.setRotationPoint(0.0F, -4.3461F, 0.0785F);
		setRotationAngle(NorthEastPanel, 0.0F, 1.0472F, 0.0F);
		Panelstuff.addChild(NorthEastPanel);

		Rotation5 = new RendererModel(this);
		Rotation5.setRotationPoint(0.0F, 6.3461F, -0.0785F);
		setRotationAngle(Rotation5, -0.2618F, 0.0F, 0.0F);
		NorthEastPanel.addChild(Rotation5);
		Rotation5.cubeList.add(new ModelBox(Rotation5, 79, 16, -3.75F, -36.0961F, 7.0785F, 7, 1, 4, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 60, 0, 7.0F, -36.3461F, 13.0785F, 3, 2, 7, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 59, 20, -4.25F, -36.0961F, 3.0785F, 8, 1, 2, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 60, 0, -1.0F, -36.3461F, 13.0785F, 3, 2, 7, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 59, 24, -5.25F, -35.8461F, 16.8285F, 2, 1, 4, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 81, 1, -0.5F, -36.8461F, 13.0785F, 2, 2, 6, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 81, 1, 7.5F, -36.8461F, 13.0785F, 2, 2, 6, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 87, 21, -8.75F, -36.0961F, 10.5785F, 2, 1, 5, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 59, 24, -7.75F, -35.8461F, 16.8285F, 2, 1, 4, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 59, 24, -10.25F, -35.8461F, 16.8285F, 2, 1, 4, 0.0F, false));
		Rotation5.cubeList.add(new ModelBox(Rotation5, 59, 24, -12.75F, -35.8461F, 16.8285F, 2, 1, 4, 0.0F, false));

		handbrake_rotate_x = new RendererModel(this);
		handbrake_rotate_x.setRotationPoint(0.5335F, -36.091F, 16.5742F);
		setRotationAngle(handbrake_rotate_x, 0.7854F, 0.0F, 0.0F);
		Rotation5.addChild(handbrake_rotate_x);
		handbrake_rotate_x.cubeList.add(new ModelBox(handbrake_rotate_x, 76, 11, -0.5335F, -2.5051F, -0.4957F, 1, 3, 1, 0.0F, false));
		handbrake_rotate_x.cubeList.add(new ModelBox(handbrake_rotate_x, 82, 11, -0.9665F, -3.4949F, -0.5043F, 2, 1, 1, 0.0F, false));

		throttle_rotate_x = new RendererModel(this);
		throttle_rotate_x.setRotationPoint(8.5335F, -36.091F, 16.5742F);
		setRotationAngle(throttle_rotate_x, 0.8727F, 0.0F, 0.0F);
		Rotation5.addChild(throttle_rotate_x);
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 61, 11, -0.5335F, -2.5051F, -0.4957F, 1, 3, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 66, 11, -0.9665F, -3.4949F, -0.5043F, 2, 1, 1, 0.0F, false));

		stabilizer_translate_y = new RendererModel(this);
		stabilizer_translate_y.setRotationPoint(-19.5F, -34.0961F, 20.0785F);
		Rotation5.addChild(stabilizer_translate_y);
		stabilizer_translate_y.cubeList.add(new ModelBox(stabilizer_translate_y, 93, 10, 22.0F, -2.0F, -6.0F, 4, 1, 3, 0.0F, false));

		multiplier_translate_x = new RendererModel(this);
		multiplier_translate_x.setRotationPoint(-0.5F, -1.0F, 1.0F);
		Rotation5.addChild(multiplier_translate_x);
		multiplier_translate_x.cubeList.add(new ModelBox(multiplier_translate_x, 18, 72, 1.75F, -35.5961F, 2.5785F, 1, 2, 1, 0.0F, false));

		xbutton_translate_y = new RendererModel(this);
		xbutton_translate_y.setRotationPoint(-11.75F, -34.0961F, 11.5785F);
		Rotation5.addChild(xbutton_translate_y);
		xbutton_translate_y.cubeList.add(new ModelBox(xbutton_translate_y, 61, 17, 14.5F, -1.75F, -6.0F, 2, 1, 1, 0.0F, false));

		ybutton_translate_y = new RendererModel(this);
		ybutton_translate_y.setRotationPoint(-14.75F, -34.0961F, 11.5785F);
		Rotation5.addChild(ybutton_translate_y);
		ybutton_translate_y.cubeList.add(new ModelBox(ybutton_translate_y, 61, 17, 13.5F, -1.75F, -6.0F, 2, 1, 1, 0.0F, false));

		zbutton_translate_y = new RendererModel(this);
		zbutton_translate_y.setRotationPoint(-17.75F, -34.0961F, 11.5785F);
		Rotation5.addChild(zbutton_translate_y);
		zbutton_translate_y.cubeList.add(new ModelBox(zbutton_translate_y, 61, 17, 12.5F, -1.75F, -6.0F, 2, 1, 1, 0.0F, false));

		MiscButton27 = new RendererModel(this);
		MiscButton27.setRotationPoint(-21.75F, -34.0961F, 13.5785F);
		Rotation5.addChild(MiscButton27);
		MiscButton27.cubeList.add(new ModelBox(MiscButton27, 69, 16, 15.0F, -1.75F, -6.0F, 2, 1, 2, 0.0F, false));

		MiscButton28 = new RendererModel(this);
		MiscButton28.setRotationPoint(-10.75F, -34.0961F, 13.5785F);
		Rotation5.addChild(MiscButton28);
		MiscButton28.cubeList.add(new ModelBox(MiscButton28, 69, 16, 15.0F, -1.75F, -6.0F, 2, 1, 2, 0.0F, false));

		Switch4 = new RendererModel(this);
		Switch4.setRotationPoint(-4.125F, -35.7981F, 18.6642F);
		setRotationAngle(Switch4, 0.0873F, 0.0F, 0.0F);
		Rotation5.addChild(Switch4);
		Switch4.cubeList.add(new ModelBox(Switch4, 72, 24, -0.625F, -0.2981F, -0.3358F, 1, 1, 2, 0.0F, false));

		Switchslope = new RendererModel(this);
		Switchslope.setRotationPoint(-0.125F, 0.7019F, -0.0858F);
		setRotationAngle(Switchslope, -0.1745F, 0.0F, 0.0F);
		Switch4.addChild(Switchslope);
		Switchslope.cubeList.add(new ModelBox(Switchslope, 80, 24, -0.5F, -1.0F, -1.5F, 1, 1, 2, 0.0F, false));

		Switch7 = new RendererModel(this);
		Switch7.setRotationPoint(-6.625F, -35.7981F, 18.6642F);
		setRotationAngle(Switch7, 0.0873F, 0.0F, 0.0F);
		Rotation5.addChild(Switch7);
		Switch7.cubeList.add(new ModelBox(Switch7, 72, 24, -0.625F, -0.2981F, -0.3358F, 1, 1, 2, 0.0F, false));

		Switchslope2 = new RendererModel(this);
		Switchslope2.setRotationPoint(-0.125F, 0.7019F, -0.0858F);
		setRotationAngle(Switchslope2, -0.1745F, 0.0F, 0.0F);
		Switch7.addChild(Switchslope2);
		Switchslope2.cubeList.add(new ModelBox(Switchslope2, 80, 24, -0.5F, -1.0F, -1.5F, 1, 1, 2, 0.0F, false));

		Switch5 = new RendererModel(this);
		Switch5.setRotationPoint(-9.125F, -35.7981F, 18.6642F);
		setRotationAngle(Switch5, 0.0873F, 0.0F, 0.0F);
		Rotation5.addChild(Switch5);
		Switch5.cubeList.add(new ModelBox(Switch5, 72, 24, -0.625F, -0.2981F, -0.3358F, 1, 1, 2, 0.0F, false));

		Switchslope3 = new RendererModel(this);
		Switchslope3.setRotationPoint(-0.125F, 0.7019F, -0.0858F);
		setRotationAngle(Switchslope3, -0.1745F, 0.0F, 0.0F);
		Switch5.addChild(Switchslope3);
		Switchslope3.cubeList.add(new ModelBox(Switchslope3, 80, 24, -0.5F, -1.0F, -1.5F, 1, 1, 2, 0.0F, false));

		Switch6 = new RendererModel(this);
		Switch6.setRotationPoint(-11.625F, -35.7981F, 18.6642F);
		setRotationAngle(Switch6, 0.0873F, 0.0F, 0.0F);
		Rotation5.addChild(Switch6);
		Switch6.cubeList.add(new ModelBox(Switch6, 72, 24, -0.625F, -0.2981F, -0.3358F, 1, 1, 2, 0.0F, false));

		Switchslope4 = new RendererModel(this);
		Switchslope4.setRotationPoint(-0.125F, 0.7019F, -0.0858F);
		setRotationAngle(Switchslope4, -0.1745F, 0.0F, 0.0F);
		Switch6.addChild(Switchslope4);
		Switchslope4.cubeList.add(new ModelBox(Switchslope4, 80, 24, -0.5F, -1.0F, -1.5F, 1, 1, 2, 0.0F, false));

		Leverslopes = new RendererModel(this);
		Leverslopes.setRotationPoint(0.5F, -35.3461F, 16.5785F);
		setRotationAngle(Leverslopes, -0.2618F, 0.0F, 0.0F);
		Rotation5.addChild(Leverslopes);
		Leverslopes.cubeList.add(new ModelBox(Leverslopes, 60, 0, -1.5F, -0.8F, -4.75F, 3, 2, 7, 0.0F, false));
		Leverslopes.cubeList.add(new ModelBox(Leverslopes, 60, 0, 6.5F, -0.8F, -4.75F, 3, 2, 7, 0.0F, false));

		NorthPanel = new RendererModel(this);
		NorthPanel.setRotationPoint(0.0F, -4.3461F, -7.9215F);
		setRotationAngle(NorthPanel, -0.2618F, 0.0F, 0.0F);
		Panelstuff.addChild(NorthPanel);
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 26, 0, -10.0F, -32.0F, 24.0F, 4, 1, 4, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 21, 14, -1.5F, -32.25F, 23.0F, 3, 1, 6, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 15, 11, -6.0F, -31.5F, 25.5F, 13, 1, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 26, 0, 6.0F, -32.0F, 24.0F, 4, 1, 4, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 0, 12, -2.5F, -31.75F, 11.0F, 5, 1, 2, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 13, 4, -3.5F, -32.0F, 15.6206F, 2, 1, 4, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 13, 4, 1.5F, -31.9915F, 15.4353F, 2, 1, 4, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 10, 16, -1.0F, -32.75F, 24.75F, 2, 2, 3, 0.0F, false));

		Door_rotate_x = new RendererModel(this);
		Door_rotate_x.setRotationPoint(0.0F, -30.5F, 26.0F);
		setRotationAngle(Door_rotate_x, 0.2618F, 0.0F, 0.0F);
		NorthPanel.addChild(Door_rotate_x);
		Door_rotate_x.cubeList.add(new ModelBox(Door_rotate_x, 0, 16, -0.5F, -3.75F, -0.25F, 1, 3, 1, 0.0F, false));
		Door_rotate_x.cubeList.add(new ModelBox(Door_rotate_x, 5, 16, -1.0F, -4.75F, -0.25F, 2, 1, 1, 0.0F, false));

		Misclever2 = new RendererModel(this);
		Misclever2.setRotationPoint(-2.5F, -31.0085F, 17.4353F);
		NorthPanel.addChild(Misclever2);
		Misclever2.cubeList.add(new ModelBox(Misclever2, 22, 0, -0.5F, -1.9915F, -0.4353F, 1, 1, 1, 0.0F, false));
		Misclever2.cubeList.add(new ModelBox(Misclever2, 26, 6, -1.0F, -2.9915F, -0.4353F, 2, 1, 1, 0.0F, false));

		Misclever = new RendererModel(this);
		Misclever.setRotationPoint(2.5F, -31.25F, 17.5F);
		NorthPanel.addChild(Misclever);
		Misclever.cubeList.add(new ModelBox(Misclever, 22, 0, -0.5F, -1.75F, -0.5F, 1, 1, 1, 0.0F, false));
		Misclever.cubeList.add(new ModelBox(Misclever, 26, 8, -1.0F, -2.75F, -0.5F, 2, 1, 1, 0.0F, false));

		MiscButton10 = new RendererModel(this);
		MiscButton10.setRotationPoint(8.25F, -31.25F, 21.5F);
		NorthPanel.addChild(MiscButton10);
		MiscButton10.cubeList.add(new ModelBox(MiscButton10, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton9 = new RendererModel(this);
		MiscButton9.setRotationPoint(6.25F, -31.25F, 17.5F);
		NorthPanel.addChild(MiscButton9);
		MiscButton9.cubeList.add(new ModelBox(MiscButton9, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton8 = new RendererModel(this);
		MiscButton8.setRotationPoint(-6.25F, -31.25F, 17.5F);
		NorthPanel.addChild(MiscButton8);
		MiscButton8.cubeList.add(new ModelBox(MiscButton8, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton7 = new RendererModel(this);
		MiscButton7.setRotationPoint(-4.25F, -31.25F, 13.5F);
		NorthPanel.addChild(MiscButton7);
		MiscButton7.cubeList.add(new ModelBox(MiscButton7, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton6 = new RendererModel(this);
		MiscButton6.setRotationPoint(4.25F, -31.25F, 13.5F);
		NorthPanel.addChild(MiscButton6);
		MiscButton6.cubeList.add(new ModelBox(MiscButton6, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton5 = new RendererModel(this);
		MiscButton5.setRotationPoint(-8.25F, -31.25F, 21.5F);
		NorthPanel.addChild(MiscButton5);
		MiscButton5.cubeList.add(new ModelBox(MiscButton5, 13, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		SwitchSlopes = new RendererModel(this);
		SwitchSlopes.setRotationPoint(0.0F, -31.15F, 26.5F);
		setRotationAngle(SwitchSlopes, -0.1745F, 0.0F, 0.0F);
		NorthPanel.addChild(SwitchSlopes);
		SwitchSlopes.cubeList.add(new ModelBox(SwitchSlopes, 21, 14, -1.5F, -1.0F, -3.5F, 3, 1, 6, 0.0F, false));
		SwitchSlopes.cubeList.add(new ModelBox(SwitchSlopes, 0, 6, 6.5F, -1.0F, -2.75F, 3, 1, 4, 0.0F, false));
		SwitchSlopes.cubeList.add(new ModelBox(SwitchSlopes, 0, 0, -9.5F, -1.0F, -2.75F, 3, 1, 4, 0.0F, false));

		NorthWestPanel = new RendererModel(this);
		NorthWestPanel.setRotationPoint(0.0F, -4.3461F, 0.0785F);
		setRotationAngle(NorthWestPanel, 0.0F, -1.0472F, 0.0F);
		Panelstuff.addChild(NorthWestPanel);

		Rotation = new RendererModel(this);
		Rotation.setRotationPoint(0.0F, 6.3461F, -0.0785F);
		setRotationAngle(Rotation, -0.2618F, 0.0F, 0.0F);
		NorthWestPanel.addChild(Rotation);
		Rotation.cubeList.add(new ModelBox(Rotation, 27, 44, -6.1951F, -35.6631F, 4.3076F, 3, 1, 3, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 0, 26, -13.0F, -35.5961F, 17.0785F, 14, 1, 3, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 23, 31, -5.75F, -35.8461F, 18.0785F, 1, 1, 1, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 23, 31, -7.75F, -35.8461F, 18.0785F, 1, 1, 1, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 23, 56, -0.75F, -36.0961F, 4.0785F, 5, 1, 1, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 0, 46, -2.25F, -35.8461F, 3.0785F, 8, 1, 3, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 5, 51, 8.5F, -35.8461F, 16.5785F, 4, 1, 4, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 41, 31, 5.25F, -35.8461F, 16.5785F, 2, 1, 5, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 41, 31, 2.75F, -35.8461F, 16.5785F, 2, 1, 5, 0.0F, false));
		Rotation.cubeList.add(new ModelBox(Rotation, 0, 37, 1.5F, -35.8461F, 8.0785F, 7, 1, 7, 0.0F, false));

		MiscButton12 = new RendererModel(this);
		MiscButton12.setRotationPoint(-0.75F, -35.8461F, 18.5785F);
		Rotation.addChild(MiscButton12);
		MiscButton12.cubeList.add(new ModelBox(MiscButton12, 33, 29, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton13 = new RendererModel(this);
		MiscButton13.setRotationPoint(-3.25F, -35.5961F, 18.5785F);
		Rotation.addChild(MiscButton13);
		MiscButton13.cubeList.add(new ModelBox(MiscButton13, 33, 25, -0.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));

		MiscButton14 = new RendererModel(this);
		MiscButton14.setRotationPoint(-10.25F, -35.8461F, 18.5785F);
		Rotation.addChild(MiscButton14);
		MiscButton14.cubeList.add(new ModelBox(MiscButton14, 28, 31, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		landtype_translate_x = new RendererModel(this);
		landtype_translate_x.setRotationPoint(3.25F, -35.5961F, 4.5785F);
		Rotation.addChild(landtype_translate_x);
		landtype_translate_x.cubeList.add(new ModelBox(landtype_translate_x, 45, 44, -0.5F, -1.0F, -1.0F, 1, 2, 2, 0.0F, false));

		MiscLever3 = new RendererModel(this);
		MiscLever3.setRotationPoint(-4.6951F, -34.6631F, 5.8076F);
		Rotation.addChild(MiscLever3);
		MiscLever3.cubeList.add(new ModelBox(MiscLever3, 40, 45, -0.5F, -2.0F, -0.5F, 1, 1, 1, 0.0F, false));
		MiscLever3.cubeList.add(new ModelBox(MiscLever3, 39, 49, -1.0F, -3.0F, -0.5F, 2, 1, 1, 0.0F, false));

		Button2 = new RendererModel(this);
		Button2.setRotationPoint(3.75F, -35.8461F, 19.3285F);
		setRotationAngle(Button2, -0.2618F, 0.0F, 0.0F);
		Rotation.addChild(Button2);
		Button2.cubeList.add(new ModelBox(Button2, 48, 22, -0.5F, -0.5F, -2.0F, 1, 1, 4, 0.0F, false));

		Button3 = new RendererModel(this);
		Button3.setRotationPoint(6.25F, -35.8461F, 19.3285F);
		setRotationAngle(Button3, -0.2618F, 0.0F, 0.0F);
		Rotation.addChild(Button3);
		Button3.cubeList.add(new ModelBox(Button3, 41, 24, -0.5F, -0.5F, -2.0F, 1, 1, 4, 0.0F, false));

		Button = new RendererModel(this);
		Button.setRotationPoint(10.5F, -35.3461F, 18.5785F);
		Rotation.addChild(Button);
		Button.cubeList.add(new ModelBox(Button, 37, 52, -1.5F, -1.0F, -1.5F, 3, 2, 3, 0.0F, false));

		Dials2 = new RendererModel(this);
		Dials2.setRotationPoint(-4.5F, -35.3461F, 11.0785F);
		setRotationAngle(Dials2, -0.1745F, 0.0F, 0.0F);
		Rotation.addChild(Dials2);
		Dials2.cubeList.add(new ModelBox(Dials2, 0, 31, -4.5F, -1.0F, -2.0F, 4, 1, 4, 0.0F, false));
		Dials2.cubeList.add(new ModelBox(Dials2, 21, 49, 0.5F, -1.0F, -2.0F, 4, 1, 4, 0.0F, false));
		Dials2.cubeList.add(new ModelBox(Dials2, 8, 65, 2.0F, -1.25F, 0.75F, 1, 1, 1, 0.0F, false));
		Dials2.cubeList.add(new ModelBox(Dials2, 8, 65, 2.5F, -1.25F, -1.0F, 0, 1, 2, 0.0F, false));

		telepathiccircuit = new RendererModel(this);
		telepathiccircuit.setRotationPoint(5.0F, -35.3461F, 10.0785F);
		setRotationAngle(telepathiccircuit, 0.4363F, 0.0F, 0.0F);
		Rotation.addChild(telepathiccircuit);
		telepathiccircuit.cubeList.add(new ModelBox(telepathiccircuit, 22, 34, -3.0F, -0.9962F, -1.9566F, 6, 3, 6, 0.0F, false));

		Buttonangle = new RendererModel(this);
		Buttonangle.setRotationPoint(3.75F, -35.3461F, 19.0785F);
		setRotationAngle(Buttonangle, -0.3491F, 0.0F, 0.0F);
		Rotation.addChild(Buttonangle);
		Buttonangle.cubeList.add(new ModelBox(Buttonangle, 41, 31, -1.0F, -0.5F, -2.5F, 2, 1, 5, 0.0F, false));
		Buttonangle.cubeList.add(new ModelBox(Buttonangle, 41, 31, 1.5F, -0.5F, -2.5F, 2, 1, 5, 0.0F, false));

		SouthWestPanel = new RendererModel(this);
		SouthWestPanel.setRotationPoint(0.0F, -4.3461F, 0.0785F);
		setRotationAngle(SouthWestPanel, 0.0F, -2.0944F, 0.0F);
		Panelstuff.addChild(SouthWestPanel);

		Rotation2 = new RendererModel(this);
		Rotation2.setRotationPoint(1.0F, 6.3461F, -0.0785F);
		setRotationAngle(Rotation2, -0.2618F, 0.0F, 0.0F);
		SouthWestPanel.addChild(Rotation2);
		Rotation2.cubeList.add(new ModelBox(Rotation2, 17, 65, 4.0F, -36.0718F, 10.2141F, 4, 1, 4, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 17, 65, -10.0F, -36.0718F, 10.2141F, 4, 1, 4, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 0, 60, -11.0F, -35.5961F, 19.0785F, 20, 2, 2, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 0, 76, -5.25F, -35.8461F, 3.0785F, 8, 1, 3, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 0, 73, -4.75F, -36.0961F, 4.0785F, 7, 1, 1, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 33, 86, -3.25F, -36.0961F, 19.5785F, 1, 1, 1, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 37, 86, -0.75F, -36.0961F, 19.5785F, 1, 1, 1, 0.0F, false));
		Rotation2.cubeList.add(new ModelBox(Rotation2, 34, 83, 2.0F, -36.3461F, 19.5785F, 6, 1, 1, 0.0F, false));

		Landingselection = new RendererModel(this);
		Landingselection.setRotationPoint(-1.5F, -1.0F, 1.0F);
		Rotation2.addChild(Landingselection);
		Landingselection.cubeList.add(new ModelBox(Landingselection, 17, 71, 1.75F, -35.5961F, 2.5785F, 1, 2, 2, 0.0F, false));

		random = new RendererModel(this);
		random.setRotationPoint(-7.0F, -35.8461F, 17.0785F);
		Rotation2.addChild(random);
		random.cubeList.add(new ModelBox(random, 35, 88, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton18 = new RendererModel(this);
		MiscButton18.setRotationPoint(-4.0F, -35.5961F, 17.0785F);
		Rotation2.addChild(MiscButton18);
		MiscButton18.cubeList.add(new ModelBox(MiscButton18, 35, 88, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton17 = new RendererModel(this);
		MiscButton17.setRotationPoint(-1.0F, -35.5961F, 17.0785F);
		Rotation2.addChild(MiscButton17);
		MiscButton17.cubeList.add(new ModelBox(MiscButton17, 35, 88, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton16 = new RendererModel(this);
		MiscButton16.setRotationPoint(2.0F, -35.5961F, 17.0785F);
		Rotation2.addChild(MiscButton16);
		MiscButton16.cubeList.add(new ModelBox(MiscButton16, 35, 88, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		fastreturn_translate_x = new RendererModel(this);
		fastreturn_translate_x.setRotationPoint(5.0F, -35.8461F, 17.0785F);
		Rotation2.addChild(fastreturn_translate_x);
		fastreturn_translate_x.cubeList.add(new ModelBox(fastreturn_translate_x, 35, 88, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscSwitch2 = new RendererModel(this);
		MiscSwitch2.setRotationPoint(-8.25F, -35.8461F, 19.8285F);
		setRotationAngle(MiscSwitch2, 0.0F, -0.7854F, 0.0F);
		Rotation2.addChild(MiscSwitch2);
		MiscSwitch2.cubeList.add(new ModelBox(MiscSwitch2, 0, 56, -0.3232F, -0.5F, -0.1161F, 1, 1, 2, 0.0F, false));

		MiscSwitch = new RendererModel(this);
		MiscSwitch.setRotationPoint(-4.75F, -35.8461F, 19.8285F);
		Rotation2.addChild(MiscSwitch);
		MiscSwitch.cubeList.add(new ModelBox(MiscSwitch, 0, 56, -0.5F, -0.5F, -0.25F, 1, 1, 2, 0.0F, false));

		Dials = new RendererModel(this);
		Dials.setRotationPoint(-1.0F, -35.3461F, 11.5785F);
		setRotationAngle(Dials, -0.1745F, 0.0F, 0.0F);
		Rotation2.addChild(Dials);
		Dials.cubeList.add(new ModelBox(Dials, 0, 65, -9.0F, -1.0F, -1.5F, 4, 1, 4, 0.0F, false));
		Dials.cubeList.add(new ModelBox(Dials, 0, 65, 5.0F, -1.0F, -1.5F, 4, 1, 4, 0.0F, false));
		Dials.cubeList.add(new ModelBox(Dials, 8, 65, 6.5F, -1.25F, 0.75F, 1, 1, 1, 0.0F, false));
		Dials.cubeList.add(new ModelBox(Dials, 8, 65, -7.5F, -1.25F, 0.75F, 1, 1, 1, 0.0F, false));
		Dials.cubeList.add(new ModelBox(Dials, 8, 65, 7.0F, -1.25F, -1.0F, 0, 1, 2, 0.0F, false));
		Dials.cubeList.add(new ModelBox(Dials, 8, 65, -7.0F, -1.25F, -1.0F, 0, 1, 2, 0.0F, false));

		SmallDials = new RendererModel(this);
		SmallDials.setRotationPoint(-1.0F, -34.8461F, 11.5785F);
		setRotationAngle(SmallDials, -0.0873F, 0.0F, 0.0F);
		Rotation2.addChild(SmallDials);
		SmallDials.cubeList.add(new ModelBox(SmallDials, 30, 76, -3.5F, -1.0F, -2.0F, 3, 1, 3, 0.0F, true));
		SmallDials.cubeList.add(new ModelBox(SmallDials, 20, 74, 0.5F, -1.0F, -2.0F, 3, 1, 3, 0.0F, true));
		SmallDials.cubeList.add(new ModelBox(SmallDials, 8, 65, 1.5F, -1.25F, 0.0F, 1, 1, 1, 0.0F, false));
		SmallDials.cubeList.add(new ModelBox(SmallDials, 8, 65, -2.5F, -1.25F, 0.0F, 1, 1, 1, 0.0F, false));
		SmallDials.cubeList.add(new ModelBox(SmallDials, 8, 65, 2.0F, -1.25F, -1.0F, 0, 1, 1, 0.0F, false));
		SmallDials.cubeList.add(new ModelBox(SmallDials, 8, 65, -2.0F, -1.25F, -1.0F, 0, 1, 1, 0.0F, false));

		SouthPanel = new RendererModel(this);
		SouthPanel.setRotationPoint(0.0F, -4.3461F, 0.0785F);
		setRotationAngle(SouthPanel, 0.0F, 3.1416F, 0.0F);
		Panelstuff.addChild(SouthPanel);

		Rotation3 = new RendererModel(this);
		Rotation3.setRotationPoint(1.0F, 6.3461F, -0.0785F);
		setRotationAngle(Rotation3, -0.2618F, 0.0F, 0.0F);
		SouthPanel.addChild(Rotation3);
		Rotation3.cubeList.add(new ModelBox(Rotation3, 33, 92, 9.75F, -35.5961F, 18.8285F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 21, 98, 5.0F, -35.5961F, 7.5785F, 1, 1, 1, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 21, 96, 5.5F, -35.5961F, 12.8285F, 1, 1, 1, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 17, 96, 6.0F, -35.5961F, 17.0785F, 1, 1, 1, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 9, 101, -5.0F, -35.8461F, 20.5785F, 4, 1, 1, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -11.0F, -36.0961F, 18.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -11.0F, -36.0961F, 14.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -11.0F, -35.8461F, 16.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -8.0F, -36.0961F, 18.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -8.0F, -35.8461F, 16.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 0, 100, -8.0F, -36.0961F, 14.0785F, 2, 1, 2, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 7, 95, 2.5F, -36.0961F, 6.0785F, 2, 1, 4, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 7, 95, 3.0F, -36.0961F, 11.0785F, 2, 1, 4, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 7, 95, 3.5F, -36.0961F, 16.0785F, 2, 1, 4, 0.0F, false));
		Rotation3.cubeList.add(new ModelBox(Rotation3, 7, 95, 0.5F, -36.0961F, 16.0785F, 2, 1, 4, 0.0F, false));

		Switch1 = new RendererModel(this);
		Switch1.setRotationPoint(4.5F, -34.5961F, 18.0785F);
		Rotation3.addChild(Switch1);
		Switch1.cubeList.add(new ModelBox(Switch1, 30, 89, -0.5F, -3.0F, -0.5F, 1, 2, 1, 0.0F, false));

		Switch8 = new RendererModel(this);
		Switch8.setRotationPoint(1.5F, -34.5961F, 18.0785F);
		Rotation3.addChild(Switch8);
		Switch8.cubeList.add(new ModelBox(Switch8, 30, 89, -0.5F, -3.0F, -0.5F, 1, 2, 1, 0.0F, false));

		Switch2 = new RendererModel(this);
		Switch2.setRotationPoint(4.0F, -34.5961F, 13.0785F);
		Rotation3.addChild(Switch2);
		Switch2.cubeList.add(new ModelBox(Switch2, 30, 89, -0.5F, -3.0F, -0.5F, 1, 2, 1, 0.0F, false));

		refuel_rotate_x = new RendererModel(this);
		refuel_rotate_x.setRotationPoint(3.5F, -34.5961F, 8.0785F);
		Rotation3.addChild(refuel_rotate_x);
		refuel_rotate_x.cubeList.add(new ModelBox(refuel_rotate_x, 30, 89, -0.5F, -3.0F, -0.5F, 1, 2, 1, 0.0F, false));

		facingcontrol_rotate_y = new RendererModel(this);
		facingcontrol_rotate_y.setRotationPoint(10.9413F, -35.5827F, 19.8114F);
		setRotationAngle(facingcontrol_rotate_y, 0.0F, 1.2217F, 0.0F);
		Rotation3.addChild(facingcontrol_rotate_y);
		facingcontrol_rotate_y.cubeList.add(new ModelBox(facingcontrol_rotate_y, 33, 96, -0.5555F, -0.5F, -2.5303F, 1, 1, 3, 0.0F, false));
		facingcontrol_rotate_y.cubeList.add(new ModelBox(facingcontrol_rotate_y, 40, 97, -0.5555F, -1.0F, -3.5303F, 1, 1, 1, 0.0F, false));

		TwistLever3 = new RendererModel(this);
		TwistLever3.setRotationPoint(-0.75F, -35.0961F, 7.7785F);
		setRotationAngle(TwistLever3, 0.0F, 0.5236F, 0.0F);
		Rotation3.addChild(TwistLever3);
		TwistLever3.cubeList.add(new ModelBox(TwistLever3, 33, 96, -0.5005F, -1.0F, -0.4992F, 1, 1, 3, 0.0F, false));
		TwistLever3.cubeList.add(new ModelBox(TwistLever3, 40, 97, -0.5005F, -1.25F, 2.5008F, 1, 1, 1, 0.0F, false));

		TwistLever4 = new RendererModel(this);
		TwistLever4.setRotationPoint(-3.25F, -35.7211F, 7.5785F);
		Rotation3.addChild(TwistLever4);
		TwistLever4.cubeList.add(new ModelBox(TwistLever4, 33, 96, -0.5F, -0.375F, -0.5F, 1, 1, 3, 0.0F, false));
		TwistLever4.cubeList.add(new ModelBox(TwistLever4, 40, 97, -0.5F, -0.625F, 2.5F, 1, 1, 1, 0.0F, false));

		communicator = new RendererModel(this);
		communicator.setRotationPoint(1.5F, -0.25F, 0.5F);
		Rotation3.addChild(communicator);
		communicator.cubeList.add(new ModelBox(communicator, 20, 96, -6.0F, -35.8461F, 13.0785F, 3, 1, 6, 0.0F, false));
		communicator.cubeList.add(new ModelBox(communicator, 24, 100, -6.0F, -36.0961F, 13.0785F, 3, 1, 2, 0.0F, false));

		dimensioncontrol = new RendererModel(this);
		dimensioncontrol.setRotationPoint(-15.0F, -34.3461F, 15.0785F);
		Rotation3.addChild(dimensioncontrol);
		dimensioncontrol.cubeList.add(new ModelBox(dimensioncontrol, 0, 90, 10.75F, -1.5F, -12.0F, 6, 1, 3, 0.0F, false));

		Miscbutton2 = new RendererModel(this);
		Miscbutton2.setRotationPoint(0.5F, -0.25F, 0.0F);
		Rotation3.addChild(Miscbutton2);
		Miscbutton2.cubeList.add(new ModelBox(Miscbutton2, 20, 90, -9.0F, -35.8461F, 9.0785F, 3, 1, 3, 0.0F, false));

		SouthEastPanel = new RendererModel(this);
		SouthEastPanel.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(SouthEastPanel, 0.0F, 2.0944F, 0.0F);
		Panelstuff.addChild(SouthEastPanel);

		Rotation4 = new RendererModel(this);
		Rotation4.setRotationPoint(1.0F, 2.0F, 0.0F);
		setRotationAngle(Rotation4, -0.2618F, 0.0F, 0.0F);
		SouthEastPanel.addChild(Rotation4);
		Rotation4.cubeList.add(new ModelBox(Rotation4, 31, 122, -9.75F, -36.0961F, 17.0785F, 1, 1, 4, 0.0F, false));
		Rotation4.cubeList.add(new ModelBox(Rotation4, 34, 111, -1.0F, -36.0961F, 18.0785F, 6, 1, 3, 0.0F, false));
		Rotation4.cubeList.add(new ModelBox(Rotation4, 42, 123, -8.75F, -36.0961F, 9.0785F, 2, 1, 4, 0.0F, false));
		Rotation4.cubeList.add(new ModelBox(Rotation4, 0, 118, -4.0F, -35.8461F, 3.0785F, 6, 1, 2, 0.0F, false));
		Rotation4.cubeList.add(new ModelBox(Rotation4, 22, 109, 8.25F, -35.5961F, 16.5785F, 3, 1, 3, 0.0F, false));
		Rotation4.cubeList.add(new ModelBox(Rotation4, 22, 109, 4.75F, -35.5961F, 12.0785F, 3, 1, 3, 0.0F, false));

		MiscButton23 = new RendererModel(this);
		MiscButton23.setRotationPoint(-1.0F, -36.0961F, 7.0785F);
		Rotation4.addChild(MiscButton23);
		MiscButton23.cubeList.add(new ModelBox(MiscButton23, 26, 119, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		MiscButton22 = new RendererModel(this);
		MiscButton22.setRotationPoint(2.75F, -35.5961F, 7.5785F);
		Rotation4.addChild(MiscButton22);
		MiscButton22.cubeList.add(new ModelBox(MiscButton22, 2, 123, -1.5F, -0.5F, -1.5F, 3, 1, 3, 0.0F, false));

		MiscButton21 = new RendererModel(this);
		MiscButton21.setRotationPoint(-4.75F, -35.5961F, 7.5785F);
		Rotation4.addChild(MiscButton21);
		MiscButton21.cubeList.add(new ModelBox(MiscButton21, 17, 123, -1.5F, -0.5F, -1.5F, 3, 1, 3, 0.0F, false));

		Sonicport = new RendererModel(this);
		Sonicport.setRotationPoint(-1.0F, -35.5961F, 11.5785F);
		setRotationAngle(Sonicport, 0.0F, -0.7854F, 0.0F);
		Rotation4.addChild(Sonicport);
		Sonicport.cubeList.add(new ModelBox(Sonicport, 38, 117, -1.5F, -0.5F, -1.5F, 3, 1, 3, 0.0F, false));

		TwistLever5 = new RendererModel(this);
		TwistLever5.setRotationPoint(6.2777F, -35.663F, 13.3712F);
		setRotationAngle(TwistLever5, 0.0F, -0.7854F, 0.0F);
		Rotation4.addChild(TwistLever5);
		TwistLever5.cubeList.add(new ModelBox(TwistLever5, 1, 111, -0.4217F, -0.5F, -0.4165F, 1, 1, 3, 0.0F, false));
		TwistLever5.cubeList.add(new ModelBox(TwistLever5, 12, 113, -0.4217F, -1.0F, 2.5835F, 1, 1, 1, 0.0F, false));

		TwistLever6 = new RendererModel(this);
		TwistLever6.setRotationPoint(9.9027F, -35.663F, 18.0212F);
		setRotationAngle(TwistLever6, 0.0F, -0.7854F, 0.0F);
		Rotation4.addChild(TwistLever6);
		TwistLever6.cubeList.add(new ModelBox(TwistLever6, 1, 111, -0.5659F, -0.5F, -0.4341F, 1, 1, 3, 0.0F, false));
		TwistLever6.cubeList.add(new ModelBox(TwistLever6, 12, 113, -0.5659F, -1.0F, 2.5659F, 1, 1, 1, 0.0F, false));

		Miscbutton3 = new RendererModel(this);
		Miscbutton3.setRotationPoint(0.5F, -0.25F, 0.0F);
		Rotation4.addChild(Miscbutton3);
		Miscbutton3.cubeList.add(new ModelBox(Miscbutton3, 17, 114, -7.0F, -35.8461F, 16.0785F, 3, 1, 3, 0.0F, false));

		Miscbutton4 = new RendererModel(this);
		Miscbutton4.setRotationPoint(8.0F, -0.25F, 2.0F);
		Rotation4.addChild(Miscbutton4);
		Miscbutton4.cubeList.add(new ModelBox(Miscbutton4, 51, 121, -8.5F, -36.3461F, 16.5785F, 5, 2, 2, 0.0F, false));

		Dimension = new RendererModel(this);
		Dimension.setRotationPoint(-7.5F, -35.8461F, 25.0785F);
		Rotation4.addChild(Dimension);
		Dimension.cubeList.add(new ModelBox(Dimension, 31, 122, -2.25F, -0.25F, -8.0F, 1, 1, 4, 0.0F, false));
		Dimension.cubeList.add(new ModelBox(Dimension, 31, 122, -3.75F, -0.25F, -8.0F, 1, 1, 4, 0.0F, false));
		Dimension.cubeList.add(new ModelBox(Dimension, 31, 122, -5.25F, -0.25F, -8.0F, 1, 1, 4, 0.0F, false));

		Baseconsole = new RendererModel(this);
		Baseconsole.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(Baseconsole, 0.0F, 3.1416F, 0.0F);

		Toppanels = new RendererModel(this);
		Toppanels.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseconsole.addChild(Toppanels);

		Borders = new RendererModel(this);
		Borders.setRotationPoint(0.0F, 0.0F, 0.0F);
		Toppanels.addChild(Borders);

		Edgeborders = new RendererModel(this);
		Edgeborders.setRotationPoint(0.0F, -5.0F, 0.0F);
		Borders.addChild(Edgeborders);
		Edgeborders.cubeList.add(new ModelBox(Edgeborders, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone34, 0.0F, -1.0472F, 0.0F);
		Edgeborders.addChild(bone34);
		bone34.cubeList.add(new ModelBox(bone34, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		bone34.addChild(bone35);
		bone35.cubeList.add(new ModelBox(bone35, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone36, 0.0F, -1.0472F, 0.0F);
		bone35.addChild(bone36);
		bone36.cubeList.add(new ModelBox(bone36, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone37, 0.0F, -1.0472F, 0.0F);
		bone36.addChild(bone37);
		bone37.cubeList.add(new ModelBox(bone37, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone38, 0.0F, -1.0472F, 0.0F);
		bone37.addChild(bone38);
		bone38.cubeList.add(new ModelBox(bone38, 52, 88, -18.0F, -21.5F, 29.2F, 36, 2, 2, 0.0F, false));

		Border = new RendererModel(this);
		Border.setRotationPoint(0.0F, -2.0F, 0.0F);
		setRotationAngle(Border, 0.0F, -0.5236F, 0.0F);
		Toppanels.addChild(Border);

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(0.1891F, -32.9951F, 11.1947F);
		setRotationAngle(bone55, -0.2967F, 0.0F, 0.0F);
		Border.addChild(bone55);
		bone55.cubeList.add(new ModelBox(bone55, 72, 59, -1.1F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(0.0F, -1.14F, 0.0F);
		setRotationAngle(bone56, 0.0F, -1.0472F, 0.0F);
		Border.addChild(bone56);

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.1891F, -31.8551F, 11.1947F);
		setRotationAngle(bone65, -0.2967F, 0.0F, 0.0F);
		bone56.addChild(bone65);
		bone65.cubeList.add(new ModelBox(bone65, 72, 59, -1.1F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone58, 0.0F, -1.0472F, 0.0F);
		bone56.addChild(bone58);

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.1891F, -31.8551F, 11.1947F);
		setRotationAngle(bone57, -0.2967F, 0.0F, 0.0F);
		bone58.addChild(bone57);
		bone57.cubeList.add(new ModelBox(bone57, 72, 59, -1.0F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone60, 0.0F, -1.0472F, 0.0F);
		bone58.addChild(bone60);

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(0.1891F, -31.8551F, 11.1947F);
		setRotationAngle(bone59, -0.2967F, 0.0F, 0.0F);
		bone60.addChild(bone59);
		bone59.cubeList.add(new ModelBox(bone59, 72, 59, -1.15F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone62, 0.0F, -1.0472F, 0.0F);
		bone60.addChild(bone62);

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.1891F, -31.8551F, 11.1947F);
		setRotationAngle(bone63, -0.2967F, 0.0F, 0.0F);
		bone62.addChild(bone63);
		bone63.cubeList.add(new ModelBox(bone63, 72, 59, -1.15F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone64, 0.0F, -1.0472F, 0.0F);
		bone62.addChild(bone64);

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.1891F, -31.8551F, 11.1947F);
		setRotationAngle(bone61, -0.2967F, 0.0F, 0.0F);
		bone64.addChild(bone61);
		bone61.cubeList.add(new ModelBox(bone61, 72, 59, -1.25F, 1.0666F, -0.3706F, 2, 2, 26, 0.0F, false));

		Underconsole = new RendererModel(this);
		Underconsole.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseconsole.addChild(Underconsole);

		Underpanel = new RendererModel(this);
		Underpanel.setRotationPoint(0.0F, -51.65F, 0.0F);
		setRotationAngle(Underpanel, 3.1416F, 0.0F, 0.0F);
		Underconsole.addChild(Underpanel);

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, -23.3505F, 30.5715F);
		setRotationAngle(bone66, -0.1745F, 0.0F, 0.0F);
		Underpanel.addChild(bone66);
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 0, 2, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 0, 2, 0.0F, false));

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(0.0F, -0.0676F, 0.0F);
		setRotationAngle(bone67, 0.0F, -1.0472F, 0.0F);
		Underpanel.addChild(bone67);

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone68, -0.1745F, 0.0F, 0.0F);
		bone67.addChild(bone68);
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 1, 2, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 1, 2, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone69, 0.0F, -1.0472F, 0.0F);
		bone67.addChild(bone69);

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone70, -0.1745F, 0.0F, 0.0F);
		bone69.addChild(bone70);
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 1, 2, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 1, 2, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		bone69.addChild(bone71);

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone72, -0.1745F, 0.0F, 0.0F);
		bone71.addChild(bone72);
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 1, 2, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 1, 2, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone73, 0.0F, -1.0472F, 0.0F);
		bone71.addChild(bone73);

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone74, -0.1745F, 0.0F, 0.0F);
		bone73.addChild(bone74);
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 1, 2, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 1, 2, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		bone73.addChild(bone75);

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone76, -0.1745F, 0.0F, 0.0F);
		bone75.addChild(bone76);
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -6.0F, -3.1115F, -22.1924F, 12, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -7.0F, -3.1115F, -20.1924F, 14, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -8.5F, -3.1115F, -18.1924F, 17, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -9.5F, -3.1115F, -16.1924F, 19, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -11.0F, -3.1115F, -14.1924F, 22, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -12.0F, -3.1115F, -12.1924F, 24, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -13.0F, -3.1115F, -10.1924F, 26, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -14.0F, -3.1115F, -8.1924F, 28, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -15.0F, -3.1115F, -6.1924F, 30, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -16.0F, -3.1115F, -4.1924F, 32, 1, 2, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 68, 54, -17.0F, -3.1115F, -2.1924F, 34, 1, 2, 0.0F, false));

		Underborder = new RendererModel(this);
		Underborder.setRotationPoint(0.0F, -2.0F, 0.0F);
		setRotationAngle(Underborder, 0.0F, -0.5236F, 0.0F);
		Underconsole.addChild(Underborder);

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, -22.0F, 16.5222F);
		setRotationAngle(bone27, 0.1745F, 0.0F, 0.0F);
		Underborder.addChild(bone27);
		bone27.cubeList.add(new ModelBox(bone27, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, -1.14F, 0.0F);
		setRotationAngle(bone22, 0.0F, -1.0472F, 0.0F);
		Underborder.addChild(bone22);

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone28, 0.1745F, 0.0F, 0.0F);
		bone22.addChild(bone28);
		bone28.cubeList.add(new ModelBox(bone28, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		bone22.addChild(bone23);

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone29, 0.1745F, 0.0F, 0.0F);
		bone23.addChild(bone29);
		bone29.cubeList.add(new ModelBox(bone29, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone24, 0.0F, -1.0472F, 0.0F);
		bone23.addChild(bone24);

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone30, 0.1745F, 0.0F, 0.0F);
		bone24.addChild(bone30);
		bone30.cubeList.add(new ModelBox(bone30, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone25, 0.0F, -1.0472F, 0.0F);
		bone24.addChild(bone25);

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone31, 0.1745F, 0.0F, 0.0F);
		bone25.addChild(bone31);
		bone31.cubeList.add(new ModelBox(bone31, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone26, 0.0F, -1.0472F, 0.0F);
		bone25.addChild(bone26);

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone33, 0.1745F, 0.0F, 0.0F);
		bone26.addChild(bone33);
		bone33.cubeList.add(new ModelBox(bone33, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, -20.86F, 16.5222F);
		setRotationAngle(bone32, 0.1745F, 0.0F, 0.0F);
		bone26.addChild(bone32);
		bone32.cubeList.add(new ModelBox(bone32, 78, 65, -1.0F, 0.462F, -2.4341F, 2, 2, 20, 0.0F, false));

		Rotorbit = new RendererModel(this);
		Rotorbit.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseconsole.addChild(Rotorbit);

		Edges = new RendererModel(this);
		Edges.setRotationPoint(0.0F, 0.0F, 0.0F);
		Rotorbit.addChild(Edges);

		Circleinner = new RendererModel(this);
		Circleinner.setRotationPoint(0.0F, -34.5F, 0.0F);
		Edges.addChild(Circleinner);
		Circleinner.cubeList.add(new ModelBox(Circleinner, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone78, 0.0F, -0.6981F, 0.0F);
		Circleinner.addChild(bone78);
		bone78.cubeList.add(new ModelBox(bone78, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone79, 0.0F, -0.6981F, 0.0F);
		bone78.addChild(bone79);
		bone79.cubeList.add(new ModelBox(bone79, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone80, 0.0F, -0.6981F, 0.0F);
		bone79.addChild(bone80);
		bone80.cubeList.add(new ModelBox(bone80, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone81, 0.0F, -0.6981F, 0.0F);
		bone80.addChild(bone81);
		bone81.cubeList.add(new ModelBox(bone81, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone82, 0.0F, -0.6981F, 0.0F);
		bone81.addChild(bone82);
		bone82.cubeList.add(new ModelBox(bone82, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone83, 0.0F, -0.6981F, 0.0F);
		bone82.addChild(bone83);
		bone83.cubeList.add(new ModelBox(bone83, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone84, 0.0F, -0.6981F, 0.0F);
		bone83.addChild(bone84);
		bone84.cubeList.add(new ModelBox(bone84, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone85, 0.0F, -0.6981F, 0.0F);
		bone84.addChild(bone85);
		bone85.cubeList.add(new ModelBox(bone85, 104, 100, -4.0F, 0.05F, -9.6F, 8, 12, 2, 0.0F, false));

		Centre5 = new RendererModel(this);
		Centre5.setRotationPoint(0.0F, 0.0F, 0.0F);
		Edges.addChild(Centre5);
		Centre5.cubeList.add(new ModelBox(Centre5, 98, 115, -6.0F, -34.5F, 8.4F, 12, 4, 2, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone50, 0.0F, -1.0472F, 0.0F);
		Centre5.addChild(bone50);
		bone50.cubeList.add(new ModelBox(bone50, 98, 115, -6.0F, -34.5F, 8.4F, 12, 4, 2, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone51, 0.0F, -1.0472F, 0.0F);
		bone50.addChild(bone51);
		bone51.cubeList.add(new ModelBox(bone51, 98, 115, -5.9933F, -34.5F, 8.3962F, 12, 4, 2, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone52, 0.0F, -1.0472F, 0.0F);
		bone51.addChild(bone52);
		bone52.cubeList.add(new ModelBox(bone52, 98, 115, -6.0F, -34.5F, 8.4F, 12, 4, 2, 0.0F, false));

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone53, 0.0F, -1.0472F, 0.0F);
		bone52.addChild(bone53);
		bone53.cubeList.add(new ModelBox(bone53, 98, 115, -6.0F, -34.5F, 8.4F, 12, 4, 2, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone54, 0.0F, -1.0472F, 0.0F);
		bone53.addChild(bone54);
		bone54.cubeList.add(new ModelBox(bone54, 98, 115, -6.0F, -34.5F, 8.4F, 12, 4, 2, 0.0F, false));

		Rotor_translate_x_rotate_y = new RendererModel(this);
		Rotor_translate_x_rotate_y.setRotationPoint(0.0F, 7.5F, 0.0F);
		Rotorbit.addChild(Rotor_translate_x_rotate_y);
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 45, 31, -8.0F, -39.5F, -8.0F, 16, 0, 16, 0.0F, false));
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 110, 29, -1.0F, -55.3F, -1.0F, 1, 16, 1, 0.0F, false));
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 110, 29, 0.0F, -55.3F, 0.0F, 1, 16, 1, 0.0F, false));
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 100, 41, -0.8F, -43.9F, -6.0F, 2, 2, 2, 0.0F, false));
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 100, 41, -0.8F, -43.9F, 3.6F, 2, 2, 2, 0.0F, false));
		Rotor_translate_x_rotate_y.cubeList.add(new ModelBox(Rotor_translate_x_rotate_y, 93, 30, 0.0F, -43.5F, -4.1F, 0, 1, 8, 0.0F, false));

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, -53.4F, 0.0F);
		setRotationAngle(bone110, 0.0F, -0.7854F, 0.0F);
		Rotor_translate_x_rotate_y.addChild(bone110);
		bone110.cubeList.add(new ModelBox(bone110, 106, 50, -5.0F, -1.0F, -0.5F, 10, 2, 1, 0.0F, false));

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(0.0F, -52.8F, 0.0F);
		setRotationAngle(bone112, 0.0F, 0.0F, -1.0472F);
		Rotor_translate_x_rotate_y.addChild(bone112);
		bone112.cubeList.add(new ModelBox(bone112, 104, 36, -3.2507F, 1.7696F, -6.0F, 0, 1, 12, 0.0F, false));
		bone112.cubeList.add(new ModelBox(bone112, 104, 36, -5.4158F, 3.0196F, -6.0F, 0, 1, 12, 0.0F, false));

		bone114 = new RendererModel(this);
		bone114.setRotationPoint(0.0F, -53.4F, 0.0F);
		setRotationAngle(bone114, 0.0F, 0.0F, 1.0472F);
		Rotor_translate_x_rotate_y.addChild(bone114);
		bone114.cubeList.add(new ModelBox(bone114, 104, 36, 2.7311F, 1.4696F, -6.0F, 0, 1, 12, 0.0F, false));
		bone114.cubeList.add(new ModelBox(bone114, 104, 36, 4.8095F, 2.6696F, -6.0F, 0, 1, 12, 0.0F, false));

		bone116 = new RendererModel(this);
		bone116.setRotationPoint(0.5F, 5.0F, 0.0F);
		Rotor_translate_x_rotate_y.addChild(bone116);
		bone116.cubeList.add(new ModelBox(bone116, 122, 32, -6.1F, -56.8F, -0.5F, 2, 14, 1, 0.0F, false));

		bone118 = new RendererModel(this);
		bone118.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone118, 0.0F, -2.0944F, 0.0F);
		bone116.addChild(bone118);
		bone118.cubeList.add(new ModelBox(bone118, 122, 32, -6.1F, -56.8F, -0.5F, 2, 14, 1, 0.0F, false));

		bone120 = new RendererModel(this);
		bone120.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone120, 0.0F, -2.0944F, 0.0F);
		bone118.addChild(bone120);
		bone120.cubeList.add(new ModelBox(bone120, 122, 32, -6.1F, -56.8F, -0.5F, 2, 14, 1, 0.0F, false));

		Panels = new RendererModel(this);
		Panels.setRotationPoint(0.0F, -5.15F, 0.0F);
		Baseconsole.addChild(Panels);

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(0.0F, -0.0676F, 0.0F);
		setRotationAngle(bone44, 0.0F, -1.0472F, 0.0F);
		Panels.addChild(bone44);

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone89, -0.2618F, 0.0F, 0.0F);
		bone44.addChild(bone89);
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone39, 0.0F, -1.0472F, 0.0F);
		bone44.addChild(bone39);

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone45, -0.2618F, 0.0F, 0.0F);
		bone39.addChild(bone45);
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		bone39.addChild(bone40);

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone46, -0.2618F, 0.0F, 0.0F);
		bone40.addChild(bone46);
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone41, 0.0F, -1.0472F, 0.0F);
		bone40.addChild(bone41);

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone47, -0.2618F, 0.0F, 0.0F);
		bone41.addChild(bone47);
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone42, 0.0F, -1.0472F, 0.0F);
		bone41.addChild(bone42);

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone48, -0.2618F, 0.0F, 0.0F);
		bone42.addChild(bone48);
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone43, 0.0F, -1.0472F, 0.0F);
		bone42.addChild(bone43);

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, -23.2829F, 30.5715F);
		setRotationAngle(bone49, -0.2618F, 0.0F, 0.0F);
		bone43.addChild(bone49);
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -6.0F, 1.8654F, -21.1113F, 12, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -7.0F, 1.8654F, -19.1113F, 14, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -8.5F, 1.8654F, -17.1113F, 17, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -10.0F, 1.8654F, -15.1113F, 20, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -11.0F, 1.8654F, -13.1113F, 22, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -12.0F, 1.8654F, -11.1113F, 24, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -13.0F, 1.8654F, -9.1113F, 26, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -14.0F, 1.8654F, -7.1113F, 28, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -15.0F, 1.8654F, -5.1113F, 30, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -16.0F, 1.8654F, -3.1113F, 32, 1, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 56, 54, -17.0F, 1.8654F, -1.1113F, 34, 1, 2, 0.0F, false));

		Base = new RendererModel(this);
		Base.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseconsole.addChild(Base);

		Centre3 = new RendererModel(this);
		Centre3.setRotationPoint(0.0F, 0.3F, 0.0F);
		Base.addChild(Centre3);

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, -1.6681F, 15.4924F);
		setRotationAngle(bone, -0.6981F, 0.0F, 0.0F);
		Centre3.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, -0.02F, 0.0F);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		Centre3.addChild(bone12);

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, -1.6481F, 15.4924F);
		setRotationAngle(bone17, -0.6981F, 0.0F, 0.0F);
		bone12.addChild(bone17);
		bone17.cubeList.add(new ModelBox(bone17, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone13, 0.0F, -1.0472F, 0.0F);
		bone12.addChild(bone13);

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, -1.6481F, 15.4924F);
		setRotationAngle(bone18, -0.6981F, 0.0F, 0.0F);
		bone13.addChild(bone18);
		bone18.cubeList.add(new ModelBox(bone18, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone14, 0.0F, -1.0472F, 0.0F);
		bone13.addChild(bone14);

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, -1.6481F, 15.4924F);
		setRotationAngle(bone19, -0.6981F, 0.0F, 0.0F);
		bone14.addChild(bone19);
		bone19.cubeList.add(new ModelBox(bone19, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone15, 0.0F, -1.0472F, 0.0F);
		bone14.addChild(bone15);

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.0F, -1.6481F, 15.4924F);
		setRotationAngle(bone20, -0.6981F, 0.0F, 0.0F);
		bone15.addChild(bone20);
		bone20.cubeList.add(new ModelBox(bone20, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone20.cubeList.add(new ModelBox(bone20, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone20.cubeList.add(new ModelBox(bone20, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone16, 0.0F, -1.0472F, 0.0F);
		bone15.addChild(bone16);

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, -1.6481F, 15.4924F);
		setRotationAngle(bone21, -0.6981F, 0.0F, 0.0F);
		bone16.addChild(bone21);
		bone21.cubeList.add(new ModelBox(bone21, 92, 95, -7.0F, 1.6681F, -3.9425F, 14, 0, 2, 0.0F, false));
		bone21.cubeList.add(new ModelBox(bone21, 92, 95, -8.0F, 1.6681F, -1.9425F, 16, 0, 2, 0.0F, false));
		bone21.cubeList.add(new ModelBox(bone21, 92, 95, -6.0F, 1.6681F, -5.9425F, 12, 0, 2, 0.0F, false));

		Centre6 = new RendererModel(this);
		Centre6.setRotationPoint(0.0F, 0.3F, 0.0F);
		Base.addChild(Centre6);
		Centre6.cubeList.add(new ModelBox(Centre6, 46, 94, -8.0F, -0.275F, -0.5261F, 16, 0, 15, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(0.0F, 0.0024F, 0.0F);
		setRotationAngle(bone111, 0.0F, -1.0472F, 0.0F);
		Centre6.addChild(bone111);
		bone111.cubeList.add(new ModelBox(bone111, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));

		bone113 = new RendererModel(this);
		bone113.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone113, 0.0F, -1.0472F, 0.0F);
		bone111.addChild(bone113);
		bone113.cubeList.add(new ModelBox(bone113, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));

		bone115 = new RendererModel(this);
		bone115.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone115, 0.0F, -1.0472F, 0.0F);
		bone113.addChild(bone115);
		bone115.cubeList.add(new ModelBox(bone115, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));

		bone117 = new RendererModel(this);
		bone117.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone117, 0.0F, -1.0472F, 0.0F);
		bone115.addChild(bone117);
		bone117.cubeList.add(new ModelBox(bone117, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));
		bone117.cubeList.add(new ModelBox(bone117, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));

		bone119 = new RendererModel(this);
		bone119.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone119, 0.0F, -1.0472F, 0.0F);
		bone117.addChild(bone119);
		bone119.cubeList.add(new ModelBox(bone119, 46, 94, -8.0F, -0.2774F, -0.5261F, 16, 0, 15, 0.0F, false));

		Centre2 = new RendererModel(this);
		Centre2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(Centre2, 0.0F, -0.5236F, 0.0F);
		Base.addChild(Centre2);
		Centre2.cubeList.add(new ModelBox(Centre2, 112, 56, -1.0F, -22.0F, 10.5237F, 2, 22, 6, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone7, 0.0F, -1.0472F, 0.0F);
		Centre2.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 112, 56, -1.0F, -22.0F, 10.5237F, 2, 22, 6, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone8, 0.0F, -1.0472F, 0.0F);
		bone7.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 112, 57, -0.9933F, -22.0F, 10.5179F, 2, 22, 6, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		bone8.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 112, 56, -1.0F, -22.0F, 10.5237F, 2, 22, 6, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone10, 0.0F, -1.0472F, 0.0F);
		bone9.addChild(bone10);
		bone10.cubeList.add(new ModelBox(bone10, 112, 56, -1.0F, -22.0F, 10.5237F, 2, 22, 6, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		bone10.addChild(bone11);
		bone11.cubeList.add(new ModelBox(bone11, 112, 56, -1.0F, -22.0F, 10.5237F, 2, 22, 6, 0.0F, false));

		Centre = new RendererModel(this);
		Centre.setRotationPoint(0.0F, 0.0F, 0.0F);
		Base.addChild(Centre);
		Centre.cubeList.add(new ModelBox(Centre, 68, 58, -6.0F, -22.0F, 8.4F, 12, 22, 2, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		Centre.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 68, 58, -6.0F, -22.0F, 8.4F, 12, 22, 2, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone2.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 68, 58, -5.9933F, -22.0F, 8.3962F, 12, 22, 2, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone3.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 68, 58, -6.0F, -22.0F, 8.4F, 12, 22, 2, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone4.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 68, 58, -6.0F, -22.0F, 8.4F, 12, 22, 2, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone5.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 68, 58, -6.0F, -22.0F, 8.4F, 12, 22, 2, 0.0F, false));
	}

	public void render(ConsoleTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.6, 0.6, 0.6);
		
		this.Rotor_translate_x_rotate_y.rotateAngleY = (float) Math.toRadians(tile.flightTicks * 2 % 360);
		this.Rotor_translate_x_rotate_y.offsetY = (float) Math.cos(tile.flightTicks * 0.1) * 0.15F;
		
		ThrottleControl throttle = tile.getControl(ThrottleControl.class);
		this.throttle_rotate_x.rotateAngleX = (float) Math.toRadians(-50 + (throttle.getAmount() * 90.0F));
		
		HandbrakeControl handBrake = tile.getControl(HandbrakeControl.class);
		this.handbrake_rotate_x.rotateAngleX = (float) Math.toRadians(handBrake.isFree() ? -45F : 40F);
		
		IncModControl inc = tile.getControl(IncModControl.class);
		this.multiplier_translate_x.offsetX = -0.29F * (inc.index / (float)IncModControl.COORD_MODS.length);
		
		LandingTypeControl land = tile.getControl(LandingTypeControl.class);
		this.landtype_translate_x.offsetX = -0.25F * (land.getLandType() == EnumLandType.UP ? 1 : 0);
		
		this.facingcontrol_rotate_y.rotateAngleY = (float)Math.toRadians(Helper.getAngleFromFacing(tile.getDirection()));
		
		RandomiserControl rand = tile.getControl(RandomiserControl.class);
		this.TwistLever6.rotateAngleY = (float) Math.toRadians((rand.getAnimationTicks() / 10.0) * 360);
	
		
		Panelstuff.render(0.0625F);
		Baseconsole.render(0.0625F);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(-0.975, -0.1, 1.09);
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getItemRenderer().renderItem(tile.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
		
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}