package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SafeExteriorModel extends Model {
	
	private final RendererModel door;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel bone4;
	private final RendererModel bone2;
	private final RendererModel base;

	public SafeExteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		door = new RendererModel(this);
		door.setRotationPoint(14.0F, -13.6667F, -11.6375F);
		door.cubeList.add(new ModelBox(door, 80, 0, -28.0F, -44.3333F, -0.1625F, 28, 78, 2, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 104, 101, -27.5F, -18.3333F, 1.8375F, 6, 25, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 140, 4, -24.9014F, -27.3333F, 1.0952F, 21, 1, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 119, -25.9014F, -28.8333F, 1.0952F, 1, 3, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 119, -3.9014F, -28.8333F, 1.0952F, 1, 3, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 140, 14, -25.9014F, -28.8333F, 5.0952F, 23, 3, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 92, 80, -27.0F, -0.8333F, -0.6625F, 4, 4, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 76, 40, -25.5F, 0.1667F, -0.6875F, 1, 2, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 130, -1.0F, -38.3333F, -1.0625F, 2, 12, 2, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 130, -1.0F, 15.6667F, -1.0625F, 2, 12, 2, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(-8.4014F, -16.8333F, 3.3452F);
		door.addChild(bone5);
		setRotationAngle(bone5, 0.0F, 0.7854F, 0.0F);
		

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(-8.4014F, -16.8333F, 3.3452F);
		door.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -0.7854F, 0.0F);
		

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(-9.4014F, -6.3333F, 2.8452F);
		door.addChild(bone4);
		setRotationAngle(bone4, 0.0F, 0.0F, 0.4363F);
		

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(-25.5F, -13.2083F, -1.1625F);
		door.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 76, 43, -0.5F, -0.875F, 0.0F, 1, 2, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 13, 99, -1.0F, -1.125F, -1.0F, 2, 10, 1, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(-30.0F, 24.0F, -12.0F);
		base.cubeList.add(new ModelBox(base, 140, 0, 16.0F, -84.0F, 0.0F, 28, 2, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 104, 95, 16.0F, -4.0F, 0.0F, 28, 4, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 116, 155, 44.0F, -84.0F, 0.0F, 4, 84, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 86, 46.0F, -84.0F, 2.0F, 2, 84, 24, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 86, 12.0F, -84.0F, 2.0F, 2, 84, 24, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 116, 56, 14.0F, -84.0F, 2.0F, 32, 2, 24, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 116, 56, 14.0F, -2.0F, 2.0F, 32, 2, 24, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 0, 12.0F, -84.0F, 26.0F, 36, 84, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 116, 155, 12.0F, -84.0F, 0.0F, 4, 84, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 104, 130, 14.0F, -71.0F, 3.0F, 32, 2, 23, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 104, 82, 14.0F, -82.0F, 2.0F, 32, 12, 1, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		
		this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.SAFE.getRotationForState(tile.getOpen()));
		
		door.render(0.0625F);
		base.render(0.0625F);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}