package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.exteriors.TTCapsuleExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TTCapsuleInteriorModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel bone2;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel BOTI;
	private final RendererModel leftdoor;
	private final RendererModel rightdoor;

	public TTCapsuleInteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(7.5F, 26.0F, 11.5F);
		

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(-5.5F, -2.0F, -6.5F);
		bone2.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 23, 6, -9.0F, -31.0F, 0.0F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 23, 6, 4.0F, -31.0F, 0.0F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 66, 8, -9.0F, -35.0F, 2.0F, 14, 35, 1, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(-5.5F, -33.0F, -6.5F);
		bone2.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 21, 104, -9.0F, -4.0F, 0.0F, 14, 4, 2, 0.0F, false));

		BOTI = new RendererModel(this);
		BOTI.setRotationPoint(-14.5F, -3.0F, -9.5F);
		bone2.addChild(BOTI);
		BOTI.cubeList.add(new ModelBox(BOTI, 100, 8, 0.0F, -34.0F, 4.95F, 14, 35, 0, 0.0F, false));

		leftdoor = new RendererModel(this);
		leftdoor.setRotationPoint(-6.0F, 8.5F, 7.0F);
		leftdoor.cubeList.add(new ModelBox(leftdoor, 55, 54, 0.0F, -15.5F, -1.0F, 6, 31, 1, 0.0F, false));

		rightdoor = new RendererModel(this);
		rightdoor.setRotationPoint(6.0F, 8.5F, 7.0F);
		rightdoor.cubeList.add(new ModelBox(rightdoor, 72, 54, -6.0F, -15.5F, -1.0F, 6, 31, 1, 0.0F, true));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(0, 0, 0.12);
		this.leftdoor.rotateAngleY = this.rightdoor.rotateAngleY = 0;
		float rot = (float)Math.toRadians(EnumDoorType.TT_CAPSULE.getRotationForState(door.getOpenState()));
		
		if(door.getOpenState() == EnumDoorState.ONE) {
			this.rightdoor.rotateAngleY = -rot;
			this.leftdoor.rotateAngleY = 0;
		}
		else {
			this.leftdoor.rotateAngleY = rot;
			this.rightdoor.rotateAngleY = -rot;
		}
		
		bone2.render(0.0625F);
		leftdoor.render(0.0625F);
		rightdoor.render(0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		return TTCapsuleExteriorRenderer.TEXTURE;
	}
}