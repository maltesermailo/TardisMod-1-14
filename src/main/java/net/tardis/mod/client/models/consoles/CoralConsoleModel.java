package net.tardis.mod.client.models.consoles;
//Made with Blockbench
//Paste this code into your mod.


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

@SuppressWarnings("deprecation")
public class CoralConsoleModel extends Model implements IConsoleModel {
	private final RendererModel all;
	private final RendererModel rotor_orig;
	private final RendererModel rotarcase2;
	private final RendererModel block;
	private final RendererModel lever6;
	private final RendererModel wires8;
	private final RendererModel wires7;
	private final RendererModel mirror;
	private final RendererModel wires6;
	private final RendererModel wires5;
	private final RendererModel wires4;
	private final RendererModel buttons;
	private final RendererModel wires3;
	private final RendererModel wires2;
	private final RendererModel wires;
	private final RendererModel bone112;
	private final RendererModel bone111;
	private final RendererModel bone106;
	private final RendererModel land_type;
	private final RendererModel bone103;
	private final RendererModel important_controls;
	private final RendererModel fastreturn;
	private final RendererModel lever;
	private final RendererModel refuel;
	private final RendererModel doorcontrol;
	private final RendererModel keyboardxyz;
	private final RendererModel throttle;
	private final RendererModel lever3;
	private final RendererModel Sonicport;
	private final RendererModel Incrementincrease;
	private final RendererModel lever2;
	private final RendererModel monitor;
	private final RendererModel handbreak;
	private final RendererModel lever4;
	private final RendererModel lever5;
	private final RendererModel wheel;
	private final RendererModel randomiser_and_facingcontrol;
	private final RendererModel throttle3;
	private final RendererModel facing_control;
	private final RendererModel randomiser;
	private final RendererModel Comuni_Phone;
	private final RendererModel telepathic_circuit;
	private final RendererModel crystal;
	private final RendererModel lever8;
	private final RendererModel nope;
	private final RendererModel keyboard_behind_monitor;
	private final RendererModel keyboard_by_monitor;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel bone102;
	private final RendererModel bone99;
	private final RendererModel bone92;
	private final RendererModel control6;
	private final RendererModel control4;
	private final RendererModel control2;
	private final RendererModel Shell;
	private final RendererModel Shellone;
	private final RendererModel bone;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone7;
	private final RendererModel bone6;
	private final RendererModel Shellone2;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel bone13;
	private final RendererModel bone14;
	private final RendererModel Shellone3;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel bone17;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel bone20;
	private final RendererModel bone21;
	private final RendererModel Shellone4;
	private final RendererModel bone22;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel bone25;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel bone28;
	private final RendererModel Shellone5;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel bone32;
	private final RendererModel bone33;
	private final RendererModel bone34;
	private final RendererModel bone35;
	private final RendererModel Shellone6;
	private final RendererModel bone36;
	private final RendererModel bone37;
	private final RendererModel bone38;
	private final RendererModel bone39;
	private final RendererModel bone40;
	private final RendererModel bone41;
	private final RendererModel bone42;
	private final RendererModel bone50;
	private final RendererModel bone51;
	private final RendererModel bone52;
	private final RendererModel bone53;
	private final RendererModel bone54;
	private final RendererModel bone55;
	private final RendererModel bone56;
	private final RendererModel bone57;
	private final RendererModel bone58;
	private final RendererModel bone59;
	private final RendererModel bone60;
	private final RendererModel bone61;
	private final RendererModel bone62;
	private final RendererModel bone63;
	private final RendererModel bone64;
	private final RendererModel bone65;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel bone69;
	private final RendererModel bone70;
	private final RendererModel bone71;
	private final RendererModel bone72;
	private final RendererModel bone73;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel bone76;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel bone81;
	private final RendererModel bone82;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel bone85;
	private final RendererModel bone43;
	private final RendererModel bone86;
	private final RendererModel bone87;
	private final RendererModel bone88;
	private final RendererModel bone89;
	private final RendererModel bone90;
	private final RendererModel bone91;
	private final RendererModel bone93;
	private final RendererModel bone94;
	private final RendererModel bone95;
	private final RendererModel bone96;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone105;
	private final RendererModel glow;
	private final RendererModel keyboard_behind_monitor_glow;
	private final RendererModel keyboard_by_monitor_glow;
	private final RendererModel bone44;
	private final RendererModel refuel2;
	private final RendererModel doorcontrol2;
	private final RendererModel land_type_red;
	private final RendererModel monitor_screen;
	private final RendererModel throttle_glow;
	private final RendererModel shell_glow;
	private final RendererModel bone150;
	private final RendererModel bone151;
	private final RendererModel bone152;
	private final RendererModel bone153;
	private final RendererModel bone154;
	private final RendererModel bone155;
	private final RendererModel rotor;
	private final RendererModel rotartop;
	private final RendererModel rotarbottom;
	private final RendererModel phone;
	private final RendererModel land_type_glow;
	private final RendererModel telepathic_circuit_glow;
	private final RendererModel crystal2;
	private final RendererModel lever7;
	private final RendererModel nope2;
	private final RendererModel block2;
	private final RendererModel lever9;
	private final RendererModel bone45;

	public CoralConsoleModel() {
		textureWidth = 200;
		textureHeight = 200;

		all = new RendererModel(this);
		all.setRotationPoint(-0.2F, 24.0F, 0.0F);

		rotor_orig = new RendererModel(this);
		rotor_orig.setRotationPoint(0.0F, 0.0F, 0.0F);
		all.addChild(rotor_orig);

		rotarcase2 = new RendererModel(this);
		rotarcase2.setRotationPoint(0.5F, -47.2F, -2.0F);
		rotor_orig.addChild(rotarcase2);
		rotarcase2.cubeList.add(new ModelBox(rotarcase2, 147, 16, -1.5F, -27.0F, 7.7F, 2, 42, 1, 0.0F, false));
		rotarcase2.cubeList.add(new ModelBox(rotarcase2, 147, 16, -1.5F, -27.0F, -4.3F, 2, 42, 1, 0.0F, false));
		rotarcase2.cubeList.add(new ModelBox(rotarcase2, 147, 16, -6.6F, -27.0F, 1.1F, 1, 42, 2, 0.0F, false));
		rotarcase2.cubeList.add(new ModelBox(rotarcase2, 147, 16, 5.0F, -27.0F, 1.1F, 1, 42, 2, 0.0F, false));

		block = new RendererModel(this);
		block.setRotationPoint(6.9F, -27.5F, 1.4F);
		setRotationAngle(block, -0.0524F, -1.0472F, -0.0873F);
		all.addChild(block);

		lever6 = new RendererModel(this);
		lever6.setRotationPoint(-2.9869F, -0.7455F, 16.8686F);
		setRotationAngle(lever6, 0.0F, 0.0F, 0.0349F);
		block.addChild(lever6);
		lever6.cubeList.add(new ModelBox(lever6, 140, 180, -2.7968F, 0.7509F, -0.0227F, 1, 1, 1, -0.2F, false));
		lever6.cubeList.add(new ModelBox(lever6, 140, 180, -2.5976F, 2.2172F, 7.4084F, 1, 1, 1, -0.2F, false));
		lever6.cubeList.add(new ModelBox(lever6, 145, 142, -1.7935F, -0.3712F, 1.5765F, 2, 2, 1, 0.1F, false));
		lever6.cubeList.add(new ModelBox(lever6, 145, 142, -2.5944F, 1.6028F, 3.9886F, 2, 2, 2, 0.1F, false));
		lever6.cubeList.add(new ModelBox(lever6, 149, 173, -2.589F, 1.0077F, 3.9121F, 2, 2, 2, -0.1F, false));
		lever6.cubeList.add(new ModelBox(lever6, 145, 142, -1.7935F, -0.3712F, -5.4235F, 2, 2, 1, 0.1F, false));
		lever6.cubeList.add(new ModelBox(lever6, 140, 180, -3.7968F, 0.7509F, -1.0227F, 1, 1, 1, -0.2F, false));
		lever6.cubeList.add(new ModelBox(lever6, 140, 180, -1.4887F, 2.2419F, 6.3644F, 1, 1, 1, -0.2F, false));
		lever6.cubeList.add(new ModelBox(lever6, 140, 180, -3.5976F, 2.2172F, 6.4084F, 1, 1, 1, -0.2F, false));

		wires8 = new RendererModel(this);
		wires8.setRotationPoint(-0.4F, -33.9F, 2.6F);
		setRotationAngle(wires8, -0.3491F, 0.5236F, 0.0175F);
		all.addChild(wires8);
		wires8.cubeList.add(new ModelBox(wires8, 0, 0, -0.6984F, 7.1499F, 13.4498F, 8, 1, 1, -0.1F, false));
		wires8.cubeList.add(new ModelBox(wires8, 170, 173, 0.3016F, 7.1499F, 13.4498F, 1, 1, 1, 0.0F, false));
		wires8.cubeList.add(new ModelBox(wires8, 48, 32, 3.3016F, 7.1499F, 13.4498F, 1, 1, 1, 0.0F, false));
		wires8.cubeList.add(new ModelBox(wires8, 0, 0, -1.3984F, 7.1499F, 8.4498F, 1, 1, 6, -0.1F, false));
		wires8.cubeList.add(new ModelBox(wires8, 170, 173, -1.3984F, 7.1499F, 12.4498F, 1, 1, 1, 0.0F, false));
		wires8.cubeList.add(new ModelBox(wires8, 170, 173, -1.3984F, 7.1499F, 9.4498F, 1, 1, 1, 0.0F, false));
		wires8.cubeList.add(new ModelBox(wires8, 178, 190, -5.0843F, 8.0173F, 18.5024F, 3, 1, 3, 0.0F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, -4.9843F, 7.6173F, 20.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -4.0843F, 7.6173F, 20.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -3.1843F, 7.6173F, 20.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -3.1843F, 7.6173F, 19.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, 0.4134F, 7.6475F, 19.2662F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 178, 169, 1.7855F, 7.5573F, 16.0213F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, -4.0843F, 7.6173F, 19.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, -0.4866F, 7.6475F, 19.2662F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, 0.8855F, 7.5573F, 16.0213F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 153, 97, -4.9843F, 7.6173F, 19.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -4.9843F, 7.6173F, 18.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, -3.1843F, 7.6173F, 18.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, 0.4134F, 7.6475F, 18.2662F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 148, 140, 1.7855F, 7.5573F, 15.0213F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -4.0843F, 7.6173F, 18.5024F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 158, 136, -0.4866F, 7.6475F, 18.2662F, 1, 1, 1, -0.2F, false));
		wires8.cubeList.add(new ModelBox(wires8, 178, 169, 0.8855F, 7.5573F, 15.0213F, 1, 1, 1, -0.2F, false));

		wires7 = new RendererModel(this);
		wires7.setRotationPoint(2.3F, -30.7F, 8.1F);
		setRotationAngle(wires7, -0.3491F, 0.5236F, 0.0175F);
		all.addChild(wires7);
		wires7.cubeList.add(new ModelBox(wires7, 0, 0, -0.6984F, 7.1499F, 13.4498F, 6, 1, 1, -0.1F, false));
		wires7.cubeList.add(new ModelBox(wires7, 170, 173, 0.3016F, 7.1499F, 13.4498F, 1, 1, 1, 0.0F, false));
		wires7.cubeList.add(new ModelBox(wires7, 48, 32, 3.3016F, 7.1499F, 13.4498F, 1, 1, 1, 0.0F, false));
		wires7.cubeList.add(new ModelBox(wires7, 0, 0, -1.3984F, 7.1499F, 8.4498F, 1, 1, 6, -0.1F, false));
		wires7.cubeList.add(new ModelBox(wires7, 170, 173, -1.3984F, 7.1499F, 12.4498F, 1, 1, 1, 0.0F, false));
		wires7.cubeList.add(new ModelBox(wires7, 170, 173, -1.3984F, 7.1499F, 9.4498F, 1, 1, 1, 0.0F, false));

		mirror = new RendererModel(this);
		mirror.setRotationPoint(-0.4F, -28.6F, 0.5F);
		setRotationAngle(mirror, -0.2618F, 2.0944F, 0.0F);
		all.addChild(mirror);
		mirror.cubeList.add(new ModelBox(mirror, 143, 139, -1.916F, 0.1712F, 8.0863F, 4, 1, 6, 0.0F, false));
		mirror.cubeList.add(new ModelBox(mirror, 145, 173, -1.916F, -0.4084F, 7.931F, 4, 1, 6, -0.2F, false));

		wires6 = new RendererModel(this);
		wires6.setRotationPoint(6.1F, -31.3F, 1.4F);
		setRotationAngle(wires6, -0.3491F, 1.5708F, 0.0524F);
		all.addChild(wires6);
		wires6.cubeList.add(new ModelBox(wires6, 0, 0, 0.715F, 6.241F, 15.9368F, 6, 1, 1, -0.1F, false));
		wires6.cubeList.add(new ModelBox(wires6, 170, 173, 1.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires6.cubeList.add(new ModelBox(wires6, 153, 180, 4.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires6.cubeList.add(new ModelBox(wires6, 0, 0, 0.015F, 6.241F, 11.9368F, 1, 1, 5, -0.1F, false));
		wires6.cubeList.add(new ModelBox(wires6, 170, 173, 0.015F, 6.241F, 14.9368F, 1, 1, 1, 0.0F, false));
		wires6.cubeList.add(new ModelBox(wires6, 170, 173, 0.015F, 6.241F, 11.9368F, 1, 1, 1, 0.0F, false));

		wires5 = new RendererModel(this);
		wires5.setRotationPoint(2.3F, -33.0F, 7.0F);
		setRotationAngle(wires5, -0.3491F, 1.5708F, 0.0524F);
		all.addChild(wires5);
		wires5.cubeList.add(new ModelBox(wires5, 0, 0, 0.715F, 6.241F, 15.9368F, 6, 1, 1, -0.1F, false));
		wires5.cubeList.add(new ModelBox(wires5, 170, 173, 1.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires5.cubeList.add(new ModelBox(wires5, 48, 32, 4.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires5.cubeList.add(new ModelBox(wires5, 0, 0, 0.015F, 6.241F, 10.9368F, 1, 1, 6, -0.1F, false));
		wires5.cubeList.add(new ModelBox(wires5, 170, 173, 0.015F, 6.241F, 14.9368F, 1, 1, 1, 0.0F, false));
		wires5.cubeList.add(new ModelBox(wires5, 170, 173, 0.015F, 6.241F, 11.9368F, 1, 1, 1, 0.0F, false));

		wires4 = new RendererModel(this);
		wires4.setRotationPoint(-1.7F, -35.0F, 1.4F);
		setRotationAngle(wires4, -0.3491F, 1.5708F, 0.0524F);
		all.addChild(wires4);
		wires4.cubeList.add(new ModelBox(wires4, 0, 0, 0.715F, 6.241F, 15.9368F, 6, 1, 1, -0.1F, false));
		wires4.cubeList.add(new ModelBox(wires4, 170, 173, 1.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires4.cubeList.add(new ModelBox(wires4, 48, 32, 4.715F, 6.241F, 15.9368F, 1, 1, 1, 0.0F, false));
		wires4.cubeList.add(new ModelBox(wires4, 0, 0, 0.015F, 6.241F, 10.9368F, 1, 1, 6, -0.1F, false));
		wires4.cubeList.add(new ModelBox(wires4, 170, 173, 0.015F, 6.241F, 14.9368F, 1, 1, 1, 0.0F, false));
		wires4.cubeList.add(new ModelBox(wires4, 170, 173, 0.015F, 6.241F, 11.9368F, 1, 1, 1, 0.0F, false));

		buttons = new RendererModel(this);
		buttons.setRotationPoint(0.7F, -19.8F, 2.1F);
		setRotationAngle(buttons, -0.2269F, 1.0472F, -0.0349F);
		all.addChild(buttons);
		buttons.cubeList.add(new ModelBox(buttons, 0, 101, 2.1221F, -6.2642F, 15.7892F, 1, 1, 2, 0.0F, false));
		buttons.cubeList.add(new ModelBox(buttons, 5, 98, 0.1221F, -6.2642F, 15.7892F, 1, 1, 2, 0.0F, false));
		buttons.cubeList.add(new ModelBox(buttons, 148, 126, 2.7221F, -6.2642F, 15.7892F, 1, 1, 2, 0.1F, false));
		buttons.cubeList.add(new ModelBox(buttons, 148, 126, -0.4779F, -6.2642F, 15.7892F, 1, 1, 2, 0.1F, false));

		wires3 = new RendererModel(this);
		wires3.setRotationPoint(-0.7F, -35.3F, 5.4F);
		setRotationAngle(wires3, -0.3665F, 2.7576F, 0.0524F);
		all.addChild(wires3);
		wires3.cubeList.add(new ModelBox(wires3, 0, 0, -1.2538F, 5.265F, 18.4746F, 6, 1, 1, -0.1F, false));
		wires3.cubeList.add(new ModelBox(wires3, 170, 173, -0.2538F, 5.265F, 18.4746F, 1, 1, 1, 0.0F, false));
		wires3.cubeList.add(new ModelBox(wires3, 6, 88, 2.7462F, 5.265F, 18.4746F, 1, 1, 1, 0.0F, false));
		wires3.cubeList.add(new ModelBox(wires3, 0, 0, -1.9538F, 5.265F, 13.4746F, 1, 1, 6, -0.1F, false));
		wires3.cubeList.add(new ModelBox(wires3, 170, 173, -1.9538F, 5.265F, 17.4746F, 1, 1, 1, 0.0F, false));
		wires3.cubeList.add(new ModelBox(wires3, 170, 173, -1.9538F, 5.265F, 14.4746F, 1, 1, 1, 0.0F, false));

		wires2 = new RendererModel(this);
		wires2.setRotationPoint(0.3F, -34.6F, 4.4F);
		setRotationAngle(wires2, -0.3665F, 2.6704F, 0.0524F);
		all.addChild(wires2);
		wires2.cubeList.add(new ModelBox(wires2, 0, 0, -1.012F, 5.301F, 18.3807F, 6, 1, 1, -0.1F, false));
		wires2.cubeList.add(new ModelBox(wires2, 10, 110, -0.012F, 5.301F, 18.3807F, 1, 1, 1, 0.0F, false));
		wires2.cubeList.add(new ModelBox(wires2, 10, 110, 2.988F, 5.301F, 18.3807F, 1, 1, 1, 0.0F, false));
		wires2.cubeList.add(new ModelBox(wires2, 0, 0, -1.712F, 5.301F, 13.3807F, 1, 1, 6, -0.1F, false));
		wires2.cubeList.add(new ModelBox(wires2, 10, 110, -1.712F, 5.301F, 17.3807F, 1, 1, 1, 0.0F, false));
		wires2.cubeList.add(new ModelBox(wires2, 13, 75, -1.712F, 5.301F, 14.3807F, 1, 1, 1, 0.0F, false));

		wires = new RendererModel(this);
		wires.setRotationPoint(0.3F, -33.2F, 3.1F);
		setRotationAngle(wires, -0.3665F, -2.6704F, 0.0F);
		all.addChild(wires);
		wires.cubeList.add(new ModelBox(wires, 0, 0, -3.7361F, 5.0583F, 18.1904F, 6, 1, 1, -0.1F, false));
		wires.cubeList.add(new ModelBox(wires, 10, 110, -2.7361F, 5.0583F, 18.1904F, 1, 1, 1, 0.0F, false));
		wires.cubeList.add(new ModelBox(wires, 10, 110, 0.2639F, 5.0583F, 18.1904F, 1, 1, 1, 0.0F, false));
		wires.cubeList.add(new ModelBox(wires, 0, 0, -4.4361F, 5.0583F, 13.1904F, 1, 1, 6, -0.1F, false));
		wires.cubeList.add(new ModelBox(wires, 10, 110, -4.4361F, 5.0583F, 17.1904F, 1, 1, 1, 0.0F, false));
		wires.cubeList.add(new ModelBox(wires, 13, 75, -4.4361F, 5.0583F, 14.1904F, 1, 1, 1, 0.0F, false));

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(1.4F, -32.7F, -12.2F);
		setRotationAngle(bone112, -0.2618F, 0.0F, 0.0F);
		all.addChild(bone112);
		bone112.cubeList.add(new ModelBox(bone112, 156, 133, -3.2F, 1.3492F, 21.1647F, 1, 2, 1, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(3.6F, -32.8F, -12.2F);
		setRotationAngle(bone111, -0.2618F, 0.0F, 0.0F);
		all.addChild(bone111);
		bone111.cubeList.add(new ModelBox(bone111, 138, 142, -3.2F, 1.3492F, 20.1647F, 2, 2, 2, 0.0F, false));
		bone111.cubeList.add(new ModelBox(bone111, 134, 167, -3.2F, 0.8145F, 20.2285F, 2, 2, 2, -0.2F, false));

		bone106 = new RendererModel(this);
		bone106.setRotationPoint(13.7F, -19.0F, -10.2F);
		setRotationAngle(bone106, 0.4363F, -0.5236F, 0.0F);
		all.addChild(bone106);
		bone106.cubeList.add(new ModelBox(bone106, 10, 110, -4.483F, -1.7618F, -3.3971F, 2, 2, 4, 0.0F, false));
		bone106.cubeList.add(new ModelBox(bone106, 15, 111, -4.483F, -2.215F, -3.1858F, 2, 2, 4, -0.2F, false));
		bone106.cubeList.add(new ModelBox(bone106, 10, 110, -6.3187F, -4.472F, 0.8894F, 1, 4, 1, 0.0F, false));

		land_type = new RendererModel(this);
		land_type.setRotationPoint(-6.0F, 0.0F, 1.45F);
		bone106.addChild(land_type);
		land_type.cubeList.add(new ModelBox(land_type, 157, 182, -0.2589F, -6.4945F, -0.1653F, 1, 3, 1, 0.0F, false));
		land_type.cubeList.add(new ModelBox(land_type, 157, 182, -0.2625F, -6.382F, -1.1073F, 1, 3, 1, 0.0F, false));
		land_type.cubeList.add(new ModelBox(land_type, 157, 182, 0.3473F, -6.6424F, -0.4825F, 1, 3, 1, 0.0F, false));
		land_type.cubeList.add(new ModelBox(land_type, 157, 182, -0.8187F, -6.6507F, -0.5002F, 1, 3, 1, 0.0F, false));

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(4.4F, -19.0F, -15.5F);
		setRotationAngle(bone103, 0.4363F, -0.0349F, 0.0524F);
		all.addChild(bone103);
		bone103.cubeList.add(new ModelBox(bone103, 144, 171, -1.0049F, -2.2733F, -2.7182F, 2, 2, 6, 0.0F, false));
		bone103.cubeList.add(new ModelBox(bone103, 0, 0, -1.5049F, -2.7733F, -3.7182F, 3, 3, 1, 0.0F, false));
		bone103.cubeList.add(new ModelBox(bone103, 0, 0, -1.5049F, -2.7733F, 3.2818F, 3, 3, 1, 0.0F, false));
		bone103.cubeList.add(new ModelBox(bone103, 0, 0, -1.4278F, -2.7021F, -0.6249F, 3, 3, 2, 0.0F, false));

		important_controls = new RendererModel(this);
		important_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		all.addChild(important_controls);

		fastreturn = new RendererModel(this);
		fastreturn.setRotationPoint(0.2F, -26.2F, 3.0F);
		important_controls.addChild(fastreturn);
		fastreturn.cubeList.add(new ModelBox(fastreturn, 14, 77, -0.7038F, 9.1768F, -26.4935F, 7, 4, 3, 0.0F, false));
		fastreturn.cubeList.add(new ModelBox(fastreturn, 14, 77, 2.2962F, 13.1768F, -26.3935F, 3, 1, 3, 0.0F, false));

		lever = new RendererModel(this);
		lever.setRotationPoint(5.0F, 11.1F, -25.4F);
		setRotationAngle(lever, 0.0F, 0.0F, -0.6981F);
		fastreturn.addChild(lever);
		lever.cubeList.add(new ModelBox(lever, 123, 174, -1.2374F, -0.5149F, -0.5935F, 6, 1, 1, 0.0F, false));
		lever.cubeList.add(new ModelBox(lever, 141, 140, 4.1626F, -1.0149F, -1.0935F, 3, 2, 2, -0.4F, false));

		refuel = new RendererModel(this);
		refuel.setRotationPoint(3.7F, -24.3F, 4.4F);
		setRotationAngle(refuel, 0.384F, -1.0472F, -0.4363F);
		important_controls.addChild(refuel);
		refuel.cubeList.add(new ModelBox(refuel, 17, 102, -16.5571F, -2.2346F, -0.0542F, 3, 1, 3, 0.0F, false));

		doorcontrol = new RendererModel(this);
		doorcontrol.setRotationPoint(-2.2F, -22.3F, 0.0F);
		setRotationAngle(doorcontrol, 0.384F, -1.0472F, -0.4363F);
		important_controls.addChild(doorcontrol);
		doorcontrol.cubeList.add(new ModelBox(doorcontrol, 16, 109, -18.2891F, -1.3017F, 3.777F, 2, 1, 2, 0.0F, false));

		keyboardxyz = new RendererModel(this);
		keyboardxyz.setRotationPoint(-2.2F, -22.3F, 0.0F);
		setRotationAngle(keyboardxyz, 0.384F, -1.0472F, -0.4363F);
		important_controls.addChild(keyboardxyz);
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 30, 189, -19.5528F, -1.5521F, -6.4794F, 4, 1, 10, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -17.5528F, -1.7521F, 2.3206F, 2, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -17.5528F, -1.7521F, -0.0794F, 2, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -17.5528F, -1.7521F, -2.5794F, 2, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -19.2528F, -1.7521F, 2.0206F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -19.2528F, -1.7521F, 0.0206F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -19.2528F, -1.7521F, -1.9794F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -19.2528F, -1.7521F, -3.9794F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -19.2528F, -1.7521F, -5.9794F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 74, 178, -17.5528F, -1.7521F, -6.2794F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 81, 180, -17.5528F, -1.7521F, -5.0794F, 2, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 88, 188, -17.5528F, -1.7521F, 1.1206F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 88, 188, -17.5528F, -1.7521F, -3.7794F, 1, 1, 1, 0.0F, false));
		keyboardxyz.cubeList.add(new ModelBox(keyboardxyz, 88, 188, -17.5528F, -1.7521F, -1.3794F, 1, 1, 1, 0.0F, false));

		throttle = new RendererModel(this);
		throttle.setRotationPoint(1.9F, -29.0F, 0.4F);
		setRotationAngle(throttle, -0.2618F, 0.0F, 0.0F);
		important_controls.addChild(throttle);
		throttle.cubeList.add(new ModelBox(throttle, 105, 136, -1.9F, -0.7235F, 11.1022F, 2, 2, 6, -0.1F, false));
		throttle.cubeList.add(new ModelBox(throttle, 74, 177, -1.9F, -1.0235F, 11.1022F, 2, 2, 4, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 79, 167, -2.4F, -0.2235F, 11.6022F, 3, 2, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 108, 16, -2.4F, -0.7235F, 10.1022F, 3, 2, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 84, 163, -0.2F, -1.2235F, 11.1022F, 1, 3, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 110, 19, -2.6F, -1.2235F, 11.1022F, 1, 2, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 12, 104, -3.9F, 0.7765F, 11.1022F, 2, 1, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 12, 104, -3.4F, 0.3901F, 10.9987F, 2, 1, 6, -0.1F, false));
		throttle.cubeList.add(new ModelBox(throttle, 100, 8, -4.1F, -0.0235F, 11.1022F, 1, 1, 6, -0.2F, false));

		lever3 = new RendererModel(this);
		lever3.setRotationPoint(-3.0F, 0.5518F, 14.9068F);
		setRotationAngle(lever3, 1.1345F, 0.0F, 0.0F);
		throttle.addChild(lever3);
		lever3.cubeList.add(new ModelBox(lever3, 141, 171, -0.5F, -4.3119F, -0.715F, 1, 5, 1, -0.1F, false));
		lever3.cubeList.add(new ModelBox(lever3, 0, 0, -0.5F, -5.1119F, -0.715F, 1, 1, 1, 0.0F, false));

		Sonicport = new RendererModel(this);
		Sonicport.setRotationPoint(1.9F, -29.0F, 0.4F);
		setRotationAngle(Sonicport, -0.2618F, 0.0F, 0.0F);
		important_controls.addChild(Sonicport);
		Sonicport.cubeList.add(new ModelBox(Sonicport, 136, 175, -3.3F, 1.2526F, 19.1389F, 1, 2, 3, 0.0F, false));
		Sonicport.cubeList.add(new ModelBox(Sonicport, 136, 175, -1.1F, 1.2526F, 19.1389F, 1, 2, 3, 0.0F, false));
		Sonicport.cubeList.add(new ModelBox(Sonicport, 136, 175, -2.7F, 1.2267F, 21.2354F, 2, 2, 1, 0.0F, false));
		Sonicport.cubeList.add(new ModelBox(Sonicport, 136, 175, -2.7F, 1.2166F, 18.9551F, 2, 2, 1, 0.0F, false));
		Sonicport.cubeList.add(new ModelBox(Sonicport, 136, 131, -3.2F, 1.4458F, 19.1906F, 3, 2, 3, -0.2F, false));

		Incrementincrease = new RendererModel(this);
		Incrementincrease.setRotationPoint(-5.3F, -21.3F, -2.0F);
		setRotationAngle(Incrementincrease, -0.2269F, 1.0297F, -0.0349F);
		important_controls.addChild(Incrementincrease);
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 132, 176, 1.9462F, -7.6683F, 13.7385F, 1, 1, 6, 0.0F, false));
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 132, 176, -0.0538F, -7.6683F, 13.7385F, 1, 1, 6, 0.0F, false));
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 148, 126, 2.5462F, -7.6683F, 13.7385F, 1, 1, 6, 0.1F, false));
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 148, 126, 0.9462F, -7.6683F, 13.7385F, 1, 1, 6, 0.1F, false));
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 148, 126, -0.6538F, -7.6683F, 13.7385F, 1, 1, 6, 0.1F, false));
		Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 132, 176, 0.9462F, -7.6683F, 19.7385F, 1, 1, 1, 0.0F, false));

		lever2 = new RendererModel(this);
		lever2.setRotationPoint(1.475F, -7.2F, 20.9F);
		Incrementincrease.addChild(lever2);
		lever2.cubeList.add(new ModelBox(lever2, 136, 10, -0.498F, -1.4443F, -0.385F, 1, 2, 1, -0.2F, false));
		lever2.cubeList.add(new ModelBox(lever2, 136, 23, -0.498F, -1.4443F, -0.385F, 1, 1, 1, -0.1F, false));

		monitor = new RendererModel(this);
		monitor.setRotationPoint(-0.1F, -28.0F, 0.4F);
		setRotationAngle(monitor, 0.0F, -0.5236F, 0.0F);
		important_controls.addChild(monitor);
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, -4.6261F, 3.0F, 12.7797F, 9, 1, 2, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, -4.6261F, 4.0F, 12.7797F, 9, 1, 3, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, -4.6261F, -4.0F, 11.7797F, 9, 1, 3, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 184, 115, -3.8065F, -3.7F, 14.9993F, 2, 2, 0, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, -3.6261F, -3.0F, 12.7797F, 7, 6, 1, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 138, 138, -1.0261F, -1.0F, 1.7797F, 2, 2, 11, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 138, 138, -1.0261F, 1.5F, 2.7797F, 2, 2, 10, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, -4.6261F, -3.0F, 12.7797F, 1, 6, 2, 0.0F, false));
		monitor.cubeList.add(new ModelBox(monitor, 132, 176, 3.3739F, -3.0F, 12.7797F, 1, 6, 2, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(-0.1F, -29.7F, 0.8F);
		setRotationAngle(handbreak, -0.3491F, 2.0944F, 0.0F);
		important_controls.addChild(handbreak);
		handbreak.cubeList.add(new ModelBox(handbreak, 145, 173, -1.4519F, -0.5427F, 15.4909F, 4, 1, 6, 0.0F, false));
		handbreak.cubeList.add(new ModelBox(handbreak, 144, 129, -0.5349F, -1.546F, 15.5001F, 3, 1, 6, 0.0F, false));
		handbreak.cubeList.add(new ModelBox(handbreak, 115, 140, -1.1519F, -2.0427F, 15.4909F, 2, 2, 6, -0.2F, false));
		handbreak.cubeList.add(new ModelBox(handbreak, 115, 140, -1.9519F, -1.5427F, 16.4909F, 2, 2, 4, 0.0F, false));

		lever4 = new RendererModel(this);
		lever4.setRotationPoint(2.5F, -0.4482F, 17.9068F);
		setRotationAngle(lever4, 1.0472F, 0.0F, 0.0F);
		handbreak.addChild(lever4);
		lever4.cubeList.add(new ModelBox(lever4, 144, 129, -1.0519F, -2.4802F, 0.7154F, 1, 5, 1, -0.1F, false));
		lever4.cubeList.add(new ModelBox(lever4, 13, 106, -3.0519F, -3.2802F, 0.7154F, 3, 1, 1, 0.0F, false));

		lever5 = new RendererModel(this);
		lever5.setRotationPoint(2.6F, -0.4482F, 14.9068F);
		setRotationAngle(lever5, 1.0472F, 0.0F, 0.0F);
		handbreak.addChild(lever5);
		lever5.cubeList.add(new ModelBox(lever5, 144, 129, -1.0519F, -2.4802F, 0.7154F, 1, 5, 1, -0.1F, false));
		lever5.cubeList.add(new ModelBox(lever5, 13, 106, -2.0519F, -3.2802F, 0.7154F, 2, 1, 1, 0.0F, false));

		wheel = new RendererModel(this);
		wheel.setRotationPoint(-4.4F, -0.4482F, 17.1068F);
		handbreak.addChild(wheel);
		wheel.cubeList.add(new ModelBox(wheel, 83, 171, 1.6481F, -2.1265F, 1.9841F, 1, 3, 1, -0.2F, false));
		wheel.cubeList.add(new ModelBox(wheel, 83, 171, 1.6481F, -2.1265F, -0.0159F, 1, 3, 1, -0.2F, false));
		wheel.cubeList.add(new ModelBox(wheel, 83, 171, 1.8481F, -1.5265F, 0.3841F, 1, 2, 2, -0.2F, false));
		wheel.cubeList.add(new ModelBox(wheel, 83, 171, 1.6481F, -2.1265F, -0.0159F, 1, 1, 3, -0.2F, false));
		wheel.cubeList.add(new ModelBox(wheel, 83, 171, 1.6481F, -0.1265F, -0.0159F, 1, 1, 3, -0.2F, false));

		randomiser_and_facingcontrol = new RendererModel(this);
		randomiser_and_facingcontrol.setRotationPoint(-7.1F, -29.0F, 3.4F);
		setRotationAngle(randomiser_and_facingcontrol, -0.3491F, 0.5236F, 0.0F);
		important_controls.addChild(randomiser_and_facingcontrol);
		randomiser_and_facingcontrol.cubeList.add(new ModelBox(randomiser_and_facingcontrol, 5, 108, 3.166F, 1.4211F, 15.5757F, 3, 2, 3, 0.0F, false));
		randomiser_and_facingcontrol.cubeList.add(new ModelBox(randomiser_and_facingcontrol, 108, 68, 6.1892F, 2.1355F, 10.689F, 3, 2, 3, 0.0F, false));
		randomiser_and_facingcontrol.cubeList.add(new ModelBox(randomiser_and_facingcontrol, 109, 81, 9.2595F, 2.6188F, 15.2089F, 3, 2, 3, 0.0F, false));
		randomiser_and_facingcontrol.cubeList.add(new ModelBox(randomiser_and_facingcontrol, 5, 108, 3.166F, 1.0211F, 15.5757F, 3, 2, 3, -0.1F, false));

		throttle3 = new RendererModel(this);
		throttle3.setRotationPoint(4.0F, 0.0F, 4.0F);
		randomiser_and_facingcontrol.addChild(throttle3);
		throttle3.cubeList.add(new ModelBox(throttle3, 5, 108, 5.0883F, 2.0547F, 15.8814F, 3, 2, 3, 0.0F, false));
		throttle3.cubeList.add(new ModelBox(throttle3, 5, 108, 5.0883F, 1.6547F, 15.8814F, 3, 2, 3, -0.1F, false));

		facing_control = new RendererModel(this);
		facing_control.setRotationPoint(7.6F, 1.2877F, 12.1F);
		setRotationAngle(facing_control, 0.0F, 3.1416F, 0.0F);
		randomiser_and_facingcontrol.addChild(facing_control);
		facing_control.cubeList.add(new ModelBox(facing_control, 9, 148, -1.5866F, -1.7285F, -1.547F, 3, 3, 3, -0.4F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 186, 86, -1.5866F, -2.0285F, -1.547F, 3, 3, 3, -0.5F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 178, 91, -1.5866F, -1.4285F, -1.297F, 3, 3, 3, -0.5F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 186, 86, -1.8366F, -1.7285F, -1.547F, 3, 3, 3, -0.5F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 186, 86, -1.5866F, -1.7285F, -1.797F, 3, 3, 3, -0.5F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 178, 91, -1.5866F, -1.7285F, -1.547F, 3, 3, 3, -0.5F, false));
		facing_control.cubeList.add(new ModelBox(facing_control, 178, 91, -1.3366F, -1.7285F, -1.547F, 3, 3, 3, -0.5F, false));

		randomiser = new RendererModel(this);
		randomiser.setRotationPoint(10.725F, 1.5F, 16.85F);
		randomiser_and_facingcontrol.addChild(randomiser);
		randomiser.cubeList.add(new ModelBox(randomiser, 9, 144, -1.4315F, -1.7356F, -1.453F, 3, 3, 3, -0.4F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.4315F, -2.0356F, -1.453F, 3, 3, 3, -0.5F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.4315F, -1.4356F, -1.453F, 3, 3, 3, -0.5F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.1315F, -1.7356F, -1.453F, 3, 3, 3, -0.5F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.4315F, -1.7356F, -1.203F, 3, 3, 3, -0.5F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.4315F, -1.7356F, -1.703F, 3, 3, 3, -0.5F, false));
		randomiser.cubeList.add(new ModelBox(randomiser, 181, 97, -1.7315F, -1.7356F, -1.453F, 3, 3, 3, -0.5F, false));

		Comuni_Phone = new RendererModel(this);
		Comuni_Phone.setRotationPoint(5.9F, -30.0F, -1.1F);
		setRotationAngle(Comuni_Phone, 0.0F, 1.5708F, 0.3491F);
		important_controls.addChild(Comuni_Phone);
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 4, 145, -1.6F, 4.9314F, 8.3125F, 5, 1, 6, 0.0F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 178, 190, -6.1F, 5.5629F, 13.6707F, 3, 1, 3, 0.0F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 108, 161, -5.1F, 5.3335F, 9.3305F, 3, 1, 3, 0.0F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 108, 161, -5.1F, 4.8892F, 9.3816F, 3, 1, 3, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 108, 161, 3.1F, 5.5287F, 13.7647F, 3, 1, 3, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 108, 161, -5.1F, 4.4449F, 9.4327F, 3, 1, 3, -0.4F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 108, 161, 3.1F, 5.0844F, 13.8158F, 3, 1, 3, -0.4F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 101, 19, 1.4F, 3.5314F, 8.3125F, 2, 1, 6, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 114, 11, 1.4F, 4.1314F, 12.3125F, 2, 1, 2, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 148, 130, 1.9F, 4.1314F, 13.8125F, 1, 1, 2, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 148, 130, 0.9F, 4.7314F, 14.8125F, 2, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 148, 130, -0.7F, 4.7314F, 15.0125F, 2, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 148, 130, -1.3F, 4.8314F, 14.0125F, 1, 1, 2, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 114, 11, 1.4F, 4.1314F, 8.3125F, 2, 1, 2, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 158, 136, -6.0F, 5.1629F, 13.6707F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 160, 100, -4.5F, 4.9936F, 8.2882F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 72, 178, -0.7F, 4.5314F, 11.3125F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 158, 136, -5.1F, 5.1629F, 13.6707F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 72, 178, -1.6F, 4.5314F, 12.3125F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 153, 97, -6.0F, 5.1629F, 14.6707F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 72, 178, 0.2F, 4.5314F, 12.3125F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 158, 136, -4.2F, 5.1629F, 14.6707F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 72, 178, -0.7F, 4.5314F, 13.3125F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 158, 136, -5.1F, 5.1629F, 15.6707F, 1, 1, 1, -0.2F, false));
		Comuni_Phone.cubeList.add(new ModelBox(Comuni_Phone, 158, 136, -4.2F, 5.1629F, 15.6707F, 1, 1, 1, -0.2F, false));

		telepathic_circuit = new RendererModel(this);
		telepathic_circuit.setRotationPoint(3.9F, -20.4F, 4.3F);
		setRotationAngle(telepathic_circuit, 0.0F, -1.5708F, 0.0F);
		important_controls.addChild(telepathic_circuit);

		crystal = new RendererModel(this);
		crystal.setRotationPoint(-4.0F, 0.5518F, 20.9068F);
		setRotationAngle(crystal, -0.4363F, 0.0F, 0.0F);
		telepathic_circuit.addChild(crystal);
		crystal.cubeList.add(new ModelBox(crystal, 105, 136, -1.0F, -0.2253F, -2.0453F, 2, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 140, 172, -1.0F, -0.2253F, 0.9547F, 2, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 105, 136, 0.6F, -0.316F, 0.5124F, 1, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 105, 136, 0.6F, -0.316F, -1.4876F, 1, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 140, 172, -1.5F, -0.316F, 0.5124F, 1, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 105, 136, -1.5F, -0.316F, -1.4876F, 1, 1, 1, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 105, 136, 1.0F, -0.2253F, -1.0453F, 1, 1, 2, -0.1F, false));
		crystal.cubeList.add(new ModelBox(crystal, 140, 172, -2.0F, -0.2253F, -1.0453F, 1, 1, 2, -0.1F, false));

		lever8 = new RendererModel(this);
		lever8.setRotationPoint(1.0F, -1.4482F, 14.9068F);
		setRotationAngle(lever8, -0.4363F, 0.0F, 0.0F);
		telepathic_circuit.addChild(lever8);
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, -2.0F, -0.979F, 6.1202F, 1, 1, 1, 0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -5.8F, -1.1744F, -1.0326F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -5.8F, -1.1988F, -1.9267F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, 0.0F, -0.5258F, 6.3315F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, -0.9F, -0.5563F, 5.2139F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, 1.0F, -0.5258F, 6.3315F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, 0.1F, -0.5563F, 5.2139F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 105, 136, -2.0F, -1.1194F, 0.9791F, 1, 1, 1, 0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -8.0F, -0.7569F, 1.1482F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -8.0F, -0.8654F, 2.0906F, 1, 1, 1, 0.0F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -10.0F, -0.8654F, 2.0906F, 1, 1, 1, 0.0F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -6.8F, -0.818F, -3.0871F, 3, 1, 3, 0.0F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -4.8F, -1.1744F, -1.0326F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -4.8F, -1.1988F, -1.9267F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -4.8F, -1.1387F, -3.002F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -6.8F, -1.1744F, -1.0326F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -6.8F, -1.1988F, -1.9267F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -6.8F, -1.1387F, -3.002F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -5.8F, -1.1387F, -3.002F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -8.0F, -0.6174F, 2.9786F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -10.0F, -0.7569F, 1.1482F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 153, 180, -10.0F, -0.6174F, 2.9786F, 1, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 141, 138, -12.8F, -0.4183F, 2.0784F, 9, 1, 1, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 141, 138, -5.8F, -0.6484F, -4.7943F, 1, 1, 6, -0.1F, false));
		lever8.cubeList.add(new ModelBox(lever8, 141, 138, -5.1F, -0.7344F, -6.5789F, 1, 1, 3, -0.1F, false));

		nope = new RendererModel(this);
		nope.setRotationPoint(-4.0124F, -1.8123F, 17.5686F);
		setRotationAngle(nope, 0.4363F, 3.1416F, 0.0F);
		telepathic_circuit.addChild(nope);
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.5F, -1.8577F, -1.4094F, 3, 3, 3, -0.5F, false));
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.5F, -1.2577F, -1.4094F, 3, 3, 3, -0.5F, false));
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.2F, -1.5577F, -1.4094F, 3, 3, 3, -0.5F, false));
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.5F, -1.5577F, -1.1094F, 3, 3, 3, -0.5F, false));
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.5F, -1.5577F, -1.7094F, 3, 3, 3, -0.5F, false));
		nope.cubeList.add(new ModelBox(nope, 155, 184, -1.8F, -1.5577F, -1.4094F, 3, 3, 3, -0.5F, false));

		keyboard_behind_monitor = new RendererModel(this);
		keyboard_behind_monitor.setRotationPoint(0.9F, -26.6F, 0.5F);
		setRotationAngle(keyboard_behind_monitor, -0.6109F, 0.9599F, -0.6981F);
		all.addChild(keyboard_behind_monitor);
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 30, 189, -14.4986F, 0.0036F, -6.3213F, 4, 1, 10, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 30, 189, -14.4986F, 0.0036F, -6.3213F, 4, 1, 10, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, -4.9213F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, -4.9213F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, 1.2787F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, 1.2787F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, 0.0787F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, 0.0787F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, -3.6213F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, -3.6213F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, -2.4213F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 81, 180, -12.4986F, -0.1964F, -2.4213F, 2, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, -1.2213F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor.cubeList.add(new ModelBox(keyboard_behind_monitor, 88, 188, -12.4986F, -0.1964F, -1.2213F, 1, 1, 1, 0.0F, false));

		keyboard_by_monitor = new RendererModel(this);
		keyboard_by_monitor.setRotationPoint(-2.2F, -25.3F, 6.7F);
		setRotationAngle(keyboard_by_monitor, -0.6109F, 0.9425F, -0.6981F);
		all.addChild(keyboard_by_monitor);
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 30, 189, -14.5034F, 0.6849F, -6.4929F, 4, 2, 10, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 30, 189, -14.5034F, 0.6849F, -6.4929F, 4, 2, 10, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, 2.3071F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, 2.3071F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, 2.0071F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, 2.0071F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, 0.0071F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, 0.0071F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -1.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -1.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -3.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -3.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -5.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -14.2034F, 0.4849F, -5.9929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -5.0929F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -5.0929F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 144, 182, -9.9174F, 1.399F, -4.6268F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 144, 182, -9.9174F, 1.399F, -4.6268F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -0.0929F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -0.0929F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 144, 182, -9.9174F, 1.399F, 0.3732F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 144, 182, -9.9174F, 1.399F, 0.3732F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -2.5929F, 2, 1, 1, 0.0F, false));
		keyboard_by_monitor.cubeList.add(new ModelBox(keyboard_by_monitor, 81, 180, -12.5034F, 0.4849F, -2.5929F, 2, 1, 1, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.2F, -26.2F, 3.0F);
		setRotationAngle(bone100, 0.1222F, -0.4712F, -0.3491F);
		all.addChild(bone100);

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(3.084F, -4.4256F, 3.9995F);
		setRotationAngle(bone101, 0.4363F, -1.0472F, -0.1745F);
		bone100.addChild(bone101);
		bone101.cubeList.add(new ModelBox(bone101, 141, 185, -17.8455F, 1.9164F, -1.2631F, 2, 1, 2, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 141, 185, -18.8177F, 1.7146F, -1.7493F, 1, 1, 3, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 140, 176, -24.513F, 2.0962F, -1.1586F, 4, 1, 2, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 140, 176, -24.5416F, 1.0896F, -2.1651F, 4, 2, 1, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 140, 176, -24.4952F, 1.0699F, 0.8345F, 4, 2, 1, 0.0F, false));

		bone102 = new RendererModel(this);
		bone102.setRotationPoint(-1.2583F, -4.3874F, -5.0836F);
		setRotationAngle(bone102, 0.6109F, -0.9948F, -0.3491F);
		bone100.addChild(bone102);
		bone102.cubeList.add(new ModelBox(bone102, 74, 190, -17.9423F, 2.1886F, -0.9521F, 2, 1, 2, 0.0F, false));
		bone102.cubeList.add(new ModelBox(bone102, 74, 190, -18.9146F, 1.9868F, -1.4383F, 1, 1, 3, 0.0F, false));

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(2.2F, -31.8F, 5.3F);
		setRotationAngle(bone99, 0.1222F, -0.5061F, -0.2618F);
		all.addChild(bone99);
		bone99.cubeList.add(new ModelBox(bone99, 143, 179, -23.6375F, 2.6065F, -4.0702F, 8, 2, 2, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 0, 0, -24.4597F, 2.3356F, -4.5597F, 1, 3, 3, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 147, 174, -27.4263F, 3.6628F, -5.098F, 2, 3, 3, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 146, 177, -27.1999F, 2.6888F, -5.1048F, 2, 3, 3, -0.5F, false));
		bone99.cubeList.add(new ModelBox(bone99, 0, 0, -17.4421F, 2.2219F, -4.5422F, 1, 3, 3, 0.0F, false));

		bone92 = new RendererModel(this);
		bone92.setRotationPoint(5.2F, -31.2F, 6.0F);
		setRotationAngle(bone92, 0.0873F, -0.5236F, -0.1745F);
		all.addChild(bone92);
		bone92.cubeList.add(new ModelBox(bone92, 0, 0, -16.4147F, 2.7866F, -3.6388F, 2, 1, 2, 0.0F, false));
		bone92.cubeList.add(new ModelBox(bone92, 126, 192, -20.4147F, 2.7866F, -4.6388F, 1, 1, 1, 0.0F, false));

		control6 = new RendererModel(this);
		control6.setRotationPoint(0.3F, -24.3F, 2.4F);
		setRotationAngle(control6, 0.384F, -1.0472F, -0.4363F);
		all.addChild(control6);
		control6.cubeList.add(new ModelBox(control6, 192, 174, -15.9698F, -0.7775F, 1.6328F, 2, 1, 2, 0.0F, false));

		control4 = new RendererModel(this);
		control4.setRotationPoint(8.0F, -25.0F, -6.4F);
		setRotationAngle(control4, 0.4363F, -1.0472F, -0.5236F);
		all.addChild(control4);
		control4.cubeList.add(new ModelBox(control4, 153, 177, -17.7883F, 1.215F, 3.8865F, 2, 1, 1, 0.0F, false));
		control4.cubeList.add(new ModelBox(control4, 140, 140, -14.4072F, 0.5995F, 5.4355F, 1, 1, 1, 0.0F, false));
		control4.cubeList.add(new ModelBox(control4, 140, 140, -5.9111F, -0.9408F, 9.7778F, 1, 1, 1, 0.0F, false));
		control4.cubeList.add(new ModelBox(control4, 140, 140, -11.3542F, -0.1671F, 13.199F, 1, 1, 1, 0.0F, false));
		control4.cubeList.add(new ModelBox(control4, 81, 178, -14.4206F, 0.7391F, 15.3264F, 2, 1, 1, 0.0F, false));

		control2 = new RendererModel(this);
		control2.setRotationPoint(0.7F, -24.0F, 3.4F);
		setRotationAngle(control2, 0.384F, -1.0472F, -0.4363F);
		all.addChild(control2);
		control2.cubeList.add(new ModelBox(control2, 148, 174, -19.5528F, -1.5521F, -4.4794F, 2, 1, 8, 0.0F, false));
		control2.cubeList.add(new ModelBox(control2, 149, 183, -19.2528F, -1.7521F, 2.0206F, 1, 1, 1, 0.0F, false));
		control2.cubeList.add(new ModelBox(control2, 149, 183, -19.2528F, -1.7521F, 0.0206F, 1, 1, 1, 0.0F, false));
		control2.cubeList.add(new ModelBox(control2, 149, 183, -19.2528F, -1.7521F, -1.9794F, 1, 1, 1, 0.0F, false));
		control2.cubeList.add(new ModelBox(control2, 149, 183, -19.2528F, -1.7521F, -3.9794F, 1, 1, 1, 0.0F, false));

		Shell = new RendererModel(this);
		Shell.setRotationPoint(-1.0F, 2.0F, -13.0F);
		all.addChild(Shell);

		Shellone = new RendererModel(this);
		Shellone.setRotationPoint(1.2F, -28.2F, 16.0F);
		Shell.addChild(Shellone);

		bone = new RendererModel(this);
		bone.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 8, 53, -1.9F, -7.8284F, -5.8284F, 5, 3, 2, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone2, 0.7854F, 0.0F, 0.0F);
		Shellone.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 9, 71, -2.9F, -18.1213F, -4.1213F, 5, 12, 2, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone3, -0.4363F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 9, 71, -2.9F, -5.0261F, -2.8191F, 5, 4, 2, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone4, -0.4363F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 9, 71, -2.9F, -3.7385F, -2.9886F, 5, 4, 2, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, -0.4363F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 9, 71, -2.9F, -6.1252F, -4.2885F, 5, 4, 2, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone7, 0.1745F, 0.0F, 0.0F);
		Shellone.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 9, 71, -1.9F, -29.4869F, 0.843F, 5, 2, 9, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone6, 0.3491F, 0.0F, 0.0F);
		Shellone.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 9, 71, -1.9F, -28.8926F, -3.0494F, 5, 2, 9, 0.0F, false));

		Shellone2 = new RendererModel(this);
		Shellone2.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(Shellone2, 0.0F, -1.0472F, 0.0F);
		Shell.addChild(Shellone2);

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone2.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 8, 53, -4.5481F, -7.8284F, -4.415F, 5, 3, 2, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone9, 0.7854F, 0.0F, 0.0F);
		Shellone2.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 9, 71, -5.5481F, -17.1219F, -3.1219F, 5, 12, 2, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone10, -0.4363F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		bone10.cubeList.add(new ModelBox(bone10, 9, 71, -5.5481F, -4.5427F, -1.4909F, 5, 4, 2, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone11, -0.4363F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		bone11.cubeList.add(new ModelBox(bone11, 9, 71, -5.5481F, -3.8617F, -1.5806F, 5, 4, 2, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone12, -0.4363F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		bone12.cubeList.add(new ModelBox(bone12, 9, 71, -5.5481F, -6.8319F, -3.0645F, 5, 4, 2, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone13, 0.1745F, 0.0F, 0.0F);
		Shellone2.addChild(bone13);
		bone13.cubeList.add(new ModelBox(bone13, 9, 71, -4.5481F, -29.2415F, 2.2349F, 5, 2, 9, 0.0F, false));

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone14, 0.3491F, 0.0F, 0.0F);
		Shellone2.addChild(bone14);
		bone14.cubeList.add(new ModelBox(bone14, 9, 71, -4.5481F, -28.4091F, -1.7213F, 5, 2, 9, 0.0F, false));

		Shellone3 = new RendererModel(this);
		Shellone3.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(Shellone3, 0.0F, -2.0944F, 0.0F);
		Shell.addChild(Shellone3);

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone3.addChild(bone15);
		bone15.cubeList.add(new ModelBox(bone15, 8, 53, -4.6481F, -7.8284F, -1.415F, 5, 3, 2, 0.0F, false));

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone16, 0.7854F, 0.0F, 0.0F);
		Shellone3.addChild(bone16);
		bone16.cubeList.add(new ModelBox(bone16, 9, 71, -5.6481F, -15.0006F, -1.0006F, 5, 12, 2, 0.0F, false));

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone17, -0.4363F, 0.0F, 0.0F);
		bone16.addChild(bone17);
		bone17.cubeList.add(new ModelBox(bone17, 9, 71, -5.6481F, -3.5166F, 1.3282F, 5, 4, 2, 0.0F, false));

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone18, -0.4363F, 0.0F, 0.0F);
		bone17.addChild(bone18);
		bone18.cubeList.add(new ModelBox(bone18, 9, 71, -5.6481F, -4.1232F, 1.408F, 5, 4, 2, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone19, -0.4363F, 0.0F, 0.0F);
		bone18.addChild(bone19);
		bone19.cubeList.add(new ModelBox(bone19, 8, 71, -5.6481F, -8.3319F, -0.4664F, 5, 4, 2, 0.0F, false));

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone20, 0.1745F, 0.0F, 0.0F);
		Shellone3.addChild(bone20);
		bone20.cubeList.add(new ModelBox(bone20, 9, 71, -4.6481F, -28.7205F, 5.1894F, 5, 2, 9, 0.0F, false));

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone21, 0.3491F, 0.0F, 0.0F);
		Shellone3.addChild(bone21);
		bone21.cubeList.add(new ModelBox(bone21, 9, 71, -4.6481F, -27.3831F, 1.0978F, 5, 2, 9, 0.0F, false));

		Shellone4 = new RendererModel(this);
		Shellone4.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(Shellone4, 0.0F, 3.1416F, 0.0F);
		Shell.addChild(Shellone4);

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone4.addChild(bone22);
		bone22.cubeList.add(new ModelBox(bone22, 8, 53, -2.1F, -7.8284F, 0.1716F, 5, 3, 2, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone23, 0.7854F, 0.0F, 0.0F);
		Shellone4.addChild(bone23);
		bone23.cubeList.add(new ModelBox(bone23, 9, 71, -3.1F, -13.8787F, 0.1213F, 5, 12, 2, 0.0F, false));

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone24, -0.4363F, 0.0F, 0.0F);
		bone23.addChild(bone24);
		bone24.cubeList.add(new ModelBox(bone24, 9, 71, -3.1F, -2.9739F, 2.8191F, 5, 4, 2, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone25, -0.4363F, 0.0F, 0.0F);
		bone24.addChild(bone25);
		bone25.cubeList.add(new ModelBox(bone25, 9, 71, -3.1F, -4.2615F, 2.9886F, 5, 4, 2, 0.0F, false));

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone26, -0.4363F, 0.0F, 0.0F);
		bone25.addChild(bone26);
		bone26.cubeList.add(new ModelBox(bone26, 9, 71, -3.1F, -9.1252F, 0.9076F, 5, 4, 2, 0.0F, false));

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone27, 0.1745F, 0.0F, 0.0F);
		Shellone4.addChild(bone27);
		bone27.cubeList.add(new ModelBox(bone27, 9, 71, -2.1F, -28.445F, 6.7519F, 5, 2, 9, 0.0F, false));

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone28, 0.3491F, 0.0F, 0.0F);
		Shellone4.addChild(bone28);
		bone28.cubeList.add(new ModelBox(bone28, 9, 71, -2.1F, -26.8404F, 2.5887F, 5, 2, 9, 0.0F, false));

		Shellone5 = new RendererModel(this);
		Shellone5.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(Shellone5, 0.0F, 1.0472F, 0.0F);
		Shell.addChild(Shellone5);

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone5.addChild(bone29);
		bone29.cubeList.add(new ModelBox(bone29, 8, 53, 0.6481F, -7.8284F, -4.2418F, 5, 3, 2, 0.0F, false));

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone30, 0.7854F, 0.0F, 0.0F);
		Shellone5.addChild(bone30);
		bone30.cubeList.add(new ModelBox(bone30, 9, 71, -0.3519F, -16.9994F, -2.9994F, 5, 12, 2, 0.0F, false));

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone31, -0.4363F, 0.0F, 0.0F);
		bone30.addChild(bone31);
		bone31.cubeList.add(new ModelBox(bone31, 8, 71, -0.3519F, -4.4834F, -1.3282F, 5, 4, 2, 0.0F, false));

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone32, -0.4363F, 0.0F, 0.0F);
		bone31.addChild(bone32);
		bone32.cubeList.add(new ModelBox(bone32, 8, 71, -0.3519F, -3.8768F, -1.408F, 5, 4, 2, 0.0F, false));

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone33, -0.4363F, 0.0F, 0.0F);
		bone32.addChild(bone33);
		bone33.cubeList.add(new ModelBox(bone33, 8, 71, -0.3519F, -6.9185F, -2.9145F, 5, 4, 2, 0.0F, false));

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone34, 0.1745F, 0.0F, 0.0F);
		Shellone5.addChild(bone34);
		bone34.cubeList.add(new ModelBox(bone34, 9, 71, 0.6481F, -29.2114F, 2.4055F, 5, 2, 9, 0.0F, false));

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone35, 0.3491F, 0.0F, 0.0F);
		Shellone5.addChild(bone35);
		bone35.cubeList.add(new ModelBox(bone35, 9, 71, 0.6481F, -28.3499F, -1.5585F, 5, 2, 9, 0.0F, false));

		Shellone6 = new RendererModel(this);
		Shellone6.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(Shellone6, 0.0F, 2.0944F, 0.0F);
		Shell.addChild(Shellone6);

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(-0.5F, 31.0F, -12.0F);
		Shellone6.addChild(bone36);
		bone36.cubeList.add(new ModelBox(bone36, 8, 53, 0.5481F, -7.8284F, -1.2418F, 5, 3, 2, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.5F, 26.0F, -10.0F);
		setRotationAngle(bone37, 0.7854F, 0.0F, 0.0F);
		Shellone6.addChild(bone37);
		bone37.cubeList.add(new ModelBox(bone37, 9, 71, -0.4519F, -14.8781F, -0.8781F, 5, 12, 2, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, -16.0F, -2.0F);
		setRotationAngle(bone38, -0.4363F, 0.0F, 0.0F);
		bone37.addChild(bone38);
		bone38.cubeList.add(new ModelBox(bone38, 9, 71, -0.4519F, -3.4573F, 1.4909F, 5, 4, 2, 0.0F, false));

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, -4.0F, 0.0F);
		setRotationAngle(bone39, -0.4363F, 0.0F, 0.0F);
		bone38.addChild(bone39);
		bone39.cubeList.add(new ModelBox(bone39, 9, 71, -0.4519F, -4.1383F, 1.5806F, 5, 4, 2, 0.0F, false));

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone40, -0.4363F, 0.0F, 0.0F);
		bone39.addChild(bone40);
		bone40.cubeList.add(new ModelBox(bone40, 9, 71, -0.4519F, -8.4185F, -0.3164F, 5, 4, 2, 0.0F, false));

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone41, 0.1745F, 0.0F, 0.0F);
		Shellone6.addChild(bone41);
		bone41.cubeList.add(new ModelBox(bone41, 9, 71, 0.5481F, -28.6905F, 5.3599F, 5, 2, 9, 0.0F, false));

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(-0.5F, 31.0F, -12.0F);
		setRotationAngle(bone42, 0.3491F, 0.0F, 0.0F);
		Shellone6.addChild(bone42);
		bone42.cubeList.add(new ModelBox(bone42, 9, 71, 0.5481F, -27.3238F, 1.2605F, 5, 2, 9, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone50, 0.0F, -0.5236F, 0.0F);
		Shell.addChild(bone50);
		bone50.cubeList.add(new ModelBox(bone50, 98, 3, -6.4003F, 7.4F, 17.9631F, 10, 5, 2, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone51, 0.0F, -0.4363F, 0.0F);
		bone50.addChild(bone51);
		bone51.cubeList.add(new ModelBox(bone51, 98, 3, -5.3638F, 7.4F, 18.7976F, 8, 5, 2, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone52, 0.0F, 0.4363F, 0.0F);
		bone50.addChild(bone52);
		bone52.cubeList.add(new ModelBox(bone52, 100, 3, -21.4879F, 7.4F, 10.0069F, 8, 5, 2, 0.0F, false));

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone53, 0.0F, -1.5708F, 0.0F);
		Shell.addChild(bone53);
		bone53.cubeList.add(new ModelBox(bone53, 98, 3, -7.9869F, 7.4F, 20.5112F, 10, 5, 2, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone54, 0.0F, -0.4363F, 0.0F);
		bone53.addChild(bone54);
		bone54.cubeList.add(new ModelBox(bone54, 98, 3, -5.7249F, 7.4F, 21.7775F, 8, 5, 2, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone55, 0.0F, 0.4363F, 0.0F);
		bone53.addChild(bone55);
		bone55.cubeList.add(new ModelBox(bone55, 100, 3, -24.0027F, 7.4F, 11.6458F, 8, 5, 2, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone56, 0.0F, -2.618F, 0.0F);
		Shell.addChild(bone56);
		bone56.cubeList.add(new ModelBox(bone56, 98, 3, -6.5735F, 7.4F, 23.1593F, 10, 5, 2, 0.0F, false));

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone57, 0.0F, -0.4363F, 0.0F);
		bone56.addChild(bone57);
		bone57.cubeList.add(new ModelBox(bone57, 98, 3, -3.3248F, 7.4F, 23.5801F, 8, 5, 2, 0.0F, false));

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone58, 0.0F, 0.4363F, 0.0F);
		bone56.addChild(bone58);
		bone58.cubeList.add(new ModelBox(bone58, 100, 3, -23.8408F, 7.4F, 14.6431F, 8, 5, 2, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone59, 0.0F, 2.618F, 0.0F);
		Shell.addChild(bone59);
		bone59.cubeList.add(new ModelBox(bone59, 98, 3, -3.5735F, 7.4F, 23.2593F, 10, 5, 2, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone60, 0.0F, -0.4363F, 0.0F);
		bone59.addChild(bone60);
		bone60.cubeList.add(new ModelBox(bone60, 98, 3, -0.5636F, 7.4F, 22.4029F, 8, 5, 2, 0.0F, false));

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone61, 0.0F, 0.4363F, 0.0F);
		bone59.addChild(bone61);
		bone61.cubeList.add(new ModelBox(bone61, 100, 3, -21.1642F, 7.4F, 16.0015F, 8, 5, 2, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone62, 0.0F, 1.5708F, 0.0F);
		Shell.addChild(bone62);
		bone62.cubeList.add(new ModelBox(bone62, 98, 3, -1.9869F, 7.4F, 20.7112F, 10, 5, 2, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone63, 0.0F, -0.4363F, 0.0F);
		bone62.addChild(bone63);
		bone63.cubeList.add(new ModelBox(bone63, 98, 3, -0.2025F, 7.4F, 19.423F, 8, 5, 2, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone64, 0.0F, 0.4363F, 0.0F);
		bone62.addChild(bone64);
		bone64.cubeList.add(new ModelBox(bone64, 100, 3, -18.6493F, 7.4F, 14.3627F, 8, 5, 2, 0.0F, false));

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone65, 0.0F, 0.5236F, 0.0F);
		Shell.addChild(bone65);
		bone65.cubeList.add(new ModelBox(bone65, 98, 3, -3.4003F, 7.4F, 18.0631F, 10, 5, 2, 0.0F, false));

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone66, 0.0F, -0.4363F, 0.0F);
		bone65.addChild(bone66);
		bone66.cubeList.add(new ModelBox(bone66, 98, 3, -2.6026F, 7.4F, 17.6204F, 8, 5, 2, 0.0F, false));

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone67, 0.0F, 0.4363F, 0.0F);
		bone65.addChild(bone67);
		bone67.cubeList.add(new ModelBox(bone67, 100, 2, -18.8112F, 7.4F, 11.3654F, 8, 5, 2, 0.0F, false));

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(2.2F, -23.2F, 16.0F);
		setRotationAngle(bone68, 0.0F, -1.5708F, 0.0F);
		Shell.addChild(bone68);
		bone68.cubeList.add(new ModelBox(bone68, 4, 15, -7.9869F, 7.4F, 10.5112F, 10, 3, 12, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone69, 0.0F, -0.4363F, 0.0F);
		bone68.addChild(bone69);
		bone69.cubeList.add(new ModelBox(bone69, 4, 15, -5.7249F, 7.4F, 11.7775F, 8, 3, 12, 0.0F, false));

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone70, 0.0F, 0.4363F, 0.0F);
		bone68.addChild(bone70);
		bone70.cubeList.add(new ModelBox(bone70, 4, 15, -24.0027F, 7.4F, 1.6458F, 8, 3, 12, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(2.2F, -23.2F, 17.0F);
		setRotationAngle(bone71, 0.0F, -2.618F, 0.0F);
		Shell.addChild(bone71);
		bone71.cubeList.add(new ModelBox(bone71, 6, 14, -6.5735F, 7.4F, 15.1593F, 10, 3, 10, 0.0F, false));

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone72, 0.0F, -0.4363F, 0.0F);
		bone71.addChild(bone72);
		bone72.cubeList.add(new ModelBox(bone72, 6, 14, -3.3248F, 7.4F, 15.5801F, 8, 3, 10, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone73, 0.0F, 0.4363F, 0.0F);
		bone71.addChild(bone73);
		bone73.cubeList.add(new ModelBox(bone73, 6, 14, -23.8408F, 7.4F, 6.6431F, 8, 3, 10, 0.0F, false));

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.2F, -23.2F, 17.0F);
		setRotationAngle(bone74, 0.0F, 2.618F, 0.0F);
		Shell.addChild(bone74);
		bone74.cubeList.add(new ModelBox(bone74, 5, 18, -3.5735F, 7.4F, 12.2593F, 10, 3, 13, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone75, 0.0F, -0.4363F, 0.0F);
		bone74.addChild(bone75);
		bone75.cubeList.add(new ModelBox(bone75, 5, 18, -0.5636F, 7.4F, 11.4029F, 8, 3, 13, 0.0F, false));

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone76, 0.0F, 0.4363F, 0.0F);
		bone74.addChild(bone76);
		bone76.cubeList.add(new ModelBox(bone76, 5, 18, -21.1642F, 7.4F, 5.0015F, 8, 3, 13, 0.0F, false));

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(0.2F, -23.2F, 16.0F);
		setRotationAngle(bone77, 0.0F, 1.5708F, 0.0F);
		Shell.addChild(bone77);
		bone77.cubeList.add(new ModelBox(bone77, 10, 16, -1.9869F, 7.4F, 9.7112F, 10, 3, 13, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone78, 0.0F, -0.4363F, 0.0F);
		bone77.addChild(bone78);
		bone78.cubeList.add(new ModelBox(bone78, 10, 16, -0.2025F, 7.4F, 8.423F, 8, 3, 13, 0.0F, false));

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone79, 0.0F, 0.4363F, 0.0F);
		bone77.addChild(bone79);
		bone79.cubeList.add(new ModelBox(bone79, 10, 16, -18.6493F, 7.4F, 3.3627F, 8, 3, 13, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(0.2F, -23.2F, 15.0F);
		setRotationAngle(bone80, 0.0F, 0.5236F, 0.0F);
		Shell.addChild(bone80);
		bone80.cubeList.add(new ModelBox(bone80, 6, 19, -3.4003F, 7.4F, 9.0631F, 10, 3, 11, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone81, 0.0F, -0.4363F, 0.0F);
		bone80.addChild(bone81);
		bone81.cubeList.add(new ModelBox(bone81, 6, 19, -2.6026F, 7.4F, 8.6204F, 8, 3, 11, 0.0F, false));

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone82, 0.0F, 0.4363F, 0.0F);
		bone80.addChild(bone82);
		bone82.cubeList.add(new ModelBox(bone82, 6, 19, -18.8112F, 7.4F, 2.3654F, 8, 3, 11, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(2.2F, -23.2F, 15.0F);
		setRotationAngle(bone83, 0.0F, -0.5236F, 0.0F);
		Shell.addChild(bone83);
		bone83.cubeList.add(new ModelBox(bone83, 7, 11, -6.4003F, 7.4F, 8.9631F, 10, 3, 11, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone84, 0.0F, -0.4363F, 0.0F);
		bone83.addChild(bone84);
		bone84.cubeList.add(new ModelBox(bone84, 7, 11, -5.3638F, 7.4F, 9.7976F, 8, 3, 11, 0.0F, false));

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(18.0F, 0.0F, 0.0F);
		setRotationAngle(bone85, 0.0F, 0.4363F, 0.0F);
		bone83.addChild(bone85);
		bone85.cubeList.add(new ModelBox(bone85, 7, 11, -21.4879F, 7.4F, 1.0069F, 8, 3, 11, 0.0F, false));

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(1.2F, -28.2F, 16.0F);
		Shell.addChild(bone43);
		bone43.cubeList.add(new ModelBox(bone43, 0, 8, -8.9F, 8.0F, -13.0F, 18, 13, 20, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 0, 11, -10.9F, 10.0F, -15.0F, 22, 7, 24, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 0, 18, -8.9F, 23.0F, -13.0F, 18, 2, 20, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 0, 0, -7.9F, 19.0F, -12.0F, 16, 5, 18, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 0, 15, -9.9F, 19.0F, -14.0F, 20, 2, 22, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(1.2F, -28.2F, 16.0F);
		Shell.addChild(bone86);
		bone86.cubeList.add(new ModelBox(bone86, 0, 0, -2.4F, -5.0F, 0.4F, 5, 7, 1, 0.0F, false));

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone87, 0.0F, -1.0472F, 0.0F);
		Shell.addChild(bone87);
		bone87.cubeList.add(new ModelBox(bone87, 0, 0, -4.9876F, -5.0F, 1.7785F, 5, 7, 1, 0.0F, false));

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone88, 0.0F, -2.0944F, 0.0F);
		Shell.addChild(bone88);
		bone88.cubeList.add(new ModelBox(bone88, 0, 0, -5.0876F, -5.0F, 4.7086F, 5, 7, 1, 0.0F, false));

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone89, 0.0F, 3.1416F, 0.0F);
		Shell.addChild(bone89);
		bone89.cubeList.add(new ModelBox(bone89, 0, 0, -2.6F, -5.0F, 6.2603F, 5, 7, 1, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone90, 0.0F, 1.0472F, 0.0F);
		Shell.addChild(bone90);
		bone90.cubeList.add(new ModelBox(bone90, 0, 0, 0.0876F, -5.0F, 1.9517F, 5, 7, 1, 0.0F, false));

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone91, 0.0F, 2.0944F, 0.0F);
		Shell.addChild(bone91);
		bone91.cubeList.add(new ModelBox(bone91, 0, 0, -0.0124F, -5.0F, 4.8818F, 5, 7, 1, 0.0F, false));

		bone93 = new RendererModel(this);
		bone93.setRotationPoint(1.2F, -37.2F, 16.0F);
		Shell.addChild(bone93);
		bone93.cubeList.add(new ModelBox(bone93, 88, 20, -4.0605F, 5.6F, 2.9952F, 8, 1, 1, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 97, 24, -4.0605F, -42.4F, 2.9952F, 8, 6, 1, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 88, 20, -4.0605F, 3.0F, 2.9952F, 8, 1, 1, 0.0F, false));

		bone94 = new RendererModel(this);
		bone94.setRotationPoint(1.2F, -37.2F, 16.0F);
		setRotationAngle(bone94, 0.0F, -1.0472F, 0.0F);
		Shell.addChild(bone94);
		bone94.cubeList.add(new ModelBox(bone94, 88, 20, -6.5703F, 5.6F, 4.5141F, 8, 1, 1, 0.0F, false));
		bone94.cubeList.add(new ModelBox(bone94, 101, 25, -6.5703F, -42.4F, 4.5141F, 8, 6, 1, 0.0F, true));
		bone94.cubeList.add(new ModelBox(bone94, 88, 20, -6.5703F, 3.0F, 4.5141F, 8, 1, 1, 0.0F, false));

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(1.2F, -37.2F, 16.0F);
		setRotationAngle(bone95, 0.0F, -2.0944F, 0.0F);
		Shell.addChild(bone95);
		bone95.cubeList.add(new ModelBox(bone95, 88, 20, -6.5098F, 5.6F, -4.5529F, 8, 1, 13, 0.0F, false));
		bone95.cubeList.add(new ModelBox(bone95, 90, 16, -6.5098F, -42.4F, -4.5529F, 8, 6, 13, 0.0F, true));
		bone95.cubeList.add(new ModelBox(bone95, 88, 20, -6.5098F, 3.0F, -4.5529F, 8, 1, 13, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(1.2F, -37.2F, 16.0F);
		setRotationAngle(bone96, 0.0F, 3.1416F, 0.0F);
		Shell.addChild(bone96);
		bone96.cubeList.add(new ModelBox(bone96, 88, 20, -3.9395F, 5.6F, -3.1388F, 8, 1, 13, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 88, 16, -3.9395F, -42.4F, -3.1388F, 8, 6, 13, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 88, 20, -3.9395F, 3.0F, -3.1388F, 8, 1, 13, 0.0F, false));

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(1.2F, -37.2F, 16.0F);
		setRotationAngle(bone97, 0.0F, 1.0472F, 0.0F);
		Shell.addChild(bone97);
		bone97.cubeList.add(new ModelBox(bone97, 88, 20, -1.4902F, 5.6F, 4.4093F, 8, 1, 1, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 93, 23, -1.4902F, -42.4F, 4.4093F, 8, 6, 1, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 88, 20, -1.4902F, 3.0F, 4.4093F, 8, 1, 1, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(1.2F, -37.2F, 16.0F);
		setRotationAngle(bone98, 0.0F, 2.0944F, 0.0F);
		Shell.addChild(bone98);
		bone98.cubeList.add(new ModelBox(bone98, 88, 20, -1.4297F, 5.6F, -4.6577F, 8, 1, 13, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 88, 16, -1.4297F, -42.4F, -4.6577F, 8, 6, 13, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 88, 20, -1.4297F, 3.0F, -4.6577F, 8, 1, 13, 0.0F, false));

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(9.7F, -19.0F, -12.2F);
		setRotationAngle(bone105, 0.4363F, -0.5236F, 0.0F);
		all.addChild(bone105);
		bone105.cubeList.add(new ModelBox(bone105, 10, 110, -4.683F, -1.9082F, -3.7111F, 4, 2, 4, 0.0F, false));
		bone105.cubeList.add(new ModelBox(bone105, 67, 185, -4.1768F, -2.22F, -3.143F, 3, 2, 3, 0.0F, false));

		glow = new RendererModel(this);
		glow.setRotationPoint(9.5F, 5.0F, -12.2F);

		keyboard_behind_monitor_glow = new RendererModel(this);
		keyboard_behind_monitor_glow.setRotationPoint(-8.8F, -7.6F, 12.7F);
		setRotationAngle(keyboard_behind_monitor_glow, -0.6109F, 0.9599F, -0.6981F);
		glow.addChild(keyboard_behind_monitor_glow);
		keyboard_behind_monitor_glow.cubeList.add(new ModelBox(keyboard_behind_monitor_glow, 185, 145, -14.1986F, -0.1964F, 2.1787F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor_glow.cubeList.add(new ModelBox(keyboard_behind_monitor_glow, 182, 124, -14.1986F, -0.1964F, 0.1787F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor_glow.cubeList.add(new ModelBox(keyboard_behind_monitor_glow, 185, 145, -14.1986F, -0.1964F, -1.8213F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor_glow.cubeList.add(new ModelBox(keyboard_behind_monitor_glow, 185, 145, -14.1986F, -0.1964F, -3.8213F, 1, 1, 1, 0.0F, false));
		keyboard_behind_monitor_glow.cubeList.add(new ModelBox(keyboard_behind_monitor_glow, 185, 145, -14.1986F, -0.1964F, -5.8213F, 1, 1, 1, 0.0F, false));

		keyboard_by_monitor_glow = new RendererModel(this);
		keyboard_by_monitor_glow.setRotationPoint(-11.9F, -6.3F, 18.9F);
		setRotationAngle(keyboard_by_monitor_glow, -0.6109F, 0.9425F, -0.6981F);
		glow.addChild(keyboard_by_monitor_glow);
		keyboard_by_monitor_glow.cubeList.add(new ModelBox(keyboard_by_monitor_glow, 186, 142, -12.5034F, 0.4849F, 1.1071F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor_glow.cubeList.add(new ModelBox(keyboard_by_monitor_glow, 186, 142, -12.5034F, 0.4849F, -1.3929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor_glow.cubeList.add(new ModelBox(keyboard_by_monitor_glow, 186, 142, -12.5034F, 0.4849F, -3.7929F, 1, 1, 1, 0.0F, false));
		keyboard_by_monitor_glow.cubeList.add(new ModelBox(keyboard_by_monitor_glow, 186, 142, -12.5034F, 0.4849F, -6.2929F, 1, 1, 1, 0.0F, false));

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(-4.5F, -12.2F, 18.2F);
		setRotationAngle(bone44, 0.0873F, -0.5236F, -0.1745F);
		glow.addChild(bone44);
		bone44.cubeList.add(new ModelBox(bone44, 188, 140, -18.4147F, 2.7866F, -4.6388F, 1, 1, 1, 0.0F, false));

		refuel2 = new RendererModel(this);
		refuel2.setRotationPoint(-6.0F, -5.3F, 16.6F);
		setRotationAngle(refuel2, 0.384F, -1.0472F, -0.4363F);
		glow.addChild(refuel2);
		refuel2.cubeList.add(new ModelBox(refuel2, 107, 17, -16.0486F, -2.4317F, 0.507F, 2, 1, 2, 0.0F, false));

		doorcontrol2 = new RendererModel(this);
		doorcontrol2.setRotationPoint(-11.9F, -3.3F, 12.2F);
		setRotationAngle(doorcontrol2, 0.384F, -1.0472F, -0.4363F);
		glow.addChild(doorcontrol2);
		doorcontrol2.cubeList.add(new ModelBox(doorcontrol2, 100, 9, -17.8461F, -1.6152F, 4.2918F, 1, 1, 1, 0.0F, false));

		land_type_red = new RendererModel(this);
		land_type_red.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(land_type_red, 0.4363F, -0.5236F, 0.0F);
		glow.addChild(land_type_red);
		land_type_red.cubeList.add(new ModelBox(land_type_red, 182, 142, -3.734F, -3.1158F, -2.6444F, 2, 2, 2, 0.0F, false));

		monitor_screen = new RendererModel(this);
		monitor_screen.setRotationPoint(-9.8F, -9.0F, 12.6F);
		setRotationAngle(monitor_screen, 0.0F, -0.5236F, 0.0F);
		glow.addChild(monitor_screen);
		monitor_screen.cubeList.add(new ModelBox(monitor_screen, 71, 142, -3.6261F, -3.0F, 12.9797F, 7, 6, 1, 0.0F, false));

		throttle_glow = new RendererModel(this);
		throttle_glow.setRotationPoint(-7.8F, -10.0F, 12.6F);
		setRotationAngle(throttle_glow, -0.2618F, 0.0F, 0.0F);
		glow.addChild(throttle_glow);
		throttle_glow.cubeList.add(new ModelBox(throttle_glow, 186, 142, -1.1F, -1.3235F, 11.6022F, 1, 1, 1, -0.1F, false));
		throttle_glow.cubeList.add(new ModelBox(throttle_glow, 195, 117, -1.1F, -1.3235F, 13.6022F, 1, 1, 1, -0.1F, false));
		throttle_glow.cubeList.add(new ModelBox(throttle_glow, 190, 99, -1.1F, -0.9235F, 15.6022F, 1, 1, 1, -0.1F, false));

		shell_glow = new RendererModel(this);
		shell_glow.setRotationPoint(-10.7F, 21.0F, -0.8F);
		glow.addChild(shell_glow);

		bone150 = new RendererModel(this);
		bone150.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone150, 0.4363F, -0.5236F, 0.0F);
		shell_glow.addChild(bone150);
		bone150.cubeList.add(new ModelBox(bone150, 96, 75, -5.0134F, -2.1191F, -9.8F, 7, 1, 2, 0.0F, false));
		bone150.cubeList.add(new ModelBox(bone150, 96, 75, -7.0134F, -2.1191F, -11.8F, 11, 1, 2, 0.0F, false));
		bone150.cubeList.add(new ModelBox(bone150, 80, 75, -9.0134F, -2.1191F, -17.8F, 15, 1, 6, 0.0F, false));
		bone150.cubeList.add(new ModelBox(bone150, 72, 85, -11.0134F, -2.1191F, -24.8F, 19, 1, 7, 0.0F, false));

		bone151 = new RendererModel(this);
		bone151.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone151, 0.4363F, -1.5708F, 0.0F);
		shell_glow.addChild(bone151);
		bone151.cubeList.add(new ModelBox(bone151, 93, 79, -6.6F, -1.0423F, -7.4906F, 7, 1, 2, 0.0F, false));
		bone151.cubeList.add(new ModelBox(bone151, 93, 79, -8.6F, -1.0423F, -9.4906F, 11, 1, 2, 0.0F, false));
		bone151.cubeList.add(new ModelBox(bone151, 80, 75, -10.6F, -1.0423F, -15.4906F, 15, 1, 6, 0.0F, false));
		bone151.cubeList.add(new ModelBox(bone151, 72, 85, -12.6F, -1.0423F, -22.4906F, 19, 1, 7, 0.0F, false));

		bone152 = new RendererModel(this);
		bone152.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone152, 0.4363F, -2.618F, 0.0F);
		shell_glow.addChild(bone152);
		bone152.cubeList.add(new ModelBox(bone152, 86, 77, -5.1866F, 0.0769F, -5.0907F, 7, 1, 2, 0.0F, false));
		bone152.cubeList.add(new ModelBox(bone152, 86, 77, -7.1866F, 0.0769F, -7.0907F, 11, 1, 2, 0.0F, false));
		bone152.cubeList.add(new ModelBox(bone152, 80, 75, -9.1866F, 0.0769F, -13.0907F, 15, 1, 6, 0.0F, false));
		bone152.cubeList.add(new ModelBox(bone152, 72, 85, -11.1866F, 0.0769F, -20.0907F, 19, 1, 7, 0.0F, false));

		bone153 = new RendererModel(this);
		bone153.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone153, 0.4363F, 2.618F, 0.0F);
		shell_glow.addChild(bone153);
		bone153.cubeList.add(new ModelBox(bone153, 78, 75, -2.1866F, 0.1191F, -5.0F, 7, 1, 2, 0.0F, false));
		bone153.cubeList.add(new ModelBox(bone153, 78, 75, -4.1866F, 0.1191F, -7.0F, 11, 1, 2, 0.0F, false));
		bone153.cubeList.add(new ModelBox(bone153, 80, 75, -6.1866F, 0.1191F, -13.0F, 15, 1, 6, 0.0F, false));
		bone153.cubeList.add(new ModelBox(bone153, 72, 85, -8.1866F, 0.1191F, -20.0F, 19, 1, 7, 0.0F, false));

		bone154 = new RendererModel(this);
		bone154.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone154, 0.4363F, 1.5708F, 0.0F);
		shell_glow.addChild(bone154);
		bone154.cubeList.add(new ModelBox(bone154, 79, 77, -0.6F, -0.9577F, -7.3094F, 7, 1, 2, 0.0F, false));
		bone154.cubeList.add(new ModelBox(bone154, 79, 77, -2.6F, -0.9577F, -9.3094F, 11, 1, 2, 0.0F, false));
		bone154.cubeList.add(new ModelBox(bone154, 80, 75, -4.6F, -0.9577F, -15.3094F, 15, 1, 6, 0.0F, false));
		bone154.cubeList.add(new ModelBox(bone154, 72, 85, -6.6F, -0.9577F, -22.3094F, 19, 1, 7, 0.0F, false));

		bone155 = new RendererModel(this);
		bone155.setRotationPoint(1.2F, -28.2F, 16.0F);
		setRotationAngle(bone155, 0.4363F, 0.5236F, 0.0F);
		shell_glow.addChild(bone155);
		bone155.cubeList.add(new ModelBox(bone155, 74, 63, -2.0134F, -2.0769F, -9.7093F, 7, 1, 2, 0.0F, false));
		bone155.cubeList.add(new ModelBox(bone155, 74, 63, -4.0134F, -2.0769F, -11.7093F, 11, 1, 2, 0.0F, false));
		bone155.cubeList.add(new ModelBox(bone155, 80, 75, -6.0134F, -2.0769F, -17.7093F, 15, 1, 6, 0.0F, false));
		bone155.cubeList.add(new ModelBox(bone155, 72, 85, -8.0134F, -2.0769F, -24.7093F, 19, 1, 7, 0.0F, false));

		rotor = new RendererModel(this);
		rotor.setRotationPoint(-9.7F, 19.0F, 12.2F);
		glow.addChild(rotor);

		rotartop = new RendererModel(this);
		rotartop.setRotationPoint(0.5F, -68.2F, 0.0F);
		rotor.addChild(rotartop);
		rotartop.cubeList.add(new ModelBox(rotartop, 164, 32, -3.9F, 2.0F, -3.5F, 7, 2, 7, 0.0F, false));
		rotartop.cubeList.add(new ModelBox(rotartop, 164, 32, -3.9F, -4.0F, -3.5F, 7, 2, 7, 0.0F, false));
		rotartop.cubeList.add(new ModelBox(rotartop, 169, 36, 0.2F, -8.0F, -3.2F, 2, 20, 2, 0.0F, false));
		rotartop.cubeList.add(new ModelBox(rotartop, 169, 36, 0.2F, -8.0F, 1.0F, 2, 20, 2, 0.0F, false));
		rotartop.cubeList.add(new ModelBox(rotartop, 169, 36, -3.1F, -8.0F, -3.2F, 2, 20, 2, 0.0F, false));
		rotartop.cubeList.add(new ModelBox(rotartop, 169, 36, -3.1F, -8.0F, 1.0F, 2, 20, 2, 0.0F, false));

		rotarbottom = new RendererModel(this);
		rotarbottom.setRotationPoint(0.5F, -47.2F, 0.0F);
		rotor.addChild(rotarbottom);
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 164, 32, -3.9F, 14.0F, -3.5F, 7, 2, 7, 0.0F, false));
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 164, 32, -3.9F, 8.0F, -3.5F, 7, 2, 7, 0.0F, false));
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 182, 14, 0.2F, 0.0F, -3.2F, 2, 20, 2, 0.0F, false));
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 182, 14, 0.2F, 0.0F, 1.0F, 2, 20, 2, 0.0F, false));
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 182, 14, -3.1F, 0.0F, -3.2F, 2, 20, 2, 0.0F, false));
		rotarbottom.cubeList.add(new ModelBox(rotarbottom, 182, 14, -3.1F, 0.0F, 1.0F, 2, 20, 2, 0.0F, false));

		phone = new RendererModel(this);
		phone.setRotationPoint(-3.8F, -11.0F, 11.1F);
		setRotationAngle(phone, 0.0F, 1.5708F, 0.3491F);
		glow.addChild(phone);
		phone.cubeList.add(new ModelBox(phone, 112, 20, -1.6F, 4.5314F, 11.3125F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 186, 142, -8.0F, 5.6327F, 13.8417F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 186, 142, -4.5F, 4.9597F, 7.2117F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 188, 97, -3.5F, 4.9597F, 7.2117F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 96, 18, -8.0F, 5.6666F, 14.9182F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 187, 121, -3.5F, 4.9936F, 8.2882F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 110, 17, 0.2F, 4.5314F, 11.3125F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 184, 95, -4.2F, 5.1629F, 13.6707F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 110, 17, -0.7F, 4.5314F, 12.3125F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 179, 123, -5.1F, 5.1629F, 14.6707F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 110, 17, -1.6F, 4.5314F, 13.3125F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 187, 140, -6.0F, 5.1629F, 15.6707F, 1, 1, 1, -0.2F, false));
		phone.cubeList.add(new ModelBox(phone, 110, 17, 0.2F, 4.5314F, 13.3125F, 1, 1, 1, -0.2F, false));

		land_type_glow = new RendererModel(this);
		land_type_glow.setRotationPoint(4.0F, 0.0F, 2.0F);
		setRotationAngle(land_type_glow, 0.4363F, -0.5236F, 0.0F);
		glow.addChild(land_type_glow);
		land_type_glow.cubeList.add(new ModelBox(land_type_glow, 104, 13, -5.0821F, -1.5669F, 1.6773F, 2, 1, 1, 0.0F, false));
		land_type_glow.cubeList.add(new ModelBox(land_type_glow, 192, 130, -2.367F, -1.2394F, 1.1965F, 1, 1, 1, 0.0F, false));
		land_type_glow.cubeList.add(new ModelBox(land_type_glow, 187, 116, -1.535F, -1.5051F, -1.503F, 1, 1, 1, 0.0F, false));
		land_type_glow.cubeList.add(new ModelBox(land_type_glow, 94, 22, -1.6421F, -1.4441F, -3.5017F, 2, 1, 1, 0.0F, false));

		telepathic_circuit_glow = new RendererModel(this);
		telepathic_circuit_glow.setRotationPoint(-5.8F, -1.4F, 16.5F);
		setRotationAngle(telepathic_circuit_glow, 0.0F, -1.5708F, 0.0F);
		glow.addChild(telepathic_circuit_glow);

		crystal2 = new RendererModel(this);
		crystal2.setRotationPoint(-4.0F, 0.5518F, 20.9068F);
		setRotationAngle(crystal2, -0.4363F, 0.0F, 0.0F);
		telepathic_circuit_glow.addChild(crystal2);
		crystal2.cubeList.add(new ModelBox(crystal2, 109, 109, -0.4F, -2.2253F, -0.5453F, 1, 3, 1, 0.0F, false));
		crystal2.cubeList.add(new ModelBox(crystal2, 109, 109, -0.4F, -2.2253F, -0.1453F, 1, 3, 1, -0.2F, false));
		crystal2.cubeList.add(new ModelBox(crystal2, 109, 109, -0.1F, -1.7253F, -0.3453F, 1, 3, 1, -0.2F, false));
		crystal2.cubeList.add(new ModelBox(crystal2, 109, 109, -0.7F, -1.7253F, -0.7453F, 1, 3, 1, -0.2F, false));
		crystal2.cubeList.add(new ModelBox(crystal2, 109, 109, -0.4F, -3.0253F, -0.4453F, 1, 3, 1, -0.2F, false));

		lever7 = new RendererModel(this);
		lever7.setRotationPoint(1.0F, -1.4482F, 14.9068F);
		setRotationAngle(lever7, -0.4363F, 0.0F, 0.0F);
		telepathic_circuit_glow.addChild(lever7);
		lever7.cubeList.add(new ModelBox(lever7, 8, 146, -2.0F, -1.0095F, 2.0026F, 1, 1, 4, 0.0F, false));
		lever7.cubeList.add(new ModelBox(lever7, 188, 119, -8.2F, -0.6892F, 5.2622F, 1, 1, 1, -0.1F, false));
		lever7.cubeList.add(new ModelBox(lever7, 152, 141, -8.2F, -0.5498F, 7.0927F, 1, 1, 1, -0.1F, false));
		lever7.cubeList.add(new ModelBox(lever7, 152, 141, -10.0F, -0.6892F, 5.2622F, 1, 1, 1, -0.1F, false));
		lever7.cubeList.add(new ModelBox(lever7, 183, 91, -11.9F, -0.6892F, 5.2622F, 1, 1, 1, -0.1F, false));
		lever7.cubeList.add(new ModelBox(lever7, 153, 184, -10.0F, -0.5498F, 7.0927F, 1, 1, 1, -0.1F, false));
		lever7.cubeList.add(new ModelBox(lever7, 153, 184, -11.9F, -0.5498F, 7.0927F, 1, 1, 1, -0.1F, false));

		nope2 = new RendererModel(this);
		nope2.setRotationPoint(-4.0124F, -1.8123F, 17.5686F);
		setRotationAngle(nope2, 0.4363F, 3.1416F, 0.0F);
		telepathic_circuit_glow.addChild(nope2);
		nope2.cubeList.add(new ModelBox(nope2, 106, 112, -1.5F, -1.5577F, -1.4094F, 3, 3, 3, -0.4F, false));

		block2 = new RendererModel(this);
		block2.setRotationPoint(-2.8F, -8.5F, 13.6F);
		setRotationAngle(block2, -0.0524F, -1.0472F, -0.0873F);
		glow.addChild(block2);

		lever9 = new RendererModel(this);
		lever9.setRotationPoint(-2.9869F, -0.7455F, 16.8686F);
		setRotationAngle(lever9, 0.0F, 0.0F, 0.0349F);
		block2.addChild(lever9);
		lever9.cubeList.add(new ModelBox(lever9, 7, 146, -1.7935F, -0.3712F, -4.4235F, 2, 2, 6, 0.0F, false));
		lever9.cubeList.add(new ModelBox(lever9, 7, 146, -2.7968F, 0.7509F, -1.0227F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 7, 146, -0.4887F, 2.2419F, 6.3644F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 7, 146, -2.5976F, 2.2172F, 6.4084F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 174, 140, -0.4887F, 2.2419F, 7.3644F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 185, 101, -3.7968F, 0.7509F, -0.0227F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 185, 101, -1.4887F, 2.2419F, 7.3644F, 1, 1, 1, -0.2F, false));
		lever9.cubeList.add(new ModelBox(lever9, 187, 119, -3.5976F, 2.2172F, 7.4084F, 1, 1, 1, -0.2F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(-8.3F, -13.7F, 0.0F);
		setRotationAngle(bone45, -0.2618F, 0.0F, 0.0F);
		glow.addChild(bone45);
		bone45.cubeList.add(new ModelBox(bone45, 189, 141, -3.2F, 0.8145F, 21.2285F, 1, 2, 1, -0.2F, false));
	}
	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void render(ConsoleTile console, float scale) {
		HandbrakeControl hand = console.getControl(HandbrakeControl.class);
        if(hand != null) {
        	this.lever4.rotateAngleX = 0;
        	this.lever4.rotateAngleZ = 0;
            this.lever4.rotateAngleX = (float)Math.toRadians(hand.isFree() ? 45 : -45);
            this.lever5.rotateAngleX = (float)Math.toRadians(hand.isFree() ? 45 : -45);
        }
        //Rotor animations
        float pixel = 0.06125F * 2.5F; 
        this.rotartop.offsetY = console.isInFlight() ? -(float) Math.cos(console.flightTicks * 0.1) * 0.15F  + pixel : 0.0F;
        this.rotarbottom.offsetY = console.isInFlight() ? (float) Math.cos(console.flightTicks * 0.1) * 0.15F  - pixel : 0.0F;

        IncModControl inc = console.getControl(IncModControl.class);
        if(inc != null) {
            lever2.rotateAngleZ = (float)Math.toRadians(inc.getAnimationTicks() * 30);
        }

        //Throttle
        ThrottleControl throttle = console.getControl(ThrottleControl.class);
        if (throttle != null) {
        	this.lever3.rotateAngleX = 0;
            this.lever3.rotateAngleX = (float) Math.toRadians(100 - (float) 75 * throttle.getAmount() - 75);
        }

        RandomiserControl randomizer = console.getControl(RandomiserControl.class);
        if(randomizer != null)
            this.randomiser.rotateAngleY = (float)Math.toRadians((randomizer.getAnimationTicks() * 36) + Minecraft.getInstance().getRenderPartialTicks() % 360.0);

        LandingTypeControl land_type = console.getControl(LandingTypeControl.class);
        if (land_type != null) {
        	this.land_type.offsetY = land_type.getLandType() == EnumLandType.DOWN ? 0F : -0.05F;
        }
        this.all.render(scale);
        ModelHelper.renderPartBrightness(1F, glow);
        
        GlStateManager.pushMatrix();
		GlStateManager.translated(0.225, 0.5, 1.8);
		GlStateManager.rotated(-15, 1, 0, 0);
		double sonic_scale = 0.75;
		GlStateManager.scaled(sonic_scale, sonic_scale, sonic_scale);
		Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
	}
}