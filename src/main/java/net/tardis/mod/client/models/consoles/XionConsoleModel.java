package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.Direction;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;
@SuppressWarnings("deprecation")
public class XionConsoleModel extends Model implements IConsoleModel {
	private final RendererModel glow;
	private final RendererModel rotor;
	private final RendererModel rotorbase;
	private final RendererModel rotor_spikes1;
	private final RendererModel rotor_spikes2;
	private final RendererModel rotor_spikes3;
	private final RendererModel glow_glass1;
	private final RendererModel panel2;
	private final RendererModel plain3;
	private final RendererModel bevel3;
	private final RendererModel front3;
	private final RendererModel underlip3;
	private final RendererModel undercurve3;
	private final RendererModel backbend3;
	private final RendererModel shin3;
	private final RendererModel ankle3;
	private final RendererModel foot3;
	private final RendererModel spine2;
	private final RendererModel rib2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel plain4;
	private final RendererModel bevel4;
	private final RendererModel front4;
	private final RendererModel underlip4;
	private final RendererModel undercurve4;
	private final RendererModel backbend4;
	private final RendererModel shin4;
	private final RendererModel ankle4;
	private final RendererModel foot4;
	private final RendererModel glow_glass2;
	private final RendererModel panel3;
	private final RendererModel plain2;
	private final RendererModel bevel2;
	private final RendererModel front5;
	private final RendererModel underlip2;
	private final RendererModel undercurve5;
	private final RendererModel backbend2;
	private final RendererModel shin2;
	private final RendererModel ankle2;
	private final RendererModel foot2;
	private final RendererModel spine3;
	private final RendererModel rib3;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel plain5;
	private final RendererModel bevel5;
	private final RendererModel front6;
	private final RendererModel underlip5;
	private final RendererModel undercurve6;
	private final RendererModel backbend5;
	private final RendererModel shin5;
	private final RendererModel ankle5;
	private final RendererModel foot5;
	private final RendererModel glow_glass3;
	private final RendererModel panel4;
	private final RendererModel plain6;
	private final RendererModel bevel6;
	private final RendererModel front7;
	private final RendererModel underlip6;
	private final RendererModel undercurve7;
	private final RendererModel backbend6;
	private final RendererModel shin6;
	private final RendererModel ankle6;
	private final RendererModel foot6;
	private final RendererModel spine4;
	private final RendererModel rib4;
	private final RendererModel bone7;
	private final RendererModel bone8;
	private final RendererModel plain7;
	private final RendererModel bevel7;
	private final RendererModel front8;
	private final RendererModel underlip7;
	private final RendererModel undercurve8;
	private final RendererModel backbend7;
	private final RendererModel shin7;
	private final RendererModel ankle7;
	private final RendererModel foot7;
	private final RendererModel glow_glass4;
	private final RendererModel panel5;
	private final RendererModel plain8;
	private final RendererModel bevel8;
	private final RendererModel front9;
	private final RendererModel underlip8;
	private final RendererModel undercurve9;
	private final RendererModel backbend8;
	private final RendererModel shin8;
	private final RendererModel ankle8;
	private final RendererModel foot8;
	private final RendererModel spine5;
	private final RendererModel rib5;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel plain9;
	private final RendererModel bevel9;
	private final RendererModel front10;
	private final RendererModel underlip9;
	private final RendererModel undercurve10;
	private final RendererModel backbend9;
	private final RendererModel shin9;
	private final RendererModel ankle9;
	private final RendererModel foot9;
	private final RendererModel glow_glass5;
	private final RendererModel panel6;
	private final RendererModel plain10;
	private final RendererModel bevel10;
	private final RendererModel front11;
	private final RendererModel underlip10;
	private final RendererModel undercurve11;
	private final RendererModel backbend10;
	private final RendererModel shin10;
	private final RendererModel ankle10;
	private final RendererModel foot10;
	private final RendererModel spine6;
	private final RendererModel rib6;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel plain11;
	private final RendererModel bevel11;
	private final RendererModel front12;
	private final RendererModel underlip11;
	private final RendererModel undercurve12;
	private final RendererModel backbend11;
	private final RendererModel shin11;
	private final RendererModel ankle11;
	private final RendererModel foot11;
	private final RendererModel glow_glass6;
	private final RendererModel panel7;
	private final RendererModel plain12;
	private final RendererModel bevel12;
	private final RendererModel front13;
	private final RendererModel underlip12;
	private final RendererModel undercurve13;
	private final RendererModel backbend12;
	private final RendererModel shin12;
	private final RendererModel ankle12;
	private final RendererModel foot12;
	private final RendererModel spine7;
	private final RendererModel rib7;
	private final RendererModel bone20;
	private final RendererModel bone21;
	private final RendererModel plain13;
	private final RendererModel bevel13;
	private final RendererModel front14;
	private final RendererModel underlip13;
	private final RendererModel undercurve14;
	private final RendererModel backbend13;
	private final RendererModel shin13;
	private final RendererModel ankle13;
	private final RendererModel foot13;
	private final RendererModel glow_guts;
	private final RendererModel stations;
	private final RendererModel coralribs1;
	private final RendererModel panel0;
	private final RendererModel front2;
	private final RendererModel undercurve2;
	private final RendererModel spine0;
	private final RendererModel rib0;
	private final RendererModel bone;
	private final RendererModel bone2;
	private final RendererModel plain0;
	private final RendererModel bevel0;
	private final RendererModel front0;
	private final RendererModel underlip0;
	private final RendererModel undercurve0;
	private final RendererModel backbend0;
	private final RendererModel shin0;
	private final RendererModel ankle0;
	private final RendererModel foot0;
	private final RendererModel coralribs2;
	private final RendererModel panel8;
	private final RendererModel front15;
	private final RendererModel undercurve15;
	private final RendererModel spine8;
	private final RendererModel rib8;
	private final RendererModel bone22;
	private final RendererModel bone23;
	private final RendererModel plain14;
	private final RendererModel bevel14;
	private final RendererModel front16;
	private final RendererModel underlip14;
	private final RendererModel undercurve16;
	private final RendererModel backbend14;
	private final RendererModel shin14;
	private final RendererModel ankle14;
	private final RendererModel foot14;
	private final RendererModel coralribs3;
	private final RendererModel panel9;
	private final RendererModel front17;
	private final RendererModel undercurve17;
	private final RendererModel spine9;
	private final RendererModel rib9;
	private final RendererModel bone24;
	private final RendererModel bone25;
	private final RendererModel plain15;
	private final RendererModel bevel15;
	private final RendererModel front18;
	private final RendererModel underlip15;
	private final RendererModel undercurve18;
	private final RendererModel backbend15;
	private final RendererModel shin15;
	private final RendererModel ankle15;
	private final RendererModel foot15;
	private final RendererModel coralribs4;
	private final RendererModel panel10;
	private final RendererModel front19;
	private final RendererModel undercurve19;
	private final RendererModel spine10;
	private final RendererModel rib10;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel plain16;
	private final RendererModel bevel16;
	private final RendererModel front20;
	private final RendererModel underlip16;
	private final RendererModel undercurve20;
	private final RendererModel backbend16;
	private final RendererModel shin16;
	private final RendererModel ankle16;
	private final RendererModel foot16;
	private final RendererModel coralribs5;
	private final RendererModel panel11;
	private final RendererModel front21;
	private final RendererModel undercurve21;
	private final RendererModel spine11;
	private final RendererModel rib11;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel plain17;
	private final RendererModel bevel17;
	private final RendererModel front22;
	private final RendererModel underlip17;
	private final RendererModel undercurve22;
	private final RendererModel backbend17;
	private final RendererModel shin17;
	private final RendererModel ankle17;
	private final RendererModel foot17;
	private final RendererModel coralribs6;
	private final RendererModel panel12;
	private final RendererModel front23;
	private final RendererModel undercurve23;
	private final RendererModel spine12;
	private final RendererModel rib12;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel plain18;
	private final RendererModel bevel18;
	private final RendererModel front24;
	private final RendererModel underlip18;
	private final RendererModel undercurve24;
	private final RendererModel backbend18;
	private final RendererModel shin18;
	private final RendererModel ankle18;
	private final RendererModel foot18;
	private final RendererModel controls;
	private final RendererModel panels;
	private final RendererModel north;
	private final RendererModel ctrlset_6;
	private final RendererModel top_6;
	private final RendererModel telepathics;
	private final RendererModel rim_6;
	private final RendererModel keypad_b6;
	private final RendererModel siderib_6;
	private final RendererModel northwest;
	private final RendererModel ctrlset_2;
	private final RendererModel top_2;
	private final RendererModel fusebox;
	private final RendererModel button_trip;
	private final RendererModel lamp;
	private final RendererModel tube;
	private final RendererModel mid_2;
	private final RendererModel wires_2;
	private final RendererModel gauge;
	private final RendererModel rim_2;
	private final RendererModel button_switch;
	private final RendererModel coms;
	private final RendererModel phone_base;
	private final RendererModel handset;
	private final RendererModel cord;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel phone_grill;
	private final RendererModel phone_keys;
	private final RendererModel siderib_2;
	private final RendererModel northeast;
	private final RendererModel ctrlset_3;
	private final RendererModel top_3;
	private final RendererModel mid_3;
	private final RendererModel bone34;
	private final RendererModel rim_3;
	private final RendererModel keyboard;
	private final RendererModel randomizer;
	private final RendererModel spinner;
	private final RendererModel bone17;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel siderib_3;
	private final RendererModel south;
	private final RendererModel ctrlset_4;
	private final RendererModel top_4;
	private final RendererModel mid_4;
	private final RendererModel spin_glass_x;
	private final RendererModel base_4;
	private final RendererModel rim_4;
	private final RendererModel keypad_a4;
	private final RendererModel keypad_b4;
	private final RendererModel siderib_4;
	private final RendererModel snake_wire_4;
	private final RendererModel gear_spin;
	private final RendererModel spike_4;
	private final RendererModel spike_4b;
	private final RendererModel bend_4;
	private final RendererModel slope_4;
	private final RendererModel vert_4;
	private final RendererModel southeast;
	private final RendererModel ctrlset_1;
	private final RendererModel top_1;
	private final RendererModel sonic_port;
	private final RendererModel xyz_cords;
	private final RendererModel inc_x;
	private final RendererModel inc_y;
	private final RendererModel inc_z;
	private final RendererModel rim_1;
	private final RendererModel keypad_a1;
	private final RendererModel keypad_b1;
	private final RendererModel siderib_1;
	private final RendererModel southwest;
	private final RendererModel ctrlset_5;
	private final RendererModel top_5;
	private final RendererModel dim_select2;
	private final RendererModel wheel_rotate_z3;
	private final RendererModel bone32;
	private final RendererModel bone33;
	private final RendererModel mid_5;
	private final RendererModel dummy_buttons_5;
	private final RendererModel globe;
	private final RendererModel spin_y;
	private final RendererModel rim_5;
	private final RendererModel keypad_a5;
	private final RendererModel keypad_b5;
	private final RendererModel dummy_buttons_2;
	private final RendererModel siderib_5;
	private final RendererModel rib_controls;
	private final RendererModel west_rib;
	private final RendererModel upper_rib;
	private final RendererModel big_gauge;
	private final RendererModel big_gauge2;
	private final RendererModel jumbo_slider;
	private final RendererModel slider_rack;
	private final RendererModel base;
	private final RendererModel bevel_rib;
	private final RendererModel handbrake;
	private final RendererModel pull_move_y;
	private final RendererModel vertical_rib;
	private final RendererModel med_plate;
	private final RendererModel nw_rib;
	private final RendererModel throttle;
	private final RendererModel throttle_rotate_x;
	private final RendererModel throttle_base;
	private final RendererModel lever_mount;
	private final RendererModel upper_rib3;
	private final RendererModel fast_return;
	private final RendererModel switches;
	private final RendererModel dummy_button2;
	private final RendererModel dummy_button3;
	private final RendererModel bevel_nwr;
	private final RendererModel vertical_rib3;
	private final RendererModel med_plate3;
	private final RendererModel sw_rib;
	private final RendererModel upper_rib6;
	private final RendererModel big_plate6;
	private final RendererModel resitsters;
	private final RendererModel landing_rotate_x;
	private final RendererModel bevel_rib5;
	private final RendererModel small_plate5;
	private final RendererModel small_plate6;
	private final RendererModel vertical_rib6;
	private final RendererModel east_rib;
	private final RendererModel upper_rib5;
	private final RendererModel refuler;
	private final RendererModel refuelknob_rotate;
	private final RendererModel tanks;
	private final RendererModel bevel_rib4;
	private final RendererModel small_plate3;
	private final RendererModel plate3_needle;
	private final RendererModel small_plate4;
	private final RendererModel vertical_rib5;
	private final RendererModel facing;
	private final RendererModel facing_rotate_z;
	private final RendererModel bone13;
	private final RendererModel bone14;
	private final RendererModel winch_plate;
	private final RendererModel ne_rib;
	private final RendererModel upper_rib4;
	private final RendererModel stablizers;
	private final RendererModel pull_rotate_x;
	private final RendererModel pull_leaverbase;
	private final RendererModel deskbell;
	private final RendererModel bevel_rib3;
	private final RendererModel small_plate2;
	private final RendererModel vertical_rib4;
	private final RendererModel med_plate4;
	private final RendererModel se_rib;
	private final RendererModel upper_rib2;
	private final RendererModel monitor;
	private final RendererModel screen3;
	private final RendererModel door_switch;
	private final RendererModel capasitors;
	private final RendererModel buttons;
	private final RendererModel bevel_rib2;
	private final RendererModel increment_select;
	private final RendererModel crank_rotate_y;
	private final RendererModel crank_mount2;
	private final RendererModel vertical_rib2;
	private final RendererModel med_plate2;
	private final RendererModel guts;
	private final RendererModel bone35;

	public XionConsoleModel() {
        textureWidth = 512;
        textureHeight = 512;

        glow = new RendererModel(this);
        glow.setRotationPoint(0.0F, 24.0F, 0.0F);

        rotor = new RendererModel(this);
        rotor.setRotationPoint(0.0F, 18.0F, 0.0F);
        glow.addChild(rotor);

        rotorbase = new RendererModel(this);
        rotorbase.setRotationPoint(0.0F, -111.0F, -0.5F);
        rotor.addChild(rotorbase);
        rotorbase.cubeList.add(new ModelBox(rotorbase, 0, 122, -18.0F, -2.0F, -9.5F, 35, 4, 20, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -15.0F, -2.0F, 10.5F, 34, 4, 4, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -15.0F, -2.0F, -13.5F, 34, 4, 4, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -11.0F, -2.0F, 14.5F, 22, 4, 4, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -11.0F, -2.0F, -17.5F, 22, 4, 4, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -4.0F, -2.0F, 18.5F, 8, 4, 4, 0.0F, false));
        rotorbase.cubeList.add(new ModelBox(rotorbase, 41, 133, -4.0F, -2.0F, -21.5F, 8, 4, 4, 0.0F, false));

        rotor_spikes1 = new RendererModel(this);
        rotor_spikes1.setRotationPoint(0.5F, -133.0F, -0.5F);
        rotor.addChild(rotor_spikes1);
        rotor_spikes1.cubeList.add(new ModelBox(rotor_spikes1, 45, 128, -2.5F, -13.0F, 8.5F, 5, 40, 5, 0.0F, false));
        rotor_spikes1.cubeList.add(new ModelBox(rotor_spikes1, 45, 128, -2.5F, -11.0F, -2.5F, 5, 31, 5, 0.0F, false));
        rotor_spikes1.cubeList.add(new ModelBox(rotor_spikes1, 45, 128, -2.5F, 3.0F, -13.5F, 5, 40, 5, 0.0F, false));

        rotor_spikes2 = new RendererModel(this);
        rotor_spikes2.setRotationPoint(0.5F, -133.0F, -0.5F);
        setRotationAngle(rotor_spikes2, 0.0F, -1.0472F, 0.0F);
        rotor.addChild(rotor_spikes2);
        rotor_spikes2.cubeList.add(new ModelBox(rotor_spikes2, 45, 128, -2.5F, -16.0F, 8.5F, 5, 40, 5, 0.0F, false));
        rotor_spikes2.cubeList.add(new ModelBox(rotor_spikes2, 45, 128, -2.5F, -3.0F, -13.5F, 5, 40, 5, 0.0F, false));

        rotor_spikes3 = new RendererModel(this);
        rotor_spikes3.setRotationPoint(0.5F, -133.0F, -0.5F);
        setRotationAngle(rotor_spikes3, 0.0F, -2.0944F, 0.0F);
        rotor.addChild(rotor_spikes3);
        rotor_spikes3.cubeList.add(new ModelBox(rotor_spikes3, 45, 128, -2.5F, -20.0F, 8.5F, 5, 42, 5, 0.0F, false));
        rotor_spikes3.cubeList.add(new ModelBox(rotor_spikes3, 45, 128, -2.5F, -6.0F, -13.5F, 5, 40, 5, 0.0F, false));

        glow_glass1 = new RendererModel(this);
        glow_glass1.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_glass1);

        panel2 = new RendererModel(this);
        panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass1.addChild(panel2);

        plain3 = new RendererModel(this);
        plain3.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain3, -1.1345F, 0.0F, 0.0F);
        panel2.addChild(plain3);
        plain3.cubeList.add(new ModelBox(plain3, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain3.cubeList.add(new ModelBox(plain3, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel3 = new RendererModel(this);
        bevel3.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel3, -0.6981F, 0.0F, 0.0F);
        panel2.addChild(bevel3);
        bevel3.cubeList.add(new ModelBox(bevel3, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front3 = new RendererModel(this);
        front3.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel2.addChild(front3);
        front3.cubeList.add(new ModelBox(front3, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip3 = new RendererModel(this);
        underlip3.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip3, 0.6109F, 0.0F, 0.0F);
        panel2.addChild(underlip3);
        underlip3.cubeList.add(new ModelBox(underlip3, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve3 = new RendererModel(this);
        undercurve3.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve3, 1.0472F, 0.0F, 0.0F);
        panel2.addChild(undercurve3);

        backbend3 = new RendererModel(this);
        backbend3.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend3, 0.3491F, 0.0F, 0.0F);
        panel2.addChild(backbend3);

        shin3 = new RendererModel(this);
        shin3.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin3, -0.0873F, 0.0F, 0.0F);
        panel2.addChild(shin3);

        ankle3 = new RendererModel(this);
        ankle3.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle3, -0.8727F, 0.0F, 0.0F);
        panel2.addChild(ankle3);

        foot3 = new RendererModel(this);
        foot3.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot3, -1.5708F, 0.0F, 0.0F);
        panel2.addChild(foot3);

        spine2 = new RendererModel(this);
        spine2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine2, 0.0F, -0.5236F, 0.0F);
        glow_glass1.addChild(spine2);

        rib2 = new RendererModel(this);
        rib2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib2, 0.0F, 0.5236F, 0.0F);
        spine2.addChild(rib2);
        rib2.cubeList.add(new ModelBox(rib2, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib2.cubeList.add(new ModelBox(rib2, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone3 = new RendererModel(this);
        bone3.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone3, -0.3491F, 0.0F, 0.0F);
        rib2.addChild(bone3);

        bone4 = new RendererModel(this);
        bone4.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone4, -0.3491F, 0.0F, 0.0F);
        rib2.addChild(bone4);

        plain4 = new RendererModel(this);
        plain4.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain4, -1.1345F, 0.0F, 0.0F);
        spine2.addChild(plain4);

        bevel4 = new RendererModel(this);
        bevel4.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel4, -0.6981F, 0.0F, 0.0F);
        spine2.addChild(bevel4);

        front4 = new RendererModel(this);
        front4.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine2.addChild(front4);
        front4.cubeList.add(new ModelBox(front4, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front4.cubeList.add(new ModelBox(front4, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip4 = new RendererModel(this);
        underlip4.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip4, 0.6109F, 0.0F, 0.0F);
        spine2.addChild(underlip4);

        undercurve4 = new RendererModel(this);
        undercurve4.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve4, 1.0472F, 0.0F, 0.0F);
        spine2.addChild(undercurve4);

        backbend4 = new RendererModel(this);
        backbend4.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend4, 0.3491F, 0.0F, 0.0F);
        spine2.addChild(backbend4);

        shin4 = new RendererModel(this);
        shin4.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin4, -0.0873F, 0.0F, 0.0F);
        spine2.addChild(shin4);

        ankle4 = new RendererModel(this);
        ankle4.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle4, -0.8727F, 0.0F, 0.0F);
        spine2.addChild(ankle4);

        foot4 = new RendererModel(this);
        foot4.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot4, -1.5708F, 0.0F, 0.0F);
        spine2.addChild(foot4);

        glow_glass2 = new RendererModel(this);
        glow_glass2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(glow_glass2, 0.0F, -1.0472F, 0.0F);
        glow.addChild(glow_glass2);

        panel3 = new RendererModel(this);
        panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass2.addChild(panel3);

        plain2 = new RendererModel(this);
        plain2.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain2, -1.1345F, 0.0F, 0.0F);
        panel3.addChild(plain2);
        plain2.cubeList.add(new ModelBox(plain2, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain2.cubeList.add(new ModelBox(plain2, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel2 = new RendererModel(this);
        bevel2.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel2, -0.6981F, 0.0F, 0.0F);
        panel3.addChild(bevel2);
        bevel2.cubeList.add(new ModelBox(bevel2, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front5 = new RendererModel(this);
        front5.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel3.addChild(front5);
        front5.cubeList.add(new ModelBox(front5, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip2 = new RendererModel(this);
        underlip2.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip2, 0.6109F, 0.0F, 0.0F);
        panel3.addChild(underlip2);
        underlip2.cubeList.add(new ModelBox(underlip2, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve5 = new RendererModel(this);
        undercurve5.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve5, 1.0472F, 0.0F, 0.0F);
        panel3.addChild(undercurve5);

        backbend2 = new RendererModel(this);
        backbend2.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend2, 0.3491F, 0.0F, 0.0F);
        panel3.addChild(backbend2);

        shin2 = new RendererModel(this);
        shin2.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin2, -0.0873F, 0.0F, 0.0F);
        panel3.addChild(shin2);

        ankle2 = new RendererModel(this);
        ankle2.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle2, -0.8727F, 0.0F, 0.0F);
        panel3.addChild(ankle2);

        foot2 = new RendererModel(this);
        foot2.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot2, -1.5708F, 0.0F, 0.0F);
        panel3.addChild(foot2);

        spine3 = new RendererModel(this);
        spine3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine3, 0.0F, -0.5236F, 0.0F);
        glow_glass2.addChild(spine3);

        rib3 = new RendererModel(this);
        rib3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib3, 0.0F, 0.5236F, 0.0F);
        spine3.addChild(rib3);
        rib3.cubeList.add(new ModelBox(rib3, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib3.cubeList.add(new ModelBox(rib3, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone5 = new RendererModel(this);
        bone5.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone5, -0.3491F, 0.0F, 0.0F);
        rib3.addChild(bone5);

        bone6 = new RendererModel(this);
        bone6.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone6, -0.3491F, 0.0F, 0.0F);
        rib3.addChild(bone6);

        plain5 = new RendererModel(this);
        plain5.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain5, -1.1345F, 0.0F, 0.0F);
        spine3.addChild(plain5);

        bevel5 = new RendererModel(this);
        bevel5.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel5, -0.6981F, 0.0F, 0.0F);
        spine3.addChild(bevel5);

        front6 = new RendererModel(this);
        front6.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine3.addChild(front6);
        front6.cubeList.add(new ModelBox(front6, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front6.cubeList.add(new ModelBox(front6, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip5 = new RendererModel(this);
        underlip5.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip5, 0.6109F, 0.0F, 0.0F);
        spine3.addChild(underlip5);

        undercurve6 = new RendererModel(this);
        undercurve6.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve6, 1.0472F, 0.0F, 0.0F);
        spine3.addChild(undercurve6);

        backbend5 = new RendererModel(this);
        backbend5.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend5, 0.3491F, 0.0F, 0.0F);
        spine3.addChild(backbend5);

        shin5 = new RendererModel(this);
        shin5.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin5, -0.0873F, 0.0F, 0.0F);
        spine3.addChild(shin5);

        ankle5 = new RendererModel(this);
        ankle5.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle5, -0.8727F, 0.0F, 0.0F);
        spine3.addChild(ankle5);

        foot5 = new RendererModel(this);
        foot5.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot5, -1.5708F, 0.0F, 0.0F);
        spine3.addChild(foot5);

        glow_glass3 = new RendererModel(this);
        glow_glass3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(glow_glass3, 0.0F, -2.0944F, 0.0F);
        glow.addChild(glow_glass3);

        panel4 = new RendererModel(this);
        panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass3.addChild(panel4);

        plain6 = new RendererModel(this);
        plain6.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain6, -1.1345F, 0.0F, 0.0F);
        panel4.addChild(plain6);
        plain6.cubeList.add(new ModelBox(plain6, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain6.cubeList.add(new ModelBox(plain6, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel6 = new RendererModel(this);
        bevel6.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel6, -0.6981F, 0.0F, 0.0F);
        panel4.addChild(bevel6);
        bevel6.cubeList.add(new ModelBox(bevel6, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front7 = new RendererModel(this);
        front7.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel4.addChild(front7);
        front7.cubeList.add(new ModelBox(front7, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip6 = new RendererModel(this);
        underlip6.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip6, 0.6109F, 0.0F, 0.0F);
        panel4.addChild(underlip6);
        underlip6.cubeList.add(new ModelBox(underlip6, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve7 = new RendererModel(this);
        undercurve7.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve7, 1.0472F, 0.0F, 0.0F);
        panel4.addChild(undercurve7);

        backbend6 = new RendererModel(this);
        backbend6.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend6, 0.3491F, 0.0F, 0.0F);
        panel4.addChild(backbend6);

        shin6 = new RendererModel(this);
        shin6.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin6, -0.0873F, 0.0F, 0.0F);
        panel4.addChild(shin6);

        ankle6 = new RendererModel(this);
        ankle6.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle6, -0.8727F, 0.0F, 0.0F);
        panel4.addChild(ankle6);

        foot6 = new RendererModel(this);
        foot6.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot6, -1.5708F, 0.0F, 0.0F);
        panel4.addChild(foot6);

        spine4 = new RendererModel(this);
        spine4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine4, 0.0F, -0.5236F, 0.0F);
        glow_glass3.addChild(spine4);

        rib4 = new RendererModel(this);
        rib4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib4, 0.0F, 0.5236F, 0.0F);
        spine4.addChild(rib4);
        rib4.cubeList.add(new ModelBox(rib4, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib4.cubeList.add(new ModelBox(rib4, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone7 = new RendererModel(this);
        bone7.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone7, -0.3491F, 0.0F, 0.0F);
        rib4.addChild(bone7);

        bone8 = new RendererModel(this);
        bone8.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone8, -0.3491F, 0.0F, 0.0F);
        rib4.addChild(bone8);

        plain7 = new RendererModel(this);
        plain7.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain7, -1.1345F, 0.0F, 0.0F);
        spine4.addChild(plain7);

        bevel7 = new RendererModel(this);
        bevel7.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel7, -0.6981F, 0.0F, 0.0F);
        spine4.addChild(bevel7);

        front8 = new RendererModel(this);
        front8.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine4.addChild(front8);
        front8.cubeList.add(new ModelBox(front8, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front8.cubeList.add(new ModelBox(front8, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip7 = new RendererModel(this);
        underlip7.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip7, 0.6109F, 0.0F, 0.0F);
        spine4.addChild(underlip7);

        undercurve8 = new RendererModel(this);
        undercurve8.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve8, 1.0472F, 0.0F, 0.0F);
        spine4.addChild(undercurve8);

        backbend7 = new RendererModel(this);
        backbend7.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend7, 0.3491F, 0.0F, 0.0F);
        spine4.addChild(backbend7);

        shin7 = new RendererModel(this);
        shin7.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin7, -0.0873F, 0.0F, 0.0F);
        spine4.addChild(shin7);

        ankle7 = new RendererModel(this);
        ankle7.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle7, -0.8727F, 0.0F, 0.0F);
        spine4.addChild(ankle7);

        foot7 = new RendererModel(this);
        foot7.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot7, -1.5708F, 0.0F, 0.0F);
        spine4.addChild(foot7);

        glow_glass4 = new RendererModel(this);
        glow_glass4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(glow_glass4, 0.0F, 3.1416F, 0.0F);
        glow.addChild(glow_glass4);

        panel5 = new RendererModel(this);
        panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass4.addChild(panel5);

        plain8 = new RendererModel(this);
        plain8.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain8, -1.1345F, 0.0F, 0.0F);
        panel5.addChild(plain8);
        plain8.cubeList.add(new ModelBox(plain8, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain8.cubeList.add(new ModelBox(plain8, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel8 = new RendererModel(this);
        bevel8.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel8, -0.6981F, 0.0F, 0.0F);
        panel5.addChild(bevel8);
        bevel8.cubeList.add(new ModelBox(bevel8, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front9 = new RendererModel(this);
        front9.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel5.addChild(front9);
        front9.cubeList.add(new ModelBox(front9, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip8 = new RendererModel(this);
        underlip8.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip8, 0.6109F, 0.0F, 0.0F);
        panel5.addChild(underlip8);
        underlip8.cubeList.add(new ModelBox(underlip8, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve9 = new RendererModel(this);
        undercurve9.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve9, 1.0472F, 0.0F, 0.0F);
        panel5.addChild(undercurve9);

        backbend8 = new RendererModel(this);
        backbend8.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend8, 0.3491F, 0.0F, 0.0F);
        panel5.addChild(backbend8);

        shin8 = new RendererModel(this);
        shin8.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin8, -0.0873F, 0.0F, 0.0F);
        panel5.addChild(shin8);

        ankle8 = new RendererModel(this);
        ankle8.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle8, -0.8727F, 0.0F, 0.0F);
        panel5.addChild(ankle8);

        foot8 = new RendererModel(this);
        foot8.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot8, -1.5708F, 0.0F, 0.0F);
        panel5.addChild(foot8);

        spine5 = new RendererModel(this);
        spine5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine5, 0.0F, -0.5236F, 0.0F);
        glow_glass4.addChild(spine5);

        rib5 = new RendererModel(this);
        rib5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib5, 0.0F, 0.5236F, 0.0F);
        spine5.addChild(rib5);
        rib5.cubeList.add(new ModelBox(rib5, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib5.cubeList.add(new ModelBox(rib5, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone9 = new RendererModel(this);
        bone9.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone9, -0.3491F, 0.0F, 0.0F);
        rib5.addChild(bone9);

        bone10 = new RendererModel(this);
        bone10.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone10, -0.3491F, 0.0F, 0.0F);
        rib5.addChild(bone10);

        plain9 = new RendererModel(this);
        plain9.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain9, -1.1345F, 0.0F, 0.0F);
        spine5.addChild(plain9);

        bevel9 = new RendererModel(this);
        bevel9.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel9, -0.6981F, 0.0F, 0.0F);
        spine5.addChild(bevel9);

        front10 = new RendererModel(this);
        front10.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine5.addChild(front10);
        front10.cubeList.add(new ModelBox(front10, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front10.cubeList.add(new ModelBox(front10, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip9 = new RendererModel(this);
        underlip9.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip9, 0.6109F, 0.0F, 0.0F);
        spine5.addChild(underlip9);

        undercurve10 = new RendererModel(this);
        undercurve10.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve10, 1.0472F, 0.0F, 0.0F);
        spine5.addChild(undercurve10);

        backbend9 = new RendererModel(this);
        backbend9.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend9, 0.3491F, 0.0F, 0.0F);
        spine5.addChild(backbend9);

        shin9 = new RendererModel(this);
        shin9.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin9, -0.0873F, 0.0F, 0.0F);
        spine5.addChild(shin9);

        ankle9 = new RendererModel(this);
        ankle9.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle9, -0.8727F, 0.0F, 0.0F);
        spine5.addChild(ankle9);

        foot9 = new RendererModel(this);
        foot9.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot9, -1.5708F, 0.0F, 0.0F);
        spine5.addChild(foot9);

        glow_glass5 = new RendererModel(this);
        glow_glass5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(glow_glass5, 0.0F, 2.0944F, 0.0F);
        glow.addChild(glow_glass5);

        panel6 = new RendererModel(this);
        panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass5.addChild(panel6);

        plain10 = new RendererModel(this);
        plain10.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain10, -1.1345F, 0.0F, 0.0F);
        panel6.addChild(plain10);
        plain10.cubeList.add(new ModelBox(plain10, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain10.cubeList.add(new ModelBox(plain10, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel10 = new RendererModel(this);
        bevel10.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel10, -0.6981F, 0.0F, 0.0F);
        panel6.addChild(bevel10);
        bevel10.cubeList.add(new ModelBox(bevel10, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front11 = new RendererModel(this);
        front11.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel6.addChild(front11);
        front11.cubeList.add(new ModelBox(front11, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip10 = new RendererModel(this);
        underlip10.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip10, 0.6109F, 0.0F, 0.0F);
        panel6.addChild(underlip10);
        underlip10.cubeList.add(new ModelBox(underlip10, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve11 = new RendererModel(this);
        undercurve11.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve11, 1.0472F, 0.0F, 0.0F);
        panel6.addChild(undercurve11);

        backbend10 = new RendererModel(this);
        backbend10.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend10, 0.3491F, 0.0F, 0.0F);
        panel6.addChild(backbend10);

        shin10 = new RendererModel(this);
        shin10.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin10, -0.0873F, 0.0F, 0.0F);
        panel6.addChild(shin10);

        ankle10 = new RendererModel(this);
        ankle10.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle10, -0.8727F, 0.0F, 0.0F);
        panel6.addChild(ankle10);

        foot10 = new RendererModel(this);
        foot10.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot10, -1.5708F, 0.0F, 0.0F);
        panel6.addChild(foot10);

        spine6 = new RendererModel(this);
        spine6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine6, 0.0F, -0.5236F, 0.0F);
        glow_glass5.addChild(spine6);

        rib6 = new RendererModel(this);
        rib6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib6, 0.0F, 0.5236F, 0.0F);
        spine6.addChild(rib6);
        rib6.cubeList.add(new ModelBox(rib6, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib6.cubeList.add(new ModelBox(rib6, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone11 = new RendererModel(this);
        bone11.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone11, -0.3491F, 0.0F, 0.0F);
        rib6.addChild(bone11);

        bone12 = new RendererModel(this);
        bone12.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone12, -0.3491F, 0.0F, 0.0F);
        rib6.addChild(bone12);

        plain11 = new RendererModel(this);
        plain11.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain11, -1.1345F, 0.0F, 0.0F);
        spine6.addChild(plain11);

        bevel11 = new RendererModel(this);
        bevel11.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel11, -0.6981F, 0.0F, 0.0F);
        spine6.addChild(bevel11);

        front12 = new RendererModel(this);
        front12.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine6.addChild(front12);
        front12.cubeList.add(new ModelBox(front12, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front12.cubeList.add(new ModelBox(front12, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip11 = new RendererModel(this);
        underlip11.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip11, 0.6109F, 0.0F, 0.0F);
        spine6.addChild(underlip11);

        undercurve12 = new RendererModel(this);
        undercurve12.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve12, 1.0472F, 0.0F, 0.0F);
        spine6.addChild(undercurve12);

        backbend11 = new RendererModel(this);
        backbend11.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend11, 0.3491F, 0.0F, 0.0F);
        spine6.addChild(backbend11);

        shin11 = new RendererModel(this);
        shin11.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin11, -0.0873F, 0.0F, 0.0F);
        spine6.addChild(shin11);

        ankle11 = new RendererModel(this);
        ankle11.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle11, -0.8727F, 0.0F, 0.0F);
        spine6.addChild(ankle11);

        foot11 = new RendererModel(this);
        foot11.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot11, -1.5708F, 0.0F, 0.0F);
        spine6.addChild(foot11);

        glow_glass6 = new RendererModel(this);
        glow_glass6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(glow_glass6, 0.0F, 1.0472F, 0.0F);
        glow.addChild(glow_glass6);

        panel7 = new RendererModel(this);
        panel7.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_glass6.addChild(panel7);

        plain12 = new RendererModel(this);
        plain12.setRotationPoint(0.0F, -70.9898F, -44.2791F);
        setRotationAngle(plain12, -1.1345F, 0.0F, 0.0F);
        panel7.addChild(plain12);
        plain12.cubeList.add(new ModelBox(plain12, 42, 22, -28.0F, 1.8312F, -1.0089F, 56, 16, 8, 0.0F, false));
        plain12.cubeList.add(new ModelBox(plain12, 46, 6, -24.0F, -14.1688F, -1.0089F, 48, 16, 8, 0.0F, false));

        bevel12 = new RendererModel(this);
        bevel12.setRotationPoint(0.0F, -60.6347F, -60.7874F);
        setRotationAngle(bevel12, -0.6981F, 0.0F, 0.0F);
        panel7.addChild(bevel12);
        bevel12.cubeList.add(new ModelBox(bevel12, 38, 40, -32.0F, -2.8097F, -2.4602F, 64, 12, 8, 0.0F, false));

        front13 = new RendererModel(this);
        front13.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel7.addChild(front13);
        front13.cubeList.add(new ModelBox(front13, 38, 54, -32.0F, -5.1759F, -4.5795F, 64, 16, 8, 0.0F, false));

        underlip12 = new RendererModel(this);
        underlip12.setRotationPoint(0.0F, -43.7571F, -64.7704F);
        setRotationAngle(underlip12, 0.6109F, 0.0F, 0.0F);
        panel7.addChild(underlip12);
        underlip12.cubeList.add(new ModelBox(underlip12, 38, 81, -32.0F, 1.5679F, -5.7479F, 64, 8, 8, 0.0F, false));

        undercurve13 = new RendererModel(this);
        undercurve13.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve13, 1.0472F, 0.0F, 0.0F);
        panel7.addChild(undercurve13);

        backbend12 = new RendererModel(this);
        backbend12.setRotationPoint(0.0F, -20.714F, -43.1042F);
        setRotationAngle(backbend12, 0.3491F, 0.0F, 0.0F);
        panel7.addChild(backbend12);

        shin12 = new RendererModel(this);
        shin12.setRotationPoint(0.0F, -15.9761F, -43.8165F);
        setRotationAngle(shin12, -0.0873F, 0.0F, 0.0F);
        panel7.addChild(shin12);

        ankle12 = new RendererModel(this);
        ankle12.setRotationPoint(0.0F, -1.3682F, -47.2239F);
        setRotationAngle(ankle12, -0.8727F, 0.0F, 0.0F);
        panel7.addChild(ankle12);

        foot12 = new RendererModel(this);
        foot12.setRotationPoint(0.0F, 0.0068F, -56.2239F);
        setRotationAngle(foot12, -1.5708F, 0.0F, 0.0F);
        panel7.addChild(foot12);

        spine7 = new RendererModel(this);
        spine7.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine7, 0.0F, -0.5236F, 0.0F);
        glow_glass6.addChild(spine7);

        rib7 = new RendererModel(this);
        rib7.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib7, 0.0F, 0.5236F, 0.0F);
        spine7.addChild(rib7);
        rib7.cubeList.add(new ModelBox(rib7, 65, 133, -12.0F, -30.0F, -48.75F, 24, 20, 12, 0.0F, false));
        rib7.cubeList.add(new ModelBox(rib7, 104, 116, -2.9874F, -118.7731F, -24.218F, 6, 48, 6, 0.0F, false));

        bone20 = new RendererModel(this);
        bone20.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone20, -0.3491F, 0.0F, 0.0F);
        rib7.addChild(bone20);

        bone21 = new RendererModel(this);
        bone21.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone21, -0.3491F, 0.0F, 0.0F);
        rib7.addChild(bone21);

        plain13 = new RendererModel(this);
        plain13.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain13, -1.1345F, 0.0F, 0.0F);
        spine7.addChild(plain13);

        bevel13 = new RendererModel(this);
        bevel13.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel13, -0.6981F, 0.0F, 0.0F);
        spine7.addChild(bevel13);

        front14 = new RendererModel(this);
        front14.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine7.addChild(front14);
        front14.cubeList.add(new ModelBox(front14, 66, 129, -13.6077F, 16.0F, 25.4308F, 28, 24, 8, 0.0F, false));
        front14.cubeList.add(new ModelBox(front14, 38, 114, -9.0F, -67.7731F, 50.3863F, 19, 47, 4, 0.0F, false));

        underlip13 = new RendererModel(this);
        underlip13.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip13, 0.6109F, 0.0F, 0.0F);
        spine7.addChild(underlip13);

        undercurve14 = new RendererModel(this);
        undercurve14.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve14, 1.0472F, 0.0F, 0.0F);
        spine7.addChild(undercurve14);

        backbend13 = new RendererModel(this);
        backbend13.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend13, 0.3491F, 0.0F, 0.0F);
        spine7.addChild(backbend13);

        shin13 = new RendererModel(this);
        shin13.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin13, -0.0873F, 0.0F, 0.0F);
        spine7.addChild(shin13);

        ankle13 = new RendererModel(this);
        ankle13.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle13, -0.8727F, 0.0F, 0.0F);
        spine7.addChild(ankle13);

        foot13 = new RendererModel(this);
        foot13.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot13, -1.5708F, 0.0F, 0.0F);
        spine7.addChild(foot13);

        glow_guts = new RendererModel(this);
        glow_guts.setRotationPoint(0.0F, -12.0F, 0.0F);
        glow.addChild(glow_guts);
        glow_guts.cubeList.add(new ModelBox(glow_guts, 41, 131, -6.0F, -7.0F, -6.0F, 12, 19, 12, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, 8.0F, -16.0F, -10.0F, 3, 9, 14, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, -15.0F, -16.0F, 13.0F, 13, 8, 2, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, -24.0F, 7.0F, 0.0F, 4, 5, 4, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, 21.0F, 7.0F, 11.0F, 4, 5, 4, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, 12.0F, 7.0F, -19.0F, 4, 5, 4, 0.0F, false));
        glow_guts.cubeList.add(new ModelBox(glow_guts, 47, 133, -15.0F, -17.0F, -1.0F, 13, 8, 2, 0.0F, false));

        stations = new RendererModel(this);
        stations.setRotationPoint(0.0F, 24.0F, 0.0F);

        coralribs1 = new RendererModel(this);
        coralribs1.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(coralribs1);

        panel0 = new RendererModel(this);
        panel0.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs1.addChild(panel0);

        front2 = new RendererModel(this);
        front2.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel0.addChild(front2);
        front2.cubeList.add(new ModelBox(front2, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front2.cubeList.add(new ModelBox(front2, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve2 = new RendererModel(this);
        undercurve2.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve2, 1.0472F, 0.0F, 0.0F);
        panel0.addChild(undercurve2);
        undercurve2.cubeList.add(new ModelBox(undercurve2, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine0 = new RendererModel(this);
        spine0.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine0, 0.0F, -0.5236F, 0.0F);
        coralribs1.addChild(spine0);

        rib0 = new RendererModel(this);
        rib0.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib0, 0.0F, 0.5236F, 0.0F);
        spine0.addChild(rib0);
        rib0.cubeList.add(new ModelBox(rib0, 157, 135, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 164, 136, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib0.cubeList.add(new ModelBox(rib0, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone = new RendererModel(this);
        bone.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone, -0.3491F, 0.0F, 0.0F);
        rib0.addChild(bone);
        bone.cubeList.add(new ModelBox(bone, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone.cubeList.add(new ModelBox(bone, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone.cubeList.add(new ModelBox(bone, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone.cubeList.add(new ModelBox(bone, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone2 = new RendererModel(this);
        bone2.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone2, -0.3491F, 0.0F, 0.0F);
        rib0.addChild(bone2);
        bone2.cubeList.add(new ModelBox(bone2, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain0 = new RendererModel(this);
        plain0.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain0, -1.1345F, 0.0F, 0.0F);
        spine0.addChild(plain0);
        plain0.cubeList.add(new ModelBox(plain0, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain0.cubeList.add(new ModelBox(plain0, 243, 38, -6.0F, -25.4559F, -5.7139F, 12, 19, 8, 0.0F, false));
        plain0.cubeList.add(new ModelBox(plain0, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel0 = new RendererModel(this);
        bevel0.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel0, -0.6981F, 0.0F, 0.0F);
        spine0.addChild(bevel0);
        bevel0.cubeList.add(new ModelBox(bevel0, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel0.cubeList.add(new ModelBox(bevel0, 243, 54, -6.0F, -26.8097F, 3.5398F, 12, 12, 8, 0.0F, false));
        bevel0.cubeList.add(new ModelBox(bevel0, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front0 = new RendererModel(this);
        front0.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine0.addChild(front0);
        front0.cubeList.add(new ModelBox(front0, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front0.cubeList.add(new ModelBox(front0, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip0 = new RendererModel(this);
        underlip0.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip0, 0.6109F, 0.0F, 0.0F);
        spine0.addChild(underlip0);
        underlip0.cubeList.add(new ModelBox(underlip0, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip0.cubeList.add(new ModelBox(underlip0, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve0 = new RendererModel(this);
        undercurve0.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve0, 1.0472F, 0.0F, 0.0F);
        spine0.addChild(undercurve0);
        undercurve0.cubeList.add(new ModelBox(undercurve0, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve0.cubeList.add(new ModelBox(undercurve0, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend0 = new RendererModel(this);
        backbend0.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend0, 0.3491F, 0.0F, 0.0F);
        spine0.addChild(backbend0);
        backbend0.cubeList.add(new ModelBox(backbend0, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin0 = new RendererModel(this);
        shin0.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin0, -0.0873F, 0.0F, 0.0F);
        spine0.addChild(shin0);
        shin0.cubeList.add(new ModelBox(shin0, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle0 = new RendererModel(this);
        ankle0.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle0, -0.8727F, 0.0F, 0.0F);
        spine0.addChild(ankle0);
        ankle0.cubeList.add(new ModelBox(ankle0, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot0 = new RendererModel(this);
        foot0.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot0, -1.5708F, 0.0F, 0.0F);
        spine0.addChild(foot0);
        foot0.cubeList.add(new ModelBox(foot0, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot0.cubeList.add(new ModelBox(foot0, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot0.cubeList.add(new ModelBox(foot0, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot0.cubeList.add(new ModelBox(foot0, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        coralribs2 = new RendererModel(this);
        coralribs2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(coralribs2, 0.0F, -1.0472F, 0.0F);
        stations.addChild(coralribs2);

        panel8 = new RendererModel(this);
        panel8.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs2.addChild(panel8);

        front15 = new RendererModel(this);
        front15.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel8.addChild(front15);
        front15.cubeList.add(new ModelBox(front15, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front15.cubeList.add(new ModelBox(front15, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve15 = new RendererModel(this);
        undercurve15.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve15, 1.0472F, 0.0F, 0.0F);
        panel8.addChild(undercurve15);
        undercurve15.cubeList.add(new ModelBox(undercurve15, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine8 = new RendererModel(this);
        spine8.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine8, 0.0F, -0.5236F, 0.0F);
        coralribs2.addChild(spine8);

        rib8 = new RendererModel(this);
        rib8.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib8, 0.0F, 0.5236F, 0.0F);
        spine8.addChild(rib8);
        rib8.cubeList.add(new ModelBox(rib8, 152, 136, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 160, 133, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib8.cubeList.add(new ModelBox(rib8, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone22 = new RendererModel(this);
        bone22.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone22, -0.3491F, 0.0F, 0.0F);
        rib8.addChild(bone22);
        bone22.cubeList.add(new ModelBox(bone22, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone22.cubeList.add(new ModelBox(bone22, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone22.cubeList.add(new ModelBox(bone22, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone22.cubeList.add(new ModelBox(bone22, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone23 = new RendererModel(this);
        bone23.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone23, -0.3491F, 0.0F, 0.0F);
        rib8.addChild(bone23);
        bone23.cubeList.add(new ModelBox(bone23, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain14 = new RendererModel(this);
        plain14.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain14, -1.1345F, 0.0F, 0.0F);
        spine8.addChild(plain14);
        plain14.cubeList.add(new ModelBox(plain14, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain14.cubeList.add(new ModelBox(plain14, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel14 = new RendererModel(this);
        bevel14.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel14, -0.6981F, 0.0F, 0.0F);
        spine8.addChild(bevel14);
        bevel14.cubeList.add(new ModelBox(bevel14, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel14.cubeList.add(new ModelBox(bevel14, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front16 = new RendererModel(this);
        front16.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine8.addChild(front16);
        front16.cubeList.add(new ModelBox(front16, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front16.cubeList.add(new ModelBox(front16, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip14 = new RendererModel(this);
        underlip14.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip14, 0.6109F, 0.0F, 0.0F);
        spine8.addChild(underlip14);
        underlip14.cubeList.add(new ModelBox(underlip14, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip14.cubeList.add(new ModelBox(underlip14, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve16 = new RendererModel(this);
        undercurve16.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve16, 1.0472F, 0.0F, 0.0F);
        spine8.addChild(undercurve16);
        undercurve16.cubeList.add(new ModelBox(undercurve16, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve16.cubeList.add(new ModelBox(undercurve16, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend14 = new RendererModel(this);
        backbend14.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend14, 0.3491F, 0.0F, 0.0F);
        spine8.addChild(backbend14);
        backbend14.cubeList.add(new ModelBox(backbend14, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin14 = new RendererModel(this);
        shin14.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin14, -0.0873F, 0.0F, 0.0F);
        spine8.addChild(shin14);
        shin14.cubeList.add(new ModelBox(shin14, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle14 = new RendererModel(this);
        ankle14.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle14, -0.8727F, 0.0F, 0.0F);
        spine8.addChild(ankle14);
        ankle14.cubeList.add(new ModelBox(ankle14, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot14 = new RendererModel(this);
        foot14.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot14, -1.5708F, 0.0F, 0.0F);
        spine8.addChild(foot14);
        foot14.cubeList.add(new ModelBox(foot14, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot14.cubeList.add(new ModelBox(foot14, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot14.cubeList.add(new ModelBox(foot14, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot14.cubeList.add(new ModelBox(foot14, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        coralribs3 = new RendererModel(this);
        coralribs3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(coralribs3, 0.0F, -2.0944F, 0.0F);
        stations.addChild(coralribs3);

        panel9 = new RendererModel(this);
        panel9.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs3.addChild(panel9);

        front17 = new RendererModel(this);
        front17.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel9.addChild(front17);
        front17.cubeList.add(new ModelBox(front17, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front17.cubeList.add(new ModelBox(front17, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve17 = new RendererModel(this);
        undercurve17.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve17, 1.0472F, 0.0F, 0.0F);
        panel9.addChild(undercurve17);
        undercurve17.cubeList.add(new ModelBox(undercurve17, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine9 = new RendererModel(this);
        spine9.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine9, 0.0F, -0.5236F, 0.0F);
        coralribs3.addChild(spine9);

        rib9 = new RendererModel(this);
        rib9.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib9, 0.0F, 0.5236F, 0.0F);
        spine9.addChild(rib9);
        rib9.cubeList.add(new ModelBox(rib9, 155, 131, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 160, 125, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib9.cubeList.add(new ModelBox(rib9, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone24 = new RendererModel(this);
        bone24.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone24, -0.3491F, 0.0F, 0.0F);
        rib9.addChild(bone24);
        bone24.cubeList.add(new ModelBox(bone24, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone24.cubeList.add(new ModelBox(bone24, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone24.cubeList.add(new ModelBox(bone24, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone24.cubeList.add(new ModelBox(bone24, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone25 = new RendererModel(this);
        bone25.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone25, -0.3491F, 0.0F, 0.0F);
        rib9.addChild(bone25);
        bone25.cubeList.add(new ModelBox(bone25, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain15 = new RendererModel(this);
        plain15.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain15, -1.1345F, 0.0F, 0.0F);
        spine9.addChild(plain15);
        plain15.cubeList.add(new ModelBox(plain15, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain15.cubeList.add(new ModelBox(plain15, 243, 38, -6.0F, -25.4559F, -5.7139F, 12, 19, 8, 0.0F, false));
        plain15.cubeList.add(new ModelBox(plain15, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel15 = new RendererModel(this);
        bevel15.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel15, -0.6981F, 0.0F, 0.0F);
        spine9.addChild(bevel15);
        bevel15.cubeList.add(new ModelBox(bevel15, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel15.cubeList.add(new ModelBox(bevel15, 243, 54, -6.0F, -26.8097F, 3.5398F, 12, 12, 8, 0.0F, false));
        bevel15.cubeList.add(new ModelBox(bevel15, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front18 = new RendererModel(this);
        front18.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine9.addChild(front18);
        front18.cubeList.add(new ModelBox(front18, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front18.cubeList.add(new ModelBox(front18, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip15 = new RendererModel(this);
        underlip15.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip15, 0.6109F, 0.0F, 0.0F);
        spine9.addChild(underlip15);
        underlip15.cubeList.add(new ModelBox(underlip15, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip15.cubeList.add(new ModelBox(underlip15, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve18 = new RendererModel(this);
        undercurve18.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve18, 1.0472F, 0.0F, 0.0F);
        spine9.addChild(undercurve18);
        undercurve18.cubeList.add(new ModelBox(undercurve18, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve18.cubeList.add(new ModelBox(undercurve18, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend15 = new RendererModel(this);
        backbend15.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend15, 0.3491F, 0.0F, 0.0F);
        spine9.addChild(backbend15);
        backbend15.cubeList.add(new ModelBox(backbend15, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin15 = new RendererModel(this);
        shin15.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin15, -0.0873F, 0.0F, 0.0F);
        spine9.addChild(shin15);
        shin15.cubeList.add(new ModelBox(shin15, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle15 = new RendererModel(this);
        ankle15.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle15, -0.8727F, 0.0F, 0.0F);
        spine9.addChild(ankle15);
        ankle15.cubeList.add(new ModelBox(ankle15, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot15 = new RendererModel(this);
        foot15.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot15, -1.5708F, 0.0F, 0.0F);
        spine9.addChild(foot15);
        foot15.cubeList.add(new ModelBox(foot15, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot15.cubeList.add(new ModelBox(foot15, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot15.cubeList.add(new ModelBox(foot15, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot15.cubeList.add(new ModelBox(foot15, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        coralribs4 = new RendererModel(this);
        coralribs4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(coralribs4, 0.0F, 3.1416F, 0.0F);
        stations.addChild(coralribs4);

        panel10 = new RendererModel(this);
        panel10.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs4.addChild(panel10);

        front19 = new RendererModel(this);
        front19.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel10.addChild(front19);
        front19.cubeList.add(new ModelBox(front19, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front19.cubeList.add(new ModelBox(front19, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve19 = new RendererModel(this);
        undercurve19.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve19, 1.0472F, 0.0F, 0.0F);
        panel10.addChild(undercurve19);
        undercurve19.cubeList.add(new ModelBox(undercurve19, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine10 = new RendererModel(this);
        spine10.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine10, 0.0F, -0.5236F, 0.0F);
        coralribs4.addChild(spine10);

        rib10 = new RendererModel(this);
        rib10.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib10, 0.0F, 0.5236F, 0.0F);
        spine10.addChild(rib10);
        rib10.cubeList.add(new ModelBox(rib10, 153, 135, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 163, 132, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib10.cubeList.add(new ModelBox(rib10, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone26 = new RendererModel(this);
        bone26.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone26, -0.3491F, 0.0F, 0.0F);
        rib10.addChild(bone26);
        bone26.cubeList.add(new ModelBox(bone26, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone26.cubeList.add(new ModelBox(bone26, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone26.cubeList.add(new ModelBox(bone26, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone26.cubeList.add(new ModelBox(bone26, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone27 = new RendererModel(this);
        bone27.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone27, -0.3491F, 0.0F, 0.0F);
        rib10.addChild(bone27);
        bone27.cubeList.add(new ModelBox(bone27, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain16 = new RendererModel(this);
        plain16.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain16, -1.1345F, 0.0F, 0.0F);
        spine10.addChild(plain16);
        plain16.cubeList.add(new ModelBox(plain16, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain16.cubeList.add(new ModelBox(plain16, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel16 = new RendererModel(this);
        bevel16.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel16, -0.6981F, 0.0F, 0.0F);
        spine10.addChild(bevel16);
        bevel16.cubeList.add(new ModelBox(bevel16, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel16.cubeList.add(new ModelBox(bevel16, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front20 = new RendererModel(this);
        front20.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine10.addChild(front20);
        front20.cubeList.add(new ModelBox(front20, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front20.cubeList.add(new ModelBox(front20, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip16 = new RendererModel(this);
        underlip16.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip16, 0.6109F, 0.0F, 0.0F);
        spine10.addChild(underlip16);
        underlip16.cubeList.add(new ModelBox(underlip16, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip16.cubeList.add(new ModelBox(underlip16, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve20 = new RendererModel(this);
        undercurve20.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve20, 1.0472F, 0.0F, 0.0F);
        spine10.addChild(undercurve20);
        undercurve20.cubeList.add(new ModelBox(undercurve20, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve20.cubeList.add(new ModelBox(undercurve20, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend16 = new RendererModel(this);
        backbend16.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend16, 0.3491F, 0.0F, 0.0F);
        spine10.addChild(backbend16);
        backbend16.cubeList.add(new ModelBox(backbend16, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin16 = new RendererModel(this);
        shin16.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin16, -0.0873F, 0.0F, 0.0F);
        spine10.addChild(shin16);
        shin16.cubeList.add(new ModelBox(shin16, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle16 = new RendererModel(this);
        ankle16.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle16, -0.8727F, 0.0F, 0.0F);
        spine10.addChild(ankle16);
        ankle16.cubeList.add(new ModelBox(ankle16, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot16 = new RendererModel(this);
        foot16.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot16, -1.5708F, 0.0F, 0.0F);
        spine10.addChild(foot16);
        foot16.cubeList.add(new ModelBox(foot16, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot16.cubeList.add(new ModelBox(foot16, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot16.cubeList.add(new ModelBox(foot16, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot16.cubeList.add(new ModelBox(foot16, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        coralribs5 = new RendererModel(this);
        coralribs5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(coralribs5, 0.0F, 2.0944F, 0.0F);
        stations.addChild(coralribs5);

        panel11 = new RendererModel(this);
        panel11.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs5.addChild(panel11);

        front21 = new RendererModel(this);
        front21.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel11.addChild(front21);
        front21.cubeList.add(new ModelBox(front21, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front21.cubeList.add(new ModelBox(front21, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve21 = new RendererModel(this);
        undercurve21.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve21, 1.0472F, 0.0F, 0.0F);
        panel11.addChild(undercurve21);
        undercurve21.cubeList.add(new ModelBox(undercurve21, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine11 = new RendererModel(this);
        spine11.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine11, 0.0F, -0.5236F, 0.0F);
        coralribs5.addChild(spine11);

        rib11 = new RendererModel(this);
        rib11.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib11, 0.0F, 0.5236F, 0.0F);
        spine11.addChild(rib11);
        rib11.cubeList.add(new ModelBox(rib11, 156, 135, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 163, 133, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib11.cubeList.add(new ModelBox(rib11, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone28 = new RendererModel(this);
        bone28.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone28, -0.3491F, 0.0F, 0.0F);
        rib11.addChild(bone28);
        bone28.cubeList.add(new ModelBox(bone28, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone28.cubeList.add(new ModelBox(bone28, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone28.cubeList.add(new ModelBox(bone28, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone28.cubeList.add(new ModelBox(bone28, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone29 = new RendererModel(this);
        bone29.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone29, -0.3491F, 0.0F, 0.0F);
        rib11.addChild(bone29);
        bone29.cubeList.add(new ModelBox(bone29, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain17 = new RendererModel(this);
        plain17.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain17, -1.1345F, 0.0F, 0.0F);
        spine11.addChild(plain17);
        plain17.cubeList.add(new ModelBox(plain17, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain17.cubeList.add(new ModelBox(plain17, 243, 38, -6.0F, -25.4559F, -5.7139F, 12, 19, 8, 0.0F, false));
        plain17.cubeList.add(new ModelBox(plain17, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel17 = new RendererModel(this);
        bevel17.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel17, -0.6981F, 0.0F, 0.0F);
        spine11.addChild(bevel17);
        bevel17.cubeList.add(new ModelBox(bevel17, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel17.cubeList.add(new ModelBox(bevel17, 243, 54, -6.0F, -26.8097F, 3.5398F, 12, 12, 8, 0.0F, false));
        bevel17.cubeList.add(new ModelBox(bevel17, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front22 = new RendererModel(this);
        front22.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine11.addChild(front22);
        front22.cubeList.add(new ModelBox(front22, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front22.cubeList.add(new ModelBox(front22, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip17 = new RendererModel(this);
        underlip17.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip17, 0.6109F, 0.0F, 0.0F);
        spine11.addChild(underlip17);
        underlip17.cubeList.add(new ModelBox(underlip17, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip17.cubeList.add(new ModelBox(underlip17, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve22 = new RendererModel(this);
        undercurve22.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve22, 1.0472F, 0.0F, 0.0F);
        spine11.addChild(undercurve22);
        undercurve22.cubeList.add(new ModelBox(undercurve22, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve22.cubeList.add(new ModelBox(undercurve22, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend17 = new RendererModel(this);
        backbend17.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend17, 0.3491F, 0.0F, 0.0F);
        spine11.addChild(backbend17);
        backbend17.cubeList.add(new ModelBox(backbend17, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin17 = new RendererModel(this);
        shin17.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin17, -0.0873F, 0.0F, 0.0F);
        spine11.addChild(shin17);
        shin17.cubeList.add(new ModelBox(shin17, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle17 = new RendererModel(this);
        ankle17.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle17, -0.8727F, 0.0F, 0.0F);
        spine11.addChild(ankle17);
        ankle17.cubeList.add(new ModelBox(ankle17, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot17 = new RendererModel(this);
        foot17.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot17, -1.5708F, 0.0F, 0.0F);
        spine11.addChild(foot17);
        foot17.cubeList.add(new ModelBox(foot17, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot17.cubeList.add(new ModelBox(foot17, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot17.cubeList.add(new ModelBox(foot17, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot17.cubeList.add(new ModelBox(foot17, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        coralribs6 = new RendererModel(this);
        coralribs6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(coralribs6, 0.0F, 1.0472F, 0.0F);
        stations.addChild(coralribs6);

        panel12 = new RendererModel(this);
        panel12.setRotationPoint(0.0F, 0.0F, 0.0F);
        coralribs6.addChild(panel12);

        front23 = new RendererModel(this);
        front23.setRotationPoint(0.0F, -50.0F, -64.0F);
        panel12.addChild(front23);
        front23.cubeList.add(new ModelBox(front23, 160, 96, -29.25F, -1.1759F, -5.5795F, 58, 9, 1, 0.0F, false));
        front23.cubeList.add(new ModelBox(front23, 161, 74, -29.5F, -0.4259F, -6.0795F, 59, 4, 1, 0.0F, false));

        undercurve23 = new RendererModel(this);
        undercurve23.setRotationPoint(0.0F, -34.0452F, -54.8586F);
        setRotationAngle(undercurve23, 1.0472F, 0.0F, 0.0F);
        panel12.addChild(undercurve23);
        undercurve23.cubeList.add(new ModelBox(undercurve23, 160, 124, -28.0F, -7.1975F, -5.7981F, 56, 16, 8, 0.0F, false));

        spine12 = new RendererModel(this);
        spine12.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(spine12, 0.0F, -0.5236F, 0.0F);
        coralribs6.addChild(spine12);

        rib12 = new RendererModel(this);
        rib12.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(rib12, 0.0F, 0.5236F, 0.0F);
        spine12.addChild(rib12);
        rib12.cubeList.add(new ModelBox(rib12, 156, 136, -24.0F, -11.0F, -50.0F, 48, 4, 12, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 162, 137, -22.0F, -22.0F, -51.0F, 44, 4, 12, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 176, 17, -9.8534F, -103.7731F, -35.8783F, 19, 3, 10, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 154, 132, -2.9874F, -77.7731F, -30.218F, 6, 8, 6, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 178, 61, -6.8534F, -100.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 206, 47, -6.8534F, -106.7731F, -30.8783F, 14, 3, 5, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 182, 48, -6.8534F, -112.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 186, 58, -6.8534F, -116.7731F, -28.8783F, 14, 3, 5, 0.0F, false));
        rib12.cubeList.add(new ModelBox(rib12, 173, 7, -6.8534F, -114.7731F, -30.8783F, 14, 3, 7, 0.0F, false));

        bone30 = new RendererModel(this);
        bone30.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
        setRotationAngle(bone30, -0.3491F, 0.0F, 0.0F);
        rib12.addChild(bone30);
        bone30.cubeList.add(new ModelBox(bone30, 236, 134, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));
        bone30.cubeList.add(new ModelBox(bone30, 206, 138, -3.5F, -1.5F, -1.5F, 6, 5, 4, 0.0F, false));
        bone30.cubeList.add(new ModelBox(bone30, 208, 68, -8.5F, -6.8474F, -1.7798F, 15, 3, 4, 0.0F, false));
        bone30.cubeList.add(new ModelBox(bone30, 166, 60, -8.5F, -11.5459F, -3.4899F, 15, 3, 4, 0.0F, false));

        bone31 = new RendererModel(this);
        bone31.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
        setRotationAngle(bone31, -0.3491F, 0.0F, 0.0F);
        rib12.addChild(bone31);
        bone31.cubeList.add(new ModelBox(bone31, 184, 136, 2.5F, -2.0F, -2.0F, 5, 5, 4, 0.0F, false));

        plain18 = new RendererModel(this);
        plain18.setRotationPoint(0.0F, -70.9898F, -52.2791F);
        setRotationAngle(plain18, -1.1345F, 0.0F, 0.0F);
        spine12.addChild(plain18);
        plain18.cubeList.add(new ModelBox(plain18, 241, 38, -8.0F, -22.1688F, -1.0089F, 16, 40, 8, 0.0F, false));
        plain18.cubeList.add(new ModelBox(plain18, 173, 36, -10.0F, -23.0751F, -0.5863F, 20, 40, 12, 0.0F, false));

        bevel18 = new RendererModel(this);
        bevel18.setRotationPoint(0.0F, -60.6347F, -68.7874F);
        setRotationAngle(bevel18, -0.6981F, 0.0F, 0.0F);
        spine12.addChild(bevel18);
        bevel18.cubeList.add(new ModelBox(bevel18, 241, 72, -8.0F, -2.8097F, -2.4602F, 16, 12, 8, 0.0F, false));
        bevel18.cubeList.add(new ModelBox(bevel18, 204, 72, -10.0F, -3.4524F, -1.6941F, 20, 12, 11, 0.0F, false));

        front24 = new RendererModel(this);
        front24.setRotationPoint(0.0F, -50.0F, -72.0F);
        spine12.addChild(front24);
        front24.cubeList.add(new ModelBox(front24, 194, 52, -10.0F, -43.2731F, 40.3863F, 20, 16, 4, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 203, 12, -11.0F, -54.2731F, 35.3863F, 22, 4, 13, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 52, -10.0F, -51.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 52, -10.0F, -57.2731F, 41.3863F, 20, 4, 7, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 52, -10.0F, -63.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 52, -10.0F, -67.2731F, 43.3863F, 20, 4, 7, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 208, 10, -11.0F, -65.2731F, 41.3863F, 22, 4, 9, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 52, -13.0F, -25.7731F, 42.3863F, 23, 9, 8, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 245, 53, -8.0F, -44.2731F, 38.3863F, 16, 16, 4, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 241, 76, -8.0F, -5.1759F, -4.5795F, 16, 16, 8, 0.0F, false));
        front24.cubeList.add(new ModelBox(front24, 194, 73, -10.0F, -5.1759F, -3.5795F, 20, 16, 13, 0.0F, false));

        underlip18 = new RendererModel(this);
        underlip18.setRotationPoint(0.0F, -43.7571F, -72.7704F);
        setRotationAngle(underlip18, 0.6109F, 0.0F, 0.0F);
        spine12.addChild(underlip18);
        underlip18.cubeList.add(new ModelBox(underlip18, 246, 80, -8.0F, 1.5679F, -5.7479F, 16, 8, 8, 0.0F, false));
        underlip18.cubeList.add(new ModelBox(underlip18, 196, 128, -12.0F, 1.1453F, -6.8416F, 24, 8, 8, 0.0F, false));

        undercurve24 = new RendererModel(this);
        undercurve24.setRotationPoint(0.0F, -34.0452F, -62.8586F);
        setRotationAngle(undercurve24, 1.0472F, 0.0F, 0.0F);
        spine12.addChild(undercurve24);
        undercurve24.cubeList.add(new ModelBox(undercurve24, 241, 52, -8.0F, -7.1975F, -5.7981F, 16, 16, 8, 0.0F, false));
        undercurve24.cubeList.add(new ModelBox(undercurve24, 196, 52, -10.0F, -7.1975F, -4.7981F, 20, 29, 11, 0.0F, false));

        backbend18 = new RendererModel(this);
        backbend18.setRotationPoint(0.0F, -20.714F, -51.1042F);
        setRotationAngle(backbend18, 0.3491F, 0.0F, 0.0F);
        spine12.addChild(backbend18);
        backbend18.cubeList.add(new ModelBox(backbend18, 241, 66, -8.0F, -10.0774F, -5.2695F, 16, 12, 8, 0.0F, false));

        shin18 = new RendererModel(this);
        shin18.setRotationPoint(0.0F, -15.9761F, -51.8165F);
        setRotationAngle(shin18, -0.0873F, 0.0F, 0.0F);
        spine12.addChild(shin18);
        shin18.cubeList.add(new ModelBox(shin18, 240, 61, -8.0F, -3.8125F, -3.6666F, 16, 11, 9, 0.0F, false));

        ankle18 = new RendererModel(this);
        ankle18.setRotationPoint(0.0F, -1.3682F, -55.2239F);
        setRotationAngle(ankle18, -0.8727F, 0.0F, 0.0F);
        spine12.addChild(ankle18);
        ankle18.cubeList.add(new ModelBox(ankle18, 245, 75, -8.0F, -8.325F, -6.5104F, 16, 13, 4, 0.0F, false));

        foot18 = new RendererModel(this);
        foot18.setRotationPoint(0.0F, 0.0068F, -64.2239F);
        setRotationAngle(foot18, -1.5708F, 0.0F, 0.0F);
        spine12.addChild(foot18);
        foot18.cubeList.add(new ModelBox(foot18, 244, 79, -9.0F, -6.0F, -4.0F, 18, 20, 4, 0.0F, false));
        foot18.cubeList.add(new ModelBox(foot18, 301, 73, -1.25F, 9.0F, -5.0F, 3, 3, 4, 0.0F, false));
        foot18.cubeList.add(new ModelBox(foot18, 303, 74, -7.0F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));
        foot18.cubeList.add(new ModelBox(foot18, 298, 81, 4.5F, 0.5F, -5.0F, 3, 3, 4, 0.0F, false));

        controls = new RendererModel(this);
        controls.setRotationPoint(0.0F, 24.0F, 0.0F);

        panels = new RendererModel(this);
        panels.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(panels);

        north = new RendererModel(this);
        north.setRotationPoint(0.0F, 0.0F, 0.0F);
        panels.addChild(north);

        ctrlset_6 = new RendererModel(this);
        ctrlset_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        north.addChild(ctrlset_6);

        top_6 = new RendererModel(this);
        top_6.setRotationPoint(4.0F, -72.8333F, -39.5F);
        setRotationAngle(top_6, 0.4363F, 0.0F, 0.0F);
        ctrlset_6.addChild(top_6);
        top_6.cubeList.add(new ModelBox(top_6, 226, 196, -4.0F, -3.6667F, -3.5F, 2, 4, 7, 0.0F, false));
        top_6.cubeList.add(new ModelBox(top_6, 231, 197, -2.0F, -3.6667F, -3.0F, 1, 4, 6, 0.0F, false));
        top_6.cubeList.add(new ModelBox(top_6, 231, 147, -6.0F, -3.9167F, -4.0F, 2, 4, 8, 0.0F, false));
        top_6.cubeList.add(new ModelBox(top_6, 226, 192, -8.0F, -3.6667F, -3.5F, 2, 4, 7, 0.0F, false));
        top_6.cubeList.add(new ModelBox(top_6, 229, 205, -9.0F, -3.6667F, -3.0F, 1, 4, 6, 0.0F, false));

        telepathics = new RendererModel(this);
        telepathics.setRotationPoint(-0.5F, -67.0F, -57.0F);
        setRotationAngle(telepathics, 0.4363F, 0.0F, 0.0F);
        ctrlset_6.addChild(telepathics);
        telepathics.cubeList.add(new ModelBox(telepathics, 68, 139, -9.5F, 0.25F, -1.0F, 19, 2, 2, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, -10.5F, 0.375F, -2.0F, 21, 2, 15, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, -5.5F, 0.375F, 13.0F, 10, 2, 10, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, 10.5F, 0.375F, 0.0F, 3, 2, 11, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, -13.5F, 0.375F, 0.0F, 3, 2, 11, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, 13.5F, 0.375F, 3.0F, 2, 2, 5, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 212, 147, -15.5F, 0.375F, 3.0F, 2, 2, 5, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 27, 142, -12.5F, 0.25F, 1.75F, 25, 2, 2, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 52, 140, -14.5F, 0.25F, 4.5F, 29, 2, 2, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 38, 144, -12.5F, 0.25F, 7.25F, 25, 2, 2, 0.0F, false));
        telepathics.cubeList.add(new ModelBox(telepathics, 41, 143, -9.5F, 0.25F, 9.75F, 19, 2, 2, 0.0F, false));

        rim_6 = new RendererModel(this);
        rim_6.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_6, 0.8727F, 0.0F, 0.0F);
        ctrlset_6.addChild(rim_6);
        rim_6.cubeList.add(new ModelBox(rim_6, 317, 191, -19.75F, 0.0F, -4.5F, 24, 11, 8, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 317, 191, -19.25F, -0.75F, -4.25F, 23, 1, 1, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 317, 191, -19.25F, -0.5F, 2.0F, 23, 1, 1, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -19.25F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -16.25F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -13.25F, -0.75F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -10.25F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -7.25F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -4.25F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -1.25F, -0.5F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, 1.75F, -1.0F, -0.5F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -19.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -16.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -13.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -10.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -7.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -4.25F, -0.5F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, -1.25F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));
        rim_6.cubeList.add(new ModelBox(rim_6, 339, 129, 1.75F, -1.0F, -3.0F, 2, 2, 2, 0.0F, false));

        keypad_b6 = new RendererModel(this);
        keypad_b6.setRotationPoint(39.5F, 63.0F, 74.0F);
        rim_6.addChild(keypad_b6);
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 218, 136, -31.0F, -64.75F, -81.5F, 10, 3, 13, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -24.5F, -65.25F, -80.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -24.5F, -65.25F, -77.75F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -24.5F, -65.25F, -75.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -24.5F, -65.25F, -72.75F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -29.5F, -65.25F, -80.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -29.5F, -65.25F, -77.75F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -29.5F, -65.25F, -75.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -29.5F, -65.25F, -72.75F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -27.0F, -65.25F, -80.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -27.0F, -65.25F, -77.75F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -27.0F, -65.25F, -75.25F, 2, 2, 2, 0.0F, false));
        keypad_b6.cubeList.add(new ModelBox(keypad_b6, 148, 206, -27.0F, -65.25F, -72.75F, 2, 2, 2, 0.0F, false));

        siderib_6 = new RendererModel(this);
        siderib_6.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_6, 0.4363F, -0.5236F, 0.0F);
        ctrlset_6.addChild(siderib_6);

        northwest = new RendererModel(this);
        northwest.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(northwest, 0.0F, -1.0472F, 0.0F);
        panels.addChild(northwest);

        ctrlset_2 = new RendererModel(this);
        ctrlset_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        northwest.addChild(ctrlset_2);

        top_2 = new RendererModel(this);
        top_2.setRotationPoint(-0.5F, -73.5F, -44.5F);
        setRotationAngle(top_2, 0.4363F, 0.0F, 0.0F);
        ctrlset_2.addChild(top_2);

        fusebox = new RendererModel(this);
        fusebox.setRotationPoint(0.5F, 73.5F, 44.5F);
        top_2.addChild(fusebox);
        fusebox.cubeList.add(new ModelBox(fusebox, 359, 72, -9.0F, -75.5F, -50.0F, 12, 4, 6, 0.0F, false));
        fusebox.cubeList.add(new ModelBox(fusebox, 359, 72, -2.0F, -76.5F, -46.0F, 3, 4, 4, 0.0F, false));

        button_trip = new RendererModel(this);
        button_trip.setRotationPoint(0.0F, 0.0F, 0.0F);
        fusebox.addChild(button_trip);
        button_trip.cubeList.add(new ModelBox(button_trip, 354, 188, -1.75F, -76.0F, -49.0F, 2, 1, 2, 0.0F, false));
        button_trip.cubeList.add(new ModelBox(button_trip, 354, 188, -4.75F, -76.0F, -49.0F, 2, 1, 2, 0.0F, false));
        button_trip.cubeList.add(new ModelBox(button_trip, 354, 188, -7.75F, -76.0F, -49.0F, 2, 1, 2, 0.0F, false));

        lamp = new RendererModel(this);
        lamp.setRotationPoint(0.5F, 73.5F, 44.5F);
        top_2.addChild(lamp);
        lamp.cubeList.add(new ModelBox(lamp, 192, 194, -8.5F, -78.5F, -39.75F, 4, 6, 4, 0.0F, false));
        lamp.cubeList.add(new ModelBox(lamp, 341, 175, -9.0F, -73.5F, -40.0F, 5, 2, 5, 0.0F, false));
        lamp.cubeList.add(new ModelBox(lamp, 341, 175, -9.0F, -75.0F, -40.0F, 5, 1, 5, 0.0F, false));
        lamp.cubeList.add(new ModelBox(lamp, 341, 175, -9.0F, -76.5F, -40.0F, 5, 1, 5, 0.0F, false));

        tube = new RendererModel(this);
        tube.setRotationPoint(0.0F, 0.0F, 0.0F);
        lamp.addChild(tube);
        tube.cubeList.add(new ModelBox(tube, 192, 194, -5.5F, -75.5F, -34.75F, 4, 2, 2, 0.0F, false));
        tube.cubeList.add(new ModelBox(tube, 192, 194, -7.5F, -75.5F, -35.75F, 2, 3, 3, 0.0F, false));
        tube.cubeList.add(new ModelBox(tube, 192, 194, -1.5F, -75.5F, -44.75F, 2, 2, 12, 0.0F, false));

        mid_2 = new RendererModel(this);
        mid_2.setRotationPoint(-0.5F, -67.0F, -57.0F);
        setRotationAngle(mid_2, 0.4363F, 0.0F, 0.0F);
        ctrlset_2.addChild(mid_2);

        wires_2 = new RendererModel(this);
        wires_2.setRotationPoint(0.5F, 74.0F, 66.75F);
        mid_2.addChild(wires_2);
        wires_2.cubeList.add(new ModelBox(wires_2, 345, 166, 10.5F, -74.25F, -56.75F, 3, 2, 3, 0.0F, false));
        wires_2.cubeList.add(new ModelBox(wires_2, 207, 134, 11.5F, -73.75F, -73.0F, 1, 2, 17, 0.0F, false));
        wires_2.cubeList.add(new ModelBox(wires_2, 203, 133, -17.0F, -74.5F, -55.75F, 8, 2, 1, 0.0F, false));
        wires_2.cubeList.add(new ModelBox(wires_2, 203, 133, -1.0F, -74.5F, -69.75F, 10, 2, 1, 0.0F, false));

        gauge = new RendererModel(this);
        gauge.setRotationPoint(-1.5F, 68.75F, 57.5F);
        mid_2.addChild(gauge);
        gauge.cubeList.add(new ModelBox(gauge, 345, 166, 10.25F, -70.5F, -61.5F, 8, 4, 8, 0.0F, false));
        gauge.cubeList.add(new ModelBox(gauge, 334, 122, 12.25F, -70.75F, -60.5F, 4, 1, 6, 0.0F, false));
        gauge.cubeList.add(new ModelBox(gauge, 334, 122, 11.25F, -70.75F, -60.0F, 1, 1, 5, 0.0F, false));
        gauge.cubeList.add(new ModelBox(gauge, 334, 122, 16.25F, -70.75F, -60.0F, 1, 1, 5, 0.0F, false));

        rim_2 = new RendererModel(this);
        rim_2.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_2, 0.8727F, 0.0F, 0.0F);
        ctrlset_2.addChild(rim_2);

        button_switch = new RendererModel(this);
        button_switch.setRotationPoint(39.5F, 65.0F, 74.0F);
        rim_2.addChild(button_switch);
        button_switch.cubeList.add(new ModelBox(button_switch, 327, 180, -28.5F, -66.25F, -73.0F, 3, 2, 3, 0.0F, false));
        button_switch.cubeList.add(new ModelBox(button_switch, 327, 180, -29.0F, -65.5F, -73.5F, 4, 2, 4, 0.0F, false));
        button_switch.cubeList.add(new ModelBox(button_switch, 263, 195, -28.0F, -66.75F, -72.5F, 2, 2, 2, 0.0F, false));

        coms = new RendererModel(this);
        coms.setRotationPoint(34.5F, 65.0F, 74.0F);
        rim_2.addChild(coms);

        phone_base = new RendererModel(this);
        phone_base.setRotationPoint(0.0F, 0.0F, 0.0F);
        coms.addChild(phone_base);
        phone_base.cubeList.add(new ModelBox(phone_base, 330, 125, -46.75F, -65.75F, -79.0F, 12, 4, 15, 0.0F, false));
        phone_base.cubeList.add(new ModelBox(phone_base, 330, 125, -46.375F, -67.75F, -69.625F, 5, 4, 1, 0.0F, false));

        handset = new RendererModel(this);
        handset.setRotationPoint(0.0F, 0.0F, 0.0F);
        coms.addChild(handset);
        handset.cubeList.add(new ModelBox(handset, 330, 125, -45.5F, -69.0F, -77.5F, 3, 1, 12, 0.0F, false));
        handset.cubeList.add(new ModelBox(handset, 320, 132, -46.0F, -68.75F, -78.5F, 4, 3, 4, 0.0F, false));
        handset.cubeList.add(new ModelBox(handset, 330, 125, -46.0F, -68.75F, -68.5F, 4, 3, 4, 0.0F, false));

        cord = new RendererModel(this);
        cord.setRotationPoint(-6.25F, -1.0F, -5.25F);
        handset.addChild(cord);

        bone15 = new RendererModel(this);
        bone15.setRotationPoint(0.0F, 0.0F, -3.25F);
        cord.addChild(bone15);
        bone15.cubeList.add(new ModelBox(bone15, 216, 145, -38.25F, -66.25F, -71.75F, 1, 1, 2, 0.0F, false));
        bone15.cubeList.add(new ModelBox(bone15, 216, 145, -39.25F, -66.25F, -72.75F, 2, 2, 1, 0.0F, false));
        bone15.cubeList.add(new ModelBox(bone15, 216, 145, -39.25F, -66.25F, -74.0F, 2, 2, 1, 0.0F, false));

        bone16 = new RendererModel(this);
        bone16.setRotationPoint(-37.8333F, -64.1667F, -78.1667F);
        setRotationAngle(bone16, 0.6981F, 0.0F, 0.0F);
        cord.addChild(bone16);
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -0.5833F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -3.0833F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -5.5833F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -1.8333F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -4.3333F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -1.1667F, -0.8333F, -6.8333F, 2, 2, 1, 0.0F, false));
        bone16.cubeList.add(new ModelBox(bone16, 216, 145, -0.4167F, -0.3333F, -5.8333F, 1, 1, 8, 0.0F, false));

        phone_grill = new RendererModel(this);
        phone_grill.setRotationPoint(0.0F, 0.0F, 0.0F);
        coms.addChild(phone_grill);
        phone_grill.cubeList.add(new ModelBox(phone_grill, 216, 145, -39.5F, -66.25F, -77.5F, 1, 1, 4, 0.0F, false));
        phone_grill.cubeList.add(new ModelBox(phone_grill, 216, 145, -38.25F, -66.25F, -78.0F, 1, 1, 5, 0.0F, false));
        phone_grill.cubeList.add(new ModelBox(phone_grill, 216, 145, -37.0F, -66.25F, -77.5F, 1, 1, 4, 0.0F, false));

        phone_keys = new RendererModel(this);
        phone_keys.setRotationPoint(0.0F, 0.0F, 0.0F);
        coms.addChild(phone_keys);
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -37.0F, -66.75F, -67.5F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -37.0F, -66.75F, -69.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -37.0F, -66.75F, -70.5F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -38.25F, -66.75F, -67.5F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -38.25F, -66.75F, -69.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -38.25F, -66.75F, -70.5F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -38.25F, -66.75F, -72.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -37.0F, -66.75F, -72.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -39.5F, -66.75F, -72.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -39.5F, -66.75F, -67.5F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -39.5F, -66.75F, -69.0F, 1, 1, 1, 0.0F, false));
        phone_keys.cubeList.add(new ModelBox(phone_keys, 306, 147, -39.5F, -66.75F, -70.5F, 1, 1, 1, 0.0F, false));

        siderib_2 = new RendererModel(this);
        siderib_2.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_2, 0.4363F, -0.5236F, 0.0F);
        ctrlset_2.addChild(siderib_2);

        northeast = new RendererModel(this);
        northeast.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(northeast, 0.0F, 1.0472F, 0.0F);
        panels.addChild(northeast);

        ctrlset_3 = new RendererModel(this);
        ctrlset_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        northeast.addChild(ctrlset_3);

        top_3 = new RendererModel(this);
        top_3.setRotationPoint(-0.5F, -73.5F, -44.5F);
        setRotationAngle(top_3, 0.4363F, 0.0F, 0.0F);
        ctrlset_3.addChild(top_3);
        top_3.cubeList.add(new ModelBox(top_3, 359, 72, -7.5F, -5.0F, 4.5F, 7, 6, 2, 0.0F, false));
        top_3.cubeList.add(new ModelBox(top_3, 359, 72, -7.5F, -5.0F, -11.0F, 7, 6, 2, 0.0F, false));
        top_3.cubeList.add(new ModelBox(top_3, 324, 109, -6.5F, -3.75F, -10.5F, 5, 4, 15, 0.0F, false));
        top_3.cubeList.add(new ModelBox(top_3, 324, 109, -5.5F, -3.75F, -12.5F, 3, 3, 2, 0.0F, false));

        mid_3 = new RendererModel(this);
        mid_3.setRotationPoint(-0.5F, -67.0F, -57.0F);
        setRotationAngle(mid_3, 0.4363F, 0.0F, 0.0F);
        ctrlset_3.addChild(mid_3);
        mid_3.cubeList.add(new ModelBox(mid_3, 377, 77, -7.5F, -0.5F, -1.0F, 7, 1, 4, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 367, 173, -12.75F, -0.5F, -5.0F, 3, 1, 3, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 367, 173, 5.0F, -0.5F, -3.0F, 3, 1, 3, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 367, 173, 5.0F, -0.5F, 5.0F, 3, 1, 3, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 367, 173, 5.0F, -0.5F, 1.0F, 3, 1, 3, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 353, 62, 9.0F, -1.5F, -3.0F, 7, 2, 11, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 14.5F, -0.5F, 1.0F, 7, 1, 1, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 8.0F, -0.5F, 2.0F, 1, 1, 1, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 8.0F, -0.5F, -2.0F, 1, 1, 1, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 8.0F, -0.5F, 6.0F, 1, 1, 1, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 2.5F, -0.5F, 15.0F, 12, 1, 1, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, 2.5F, -0.5F, 16.0F, 1, 1, 12, 0.0F, false));
        mid_3.cubeList.add(new ModelBox(mid_3, 219, 142, -4.5F, -0.5F, 16.0F, 1, 1, 12, 0.0F, false));

        bone34 = new RendererModel(this);
        bone34.setRotationPoint(0.5F, 71.0F, 66.0F);
        mid_3.addChild(bone34);
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 9.0F, -74.5F, -60.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 9.0F, -74.5F, -68.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 9.0F, -74.5F, -62.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 9.0F, -74.5F, -66.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 9.0F, -74.5F, -64.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 14.0F, -74.5F, -60.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 14.0F, -74.5F, -68.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 14.0F, -74.5F, -62.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 14.0F, -74.5F, -66.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 322, 206, 14.0F, -74.5F, -64.0F, 1, 2, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 252, 185, 10.0F, -74.5F, -60.0F, 4, 1, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 252, 185, 10.0F, -74.5F, -68.0F, 4, 1, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 350, 193, 10.0F, -74.5F, -62.0F, 4, 1, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 269, 208, 10.0F, -74.5F, -66.0F, 4, 1, 1, 0.0F, false));
        bone34.cubeList.add(new ModelBox(bone34, 252, 185, 10.0F, -74.5F, -64.0F, 4, 1, 1, 0.0F, false));

        rim_3 = new RendererModel(this);
        rim_3.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_3, 0.8727F, 0.0F, 0.0F);
        ctrlset_3.addChild(rim_3);

        keyboard = new RendererModel(this);
        keyboard.setRotationPoint(39.5F, 65.0F, 74.0F);
        rim_3.addChild(keyboard);
        keyboard.cubeList.add(new ModelBox(keyboard, 345, 202, -37.5F, -65.25F, -79.5F, 20, 8, 10, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -35.875F, -66.25F, -78.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -20.875F, -66.25F, -78.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -20.875F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -30.375F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -23.625F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -33.125F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -32.875F, -66.25F, -78.5F, 11, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -26.375F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -35.875F, -66.25F, -75.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -20.875F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -30.375F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -23.625F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -33.125F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -26.375F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));
        keyboard.cubeList.add(new ModelBox(keyboard, 347, 65, -35.875F, -66.25F, -72.5F, 2, 1, 2, 0.0F, false));

        randomizer = new RendererModel(this);
        randomizer.setRotationPoint(2.9661F, 81.4F, 47.285F);
        rim_3.addChild(randomizer);
        randomizer.cubeList.add(new ModelBox(randomizer, 309, 47, -19.0F, -81.5F, -52.0F, 9, 1, 9, 0.0F, false));

        spinner = new RendererModel(this);
        spinner.setRotationPoint(-14.2797F, -85.39F, -47.371F);
        randomizer.addChild(spinner);
        spinner.cubeList.add(new ModelBox(spinner, 296, 128, -3.7203F, -0.11F, -3.629F, 7, 4, 7, 0.0F, false));
        spinner.cubeList.add(new ModelBox(spinner, 296, 128, -3.2203F, -1.36F, -3.129F, 6, 2, 6, 0.0F, false));

        bone17 = new RendererModel(this);
        bone17.setRotationPoint(14.2797F, 85.39F, 47.371F);
        spinner.addChild(bone17);
        bone17.cubeList.add(new ModelBox(bone17, 296, 128, -17.0F, -84.5F, -54.0F, 5, 3, 3, 0.0F, false));

        bone18 = new RendererModel(this);
        bone18.setRotationPoint(-0.2203F, 2.39F, -0.379F);
        setRotationAngle(bone18, 0.0F, -2.0944F, 0.0F);
        spinner.addChild(bone18);
        bone18.cubeList.add(new ModelBox(bone18, 296, 128, -2.5F, -1.5F, -6.25F, 5, 3, 5, 0.0F, false));

        bone19 = new RendererModel(this);
        bone19.setRotationPoint(-0.2203F, 2.39F, -0.379F);
        setRotationAngle(bone19, 0.0F, 2.0944F, 0.0F);
        spinner.addChild(bone19);
        bone19.cubeList.add(new ModelBox(bone19, 296, 128, -2.5F, -1.5F, -6.25F, 5, 3, 4, 0.0F, false));

        siderib_3 = new RendererModel(this);
        siderib_3.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_3, 0.4363F, -0.5236F, 0.0F);
        ctrlset_3.addChild(siderib_3);

        south = new RendererModel(this);
        south.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(south, 0.0F, 3.1416F, 0.0F);
        panels.addChild(south);

        ctrlset_4 = new RendererModel(this);
        ctrlset_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        south.addChild(ctrlset_4);

        top_4 = new RendererModel(this);
        top_4.setRotationPoint(-0.5F, -73.5F, -44.5F);
        setRotationAngle(top_4, 0.4363F, 0.0F, 0.0F);
        ctrlset_4.addChild(top_4);
        top_4.cubeList.add(new ModelBox(top_4, 368, 187, -3.5F, 0.0F, -0.5F, 4, 2, 4, 0.0F, false));
        top_4.cubeList.add(new ModelBox(top_4, 263, 204, -3.0F, -1.0F, -0.25F, 3, 2, 3, 0.0F, false));
        top_4.cubeList.add(new ModelBox(top_4, 234, 149, -2.5F, 1.0F, 3.5F, 1, 1, 10, 0.0F, false));

        mid_4 = new RendererModel(this);
        mid_4.setRotationPoint(2.5F, -67.0F, -57.0F);
        setRotationAngle(mid_4, 0.4363F, 0.0F, 0.0F);
        ctrlset_4.addChild(mid_4);

        spin_glass_x = new RendererModel(this);
        spin_glass_x.setRotationPoint(2.5F, -4.5F, -0.5F);
        setRotationAngle(spin_glass_x, 1.309F, 0.0F, 0.0F);
        mid_4.addChild(spin_glass_x);
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, -8.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F, false));
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, 5.0F, -2.0F, -3.0F, 2, 4, 4, 0.0F, false));
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, -7.0F, -2.0F, -1.25F, 2, 4, 4, 0.0F, false));
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, -1.0F, -2.0F, -2.0F, 2, 4, 4, 0.0F, false));
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, 1.5F, -3.0F, -1.75F, 3, 5, 5, 0.0F, false));
        spin_glass_x.cubeList.add(new ModelBox(spin_glass_x, 89, 142, -4.5F, -3.0F, -2.75F, 3, 5, 5, 0.0F, false));

        base_4 = new RendererModel(this);
        base_4.setRotationPoint(0.5F, 73.0F, 65.0F);
        mid_4.addChild(base_4);
        base_4.cubeList.add(new ModelBox(base_4, 320, 60, -9.0F, -73.75F, -72.0F, 22, 5, 11, 0.0F, false));
        base_4.cubeList.add(new ModelBox(base_4, 320, 60, -8.0F, -79.75F, -68.0F, 2, 6, 5, 0.0F, false));
        base_4.cubeList.add(new ModelBox(base_4, 320, 60, 10.0F, -79.75F, -68.0F, 2, 6, 5, 0.0F, false));

        rim_4 = new RendererModel(this);
        rim_4.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_4, 0.8727F, 0.0F, 0.0F);
        ctrlset_4.addChild(rim_4);

        keypad_a4 = new RendererModel(this);
        keypad_a4.setRotationPoint(3.5F, 65.0F, 74.0F);
        rim_4.addChild(keypad_a4);
        keypad_a4.cubeList.add(new ModelBox(keypad_a4, 218, 136, -3.0F, -65.75F, -78.5F, 2, 3, 8, 0.0F, false));
        keypad_a4.cubeList.add(new ModelBox(keypad_a4, 337, 198, -4.5F, -66.75F, -82.5F, 4, 7, 5, 0.0F, false));
        keypad_a4.cubeList.add(new ModelBox(keypad_a4, 337, 198, 0.75F, -66.75F, -82.5F, 4, 7, 5, 0.0F, false));
        keypad_a4.cubeList.add(new ModelBox(keypad_a4, 317, 206, -5.375F, -66.5F, -82.0F, 11, 6, 4, 0.0F, false));
        keypad_a4.cubeList.add(new ModelBox(keypad_a4, 218, 136, 1.0F, -65.75F, -78.5F, 2, 3, 8, 0.0F, false));

        keypad_b4 = new RendererModel(this);
        keypad_b4.setRotationPoint(39.5F, 65.0F, 74.0F);
        rim_4.addChild(keypad_b4);
        keypad_b4.cubeList.add(new ModelBox(keypad_b4, 336, 172, -22.5F, -65.25F, -79.0F, 4, 2, 4, 0.0F, false));
        keypad_b4.cubeList.add(new ModelBox(keypad_b4, 265, 219, -22.0F, -66.25F, -78.5F, 3, 2, 3, 0.0F, false));

        siderib_4 = new RendererModel(this);
        siderib_4.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_4, 0.4363F, -0.5236F, 0.0F);
        ctrlset_4.addChild(siderib_4);

        snake_wire_4 = new RendererModel(this);
        snake_wire_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        ctrlset_4.addChild(snake_wire_4);

        gear_spin = new RendererModel(this);
        gear_spin.setRotationPoint(-15.0F, -68.0F, -57.25F);
        snake_wire_4.addChild(gear_spin);

        spike_4 = new RendererModel(this);
        spike_4.setRotationPoint(-0.0243F, 0.1842F, 0.0346F);
        setRotationAngle(spike_4, -0.7854F, 0.0F, 0.0F);
        gear_spin.addChild(spike_4);
        spike_4.cubeList.add(new ModelBox(spike_4, 308, 139, -1.5F, -3.0F, -3.0F, 3, 6, 6, 0.0F, false));

        spike_4b = new RendererModel(this);
        spike_4b.setRotationPoint(-0.0243F, 0.1842F, 0.0346F);
        setRotationAngle(spike_4b, -1.5708F, 0.0F, 0.0F);
        gear_spin.addChild(spike_4b);
        spike_4b.cubeList.add(new ModelBox(spike_4b, 308, 139, -1.0F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));

        bend_4 = new RendererModel(this);
        bend_4.setRotationPoint(-14.25F, -71.9216F, -65.3358F);
        snake_wire_4.addChild(bend_4);
        bend_4.cubeList.add(new ModelBox(bend_4, 216, 145, -3.2743F, 3.8558F, 2.6204F, 5, 6, 11, 0.0F, false));
        bend_4.cubeList.add(new ModelBox(bend_4, 216, 145, -3.2743F, 1.8558F, 7.1204F, 5, 2, 2, 0.0F, false));

        slope_4 = new RendererModel(this);
        slope_4.setRotationPoint(-15.0F, -59.75F, -68.5F);
        setRotationAngle(slope_4, -0.7854F, 0.0F, 0.0F);
        snake_wire_4.addChild(slope_4);
        slope_4.cubeList.add(new ModelBox(slope_4, 216, 145, -1.5F, -8.1716F, 1.4142F, 3, 11, 2, 0.0F, false));

        vert_4 = new RendererModel(this);
        vert_4.setRotationPoint(23.75F, 3.5F, 6.5F);
        snake_wire_4.addChild(vert_4);
        vert_4.cubeList.add(new ModelBox(vert_4, 216, 145, -40.25F, -60.25F, -76.0F, 3, 6, 2, 0.0F, false));
        vert_4.cubeList.add(new ModelBox(vert_4, 325, 183, -42.25F, -57.5F, -77.25F, 7, 4, 3, 0.0F, false));
        vert_4.cubeList.add(new ModelBox(vert_4, 61, 142, -41.5F, -56.5F, -78.25F, 2, 2, 1, 0.0F, false));
        vert_4.cubeList.add(new ModelBox(vert_4, 61, 142, -38.25F, -56.5F, -78.25F, 2, 2, 1, 0.0F, false));

        southeast = new RendererModel(this);
        southeast.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(southeast, 0.0F, 2.0944F, 0.0F);
        panels.addChild(southeast);

        ctrlset_1 = new RendererModel(this);
        ctrlset_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        southeast.addChild(ctrlset_1);

        top_1 = new RendererModel(this);
        top_1.setRotationPoint(-0.5F, -73.5F, -44.5F);
        setRotationAngle(top_1, 0.4363F, 0.0F, 0.0F);
        ctrlset_1.addChild(top_1);
        top_1.cubeList.add(new ModelBox(top_1, 359, 72, -15.5F, -1.0F, -11.5F, 4, 4, 5, 0.0F, false));
        top_1.cubeList.add(new ModelBox(top_1, 214, 141, -13.5F, -1.0F, -6.5F, 1, 1, 2, 0.0F, false));
        top_1.cubeList.add(new ModelBox(top_1, 214, 141, 2.0F, -1.0F, -12.5F, 1, 1, 10, 0.0F, false));
        top_1.cubeList.add(new ModelBox(top_1, 214, 141, 0.0F, 0.0F, 2.5F, 1, 1, 11, 0.0F, false));
        top_1.cubeList.add(new ModelBox(top_1, 214, 141, -13.5F, -1.0F, -4.5F, 12, 1, 1, 0.0F, false));
        top_1.cubeList.add(new ModelBox(top_1, 214, 141, -2.5F, -1.0F, -3.5F, 1, 1, 1, 0.0F, false));

        sonic_port = new RendererModel(this);
        sonic_port.setRotationPoint(-1.5F, 73.5F, 47.5F);
        top_1.addChild(sonic_port);
        sonic_port.cubeList.add(new ModelBox(sonic_port, 389, 175, -2.0F, -75.5F, -50.0F, 7, 5, 2, 0.0F, false));
        sonic_port.cubeList.add(new ModelBox(sonic_port, 199, 140, -1.0F, -74.5F, -49.0F, 5, 4, 5, 0.0F, false));
        sonic_port.cubeList.add(new ModelBox(sonic_port, 389, 175, -2.0F, -75.5F, -45.0F, 7, 5, 2, 0.0F, false));
        sonic_port.cubeList.add(new ModelBox(sonic_port, 389, 175, -3.0F, -75.5F, -48.0F, 3, 5, 3, 0.0F, false));
        sonic_port.cubeList.add(new ModelBox(sonic_port, 389, 175, 3.0F, -75.5F, -48.0F, 3, 5, 3, 0.0F, false));

        xyz_cords = new RendererModel(this);
        xyz_cords.setRotationPoint(-0.5F, -67.0F, -57.0F);
        setRotationAngle(xyz_cords, 0.4363F, 0.0F, 0.0F);
        ctrlset_1.addChild(xyz_cords);
        xyz_cords.cubeList.add(new ModelBox(xyz_cords, 320, 60, -10.5F, -0.25F, -6.0F, 23, 3, 10, 0.0F, false));

        inc_x = new RendererModel(this);
        inc_x.setRotationPoint(0.5F, 73.0F, 65.0F);
        xyz_cords.addChild(inc_x);
        inc_x.cubeList.add(new ModelBox(inc_x, 210, 133, -10.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_x.cubeList.add(new ModelBox(inc_x, 210, 133, -10.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));
        inc_x.cubeList.add(new ModelBox(inc_x, 210, 133, -7.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_x.cubeList.add(new ModelBox(inc_x, 210, 133, -7.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));

        inc_y = new RendererModel(this);
        inc_y.setRotationPoint(0.5F, 73.0F, 65.0F);
        xyz_cords.addChild(inc_y);
        inc_y.cubeList.add(new ModelBox(inc_y, 210, 133, -2.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_y.cubeList.add(new ModelBox(inc_y, 210, 133, -2.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));
        inc_y.cubeList.add(new ModelBox(inc_y, 210, 133, 1.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_y.cubeList.add(new ModelBox(inc_y, 210, 133, 1.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));

        inc_z = new RendererModel(this);
        inc_z.setRotationPoint(0.5F, 73.0F, 65.0F);
        xyz_cords.addChild(inc_z);
        inc_z.cubeList.add(new ModelBox(inc_z, 210, 133, 6.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_z.cubeList.add(new ModelBox(inc_z, 210, 133, 6.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));
        inc_z.cubeList.add(new ModelBox(inc_z, 210, 133, 9.0F, -73.75F, -64.0F, 2, 2, 2, 0.0F, false));
        inc_z.cubeList.add(new ModelBox(inc_z, 210, 133, 9.0F, -73.75F, -67.0F, 2, 2, 2, 0.0F, false));

        rim_1 = new RendererModel(this);
        rim_1.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_1, 0.8727F, 0.0F, 0.0F);
        ctrlset_1.addChild(rim_1);
        rim_1.cubeList.add(new ModelBox(rim_1, 218, 136, -12.5F, 0.25F, -4.5F, 25, 2, 8, 0.0F, false));

        keypad_a1 = new RendererModel(this);
        keypad_a1.setRotationPoint(3.5F, 65.0F, 74.0F);
        rim_1.addChild(keypad_a1);
        keypad_a1.cubeList.add(new ModelBox(keypad_a1, 315, 201, -25.0F, -66.75F, -78.5F, 6, 4, 6, 0.0F, false));

        keypad_b1 = new RendererModel(this);
        keypad_b1.setRotationPoint(39.5F, 65.0F, 74.0F);
        rim_1.addChild(keypad_b1);
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 351, 192, -20.0F, -64.75F, -78.5F, 3, 2, 5, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -51.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -51.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -48.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -45.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -42.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -39.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -36.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -33.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 330, 174, -30.5F, -65.25F, -75.0F, 2, 2, 4, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -48.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -45.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -42.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -39.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -36.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -33.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b1.cubeList.add(new ModelBox(keypad_b1, 309, 8, -30.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));

        siderib_1 = new RendererModel(this);
        siderib_1.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_1, 0.4363F, -0.5236F, 0.0F);
        ctrlset_1.addChild(siderib_1);

        southwest = new RendererModel(this);
        southwest.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(southwest, 0.0F, -2.0944F, 0.0F);
        panels.addChild(southwest);

        ctrlset_5 = new RendererModel(this);
        ctrlset_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        southwest.addChild(ctrlset_5);

        top_5 = new RendererModel(this);
        top_5.setRotationPoint(-0.5F, -73.5F, -44.5F);
        setRotationAngle(top_5, 0.4363F, 0.0F, 0.0F);
        ctrlset_5.addChild(top_5);
        top_5.cubeList.add(new ModelBox(top_5, 224, 142, -2.5F, 1.0F, -11.5F, 1, 2, 8, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 359, 72, -6.5F, -2.0F, -4.5F, 14, 5, 15, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 297, 203, -2.5F, -4.0F, 0.5F, 6, 2, 6, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 297, 203, 3.5F, -3.0F, 1.25F, 2, 2, 5, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 297, 203, -4.5F, -3.0F, 1.25F, 2, 2, 5, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 297, 203, -2.0F, -3.0F, -1.5F, 5, 1, 2, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 297, 203, -2.0F, -3.0F, 6.5F, 5, 1, 2, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 359, 72, -4.5F, -1.0F, 9.5F, 1, 4, 5, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 359, 72, 4.5F, -1.0F, 9.5F, 1, 4, 5, 0.0F, false));
        top_5.cubeList.add(new ModelBox(top_5, 359, 72, 0.0F, -1.0F, 9.5F, 1, 4, 5, 0.0F, false));

        dim_select2 = new RendererModel(this);
        dim_select2.setRotationPoint(0.5741F, -4.7897F, 1.6538F);
        setRotationAngle(dim_select2, -1.5708F, 0.0F, 0.0F);
        top_5.addChild(dim_select2);

        wheel_rotate_z3 = new RendererModel(this);
        wheel_rotate_z3.setRotationPoint(0.0821F, -1.2728F, -3.1538F);
        dim_select2.addChild(wheel_rotate_z3);

        bone32 = new RendererModel(this);
        bone32.setRotationPoint(24.8099F, 74.4625F, -77.465F);
        wheel_rotate_z3.addChild(bone32);
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -28.3179F, -80.9793F, 77.5226F, 7, 2, 2, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -28.3179F, -69.9793F, 77.5226F, 7, 2, 2, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -20.3179F, -77.9793F, 77.5226F, 2, 7, 2, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -31.3179F, -77.9793F, 77.5226F, 2, 7, 2, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -29.8179F, -74.9793F, 78.2726F, 10, 1, 1, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 396, 64, -25.8179F, -75.4793F, 77.2726F, 2, 2, 4, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 396, 64, -20.0679F, -75.4793F, 73.2726F, 2, 2, 4, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 364, 183, -19.5679F, -74.9793F, 73.0226F, 1, 1, 6, 0.0F, false));
        bone32.cubeList.add(new ModelBox(bone32, 398, 203, -25.3179F, -78.9793F, 78.2726F, 1, 9, 1, 0.0F, false));

        bone33 = new RendererModel(this);
        bone33.setRotationPoint(15.3099F, 19.4625F, -146.215F);
        setRotationAngle(bone33, 0.0F, 0.0F, 0.7854F);
        wheel_rotate_z3.addChild(bone33);
        bone33.cubeList.add(new ModelBox(bone33, 398, 203, -27.1054F, -9.4425F, 146.5226F, 5, 2, 2, 0.0F, false));
        bone33.cubeList.add(new ModelBox(bone33, 398, 203, -27.1054F, 1.5575F, 146.5226F, 5, 2, 2, 0.0F, false));
        bone33.cubeList.add(new ModelBox(bone33, 398, 203, -20.1054F, -5.4425F, 146.5226F, 2, 5, 2, 0.0F, false));
        bone33.cubeList.add(new ModelBox(bone33, 398, 203, -31.1054F, -5.4425F, 146.5226F, 2, 5, 2, 0.0F, false));

        mid_5 = new RendererModel(this);
        mid_5.setRotationPoint(-0.5F, -67.0F, -57.0F);
        setRotationAngle(mid_5, 0.4363F, 0.0F, 0.0F);
        ctrlset_5.addChild(mid_5);
        mid_5.cubeList.add(new ModelBox(mid_5, 320, 60, -6.5F, -1.75F, -4.0F, 10, 6, 8, 0.0F, false));
        mid_5.cubeList.add(new ModelBox(mid_5, 34, 145, -5.5F, -2.25F, -3.0F, 2, 6, 4, 0.0F, false));
        mid_5.cubeList.add(new ModelBox(mid_5, 34, 145, 0.5F, -2.25F, -3.0F, 2, 6, 4, 0.0F, false));
        mid_5.cubeList.add(new ModelBox(mid_5, 34, 145, -2.5F, -2.25F, -3.0F, 2, 6, 4, 0.0F, false));

        dummy_buttons_5 = new RendererModel(this);
        dummy_buttons_5.setRotationPoint(0.5F, 73.0F, 65.0F);
        mid_5.addChild(dummy_buttons_5);
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 362, 59, 7.0F, -72.75F, -69.0F, 4, 2, 9, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 196, 139, 8.0F, -73.75F, -63.25F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 271, 193, 14.0F, -73.25F, -63.25F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 258, 198, 8.0F, -73.25F, -65.75F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 196, 139, 14.0F, -73.75F, -65.75F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 196, 139, 8.0F, -73.75F, -68.25F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 196, 139, 14.0F, -73.75F, -68.25F, 2, 2, 2, 0.0F, false));
        dummy_buttons_5.cubeList.add(new ModelBox(dummy_buttons_5, 362, 59, 13.0F, -72.75F, -69.0F, 4, 2, 9, 0.0F, false));

        globe = new RendererModel(this);
        globe.setRotationPoint(0.5F, 73.0F, 65.0F);
        mid_5.addChild(globe);
        globe.cubeList.add(new ModelBox(globe, 320, 60, -18.0F, -72.75F, -73.0F, 10, 4, 12, 0.0F, false));
        globe.cubeList.add(new ModelBox(globe, 320, 60, -17.0F, -73.75F, -72.0F, 8, 1, 9, 0.0F, false));

        spin_y = new RendererModel(this);
        spin_y.setRotationPoint(-12.725F, -76.4688F, -67.825F);
        globe.addChild(spin_y);
        spin_y.cubeList.add(new ModelBox(spin_y, 60, 82, 0.475F, -2.2813F, -2.3F, 2, 2, 2, 0.0F, false));
        spin_y.cubeList.add(new ModelBox(spin_y, 326, 60, -1.775F, -0.7813F, -1.175F, 3, 5, 3, 0.0F, false));
        spin_y.cubeList.add(new ModelBox(spin_y, 70, 129, -2.775F, -2.7813F, -2.175F, 5, 5, 5, 0.0F, false));
        spin_y.cubeList.add(new ModelBox(spin_y, 60, 82, -2.9F, -2.875F, 0.95F, 2, 3, 2, 0.0F, false));
        spin_y.cubeList.add(new ModelBox(spin_y, 60, 82, -0.525F, 0.7188F, -2.3F, 3, 1, 2, 0.0F, false));

        rim_5 = new RendererModel(this);
        rim_5.setRotationPoint(-0.5F, -60.5F, -65.0F);
        setRotationAngle(rim_5, 0.8727F, 0.0F, 0.0F);
        ctrlset_5.addChild(rim_5);

        keypad_a5 = new RendererModel(this);
        keypad_a5.setRotationPoint(3.5F, 65.0F, 74.0F);
        rim_5.addChild(keypad_a5);
        keypad_a5.cubeList.add(new ModelBox(keypad_a5, 218, 136, -18.0F, -64.75F, -74.5F, 20, 2, 1, 0.0F, false));
        keypad_a5.cubeList.add(new ModelBox(keypad_a5, 218, 136, -18.0F, -64.75F, -73.5F, 1, 2, 4, 0.0F, false));
        keypad_a5.cubeList.add(new ModelBox(keypad_a5, 218, 136, 12.75F, -64.75F, -79.5F, 7, 2, 7, 0.0F, false));

        keypad_b5 = new RendererModel(this);
        keypad_b5.setRotationPoint(39.5F, 65.0F, 74.0F);
        rim_5.addChild(keypad_b5);
        keypad_b5.cubeList.add(new ModelBox(keypad_b5, 385, 170, -22.0F, -65.25F, -75.5F, 2, 2, 2, 0.0F, false));
        keypad_b5.cubeList.add(new ModelBox(keypad_b5, 309, 8, -22.0F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));
        keypad_b5.cubeList.add(new ModelBox(keypad_b5, 309, 8, -19.5F, -65.25F, -75.5F, 2, 2, 2, 0.0F, false));
        keypad_b5.cubeList.add(new ModelBox(keypad_b5, 331, 181, -19.5F, -65.25F, -78.0F, 2, 2, 2, 0.0F, false));

        dummy_buttons_2 = new RendererModel(this);
        dummy_buttons_2.setRotationPoint(25.5F, 65.375F, 73.375F);
        rim_5.addChild(dummy_buttons_2);
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 365, 125, -13.5F, -65.75F, -75.5F, 1, 3, 5, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -15.75F, -65.75F, -75.25F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -15.75F, -65.75F, -72.5F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -18.25F, -65.75F, -72.5F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -18.25F, -65.75F, -75.25F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -20.75F, -65.75F, -72.5F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 200, 135, -20.75F, -65.75F, -75.25F, 2, 3, 2, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 365, 125, -22.0F, -65.75F, -75.5F, 1, 3, 5, 0.0F, false));
        dummy_buttons_2.cubeList.add(new ModelBox(dummy_buttons_2, 368, 191, -22.75F, -65.375F, -75.875F, 11, 3, 6, 0.0F, false));

        siderib_5 = new RendererModel(this);
        siderib_5.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
        setRotationAngle(siderib_5, 0.4363F, -0.5236F, 0.0F);
        ctrlset_5.addChild(siderib_5);

        rib_controls = new RendererModel(this);
        rib_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(rib_controls);

        west_rib = new RendererModel(this);
        west_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(west_rib, 0.0F, -1.5708F, 0.0F);
        rib_controls.addChild(west_rib);

        upper_rib = new RendererModel(this);
        upper_rib.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib, 0.4363F, 0.0F, 0.0F);
        west_rib.addChild(upper_rib);

        big_gauge = new RendererModel(this);
        big_gauge.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib.addChild(big_gauge);
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -19.0F, -86.5F, -19.5F, 6, 6, 1, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -14.5F, -86.0F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -14.5F, -82.25F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -18.5F, -86.0F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -18.5F, -82.25F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 309, 47, -19.0F, -86.5F, -32.5F, 6, 6, 1, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 269, 199, -17.5F, -85.25F, -31.5F, 3, 3, 3, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 351, 55, -17.5F, -85.25F, -33.0F, 3, 3, 1, 0.0F, false));
        big_gauge.cubeList.add(new ModelBox(big_gauge, 316, 117, -17.5F, -85.25F, -28.5F, 3, 3, 9, 0.0F, false));

        big_gauge2 = new RendererModel(this);
        big_gauge2.setRotationPoint(18.9661F, 82.15F, 47.785F);
        upper_rib.addChild(big_gauge2);
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -19.0F, -86.5F, -19.5F, 6, 6, 1, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -14.5F, -86.0F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -14.5F, -82.25F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -18.5F, -86.0F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -18.5F, -82.25F, -31.5F, 1, 1, 12, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 309, 47, -19.0F, -86.5F, -32.5F, 6, 6, 1, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 269, 199, -17.5F, -85.25F, -31.5F, 3, 3, 8, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 351, 55, -17.5F, -85.25F, -33.0F, 3, 3, 1, 0.0F, false));
        big_gauge2.cubeList.add(new ModelBox(big_gauge2, 316, 117, -17.5F, -85.25F, -23.5F, 3, 3, 4, 0.0F, false));

        jumbo_slider = new RendererModel(this);
        jumbo_slider.setRotationPoint(10.9661F, 81.15F, 47.785F);
        upper_rib.addChild(jumbo_slider);

        slider_rack = new RendererModel(this);
        slider_rack.setRotationPoint(4.25F, 1.5F, -1.0F);
        jumbo_slider.addChild(slider_rack);
        slider_rack.cubeList.add(new ModelBox(slider_rack, 331, 192, -12.5F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 347, 71, -21.5F, -84.5F, -52.5F, 3, 2, 2, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 347, 71, -13.5F, -84.5F, -52.5F, 3, 2, 2, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 347, 71, -14.75F, -84.5F, -52.5F, 1, 2, 2, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 347, 71, -18.25F, -84.5F, -52.5F, 1, 2, 2, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 347, 71, -17.0F, -84.5F, -52.5F, 2, 2, 2, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 327, 180, -22.0F, -84.0F, -52.0F, 12, 1, 1, 0.0F, false));
        slider_rack.cubeList.add(new ModelBox(slider_rack, 331, 192, -20.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));

        base = new RendererModel(this);
        base.setRotationPoint(8.0F, 2.5F, 13.0F);
        jumbo_slider.addChild(base);
        base.cubeList.add(new ModelBox(base, 201, 135, -25.0F, -82.25F, -67.0F, 11, 1, 13, 0.0F, false));
        base.cubeList.add(new ModelBox(base, 214, 64, -26.0F, -82.5F, -66.0F, 2, 2, 11, 0.0F, false));
        base.cubeList.add(new ModelBox(base, 243, 65, -22.5F, -82.5F, -66.0F, 6, 2, 11, 0.0F, false));
        base.cubeList.add(new ModelBox(base, 214, 64, -15.0F, -82.5F, -66.0F, 2, 2, 11, 0.0F, false));
        base.cubeList.add(new ModelBox(base, 248, 64, -26.0F, -82.5F, -68.0F, 13, 2, 2, 0.0F, false));
        base.cubeList.add(new ModelBox(base, 248, 64, -26.0F, -82.5F, -55.0F, 13, 2, 2, 0.0F, false));

        bevel_rib = new RendererModel(this);
        bevel_rib.setRotationPoint(0.5F, -60.75F, -72.5F);
        setRotationAngle(bevel_rib, 0.8727F, 0.0F, 0.0F);
        west_rib.addChild(bevel_rib);

        handbrake = new RendererModel(this);
        handbrake.setRotationPoint(10.9661F, 82.15F, 47.785F);
        bevel_rib.addChild(handbrake);
        handbrake.cubeList.add(new ModelBox(handbrake, 243, 88, -17.0F, -80.5F, -64.0F, 11, 7, 8, 0.0F, false));
        handbrake.cubeList.add(new ModelBox(handbrake, 304, 72, -14.0F, -81.5F, -62.5F, 5, 2, 4, 0.0F, false));

        pull_move_y = new RendererModel(this);
        pull_move_y.setRotationPoint(-0.5535F, 1.0424F, 0.25F);
        handbrake.addChild(pull_move_y);
        pull_move_y.cubeList.add(new ModelBox(pull_move_y, 339, 116, -12.0F, -85.5F, -62.0F, 2, 11, 2, 0.0F, false));
        pull_move_y.cubeList.add(new ModelBox(pull_move_y, 344, 69, -12.5625F, -88.5F, -62.5312F, 3, 3, 3, 0.0F, false));

        vertical_rib = new RendererModel(this);
        vertical_rib.setRotationPoint(0.5F, -69.75F, -61.5F);
        west_rib.addChild(vertical_rib);

        med_plate = new RendererModel(this);
        med_plate.setRotationPoint(10.9661F, 82.15F, 47.785F);
        vertical_rib.addChild(med_plate);

        nw_rib = new RendererModel(this);
        nw_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(nw_rib, 0.0F, -0.5236F, 0.0F);
        rib_controls.addChild(nw_rib);

        throttle = new RendererModel(this);
        throttle.setRotationPoint(-0.4133F, -61.2385F, -54.2589F);
        setRotationAngle(throttle, 0.6981F, 0.0F, 0.0F);
        nw_rib.addChild(throttle);

        throttle_rotate_x = new RendererModel(this);
        throttle_rotate_x.setRotationPoint(0.3794F, -12.7782F, -13.4562F);
        setRotationAngle(throttle_rotate_x, 1.5708F, 0.0F, 0.0F);
        throttle.addChild(throttle_rotate_x);
        throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 354, 64, -4.0F, -2.1487F, 4.4971F, 8, 2, 2, 0.0F, false));
        throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 346, 128, 3.25F, -1.3987F, -1.0029F, 1, 1, 7, 0.0F, false));
        throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 346, 128, -4.25F, -1.3987F, -1.0029F, 1, 1, 7, 0.0F, false));

        throttle_base = new RendererModel(this);
        throttle_base.setRotationPoint(-0.6206F, 70.1385F, 37.7938F);
        throttle.addChild(throttle_base);
        throttle_base.cubeList.add(new ModelBox(throttle_base, 322, 203, -4.0F, -82.5F, -53.0F, 10, 2, 1, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 325, 201, -4.0F, -82.5F, -43.0F, 10, 2, 1, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 342, 197, -3.0F, -83.5F, -42.0F, 8, 3, 1, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 352, 140, 0.5F, -83.5F, -44.25F, 1, 3, 1, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 314, 195, -5.0F, -82.5F, -46.0F, 12, 2, 3, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 329, 210, -5.0F, -82.5F, -52.0F, 12, 2, 2, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 311, 191, 3.0F, -82.5F, -50.0F, 4, 2, 4, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 316, 202, -5.0F, -82.5F, -50.0F, 4, 2, 4, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 325, 203, -3.0F, -83.5F, -54.0F, 8, 3, 1, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 353, 203, 7.0F, -83.5F, -51.0F, 1, 3, 7, 0.0F, false));
        throttle_base.cubeList.add(new ModelBox(throttle_base, 330, 197, -6.0F, -83.5F, -51.0F, 1, 3, 7, 0.0F, false));

        lever_mount = new RendererModel(this);
        lever_mount.setRotationPoint(5.1294F, 68.6385F, 36.5438F);
        throttle.addChild(lever_mount);
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -0.5F, -83.5F, -52.0F, 1, 4, 2, 0.0F, false));
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -2.5F, -83.5F, -52.0F, 1, 4, 2, 0.0F, false));
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -1.5F, -83.5F, -52.0F, 1, 4, 1, 0.0F, false));
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -9.0F, -83.5F, -52.0F, 1, 4, 1, 0.0F, false));
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -10.0F, -83.5F, -52.0F, 1, 4, 2, 0.0F, false));
        lever_mount.cubeList.add(new ModelBox(lever_mount, 354, 64, -8.0F, -83.5F, -52.0F, 1, 4, 2, 0.0F, false));

        upper_rib3 = new RendererModel(this);
        upper_rib3.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib3, 0.4363F, 0.0F, 0.0F);
        nw_rib.addChild(upper_rib3);

        fast_return = new RendererModel(this);
        fast_return.setRotationPoint(-33.7679F, 107.5772F, 85.4353F);
        upper_rib3.addChild(fast_return);
        fast_return.cubeList.add(new ModelBox(fast_return, 263, 209, 34.5F, -112.335F, -65.2323F, 2, 1, 2, 0.0F, false));
        fast_return.cubeList.add(new ModelBox(fast_return, 334, 187, 34.0F, -111.835F, -65.7323F, 3, 1, 3, 0.0F, false));

        switches = new RendererModel(this);
        switches.setRotationPoint(44.734F, -25.4272F, -37.6504F);
        fast_return.addChild(switches);
        switches.cubeList.add(new ModelBox(switches, 309, 47, -17.0F, -85.75F, -30.0F, 11, 1, 11, 0.0F, false));

        dummy_button2 = new RendererModel(this);
        dummy_button2.setRotationPoint(-33.7679F, 107.5772F, 89.4353F);
        upper_rib3.addChild(dummy_button2);
        dummy_button2.cubeList.add(new ModelBox(dummy_button2, 275, 204, 34.5F, -112.335F, -65.2323F, 2, 1, 2, 0.0F, false));
        dummy_button2.cubeList.add(new ModelBox(dummy_button2, 334, 187, 34.0F, -111.835F, -65.7323F, 3, 1, 3, 0.0F, false));

        dummy_button3 = new RendererModel(this);
        dummy_button3.setRotationPoint(-38.7679F, 107.5772F, 89.4353F);
        upper_rib3.addChild(dummy_button3);
        dummy_button3.cubeList.add(new ModelBox(dummy_button3, 349, 122, 34.5F, -113.335F, -64.2323F, 1, 2, 1, 0.0F, false));
        dummy_button3.cubeList.add(new ModelBox(dummy_button3, 349, 122, 34.5F, -113.335F, -65.7323F, 1, 2, 1, 0.0F, false));
        dummy_button3.cubeList.add(new ModelBox(dummy_button3, 349, 122, 34.5F, -113.335F, -67.2323F, 1, 2, 1, 0.0F, false));
        dummy_button3.cubeList.add(new ModelBox(dummy_button3, 349, 122, 34.5F, -113.335F, -68.7323F, 1, 2, 1, 0.0F, false));
        dummy_button3.cubeList.add(new ModelBox(dummy_button3, 186, 148, 34.0F, -111.835F, -69.7323F, 2, 1, 7, 0.0F, false));

        bevel_nwr = new RendererModel(this);
        bevel_nwr.setRotationPoint(0.0F, -73.4443F, -51.9976F);
        setRotationAngle(bevel_nwr, -0.6981F, 0.0F, 0.0F);
        nw_rib.addChild(bevel_nwr);

        vertical_rib3 = new RendererModel(this);
        vertical_rib3.setRotationPoint(0.5F, -69.75F, -61.5F);
        nw_rib.addChild(vertical_rib3);

        med_plate3 = new RendererModel(this);
        med_plate3.setRotationPoint(10.9661F, 82.15F, 47.785F);
        vertical_rib3.addChild(med_plate3);

        sw_rib = new RendererModel(this);
        sw_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(sw_rib, 0.0F, -2.618F, 0.0F);
        rib_controls.addChild(sw_rib);

        upper_rib6 = new RendererModel(this);
        upper_rib6.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib6, 0.4363F, 0.0F, 0.0F);
        sw_rib.addChild(upper_rib6);

        big_plate6 = new RendererModel(this);
        big_plate6.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib6.addChild(big_plate6);
        big_plate6.cubeList.add(new ModelBox(big_plate6, 309, 47, -17.0F, -81.5F, -53.0F, 11, 1, 8, 0.0F, false));
        big_plate6.cubeList.add(new ModelBox(big_plate6, 394, 72, -13.5F, -86.0F, -31.0F, 9, 6, 10, 0.0F, false));
        big_plate6.cubeList.add(new ModelBox(big_plate6, 394, 72, -10.5F, -88.0F, -29.0F, 6, 2, 5, 0.0F, false));

        resitsters = new RendererModel(this);
        resitsters.setRotationPoint(0.0F, -1.0F, 0.0F);
        big_plate6.addChild(resitsters);
        resitsters.cubeList.add(new ModelBox(resitsters, 252, 199, -16.0F, -82.5F, -51.0F, 1, 1, 4, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 252, 199, -14.0F, -82.5F, -51.0F, 1, 1, 4, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 252, 199, -12.0F, -82.5F, -51.0F, 1, 1, 4, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 212, 136, -10.0F, -82.5F, -51.0F, 1, 1, 4, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 252, 199, -8.0F, -82.5F, -51.0F, 1, 1, 4, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -16.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -14.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -12.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -10.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -8.0F, -82.5F, -52.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -16.0F, -82.5F, -47.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -14.0F, -82.5F, -47.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -12.0F, -82.5F, -47.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -10.0F, -82.5F, -47.0F, 1, 2, 1, 0.0F, false));
        resitsters.cubeList.add(new ModelBox(resitsters, 316, 205, -8.0F, -82.5F, -47.0F, 1, 2, 1, 0.0F, false));

        landing_rotate_x = new RendererModel(this);
        landing_rotate_x.setRotationPoint(-3.0447F, -83.5443F, -25.9124F);
        big_plate6.addChild(landing_rotate_x);
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 239, 127, -1.0F, -9.25F, -1.0F, 2, 1, 2, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 239, 127, -1.0F, -13.75F, -1.0F, 2, 1, 2, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 239, 127, -1.0F, -12.5F, -1.0F, 2, 3, 2, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 299, 127, -1.0F, -6.25F, -1.0F, 2, 4, 2, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 299, 127, -0.5F, -14.25F, -0.5F, 1, 8, 1, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 299, 127, -2.0F, -2.25F, -2.0F, 3, 4, 4, 0.0F, false));
        landing_rotate_x.cubeList.add(new ModelBox(landing_rotate_x, 298, 181, -4.75F, -1.25F, -1.0F, 6, 2, 2, 0.0F, false));

        bevel_rib5 = new RendererModel(this);
        bevel_rib5.setRotationPoint(0.5F, -60.75F, -72.5F);
        setRotationAngle(bevel_rib5, 0.8727F, 0.0F, 0.0F);
        sw_rib.addChild(bevel_rib5);

        small_plate5 = new RendererModel(this);
        small_plate5.setRotationPoint(10.9661F, 82.15F, 47.785F);
        bevel_rib5.addChild(small_plate5);
        small_plate5.cubeList.add(new ModelBox(small_plate5, 314, 76, -18.0F, -82.5F, -52.0F, 15, 2, 8, 0.0F, false));
        small_plate5.cubeList.add(new ModelBox(small_plate5, 313, 145, -17.0F, -83.0F, -46.75F, 2, 2, 2, 0.0F, false));
        small_plate5.cubeList.add(new ModelBox(small_plate5, 313, 145, -17.0F, -83.0F, -49.25F, 2, 2, 2, 0.0F, false));
        small_plate5.cubeList.add(new ModelBox(small_plate5, 196, 156, -14.0F, -83.0F, -51.25F, 9, 2, 6, 0.0F, false));
        small_plate5.cubeList.add(new ModelBox(small_plate5, 313, 145, -17.0F, -83.0F, -51.75F, 2, 2, 2, 0.0F, false));

        small_plate6 = new RendererModel(this);
        small_plate6.setRotationPoint(10.9661F, 88.15F, 71.785F);
        bevel_rib5.addChild(small_plate6);
        small_plate6.cubeList.add(new ModelBox(small_plate6, 305, 76, -16.0F, -82.5F, -52.0F, 9, 2, 8, 0.0F, false));
        small_plate6.cubeList.add(new ModelBox(small_plate6, 323, 104, -15.0F, -82.75F, -47.0F, 7, 1, 2, 0.0F, false));
        small_plate6.cubeList.add(new ModelBox(small_plate6, 323, 104, -15.0F, -82.75F, -51.0F, 7, 1, 2, 0.0F, false));

        vertical_rib6 = new RendererModel(this);
        vertical_rib6.setRotationPoint(0.5F, -69.75F, -61.5F);
        sw_rib.addChild(vertical_rib6);

        east_rib = new RendererModel(this);
        east_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(east_rib, 0.0F, 1.5708F, 0.0F);
        rib_controls.addChild(east_rib);

        upper_rib5 = new RendererModel(this);
        upper_rib5.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib5, 0.4363F, 0.0F, 0.0F);
        east_rib.addChild(upper_rib5);

        refuler = new RendererModel(this);
        refuler.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib5.addChild(refuler);

        refuelknob_rotate = new RendererModel(this);
        refuelknob_rotate.setRotationPoint(-12.0F, -87.0288F, -54.3851F);
        refuler.addChild(refuelknob_rotate);
        refuelknob_rotate.cubeList.add(new ModelBox(refuelknob_rotate, 308, 170, -0.5F, -1.2467F, -0.6128F, 1, 5, 1, 0.0F, false));
        refuelknob_rotate.cubeList.add(new ModelBox(refuelknob_rotate, 242, 145, -1.0F, -1.0F, -2.5F, 2, 2, 5, 0.0F, false));

        tanks = new RendererModel(this);
        tanks.setRotationPoint(0.0F, 0.0F, 0.0F);
        refuler.addChild(tanks);
        tanks.cubeList.add(new ModelBox(tanks, 186, 138, -15.5F, -86.1701F, -34.411F, 1, 1, 21, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 186, 138, -9.5F, -86.1701F, -34.411F, 1, 1, 21, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 350, 179, -10.0F, -86.6701F, -34.411F, 2, 2, 1, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 350, 179, -10.0F, -86.6701F, -18.411F, 2, 2, 1, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 350, 179, -16.0F, -86.6701F, -30.411F, 2, 2, 1, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 301, 134, -17.0F, -84.5F, -59.0F, 4, 4, 15, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 301, 134, -11.0F, -84.5F, -59.0F, 4, 4, 15, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 298, 97, -18.0F, -85.5F, -56.0F, 12, 6, 3, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 311, 128, -16.5F, -84.0F, -44.0F, 3, 3, 2, 0.0F, false));
        tanks.cubeList.add(new ModelBox(tanks, 311, 128, -10.5F, -84.0F, -44.0F, 3, 3, 2, 0.0F, false));

        bevel_rib4 = new RendererModel(this);
        bevel_rib4.setRotationPoint(0.5F, -60.75F, -72.5F);
        setRotationAngle(bevel_rib4, 0.8727F, 0.0F, 0.0F);
        east_rib.addChild(bevel_rib4);

        small_plate3 = new RendererModel(this);
        small_plate3.setRotationPoint(10.9661F, 82.15F, 47.785F);
        bevel_rib4.addChild(small_plate3);
        small_plate3.cubeList.add(new ModelBox(small_plate3, 305, 76, -18.0F, -82.5F, -54.0F, 8, 2, 8, 0.0F, false));
        small_plate3.cubeList.add(new ModelBox(small_plate3, 301, 199, -17.5F, -84.0F, -53.5F, 7, 2, 7, 0.0F, false));
        small_plate3.cubeList.add(new ModelBox(small_plate3, 343, 11, -17.0F, -84.25F, -53.0F, 6, 2, 6, 0.0F, false));
        small_plate3.cubeList.add(new ModelBox(small_plate3, 183, 142, -15.0F, -84.75F, -51.0F, 2, 2, 2, 0.0F, false));

        plate3_needle = new RendererModel(this);
        plate3_needle.setRotationPoint(-13.8125F, -84.625F, -50.0937F);
        small_plate3.addChild(plate3_needle);
        plate3_needle.cubeList.add(new ModelBox(plate3_needle, 273, 204, -0.6875F, 0.125F, 0.0938F, 1, 2, 3, 0.0F, false));

        small_plate4 = new RendererModel(this);
        small_plate4.setRotationPoint(10.9661F, 88.15F, 71.785F);
        bevel_rib4.addChild(small_plate4);
        small_plate4.cubeList.add(new ModelBox(small_plate4, 206, 142, -15.5F, -83.5F, -53.0F, 1, 1, 9, 0.0F, false));
        small_plate4.cubeList.add(new ModelBox(small_plate4, 206, 142, -9.5F, -83.5F, -53.0F, 1, 1, 9, 0.0F, false));
        small_plate4.cubeList.add(new ModelBox(small_plate4, 333, 185, -10.0F, -83.9532F, -44.7887F, 2, 2, 1, 0.0F, false));

        vertical_rib5 = new RendererModel(this);
        vertical_rib5.setRotationPoint(0.5F, -69.75F, -61.5F);
        east_rib.addChild(vertical_rib5);

        facing = new RendererModel(this);
        facing.setRotationPoint(10.9661F, 82.15F, 47.785F);
        vertical_rib5.addChild(facing);

        facing_rotate_z = new RendererModel(this);
        facing_rotate_z.setRotationPoint(-10.9661F, -59.65F, -68.285F);
        facing.addChild(facing_rotate_z);

        bone13 = new RendererModel(this);
        bone13.setRotationPoint(9.4661F, 55.15F, 70.535F);
        facing_rotate_z.addChild(bone13);
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -13.0F, -61.5F, -70.0F, 7, 2, 2, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -13.0F, -50.5F, -70.0F, 7, 2, 2, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -5.0F, -58.5F, -70.0F, 2, 7, 2, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -16.0F, -58.5F, -70.0F, 2, 7, 2, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -14.5F, -55.5F, -69.25F, 10, 1, 1, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 396, 64, -10.5F, -56.0F, -70.25F, 2, 2, 4, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 396, 64, -4.75F, -56.0F, -74.25F, 2, 2, 4, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 364, 183, -4.25F, -55.5F, -74.5F, 1, 1, 6, 0.0F, false));
        bone13.cubeList.add(new ModelBox(bone13, 398, 203, -10.0F, -59.5F, -69.25F, 1, 9, 1, 0.0F, false));

        bone14 = new RendererModel(this);
        bone14.setRotationPoint(-0.0339F, 0.15F, 1.785F);
        setRotationAngle(bone14, 0.0F, 0.0F, 0.7854F);
        facing_rotate_z.addChild(bone14);
        bone14.cubeList.add(new ModelBox(bone14, 398, 203, -2.5F, -6.5F, -1.0F, 5, 2, 2, 0.0F, false));
        bone14.cubeList.add(new ModelBox(bone14, 398, 203, -2.5F, 4.5F, -1.0F, 5, 2, 2, 0.0F, false));
        bone14.cubeList.add(new ModelBox(bone14, 398, 203, 4.5F, -2.5F, -1.0F, 2, 5, 2, 0.0F, false));
        bone14.cubeList.add(new ModelBox(bone14, 398, 203, -6.5F, -2.5F, -1.0F, 2, 5, 2, 0.0F, false));

        winch_plate = new RendererModel(this);
        winch_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        facing.addChild(winch_plate);
        winch_plate.cubeList.add(new ModelBox(winch_plate, 396, 64, -17.0F, -65.5F, -64.0F, 12, 12, 1, 0.0F, false));

        ne_rib = new RendererModel(this);
        ne_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ne_rib, 0.0F, 0.5236F, 0.0F);
        rib_controls.addChild(ne_rib);

        upper_rib4 = new RendererModel(this);
        upper_rib4.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib4, 0.4363F, 0.0F, 0.0F);
        ne_rib.addChild(upper_rib4);

        stablizers = new RendererModel(this);
        stablizers.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib4.addChild(stablizers);

        pull_rotate_x = new RendererModel(this);
        pull_rotate_x.setRotationPoint(-12.0859F, -85.5F, -47.9375F);
        setRotationAngle(pull_rotate_x, -2.8798F, 0.0F, 0.0F);
        stablizers.addChild(pull_rotate_x);
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 198, 138, -1.6641F, -2.0F, -2.0625F, 2, 4, 4, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 198, 138, -1.6641F, -1.0F, 1.9375F, 2, 2, 4, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 198, 138, -2.6641F, -1.0F, 3.9375F, 1, 2, 10, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 305, 182, -1.6641F, -1.0F, 11.9375F, 1, 2, 2, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 305, 182, 2.8359F, -1.0F, 11.9375F, 1, 2, 2, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 305, 182, -0.4141F, -1.0F, 11.9375F, 3, 2, 2, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 240, 157, -0.9141F, -0.5F, 12.4375F, 5, 1, 1, 0.0F, false));
        pull_rotate_x.cubeList.add(new ModelBox(pull_rotate_x, 305, 182, -3.3516F, -1.0F, -1.0625F, 4, 2, 2, 0.0F, false));

        pull_leaverbase = new RendererModel(this);
        pull_leaverbase.setRotationPoint(0.0F, 0.0F, 0.0F);
        stablizers.addChild(pull_leaverbase);
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 185, 133, -16.0F, -81.5F, -53.0F, 11, 1, 11, 0.0F, false));
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 186, 139, -16.0F, -81.5F, -57.0F, 8, 1, 4, 0.0F, false));
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 339, 66, -6.5F, -82.0F, -44.0F, 1, 1, 1, 0.0F, false));
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 339, 66, -6.5F, -82.0F, -50.0F, 1, 1, 1, 0.0F, false));
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 198, 138, -15.0F, -87.5F, -50.0F, 1, 6, 4, 0.0F, false));
        pull_leaverbase.cubeList.add(new ModelBox(pull_leaverbase, 198, 138, -14.0F, -82.5F, -50.0F, 4, 1, 7, 0.0F, false));

        deskbell = new RendererModel(this);
        deskbell.setRotationPoint(12.2161F, 82.15F, 69.535F);
        upper_rib4.addChild(deskbell);
        deskbell.cubeList.add(new ModelBox(deskbell, 386, 73, -17.0F, -81.5F, -58.0F, 9, 1, 9, 0.0F, false));
        deskbell.cubeList.add(new ModelBox(deskbell, 300, 202, -16.0F, -83.5F, -57.0F, 7, 2, 7, 0.0F, false));
        deskbell.cubeList.add(new ModelBox(deskbell, 300, 202, -15.5F, -84.5F, -56.5F, 6, 2, 6, 0.0F, false));
        deskbell.cubeList.add(new ModelBox(deskbell, 300, 202, -13.5F, -85.5F, -54.5F, 2, 1, 2, 0.0F, false));

        bevel_rib3 = new RendererModel(this);
        bevel_rib3.setRotationPoint(0.5F, -60.75F, -72.5F);
        setRotationAngle(bevel_rib3, 0.8727F, 0.0F, 0.0F);
        ne_rib.addChild(bevel_rib3);

        small_plate2 = new RendererModel(this);
        small_plate2.setRotationPoint(10.9661F, 82.15F, 47.785F);
        bevel_rib3.addChild(small_plate2);
        small_plate2.cubeList.add(new ModelBox(small_plate2, 193, 133, -16.0F, -82.8139F, -47.4462F, 8, 2, 5, 0.0F, false));

        vertical_rib4 = new RendererModel(this);
        vertical_rib4.setRotationPoint(0.5F, -69.75F, -61.5F);
        ne_rib.addChild(vertical_rib4);

        med_plate4 = new RendererModel(this);
        med_plate4.setRotationPoint(10.9661F, 82.15F, 47.785F);
        vertical_rib4.addChild(med_plate4);

        se_rib = new RendererModel(this);
        se_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(se_rib, 0.0F, 2.618F, 0.0F);
        rib_controls.addChild(se_rib);

        upper_rib2 = new RendererModel(this);
        upper_rib2.setRotationPoint(0.5F, -69.75F, -61.5F);
        setRotationAngle(upper_rib2, 0.4363F, 0.0F, 0.0F);
        se_rib.addChild(upper_rib2);

        monitor = new RendererModel(this);
        monitor.setRotationPoint(10.9661F, 82.15F, 47.785F);
        setRotationAngle(monitor, -0.3491F, 0.0F, 0.0F);
        upper_rib2.addChild(monitor);
        monitor.cubeList.add(new ModelBox(monitor, 334, 112, -29.0F, -99.5F, -49.0F, 35, 23, 2, 0.0F, false));
        monitor.cubeList.add(new ModelBox(monitor, 334, 112, -29.0F, -78.5F, -51.0F, 35, 2, 2, 0.0F, false));
        monitor.cubeList.add(new ModelBox(monitor, 183, 130, -27.0F, -98.5F, -48.0F, 31, 16, 4, 0.0F, false));

        screen3 = new RendererModel(this);
        screen3.setRotationPoint(-11.5F, -89.0F, -48.75F);
        setRotationAngle(screen3, 0.0F, 0.0F, -1.5708F);
        monitor.addChild(screen3);
        screen3.cubeList.add(new ModelBox(screen3, 51, 190, -9.5F, -16.5F, -0.5F, 19, 33, 1, 0.0F, false));

        door_switch = new RendererModel(this);
        door_switch.setRotationPoint(-4.2886F, 0.2417F, 5.5714F);
        upper_rib2.addChild(door_switch);
        door_switch.cubeList.add(new ModelBox(door_switch, 296, 128, -1.0F, -3.025F, -1.5938F, 2, 5, 5, 0.0F, false));
        door_switch.cubeList.add(new ModelBox(door_switch, 296, 128, -2.0F, -1.025F, -2.5938F, 4, 4, 5, 0.0F, false));
        door_switch.cubeList.add(new ModelBox(door_switch, 296, 128, -3.0F, -1.025F, -1.5938F, 1, 4, 3, 0.0F, false));
        door_switch.cubeList.add(new ModelBox(door_switch, 296, 128, 2.0F, -1.025F, -1.5938F, 1, 4, 3, 0.0F, false));
        door_switch.cubeList.add(new ModelBox(door_switch, 224, 154, -0.5F, -4.4F, -2.125F, 1, 6, 4, 0.0F, false));

        capasitors = new RendererModel(this);
        capasitors.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib2.addChild(capasitors);
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -17.75F, -82.5F, -27.0F, 12, 1, 1, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 190, 133, -19.0F, -81.5F, -28.0F, 14, 1, 9, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -18.0F, -84.5F, -22.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -15.0F, -84.5F, -22.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -12.0F, -84.5F, -22.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 264, 192, -8.75F, -84.5F, -22.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -10.75F, -84.5F, -25.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -13.75F, -84.5F, -25.0F, 2, 3, 2, 0.0F, false));
        capasitors.cubeList.add(new ModelBox(capasitors, 296, 128, -16.75F, -84.5F, -25.0F, 2, 3, 2, 0.0F, false));

        buttons = new RendererModel(this);
        buttons.setRotationPoint(10.9661F, 82.15F, 47.785F);
        upper_rib2.addChild(buttons);
        buttons.cubeList.add(new ModelBox(buttons, 308, 205, -8.5F, -81.75F, -39.25F, 4, 4, 4, 0.0F, false));
        buttons.cubeList.add(new ModelBox(buttons, 224, 154, -8.0F, -82.75F, -38.75F, 3, 4, 3, 0.0F, false));
        buttons.cubeList.add(new ModelBox(buttons, 224, 154, -8.0F, -82.75F, -34.0F, 3, 4, 3, 0.0F, false));
        buttons.cubeList.add(new ModelBox(buttons, 308, 205, -8.5F, -81.75F, -34.5F, 4, 4, 4, 0.0F, false));

        bevel_rib2 = new RendererModel(this);
        bevel_rib2.setRotationPoint(0.5F, -60.75F, -72.5F);
        setRotationAngle(bevel_rib2, 0.8727F, 0.0F, 0.0F);
        se_rib.addChild(bevel_rib2);

        increment_select = new RendererModel(this);
        increment_select.setRotationPoint(10.9661F, 82.15F, 47.785F);
        bevel_rib2.addChild(increment_select);

        crank_rotate_y = new RendererModel(this);
        crank_rotate_y.setRotationPoint(-10.9167F, -89.5F, -48.0F);
        increment_select.addChild(crank_rotate_y);
        crank_rotate_y.cubeList.add(new ModelBox(crank_rotate_y, 361, 64, -1.0833F, -1.0F, -1.0F, 2, 8, 2, 0.0F, false));
        crank_rotate_y.cubeList.add(new ModelBox(crank_rotate_y, 306, 131, -7.0833F, -0.5F, -0.5F, 9, 1, 1, 0.0F, false));
        crank_rotate_y.cubeList.add(new ModelBox(crank_rotate_y, 295, 64, -8.0833F, -6.0F, -1.0F, 2, 6, 2, 0.0F, false));

        crank_mount2 = new RendererModel(this);
        crank_mount2.setRotationPoint(0.0F, 0.0F, 0.0F);
        increment_select.addChild(crank_mount2);
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 305, 76, -15.0F, -82.5F, -52.0F, 8, 2, 8, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 305, 76, -14.0F, -88.5F, -51.0F, 6, 1, 6, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 304, 180, -13.0F, -83.5F, -50.0F, 4, 1, 4, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 304, 180, -13.0F, -85.5F, -50.0F, 4, 1, 4, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 304, 180, -13.0F, -87.5F, -50.0F, 4, 1, 4, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 333, 73, -9.0F, -87.5F, -51.0F, 1, 5, 1, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 333, 73, -14.0F, -87.5F, -51.0F, 1, 5, 1, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 333, 73, -14.0F, -87.5F, -46.0F, 1, 5, 1, 0.0F, false));
        crank_mount2.cubeList.add(new ModelBox(crank_mount2, 333, 73, -9.0F, -87.5F, -46.0F, 1, 5, 1, 0.0F, false));

        vertical_rib2 = new RendererModel(this);
        vertical_rib2.setRotationPoint(0.5F, -69.75F, -61.5F);
        se_rib.addChild(vertical_rib2);

        med_plate2 = new RendererModel(this);
        med_plate2.setRotationPoint(10.9661F, 82.15F, 47.785F);
        vertical_rib2.addChild(med_plate2);

        guts = new RendererModel(this);
        guts.setRotationPoint(0.0F, 12.0F, 0.0F);
        guts.cubeList.add(new ModelBox(guts, 295, 43, -25.0F, -23.0F, -38.0F, 50, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 193, 131, -14.0F, -15.0F, -38.0F, 15, 12, 8, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -25.0F, -23.0F, -19.0F, 50, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 188, 120, -30.0F, -16.0F, -8.0F, 17, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, 3.0F, -16.0F, -20.0F, 2, 9, 7, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, 0.0F, -16.0F, -20.0F, 2, 9, 7, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -3.0F, -16.0F, -20.0F, 2, 9, 7, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -27.0F, -16.0F, 9.0F, 12, 10, 9, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 334, 121, -18.0F, -16.0F, -23.0F, 2, 8, 15, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, -8.0F, -17.25F, -8.0F, 16, 8, 16, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, -8.0F, -7.5F, -8.0F, 16, 4, 16, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, -8.0F, -2.25F, -8.0F, 16, 4, 16, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, -8.0F, 2.75F, -8.0F, 16, 4, 16, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, 11.0F, -3.0F, -20.0F, 6, 14, 6, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, 20.0F, -11.0F, 10.0F, 6, 22, 6, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 185, 125, -25.0F, -4.0F, -1.0F, 6, 15, 6, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 342, 179, -11.0F, -16.0F, -21.0F, 17, 8, 9, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 342, 179, -20.0F, -16.0F, -27.0F, 6, 10, 6, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -2.0F, -16.0F, 4.0F, 17, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 304, 132, 0.0F, -4.0F, 6.0F, 13, 2, 15, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 306, 115, 6.0F, -16.0F, -23.0F, 15, 12, 13, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -25.0F, -23.0F, 0.0F, 50, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 293, 43, -25.0F, -23.0F, 19.0F, 50, 12, 19, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 207, 129, -18.0F, -16.0F, 23.0F, 13, 12, 15, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, 25.0F, -23.0F, -30.0F, 13, 12, 30, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 194, 130, 25.0F, -18.0F, -13.0F, 13, 12, 16, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -39.0F, -23.0F, -29.0F, 14, 12, 29, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, 25.0F, -23.0F, 0.0F, 13, 12, 31, 0.0F, false));
        guts.cubeList.add(new ModelBox(guts, 295, 43, -39.0F, -23.0F, 0.0F, 14, 12, 30, 0.0F, false));

        bone35 = new RendererModel(this);
        bone35.setRotationPoint(0.0F, -0.25F, 0.0F);
        guts.addChild(bone35);
        bone35.cubeList.add(new ModelBox(bone35, 185, 125, -8.0F, 8.0F, -6.0F, 2, 4, 12, 0.0F, false));
        bone35.cubeList.add(new ModelBox(bone35, 185, 125, 6.0F, 8.0F, -6.0F, 2, 4, 12, 0.0F, false));
        bone35.cubeList.add(new ModelBox(bone35, 185, 125, -8.0F, 8.0F, -8.0F, 16, 4, 2, 0.0F, false));
        bone35.cubeList.add(new ModelBox(bone35, 185, 125, -8.0F, 8.0F, 6.0F, 16, 4, 2, 0.0F, false));
	}

	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ConsoleTile console, float scale) {
        //Rotor Animations
        int prev = console.isInFlight() ? console.flightTicks - 1 : 0;
        this.rotor.rotateAngleY = (float) Math.toRadians(prev - (console.flightTicks - prev) * Minecraft.getInstance().getRenderPartialTicks());
        this.rotor.offsetY = (float) (Math.cos(console.flightTicks * 0.1F) * 0.5F) - 0.825F;

        //Throttle
        //Offset throttle by 90 degrees because default angle on model is 90
        this.throttle_rotate_x.rotateAngleX =  (float) Math.toRadians(90 - 100 * console.getControl(ThrottleControl.class).getAmount());

        //Handbrake
        this.pull_move_y.offsetY = (float) Math.toRadians(console.getControl(HandbrakeControl.class).isFree() ? 5 : -15);

        //Facing
        FacingControl facing = console.getControl(FacingControl.class);
        if (facing != null) {
        	this.facing_rotate_z.rotateAngleZ =  (float)Math.toRadians(
                	facing.getDirection() == Direction.NORTH ? 270 : //This is because default rotation is 90 degrees, so it is already facing east
                        (facing.getDirection() == Direction.SOUTH ? 90 :
                        (facing.getDirection() == Direction.WEST ? 180 : 0)));
        }

        //Stabilisers
        this.pull_rotate_x.rotateAngleX = (float) Math.toRadians(console.getControl(StabilizerControl.class).isStabilized() ? 165 : -10);

        //Randomiser
        this.spinner.rotateAngleY = (float) Math.toRadians(console.getControl(RandomiserControl.class).getAnimationTicks() * 36 + Minecraft.getInstance().getRenderPartialTicks() % 360.0);

        //Refueler
        this.refuelknob_rotate.rotateAngleY = (float) Math.toRadians(console.getControl(RefuelerControl.class).isRefueling() ? 90 : 0);

        //Inc Mod
        IncModControl coord = console.getControl(IncModControl.class);
        if(coord != null) {
        	this.crank_rotate_y.rotateAngleY = (float) Math.toRadians(coord.index * -90F);
        }

        //Land Type
        LandingTypeControl land_type = console.getControl(LandingTypeControl.class);
        if (land_type != null) {
        	this.landing_rotate_x.rotateAngleX = (float) Math.toRadians(land_type.getLandType() == EnumLandType.DOWN ? 90 : 0);
        }

        //Dim Select
        DimensionControl dim = console.getControl(DimensionControl.class);
        if (dim != null) {
        	this.wheel_rotate_z3.rotateAngleZ = (float) Math.toRadians((dim.getAnimationTicks() * 36) + Minecraft.getInstance().getRenderPartialTicks() % 360.0);
        }
        
        console.getDoor().ifPresent(door -> {
        	this.door_switch.rotateAngleY = (float)Math.toRadians(door.getOpenState() == EnumDoorState.CLOSED ? 0 : 90);
        });

        stations.render(scale);
        controls.render(scale);
        guts.render(scale);
        ModelHelper.renderPartBrightness(1F, this.glow);
        //Sonics
        GlStateManager.pushMatrix();
        GlStateManager.translated(-1.94, -3, 2.44);
        GlStateManager.rotated(-25, 0.5, 0, 1);
        double sonic_scale = 1.25;
        GlStateManager.scaled(sonic_scale, sonic_scale, sonic_scale);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE);
        GlStateManager.popMatrix();
	}
}