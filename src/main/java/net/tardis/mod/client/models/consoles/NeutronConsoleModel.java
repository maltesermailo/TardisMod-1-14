package net.tardis.mod.client.models.consoles;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.Direction;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FastReturnControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class NeutronConsoleModel extends Model {
	private final RendererModel glow;
	private final RendererModel glow_inner;
	private final RendererModel glow_trim;
	private final RendererModel grill_corner_light;
	private final RendererModel gril_light;
	private final RendererModel glow_bar;
	private final RendererModel glow_corner_fill;
	private final RendererModel glow_trim2;
	private final RendererModel grill_corner_light2;
	private final RendererModel gril_light2;
	private final RendererModel glow_bar2;
	private final RendererModel glow_corner_fill2;
	private final RendererModel glow_trim3;
	private final RendererModel grill_corner_light3;
	private final RendererModel gril_light3;
	private final RendererModel glow_bar3;
	private final RendererModel glow_corner_fill3;
	private final RendererModel glow_trim4;
	private final RendererModel grill_corner_light4;
	private final RendererModel gril_light4;
	private final RendererModel glow_bar4;
	private final RendererModel glow_corner_fill4;
	private final RendererModel glow_trim5;
	private final RendererModel grill_corner_light5;
	private final RendererModel gril_light5;
	private final RendererModel glow_bar5;
	private final RendererModel glow_corner_fill5;
	private final RendererModel glow_trim6;
	private final RendererModel grill_corner_light6;
	private final RendererModel gril_light6;
	private final RendererModel glow_bar6;
	private final RendererModel glow_corner_fill6;
	private final RendererModel glow_rotor_bob_y;
	private final RendererModel crystal_spin_y;
	private final RendererModel y45;
	private final RendererModel z45;
	private final RendererModel x45;
	private final RendererModel hover_rings;
	private final RendererModel outer_rotate_x;
	private final RendererModel bone61;
	private final RendererModel bone62;
	private final RendererModel bone63;
	private final RendererModel inner_rotate_x;
	private final RendererModel bone64;
	private final RendererModel bone65;
	private final RendererModel bone66;
	private final RendererModel glow_posts;
	private final RendererModel basepost1;
	private final RendererModel basepost2;
	private final RendererModel basepost3;
	private final RendererModel basepost4;
	private final RendererModel basepost5;
	private final RendererModel basepost6;
	private final RendererModel glow_controls;
	private final RendererModel controls8;
	private final RendererModel base_plate_7;
	private final RendererModel buttonstrip2;
	private final RendererModel buttons_lit2;
	private final RendererModel small_screen2;
	private final RendererModel indicator4;
	private final RendererModel indicator5;
	private final RendererModel indicator6;
	private final RendererModel coms2;
	private final RendererModel controls9;
	private final RendererModel base_plate_8;
	private final RendererModel dummy_buttons13;
	private final RendererModel dummy_buttons14;
	private final RendererModel dummy_buttons15;
	private final RendererModel controls10;
	private final RendererModel base_plate_9;
	private final RendererModel base2;
	private final RendererModel controls11;
	private final RendererModel base_plate_10;
	private final RendererModel coords_z2;
	private final RendererModel toggle_base4;
	private final RendererModel coords_y2;
	private final RendererModel toggle_base5;
	private final RendererModel toggle5;
	private final RendererModel coords_x2;
	private final RendererModel toggle_base6;
	private final RendererModel radar2;
	private final RendererModel bigslider2;
	private final RendererModel button_array2;
	private final RendererModel buttons2;
	private final RendererModel controls12;
	private final RendererModel base_plate_11;
	private final RendererModel angeled_panel2;
	private final RendererModel dummy_buttons19;
	private final RendererModel refuler2;
	private final RendererModel glass2;
	private final RendererModel controls13;
	private final RendererModel base_plate_12;
	private final RendererModel dummy_buttons24;
	private final RendererModel dial3;
	private final RendererModel dial_glass3;
	private final RendererModel dial4;
	private final RendererModel dial_glass4;
	private final RendererModel rib_control2;
	private final RendererModel spine_con4;
	private final RendererModel plain10;
	private final RendererModel spine_con6;
	private final RendererModel plain12;
	private final RendererModel telepathic3;
	private final RendererModel microscope2;
	private final RendererModel spines;
	private final RendererModel silverib1;
	private final RendererModel spine1;
	private final RendererModel plain1;
	private final RendererModel bevel1;
	private final RendererModel front1;
	private final RendererModel bone;
	private final RendererModel ring;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel ring2;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel glow__innards;
	private final RendererModel undercurve1;
	private final RendererModel backbend1;
	private final RendererModel shin1;
	private final RendererModel bone16;
	private final RendererModel ribfoot1;
	private final RendererModel silverib2;
	private final RendererModel spine2;
	private final RendererModel plain2;
	private final RendererModel bevel2;
	private final RendererModel front2;
	private final RendererModel bone8;
	private final RendererModel ring5;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel ring6;
	private final RendererModel bone13;
	private final RendererModel bone14;
	private final RendererModel glow__innards2;
	private final RendererModel undercurve2;
	private final RendererModel backbend2;
	private final RendererModel shin2;
	private final RendererModel bone15;
	private final RendererModel ribfoot2;
	private final RendererModel silverib3;
	private final RendererModel spine3;
	private final RendererModel plain3;
	private final RendererModel bevel3;
	private final RendererModel front3;
	private final RendererModel bone17;
	private final RendererModel ring7;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel ring8;
	private final RendererModel bone20;
	private final RendererModel bone21;
	private final RendererModel glow__innards3;
	private final RendererModel undercurve3;
	private final RendererModel backbend3;
	private final RendererModel shin3;
	private final RendererModel bone22;
	private final RendererModel ribfoot3;
	private final RendererModel silverib4;
	private final RendererModel spine4;
	private final RendererModel plain4;
	private final RendererModel bevel4;
	private final RendererModel front4;
	private final RendererModel bone23;
	private final RendererModel ring9;
	private final RendererModel bone24;
	private final RendererModel bone25;
	private final RendererModel ring10;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel glow__innards4;
	private final RendererModel undercurve4;
	private final RendererModel backbend4;
	private final RendererModel shin4;
	private final RendererModel bone28;
	private final RendererModel ribfoot4;
	private final RendererModel silverib5;
	private final RendererModel spine5;
	private final RendererModel plain5;
	private final RendererModel bevel5;
	private final RendererModel front5;
	private final RendererModel bone29;
	private final RendererModel ring11;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel ring12;
	private final RendererModel bone32;
	private final RendererModel bone33;
	private final RendererModel glow__innards5;
	private final RendererModel undercurve5;
	private final RendererModel backbend5;
	private final RendererModel shin5;
	private final RendererModel bone34;
	private final RendererModel ribfoot5;
	private final RendererModel silverib6;
	private final RendererModel spine6;
	private final RendererModel plain6;
	private final RendererModel bevel6;
	private final RendererModel front6;
	private final RendererModel bone35;
	private final RendererModel ring13;
	private final RendererModel bone36;
	private final RendererModel bone37;
	private final RendererModel ring14;
	private final RendererModel bone38;
	private final RendererModel bone39;
	private final RendererModel glow__innards6;
	private final RendererModel undercurve6;
	private final RendererModel backbend6;
	private final RendererModel shin6;
	private final RendererModel bone40;
	private final RendererModel ribfoot6;
	private final RendererModel stations;
	private final RendererModel station1;
	private final RendererModel st_plain1;
	private final RendererModel st_bevel1;
	private final RendererModel st_front1;
	private final RendererModel rotorbevel;
	private final RendererModel ring4;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel ring3;
	private final RendererModel bone6;
	private final RendererModel bone7;
	private final RendererModel st_under1;
	private final RendererModel back1;
	private final RendererModel box1;
	private final RendererModel trim1;
	private final RendererModel station2;
	private final RendererModel st_plain2;
	private final RendererModel st_bevel2;
	private final RendererModel st_front2;
	private final RendererModel rotorbevel2;
	private final RendererModel ring15;
	private final RendererModel bone41;
	private final RendererModel bone42;
	private final RendererModel ring16;
	private final RendererModel bone43;
	private final RendererModel bone44;
	private final RendererModel st_under2;
	private final RendererModel back2;
	private final RendererModel box2;
	private final RendererModel trim2;
	private final RendererModel station3;
	private final RendererModel st_plain3;
	private final RendererModel st_bevel3;
	private final RendererModel st_front3;
	private final RendererModel rotorbevel3;
	private final RendererModel ring17;
	private final RendererModel bone45;
	private final RendererModel bone46;
	private final RendererModel ring18;
	private final RendererModel bone47;
	private final RendererModel bone48;
	private final RendererModel st_under3;
	private final RendererModel back3;
	private final RendererModel box3;
	private final RendererModel trim3;
	private final RendererModel station4;
	private final RendererModel st_plain4;
	private final RendererModel st_bevel4;
	private final RendererModel st_front4;
	private final RendererModel rotorbevel4;
	private final RendererModel ring19;
	private final RendererModel bone49;
	private final RendererModel bone50;
	private final RendererModel ring20;
	private final RendererModel bone51;
	private final RendererModel bone52;
	private final RendererModel st_under4;
	private final RendererModel back4;
	private final RendererModel box4;
	private final RendererModel trim4;
	private final RendererModel station5;
	private final RendererModel st_plain5;
	private final RendererModel st_bevel5;
	private final RendererModel st_front5;
	private final RendererModel rotorbevel5;
	private final RendererModel ring21;
	private final RendererModel bone53;
	private final RendererModel bone54;
	private final RendererModel ring22;
	private final RendererModel bone55;
	private final RendererModel bone56;
	private final RendererModel st_under5;
	private final RendererModel back5;
	private final RendererModel box5;
	private final RendererModel trim5;
	private final RendererModel station6;
	private final RendererModel st_plain6;
	private final RendererModel st_bevel6;
	private final RendererModel st_front6;
	private final RendererModel rotorbevel6;
	private final RendererModel ring23;
	private final RendererModel bone57;
	private final RendererModel bone58;
	private final RendererModel ring24;
	private final RendererModel bone59;
	private final RendererModel bone60;
	private final RendererModel st_under6;
	private final RendererModel back6;
	private final RendererModel box6;
	private final RendererModel trim6;
	private final RendererModel controls;
	private final RendererModel controls1;
	private final RendererModel base_plate_1;
	private final RendererModel turn_dial;
	private final RendererModel guide;
	private final RendererModel knob4;
	private final RendererModel buttonstrip;
	private final RendererModel buttons_lit;
	private final RendererModel small_screen;
	private final RendererModel four_square;
	private final RendererModel indicator;
	private final RendererModel indicator2;
	private final RendererModel indicator3;
	private final RendererModel coms;
	private final RendererModel com_needle_move_x;
	private final RendererModel fast_return_rotate_x;
	private final RendererModel bone68;
	private final RendererModel controls2;
	private final RendererModel base_plate_2;
	private final RendererModel lever;
	private final RendererModel lever_turn;
	private final RendererModel lever2;
	private final RendererModel door_turn;
	private final RendererModel wide_strip;
	private final RendererModel sonic_port;
	private final RendererModel controls3;
	private final RendererModel base_plate_3;
	private final RendererModel facing;
	private final RendererModel joystick;
	private final RendererModel base;
	private final RendererModel pull_tab;
	private final RendererModel pull_tab2;
	private final RendererModel pull_tab3;
	private final RendererModel randomizers;
	private final RendererModel key_white1;
	private final RendererModel key_black1;
	private final RendererModel key_white2;
	private final RendererModel key_black2;
	private final RendererModel key_white3;
	private final RendererModel key_white4;
	private final RendererModel key_black3;
	private final RendererModel key_white5;
	private final RendererModel controls4;
	private final RendererModel base_plate_4;
	private final RendererModel coords_z;
	private final RendererModel toggle_base;
	private final RendererModel toggle1;
	private final RendererModel coords_y;
	private final RendererModel toggle_base2;
	private final RendererModel toggle2;
	private final RendererModel coords_x;
	private final RendererModel toggle_base3;
	private final RendererModel toggle3;
	private final RendererModel radar;
	private final RendererModel bigslider;
	private final RendererModel inc_slider_move_y;
	private final RendererModel slider_knob;
	private final RendererModel twist;
	private final RendererModel track;
	private final RendererModel button_array;
	private final RendererModel base_plate;
	private final RendererModel controls5;
	private final RendererModel base_plate_5;
	private final RendererModel landing_type;
	private final RendererModel knob;
	private final RendererModel angeled_panel;
	private final RendererModel dummy_buttons;
	private final RendererModel dummy_buttons2;
	private final RendererModel dummy_buttons7;
	private final RendererModel dummy_buttons8;
	private final RendererModel dummy_buttons3;
	private final RendererModel dummy_buttons4;
	private final RendererModel dummy_buttons5;
	private final RendererModel dummy_buttons6;
	private final RendererModel bone67;
	private final RendererModel panels;
	private final RendererModel obtuse_panel;
	private final RendererModel refuler;
	private final RendererModel needle;
	private final RendererModel trim;
	private final RendererModel glass;
	private final RendererModel controls6;
	private final RendererModel base_plate_6;
	private final RendererModel bone69;
	private final RendererModel dummy_buttons9;
	private final RendererModel dial;
	private final RendererModel shield_health;
	private final RendererModel dial_trim;
	private final RendererModel dial_glass;
	private final RendererModel dial2;
	private final RendererModel dial_trim2;
	private final RendererModel dial_glass2;
	private final RendererModel flight_time;
	private final RendererModel slider_frames;
	private final RendererModel stablizers;
	private final RendererModel slider_left_move_y;
	private final RendererModel slider_right_move_y;
	private final RendererModel rib_control;
	private final RendererModel spine_con1;
	private final RendererModel plain7;
	private final RendererModel bar;
	private final RendererModel handbrake_x;
	private final RendererModel bevel7;
	private final RendererModel spine_con2;
	private final RendererModel plain8;
	private final RendererModel winch;
	private final RendererModel case_model;
	private final RendererModel throttle_x;
	private final RendererModel bevel8;
	private final RendererModel spine_con3;
	private final RendererModel plain9;
	private final RendererModel telepathic2;
	private final RendererModel microscope;
	private final RendererModel center;
	private final RendererModel bottom;
	private final RendererModel innards;
	private final RendererModel top;

	public NeutronConsoleModel() {
		textureWidth = 256;
		textureHeight = 256;

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		glow_inner = new RendererModel(this);
		glow_inner.setRotationPoint(-31.0F, 33.0F, -7.0F);
		glow.addChild(glow_inner);
		

		glow_trim = new RendererModel(this);
		glow_trim.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim);
		

		grill_corner_light = new RendererModel(this);
		grill_corner_light.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim.addChild(grill_corner_light);
		setRotationAngle(grill_corner_light, -0.2182F, 0.0F, 0.0F);
		grill_corner_light.cubeList.add(new ModelBox(grill_corner_light, 190, 22, -4.0F, 0.5188F, -26.2471F, 7, 8, 1, 0.0F, false));

		gril_light = new RendererModel(this);
		gril_light.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim.addChild(gril_light);
		setRotationAngle(gril_light, -0.2618F, 0.5236F, 0.0F);
		gril_light.cubeList.add(new ModelBox(gril_light, 190, 21, -7.0F, -1.2227F, -23.3118F, 14, 9, 1, 0.0F, false));

		glow_bar = new RendererModel(this);
		glow_bar.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim.addChild(glow_bar);
		setRotationAngle(glow_bar, 0.0F, 0.5236F, 0.0F);
		glow_bar.cubeList.add(new ModelBox(glow_bar, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar.cubeList.add(new ModelBox(glow_bar, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill = new RendererModel(this);
		glow_corner_fill.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim.addChild(glow_corner_fill);
		glow_corner_fill.cubeList.add(new ModelBox(glow_corner_fill, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill.cubeList.add(new ModelBox(glow_corner_fill, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_trim2 = new RendererModel(this);
		glow_trim2.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim2);
		setRotationAngle(glow_trim2, 0.0F, -1.0472F, 0.0F);
		

		grill_corner_light2 = new RendererModel(this);
		grill_corner_light2.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim2.addChild(grill_corner_light2);
		setRotationAngle(grill_corner_light2, -0.2182F, 0.0F, 0.0F);
		grill_corner_light2.cubeList.add(new ModelBox(grill_corner_light2, 190, 22, -4.0F, 0.5188F, -26.2471F, 8, 8, 1, 0.0F, false));

		gril_light2 = new RendererModel(this);
		gril_light2.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim2.addChild(gril_light2);
		setRotationAngle(gril_light2, -0.2618F, 0.5236F, 0.0F);
		gril_light2.cubeList.add(new ModelBox(gril_light2, 190, 21, -7.0F, -1.2227F, -23.3118F, 15, 9, 1, 0.0F, false));

		glow_bar2 = new RendererModel(this);
		glow_bar2.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim2.addChild(glow_bar2);
		setRotationAngle(glow_bar2, 0.0F, 0.5236F, 0.0F);
		glow_bar2.cubeList.add(new ModelBox(glow_bar2, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar2.cubeList.add(new ModelBox(glow_bar2, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill2 = new RendererModel(this);
		glow_corner_fill2.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim2.addChild(glow_corner_fill2);
		glow_corner_fill2.cubeList.add(new ModelBox(glow_corner_fill2, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill2.cubeList.add(new ModelBox(glow_corner_fill2, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_trim3 = new RendererModel(this);
		glow_trim3.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim3);
		setRotationAngle(glow_trim3, 0.0F, -2.0944F, 0.0F);
		

		grill_corner_light3 = new RendererModel(this);
		grill_corner_light3.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim3.addChild(grill_corner_light3);
		setRotationAngle(grill_corner_light3, -0.2182F, 0.0F, 0.0F);
		grill_corner_light3.cubeList.add(new ModelBox(grill_corner_light3, 190, 22, -4.0F, 0.5188F, -27.2471F, 7, 8, 1, 0.0F, false));

		gril_light3 = new RendererModel(this);
		gril_light3.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim3.addChild(gril_light3);
		setRotationAngle(gril_light3, -0.2618F, 0.5236F, 0.0F);
		gril_light3.cubeList.add(new ModelBox(gril_light3, 190, 21, -7.0F, -1.2227F, -24.3118F, 15, 9, 1, 0.0F, false));

		glow_bar3 = new RendererModel(this);
		glow_bar3.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim3.addChild(glow_bar3);
		setRotationAngle(glow_bar3, 0.0F, 0.5236F, 0.0F);
		glow_bar3.cubeList.add(new ModelBox(glow_bar3, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar3.cubeList.add(new ModelBox(glow_bar3, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill3 = new RendererModel(this);
		glow_corner_fill3.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim3.addChild(glow_corner_fill3);
		glow_corner_fill3.cubeList.add(new ModelBox(glow_corner_fill3, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill3.cubeList.add(new ModelBox(glow_corner_fill3, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_trim4 = new RendererModel(this);
		glow_trim4.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim4);
		setRotationAngle(glow_trim4, 0.0F, 3.1416F, 0.0F);
		

		grill_corner_light4 = new RendererModel(this);
		grill_corner_light4.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim4.addChild(grill_corner_light4);
		setRotationAngle(grill_corner_light4, -0.2182F, 0.0F, 0.0F);
		grill_corner_light4.cubeList.add(new ModelBox(grill_corner_light4, 190, 22, -5.0F, 0.5188F, -27.2471F, 8, 8, 1, 0.0F, false));

		gril_light4 = new RendererModel(this);
		gril_light4.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim4.addChild(gril_light4);
		setRotationAngle(gril_light4, -0.2618F, 0.5236F, 0.0F);
		gril_light4.cubeList.add(new ModelBox(gril_light4, 190, 21, -8.0F, -1.2227F, -23.3118F, 15, 9, 1, 0.0F, false));

		glow_bar4 = new RendererModel(this);
		glow_bar4.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim4.addChild(glow_bar4);
		setRotationAngle(glow_bar4, 0.0F, 0.5236F, 0.0F);
		glow_bar4.cubeList.add(new ModelBox(glow_bar4, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar4.cubeList.add(new ModelBox(glow_bar4, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill4 = new RendererModel(this);
		glow_corner_fill4.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim4.addChild(glow_corner_fill4);
		glow_corner_fill4.cubeList.add(new ModelBox(glow_corner_fill4, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill4.cubeList.add(new ModelBox(glow_corner_fill4, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_trim5 = new RendererModel(this);
		glow_trim5.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim5);
		setRotationAngle(glow_trim5, 0.0F, 2.0944F, 0.0F);
		

		grill_corner_light5 = new RendererModel(this);
		grill_corner_light5.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim5.addChild(grill_corner_light5);
		setRotationAngle(grill_corner_light5, -0.2182F, 0.0F, 0.0F);
		grill_corner_light5.cubeList.add(new ModelBox(grill_corner_light5, 190, 22, -5.0F, 0.5188F, -27.2471F, 8, 8, 1, 0.0F, false));

		gril_light5 = new RendererModel(this);
		gril_light5.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim5.addChild(gril_light5);
		setRotationAngle(gril_light5, -0.2618F, 0.5236F, 0.0F);
		gril_light5.cubeList.add(new ModelBox(gril_light5, 190, 21, -7.0F, -1.2227F, -24.3118F, 14, 9, 1, 0.0F, false));

		glow_bar5 = new RendererModel(this);
		glow_bar5.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim5.addChild(glow_bar5);
		setRotationAngle(glow_bar5, 0.0F, 0.5236F, 0.0F);
		glow_bar5.cubeList.add(new ModelBox(glow_bar5, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar5.cubeList.add(new ModelBox(glow_bar5, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill5 = new RendererModel(this);
		glow_corner_fill5.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim5.addChild(glow_corner_fill5);
		glow_corner_fill5.cubeList.add(new ModelBox(glow_corner_fill5, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill5.cubeList.add(new ModelBox(glow_corner_fill5, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_trim6 = new RendererModel(this);
		glow_trim6.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim6);
		setRotationAngle(glow_trim6, 0.0F, 1.0472F, 0.0F);
		

		grill_corner_light6 = new RendererModel(this);
		grill_corner_light6.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim6.addChild(grill_corner_light6);
		setRotationAngle(grill_corner_light6, -0.2182F, 0.0F, 0.0F);
		grill_corner_light6.cubeList.add(new ModelBox(grill_corner_light6, 190, 22, -5.0F, 0.5188F, -27.2471F, 8, 8, 1, 0.0F, false));

		gril_light6 = new RendererModel(this);
		gril_light6.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim6.addChild(gril_light6);
		setRotationAngle(gril_light6, -0.2618F, 0.5236F, 0.0F);
		gril_light6.cubeList.add(new ModelBox(gril_light6, 190, 21, -7.0F, -1.2227F, -24.3118F, 14, 9, 1, 0.0F, false));

		glow_bar6 = new RendererModel(this);
		glow_bar6.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim6.addChild(glow_bar6);
		setRotationAngle(glow_bar6, 0.0F, 0.5236F, 0.0F);
		glow_bar6.cubeList.add(new ModelBox(glow_bar6, 198, 29, -12.0F, -3.7731F, -22.1137F, 24, 1, 1, 0.0F, false));
		glow_bar6.cubeList.add(new ModelBox(glow_bar6, 198, 29, -12.0F, -9.7731F, -19.6137F, 24, 1, 1, 0.0F, false));

		glow_corner_fill6 = new RendererModel(this);
		glow_corner_fill6.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim6.addChild(glow_corner_fill6);
		glow_corner_fill6.cubeList.add(new ModelBox(glow_corner_fill6, 208, 25, 24.1556F, -119.1481F, -17.0123F, 14, 2, 1, 0.0F, false));
		glow_corner_fill6.cubeList.add(new ModelBox(glow_corner_fill6, 200, 24, 25.7091F, -125.1481F, -15.4142F, 11, 2, 2, 0.0F, false));

		glow_rotor_bob_y = new RendererModel(this);
		glow_rotor_bob_y.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_rotor_bob_y);
		

		crystal_spin_y = new RendererModel(this);
		crystal_spin_y.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_rotor_bob_y.addChild(crystal_spin_y);
		

		y45 = new RendererModel(this);
		y45.setRotationPoint(0.0F, 0.0F, 0.0F);
		crystal_spin_y.addChild(y45);
		setRotationAngle(y45, 0.0F, -0.7854F, 0.0F);
		y45.cubeList.add(new ModelBox(y45, 125, 1, -8.0F, -104.0F, -8.0F, 16, 16, 16, 0.0F, false));

		z45 = new RendererModel(this);
		z45.setRotationPoint(0.0F, -96.0F, 0.0F);
		crystal_spin_y.addChild(z45);
		setRotationAngle(z45, 0.0F, 0.0F, 0.7854F);
		z45.cubeList.add(new ModelBox(z45, 125, 1, -8.0F, -8.0F, -8.0F, 16, 16, 16, 0.0F, false));

		x45 = new RendererModel(this);
		x45.setRotationPoint(0.0F, -96.0F, 0.0F);
		crystal_spin_y.addChild(x45);
		setRotationAngle(x45, -0.7854F, 0.0F, 0.0F);
		x45.cubeList.add(new ModelBox(x45, 125, 1, -8.0F, -8.0F, -8.0F, 16, 16, 16, 0.0F, false));

		hover_rings = new RendererModel(this);
		hover_rings.setRotationPoint(0.0F, -6.0F, 0.0F);
		glow_rotor_bob_y.addChild(hover_rings);
		

		outer_rotate_x = new RendererModel(this);
		outer_rotate_x.setRotationPoint(0.0F, -91.0F, 0.0F);
		hover_rings.addChild(outer_rotate_x);
		setRotationAngle(outer_rotate_x, 0.7854F, 0.0F, 0.0F);
		

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.0934F, -0.0625F, -0.0611F);
		outer_rotate_x.addChild(bone61);
		setRotationAngle(bone61, 0.0F, -0.5236F, 0.0F);
		bone61.cubeList.add(new ModelBox(bone61, 210, 23, -10.0F, -1.5F, 16.3205F, 20, 3, 1, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 210, 23, -10.0F, -1.5F, -17.3205F, 20, 3, 1, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(-1.5684F, -0.0625F, 5.9867F);
		outer_rotate_x.addChild(bone62);
		setRotationAngle(bone62, 0.0F, -1.5708F, 0.0F);
		bone62.cubeList.add(new ModelBox(bone62, 210, 23, -16.0477F, -1.5F, 14.6587F, 20, 3, 1, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 210, 23, -16.0477F, -1.5F, -18.9823F, 20, 3, 1, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(-5.2957F, -0.0625F, 18.1362F);
		outer_rotate_x.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -2.618F, 0.0F);
		bone63.cubeList.add(new ModelBox(bone63, 210, 23, -23.7658F, -1.5F, 29.3852F, 20, 3, 1, 0.0F, false));
		bone63.cubeList.add(new ModelBox(bone63, 210, 23, -23.7658F, -1.5F, -4.2558F, 20, 3, 1, 0.0F, false));

		inner_rotate_x = new RendererModel(this);
		inner_rotate_x.setRotationPoint(0.0F, -91.0F, 0.0F);
		hover_rings.addChild(inner_rotate_x);
		setRotationAngle(inner_rotate_x, -0.7854F, 0.0F, 0.0F);
		

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(-0.0925F, -0.3125F, -0.0206F);
		inner_rotate_x.addChild(bone64);
		setRotationAngle(bone64, 0.0F, -0.5236F, 0.0F);
		bone64.cubeList.add(new ModelBox(bone64, 210, 23, -9.0F, -2.5F, 14.5885F, 18, 5, 1, 0.0F, false));
		bone64.cubeList.add(new ModelBox(bone64, 210, 23, -9.0F, -2.5F, -15.5885F, 18, 5, 1, 0.0F, false));

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(-2.3141F, -0.3125F, 5.6948F);
		inner_rotate_x.addChild(bone65);
		setRotationAngle(bone65, 0.0F, -1.5708F, 0.0F);
		bone65.cubeList.add(new ModelBox(bone65, 210, 23, -14.7154F, -2.5F, 12.3668F, 18, 5, 1, 0.0F, false));
		bone65.cubeList.add(new ModelBox(bone65, 210, 23, -14.7154F, -2.5F, -17.8101F, 18, 5, 1, 0.0F, false));

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(-5.9332F, -0.3125F, 16.7198F);
		inner_rotate_x.addChild(bone66);
		setRotationAngle(bone66, 0.0F, -2.618F, 0.0F);
		bone66.cubeList.add(new ModelBox(bone66, 210, 23, -22.4284F, -2.5F, 26.1658F, 18, 5, 1, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 210, 23, -22.4284F, -2.5F, -4.0111F, 18, 5, 1, 0.0F, false));

		glow_posts = new RendererModel(this);
		glow_posts.setRotationPoint(0.0F, -1.9685F, 0.0246F);
		glow.addChild(glow_posts);
		

		basepost1 = new RendererModel(this);
		basepost1.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost1);
		basepost1.cubeList.add(new ModelBox(basepost1, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		basepost2 = new RendererModel(this);
		basepost2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost2);
		setRotationAngle(basepost2, 0.0F, -1.0472F, 0.0F);
		basepost2.cubeList.add(new ModelBox(basepost2, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		basepost3 = new RendererModel(this);
		basepost3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost3);
		setRotationAngle(basepost3, 0.0F, -2.0944F, 0.0F);
		basepost3.cubeList.add(new ModelBox(basepost3, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		basepost4 = new RendererModel(this);
		basepost4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost4);
		setRotationAngle(basepost4, 0.0F, 1.0472F, 0.0F);
		basepost4.cubeList.add(new ModelBox(basepost4, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		basepost5 = new RendererModel(this);
		basepost5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost5);
		setRotationAngle(basepost5, 0.0F, 2.0944F, 0.0F);
		basepost5.cubeList.add(new ModelBox(basepost5, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		basepost6 = new RendererModel(this);
		basepost6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost6);
		setRotationAngle(basepost6, 0.0F, 3.1416F, 0.0F);
		basepost6.cubeList.add(new ModelBox(basepost6, 190, 4, -3.0F, -23.0F, -26.25F, 6, 19, 6, 0.0F, false));

		glow_controls = new RendererModel(this);
		glow_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_controls);
		setRotationAngle(glow_controls, 0.0F, -0.5236F, 0.0F);
		

		controls8 = new RendererModel(this);
		controls8.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls8);
		setRotationAngle(controls8, 0.0F, -0.5236F, 0.0F);
		

		base_plate_7 = new RendererModel(this);
		base_plate_7.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls8.addChild(base_plate_7);
		setRotationAngle(base_plate_7, -1.309F, 0.0F, 0.0F);
		

		buttonstrip2 = new RendererModel(this);
		buttonstrip2.setRotationPoint(-32.0F, 93.3792F, 56.7684F);
		base_plate_7.addChild(buttonstrip2);
		

		buttons_lit2 = new RendererModel(this);
		buttons_lit2.setRotationPoint(0.0F, 0.0F, 0.0F);
		buttonstrip2.addChild(buttons_lit2);
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 15.5F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 33.5F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 24.5F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 42.5F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 20.0F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 38.0F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 29.0F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 47.0F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 17.75F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 35.75F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 26.75F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 44.75F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 22.25F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 40.25F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));
		buttons_lit2.cubeList.add(new ModelBox(buttons_lit2, 164, 80, 31.25F, -87.2042F, -56.6047F, 1, 2, 1, 0.0F, false));

		small_screen2 = new RendererModel(this);
		small_screen2.setRotationPoint(9.0F, 0.1749F, -2.3363F);
		base_plate_7.addChild(small_screen2);
		setRotationAngle(small_screen2, 0.6109F, 0.0F, 0.0F);
		small_screen2.cubeList.add(new ModelBox(small_screen2, 208, 23, -2.2188F, -3.0F, 0.4375F, 6, 6, 1, 0.0F, false));

		indicator4 = new RendererModel(this);
		indicator4.setRotationPoint(-7.25F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator4);
		setRotationAngle(indicator4, -1.0472F, 0.0F, 0.0F);
		indicator4.cubeList.add(new ModelBox(indicator4, 167, 78, -1.0F, -0.625F, -0.875F, 2, 2, 2, 0.0F, false));

		indicator5 = new RendererModel(this);
		indicator5.setRotationPoint(-2.25F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator5);
		setRotationAngle(indicator5, -1.0472F, 0.0F, 0.0F);
		indicator5.cubeList.add(new ModelBox(indicator5, 167, 78, -1.0F, -0.625F, -0.875F, 2, 2, 2, 0.0F, false));

		indicator6 = new RendererModel(this);
		indicator6.setRotationPoint(2.75F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator6);
		setRotationAngle(indicator6, -1.0472F, 0.0F, 0.0F);
		indicator6.cubeList.add(new ModelBox(indicator6, 167, 78, -1.0F, -0.625F, -0.875F, 2, 2, 2, 0.0F, false));

		coms2 = new RendererModel(this);
		coms2.setRotationPoint(0.0F, -13.0751F, -0.0863F);
		base_plate_7.addChild(coms2);
		setRotationAngle(coms2, -0.7854F, 0.0F, 0.0F);
		coms2.cubeList.add(new ModelBox(coms2, 164, 76, -5.5F, -1.5F, -1.5F, 11, 3, 3, 0.0F, false));

		controls9 = new RendererModel(this);
		controls9.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls9);
		setRotationAngle(controls9, 0.0F, -1.5708F, 0.0F);
		

		base_plate_8 = new RendererModel(this);
		base_plate_8.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls9.addChild(base_plate_8);
		setRotationAngle(base_plate_8, -1.309F, 0.0F, 0.0F);
		

		dummy_buttons13 = new RendererModel(this);
		dummy_buttons13.setRotationPoint(6.0F, -11.0751F, 0.5387F);
		base_plate_8.addChild(dummy_buttons13);
		dummy_buttons13.cubeList.add(new ModelBox(dummy_buttons13, 167, 76, -1.0F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons13.cubeList.add(new ModelBox(dummy_buttons13, 167, 76, -1.0F, -1.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons13.cubeList.add(new ModelBox(dummy_buttons13, 167, 76, -1.0F, 2.0F, -1.5F, 2, 2, 3, 0.0F, false));

		dummy_buttons14 = new RendererModel(this);
		dummy_buttons14.setRotationPoint(-6.0F, -11.0751F, 0.5387F);
		base_plate_8.addChild(dummy_buttons14);
		dummy_buttons14.cubeList.add(new ModelBox(dummy_buttons14, 167, 76, -1.0F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons14.cubeList.add(new ModelBox(dummy_buttons14, 167, 76, -1.0F, -1.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons14.cubeList.add(new ModelBox(dummy_buttons14, 167, 76, -1.0F, 2.0F, -1.5F, 2, 2, 3, 0.0F, false));

		dummy_buttons15 = new RendererModel(this);
		dummy_buttons15.setRotationPoint(-4.0F, 8.9249F, -0.2113F);
		base_plate_8.addChild(dummy_buttons15);
		dummy_buttons15.cubeList.add(new ModelBox(dummy_buttons15, 167, 76, -0.6875F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons15.cubeList.add(new ModelBox(dummy_buttons15, 167, 76, 1.7188F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons15.cubeList.add(new ModelBox(dummy_buttons15, 167, 76, 4.0938F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));
		dummy_buttons15.cubeList.add(new ModelBox(dummy_buttons15, 167, 76, 6.4375F, -4.0F, -1.5F, 2, 2, 3, 0.0F, false));

		controls10 = new RendererModel(this);
		controls10.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls10);
		setRotationAngle(controls10, 0.0F, -2.618F, 0.0F);
		

		base_plate_9 = new RendererModel(this);
		base_plate_9.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls10.addChild(base_plate_9);
		setRotationAngle(base_plate_9, -1.309F, 0.0F, 0.0F);
		

		base2 = new RendererModel(this);
		base2.setRotationPoint(1.1071F, -0.2894F, 0.2976F);
		base_plate_9.addChild(base2);
		base2.cubeList.add(new ModelBox(base2, 199, 20, -5.6071F, -11.0357F, -1.1339F, 9, 9, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 164, 73, -11.1071F, 4.9643F, -1.1339F, 3, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 164, 73, -11.1071F, 0.9643F, -1.1339F, 3, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 165, 74, 5.8929F, 4.9643F, -1.1339F, 3, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 165, 74, 5.8929F, 0.9643F, -1.1339F, 3, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 165, 74, 5.8929F, -3.0357F, -1.1339F, 3, 3, 1, 0.0F, false));

		controls11 = new RendererModel(this);
		controls11.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls11);
		setRotationAngle(controls11, 0.0F, 2.618F, 0.0F);
		

		base_plate_10 = new RendererModel(this);
		base_plate_10.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls11.addChild(base_plate_10);
		setRotationAngle(base_plate_10, -1.309F, 0.0F, 0.0F);
		

		coords_z2 = new RendererModel(this);
		coords_z2.setRotationPoint(-12.5F, 6.3792F, 0.7684F);
		base_plate_10.addChild(coords_z2);
		

		toggle_base4 = new RendererModel(this);
		toggle_base4.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_z2.addChild(toggle_base4);
		toggle_base4.cubeList.add(new ModelBox(toggle_base4, 129, 56, 32.0F, -86.4542F, -57.6047F, 1, 3, 1, 0.0F, false));
		toggle_base4.cubeList.add(new ModelBox(toggle_base4, 106, 10, 33.0F, -86.4542F, -57.6047F, 5, 3, 1, 0.0F, false));

		coords_y2 = new RendererModel(this);
		coords_y2.setRotationPoint(-12.5F, 0.3792F, 0.7684F);
		base_plate_10.addChild(coords_y2);
		

		toggle_base5 = new RendererModel(this);
		toggle_base5.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_y2.addChild(toggle_base5);
		toggle_base5.cubeList.add(new ModelBox(toggle_base5, 106, 10, 33.0F, -86.4542F, -57.6047F, 5, 3, 1, 0.0F, false));
		toggle_base5.cubeList.add(new ModelBox(toggle_base5, 145, 73, 32.0F, -86.4542F, -57.6047F, 1, 3, 1, 0.0F, false));

		toggle5 = new RendererModel(this);
		toggle5.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_y2.addChild(toggle5);
		setRotationAngle(toggle5, 0.2182F, 0.0F, 0.0F);
		

		coords_x2 = new RendererModel(this);
		coords_x2.setRotationPoint(-12.5F, -5.6208F, 0.7684F);
		base_plate_10.addChild(coords_x2);
		

		toggle_base6 = new RendererModel(this);
		toggle_base6.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_x2.addChild(toggle_base6);
		toggle_base6.cubeList.add(new ModelBox(toggle_base6, 106, 10, 33.0F, -86.4542F, -57.6047F, 5, 3, 1, 0.0F, false));
		toggle_base6.cubeList.add(new ModelBox(toggle_base6, 129, 39, 32.0F, -86.4542F, -57.6047F, 1, 3, 1, 0.0F, false));

		radar2 = new RendererModel(this);
		radar2.setRotationPoint(9.5F, 2.4249F, 0.2887F);
		base_plate_10.addChild(radar2);
		radar2.cubeList.add(new ModelBox(radar2, 146, 72, -5.5F, -2.5F, -0.5F, 1, 5, 1, 0.0F, false));
		radar2.cubeList.add(new ModelBox(radar2, 146, 72, 4.5F, -2.5F, -0.5F, 1, 5, 1, 0.0F, false));
		radar2.cubeList.add(new ModelBox(radar2, 146, 72, -4.5F, -4.5F, -0.5F, 2, 9, 1, 0.0F, false));
		radar2.cubeList.add(new ModelBox(radar2, 146, 72, 2.5F, -4.5F, -0.5F, 2, 9, 1, 0.0F, false));
		radar2.cubeList.add(new ModelBox(radar2, 146, 72, -2.5F, -5.5F, -0.5F, 5, 11, 1, 0.0F, false));

		bigslider2 = new RendererModel(this);
		bigslider2.setRotationPoint(-0.5F, 0.4898F, -0.2209F);
		base_plate_10.addChild(bigslider2);
		

		button_array2 = new RendererModel(this);
		button_array2.setRotationPoint(-31.75F, 91.3792F, 56.7684F);
		base_plate_10.addChild(button_array2);
		

		buttons2 = new RendererModel(this);
		buttons2.setRotationPoint(0.0F, 0.0F, -0.25F);
		button_array2.addChild(buttons2);
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 24.0F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 28.5F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 33.0F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 26.25F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 30.75F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 35.25F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 37.5F, -107.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 24.0F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 28.5F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 33.0F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 26.25F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 30.75F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 35.25F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 37.5F, -105.4542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 24.0F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 28.5F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 33.0F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 26.25F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 30.75F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 35.25F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));
		buttons2.cubeList.add(new ModelBox(buttons2, 165, 76, 37.5F, -102.9542F, -56.3547F, 2, 2, 2, 0.0F, false));

		controls12 = new RendererModel(this);
		controls12.setRotationPoint(0.0F, 0.0F, -0.75F);
		glow_controls.addChild(controls12);
		setRotationAngle(controls12, 0.0F, 1.5708F, 0.0F);
		

		base_plate_11 = new RendererModel(this);
		base_plate_11.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls12.addChild(base_plate_11);
		setRotationAngle(base_plate_11, -1.309F, 0.0F, 0.0F);
		

		angeled_panel2 = new RendererModel(this);
		angeled_panel2.setRotationPoint(-32.0F, 92.8792F, 58.4871F);
		base_plate_11.addChild(angeled_panel2);
		

		dummy_buttons19 = new RendererModel(this);
		dummy_buttons19.setRotationPoint(12.5F, -22.75F, -1.25F);
		angeled_panel2.addChild(dummy_buttons19);
		dummy_buttons19.cubeList.add(new ModelBox(dummy_buttons19, 166, 77, 13.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));
		dummy_buttons19.cubeList.add(new ModelBox(dummy_buttons19, 148, 75, 17.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));
		dummy_buttons19.cubeList.add(new ModelBox(dummy_buttons19, 166, 77, 19.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));
		dummy_buttons19.cubeList.add(new ModelBox(dummy_buttons19, 166, 77, 23.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));

		refuler2 = new RendererModel(this);
		refuler2.setRotationPoint(-44.25F, 92.1292F, 58.3934F);
		base_plate_11.addChild(refuler2);
		

		glass2 = new RendererModel(this);
		glass2.setRotationPoint(0.0F, 0.0F, -0.25F);
		refuler2.addChild(glass2);
		glass2.cubeList.add(new ModelBox(glass2, 164, 75, 36.25F, -90.4542F, -57.6047F, 1, 3, 1, 0.0F, false));
		glass2.cubeList.add(new ModelBox(glass2, 164, 75, 44.25F, -90.4542F, -57.6047F, 1, 3, 1, 0.0F, false));
		glass2.cubeList.add(new ModelBox(glass2, 164, 75, 37.25F, -92.4542F, -57.6047F, 2, 5, 1, 0.0F, false));
		glass2.cubeList.add(new ModelBox(glass2, 164, 75, 42.25F, -92.4542F, -57.6047F, 2, 5, 1, 0.0F, false));
		glass2.cubeList.add(new ModelBox(glass2, 164, 75, 39.25F, -93.4542F, -57.6047F, 3, 6, 1, 0.0F, false));

		controls13 = new RendererModel(this);
		controls13.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls13);
		setRotationAngle(controls13, 0.0F, 0.5236F, 0.0F);
		

		base_plate_12 = new RendererModel(this);
		base_plate_12.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls13.addChild(base_plate_12);
		setRotationAngle(base_plate_12, -1.309F, 0.0F, 0.0F);
		

		dummy_buttons24 = new RendererModel(this);
		dummy_buttons24.setRotationPoint(-15.0F, 90.5979F, 56.5184F);
		base_plate_12.addChild(dummy_buttons24);
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 147, 77, 14.0F, -101.4542F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 147, 77, 14.0F, -104.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 168, 76, 11.5F, -102.9542F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 166, 74, 16.5F, -105.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 168, 77, 20.5F, -98.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 167, 78, 7.5F, -95.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons24.cubeList.add(new ModelBox(dummy_buttons24, 166, 75, 4.5F, -95.2042F, -56.6047F, 2, 2, 1, 0.0F, false));

		dial3 = new RendererModel(this);
		dial3.setRotationPoint(-14.0F, 3.4898F, -3.2209F);
		base_plate_12.addChild(dial3);
		

		dial_glass3 = new RendererModel(this);
		dial_glass3.setRotationPoint(3.5F, -0.6106F, 4.1143F);
		dial3.addChild(dial_glass3);
		dial_glass3.cubeList.add(new ModelBox(dial_glass3, 165, 75, -2.0F, -2.4542F, -1.6047F, 3, 5, 1, 0.0F, false));
		dial_glass3.cubeList.add(new ModelBox(dial_glass3, 165, 75, 1.0F, -1.4542F, -1.6047F, 1, 4, 1, 0.0F, false));
		dial_glass3.cubeList.add(new ModelBox(dial_glass3, 165, 75, -3.0F, -1.4542F, -1.6047F, 1, 4, 1, 0.0F, false));
		dial_glass3.cubeList.add(new ModelBox(dial_glass3, 165, 75, 2.0F, -0.4542F, -1.6047F, 1, 3, 1, 0.0F, false));
		dial_glass3.cubeList.add(new ModelBox(dial_glass3, 165, 75, -4.0F, -0.4542F, -1.6047F, 1, 3, 1, 0.0F, false));

		dial4 = new RendererModel(this);
		dial4.setRotationPoint(11.0F, 4.4898F, -0.2209F);
		base_plate_12.addChild(dial4);
		

		dial_glass4 = new RendererModel(this);
		dial_glass4.setRotationPoint(-22.5F, 87.3894F, 56.1143F);
		dial4.addChild(dial_glass4);
		dial_glass4.cubeList.add(new ModelBox(dial_glass4, 165, 74, 21.0F, -91.4542F, -56.6047F, 3, 5, 1, 0.0F, false));
		dial_glass4.cubeList.add(new ModelBox(dial_glass4, 165, 74, 24.0F, -90.4542F, -56.6047F, 1, 4, 1, 0.0F, false));
		dial_glass4.cubeList.add(new ModelBox(dial_glass4, 165, 74, 20.0F, -90.4542F, -56.6047F, 1, 4, 1, 0.0F, false));
		dial_glass4.cubeList.add(new ModelBox(dial_glass4, 165, 74, 25.0F, -89.4542F, -56.6047F, 1, 3, 1, 0.0F, false));
		dial_glass4.cubeList.add(new ModelBox(dial_glass4, 165, 74, 19.0F, -89.4542F, -56.6047F, 1, 3, 1, 0.0F, false));

		rib_control2 = new RendererModel(this);
		rib_control2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(rib_control2);
		setRotationAngle(rib_control2, 0.0F, -0.5236F, 0.0F);
		

		spine_con4 = new RendererModel(this);
		spine_con4.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control2.addChild(spine_con4);
		setRotationAngle(spine_con4, 0.0F, -0.5236F, 0.0F);
		

		plain10 = new RendererModel(this);
		plain10.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con4.addChild(plain10);
		setRotationAngle(plain10, -1.309F, 0.0F, 0.0F);
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, -2.5F, -13.0751F, -2.8363F, 2, 2, 2, 0.0F, false));
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, -2.5F, -16.0751F, -2.8363F, 2, 2, 2, 0.0F, false));
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, -2.5F, -19.0751F, -2.8363F, 2, 2, 2, 0.0F, false));
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, 0.5F, -13.0751F, -2.8363F, 2, 2, 2, 0.0F, false));
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, 0.5F, -16.0751F, -2.8363F, 2, 2, 2, 0.0F, false));
		plain10.cubeList.add(new ModelBox(plain10, 164, 70, 0.5F, -19.0751F, -2.8363F, 2, 2, 2, 0.0F, false));

		spine_con6 = new RendererModel(this);
		spine_con6.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control2.addChild(spine_con6);
		setRotationAngle(spine_con6, 0.0F, 1.5708F, 0.0F);
		

		plain12 = new RendererModel(this);
		plain12.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con6.addChild(plain12);
		setRotationAngle(plain12, -1.309F, 0.0F, 0.0F);
		

		telepathic3 = new RendererModel(this);
		telepathic3.setRotationPoint(-31.5F, 90.6292F, 59.7684F);
		plain12.addChild(telepathic3);
		

		microscope2 = new RendererModel(this);
		microscope2.setRotationPoint(31.5F, -77.7189F, -56.9429F);
		telepathic3.addChild(microscope2);
		setRotationAngle(microscope2, 0.5236F, 0.0F, 0.0F);
		microscope2.cubeList.add(new ModelBox(microscope2, 163, 72, -2.0F, -6.6013F, -3.9118F, 4, 4, 1, 0.0F, false));
		microscope2.cubeList.add(new ModelBox(microscope2, 110, 5, -1.0F, -5.6013F, -7.4118F, 2, 2, 1, 0.0F, false));
		microscope2.cubeList.add(new ModelBox(microscope2, 148, 59, -1.5F, -6.1013F, -20.9118F, 3, 3, 1, 0.0F, false));

		spines = new RendererModel(this);
		spines.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		silverib1 = new RendererModel(this);
		silverib1.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib1);
		setRotationAngle(silverib1, 0.0F, -1.0472F, 0.0F);
		

		spine1 = new RendererModel(this);
		spine1.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib1.addChild(spine1);
		setRotationAngle(spine1, 0.0F, -0.5236F, 0.0F);
		

		plain1 = new RendererModel(this);
		plain1.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine1.addChild(plain1);
		setRotationAngle(plain1, -1.309F, 0.0F, 0.0F);
		plain1.cubeList.add(new ModelBox(plain1, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 24, 2, 0.0F, false));
		plain1.cubeList.add(new ModelBox(plain1, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain1.cubeList.add(new ModelBox(plain1, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain1.cubeList.add(new ModelBox(plain1, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain1.cubeList.add(new ModelBox(plain1, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel1 = new RendererModel(this);
		bevel1.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine1.addChild(bevel1);
		setRotationAngle(bevel1, -0.6981F, 0.0F, 0.0F);
		bevel1.cubeList.add(new ModelBox(bevel1, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel1.cubeList.add(new ModelBox(bevel1, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel1.cubeList.add(new ModelBox(bevel1, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel1.cubeList.add(new ModelBox(bevel1, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front1 = new RendererModel(this);
		front1.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine1.addChild(front1);
		front1.cubeList.add(new ModelBox(front1, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front1.cubeList.add(new ModelBox(front1, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front1.cubeList.add(new ModelBox(front1, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front1.addChild(bone);
		setRotationAngle(bone, -0.2618F, 0.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring = new RendererModel(this);
		ring.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front1.addChild(ring);
		ring.cubeList.add(new ModelBox(ring, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring.addChild(bone2);
		setRotationAngle(bone2, -0.2618F, 0.0F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring.addChild(bone3);
		setRotationAngle(bone3, 0.2618F, 0.0F, 0.0F);
		bone3.cubeList.add(new ModelBox(bone3, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring2 = new RendererModel(this);
		ring2.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front1.addChild(ring2);
		ring2.cubeList.add(new ModelBox(ring2, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring2.addChild(bone4);
		setRotationAngle(bone4, -0.2618F, 0.0F, 0.0F);
		bone4.cubeList.add(new ModelBox(bone4, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring2.addChild(bone5);
		setRotationAngle(bone5, 0.2618F, 0.0F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards = new RendererModel(this);
		glow__innards.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring2.addChild(glow__innards);
		

		undercurve1 = new RendererModel(this);
		undercurve1.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine1.addChild(undercurve1);
		setRotationAngle(undercurve1, 1.309F, 0.0F, 0.0F);
		undercurve1.cubeList.add(new ModelBox(undercurve1, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve1.cubeList.add(new ModelBox(undercurve1, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve1.cubeList.add(new ModelBox(undercurve1, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve1.cubeList.add(new ModelBox(undercurve1, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve1.cubeList.add(new ModelBox(undercurve1, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve1.cubeList.add(new ModelBox(undercurve1, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend1 = new RendererModel(this);
		backbend1.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine1.addChild(backbend1);
		setRotationAngle(backbend1, 0.3491F, 0.0F, 0.0F);
		backbend1.cubeList.add(new ModelBox(backbend1, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend1.cubeList.add(new ModelBox(backbend1, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend1.cubeList.add(new ModelBox(backbend1, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend1.cubeList.add(new ModelBox(backbend1, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend1.cubeList.add(new ModelBox(backbend1, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin1 = new RendererModel(this);
		shin1.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine1.addChild(shin1);
		setRotationAngle(shin1, -0.2618F, 0.0F, 0.0F);
		

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin1.addChild(bone16);
		

		ribfoot1 = new RendererModel(this);
		ribfoot1.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine1.addChild(ribfoot1);
		ribfoot1.cubeList.add(new ModelBox(ribfoot1, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		silverib2 = new RendererModel(this);
		silverib2.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib2);
		

		spine2 = new RendererModel(this);
		spine2.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib2.addChild(spine2);
		setRotationAngle(spine2, 0.0F, -0.5236F, 0.0F);
		

		plain2 = new RendererModel(this);
		plain2.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine2.addChild(plain2);
		setRotationAngle(plain2, -1.309F, 0.0F, 0.0F);
		plain2.cubeList.add(new ModelBox(plain2, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 24, 2, 0.0F, false));
		plain2.cubeList.add(new ModelBox(plain2, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain2.cubeList.add(new ModelBox(plain2, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain2.cubeList.add(new ModelBox(plain2, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain2.cubeList.add(new ModelBox(plain2, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel2 = new RendererModel(this);
		bevel2.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine2.addChild(bevel2);
		setRotationAngle(bevel2, -0.6981F, 0.0F, 0.0F);
		bevel2.cubeList.add(new ModelBox(bevel2, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel2.cubeList.add(new ModelBox(bevel2, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel2.cubeList.add(new ModelBox(bevel2, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel2.cubeList.add(new ModelBox(bevel2, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front2 = new RendererModel(this);
		front2.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine2.addChild(front2);
		front2.cubeList.add(new ModelBox(front2, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front2.cubeList.add(new ModelBox(front2, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front2.cubeList.add(new ModelBox(front2, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front2.addChild(bone8);
		setRotationAngle(bone8, -0.2618F, 0.0F, 0.0F);
		bone8.cubeList.add(new ModelBox(bone8, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring5 = new RendererModel(this);
		ring5.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front2.addChild(ring5);
		ring5.cubeList.add(new ModelBox(ring5, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring5.addChild(bone9);
		setRotationAngle(bone9, -0.2618F, 0.0F, 0.0F);
		bone9.cubeList.add(new ModelBox(bone9, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring5.addChild(bone10);
		setRotationAngle(bone10, 0.2618F, 0.0F, 0.0F);
		bone10.cubeList.add(new ModelBox(bone10, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring6 = new RendererModel(this);
		ring6.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front2.addChild(ring6);
		ring6.cubeList.add(new ModelBox(ring6, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring6.addChild(bone13);
		setRotationAngle(bone13, -0.2618F, 0.0F, 0.0F);
		bone13.cubeList.add(new ModelBox(bone13, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring6.addChild(bone14);
		setRotationAngle(bone14, 0.2618F, 0.0F, 0.0F);
		bone14.cubeList.add(new ModelBox(bone14, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards2 = new RendererModel(this);
		glow__innards2.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring6.addChild(glow__innards2);
		

		undercurve2 = new RendererModel(this);
		undercurve2.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine2.addChild(undercurve2);
		setRotationAngle(undercurve2, 1.309F, 0.0F, 0.0F);
		undercurve2.cubeList.add(new ModelBox(undercurve2, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve2.cubeList.add(new ModelBox(undercurve2, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve2.cubeList.add(new ModelBox(undercurve2, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve2.cubeList.add(new ModelBox(undercurve2, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve2.cubeList.add(new ModelBox(undercurve2, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve2.cubeList.add(new ModelBox(undercurve2, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend2 = new RendererModel(this);
		backbend2.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine2.addChild(backbend2);
		setRotationAngle(backbend2, 0.3491F, 0.0F, 0.0F);
		backbend2.cubeList.add(new ModelBox(backbend2, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend2.cubeList.add(new ModelBox(backbend2, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend2.cubeList.add(new ModelBox(backbend2, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend2.cubeList.add(new ModelBox(backbend2, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend2.cubeList.add(new ModelBox(backbend2, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin2 = new RendererModel(this);
		shin2.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine2.addChild(shin2);
		setRotationAngle(shin2, -0.2618F, 0.0F, 0.0F);
		

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin2.addChild(bone15);
		

		ribfoot2 = new RendererModel(this);
		ribfoot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine2.addChild(ribfoot2);
		ribfoot2.cubeList.add(new ModelBox(ribfoot2, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		silverib3 = new RendererModel(this);
		silverib3.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib3);
		setRotationAngle(silverib3, 0.0F, 1.0472F, 0.0F);
		

		spine3 = new RendererModel(this);
		spine3.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib3.addChild(spine3);
		setRotationAngle(spine3, 0.0F, -0.5236F, 0.0F);
		

		plain3 = new RendererModel(this);
		plain3.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine3.addChild(plain3);
		setRotationAngle(plain3, -1.309F, 0.0F, 0.0F);
		plain3.cubeList.add(new ModelBox(plain3, 1, 154, -10.0F, 12.9249F, -0.5863F, 20, 4, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 16, 146, 5.0F, 3.9249F, -0.5863F, 5, 9, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 11, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 1, 134, -10.0F, 3.9249F, -0.5863F, 5, 9, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain3.cubeList.add(new ModelBox(plain3, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel3 = new RendererModel(this);
		bevel3.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine3.addChild(bevel3);
		setRotationAngle(bevel3, -0.6981F, 0.0F, 0.0F);
		bevel3.cubeList.add(new ModelBox(bevel3, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel3.cubeList.add(new ModelBox(bevel3, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel3.cubeList.add(new ModelBox(bevel3, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel3.cubeList.add(new ModelBox(bevel3, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front3 = new RendererModel(this);
		front3.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine3.addChild(front3);
		front3.cubeList.add(new ModelBox(front3, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front3.cubeList.add(new ModelBox(front3, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front3.cubeList.add(new ModelBox(front3, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front3.addChild(bone17);
		setRotationAngle(bone17, -0.2618F, 0.0F, 0.0F);
		bone17.cubeList.add(new ModelBox(bone17, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring7 = new RendererModel(this);
		ring7.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front3.addChild(ring7);
		ring7.cubeList.add(new ModelBox(ring7, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring7.addChild(bone18);
		setRotationAngle(bone18, -0.2618F, 0.0F, 0.0F);
		bone18.cubeList.add(new ModelBox(bone18, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring7.addChild(bone19);
		setRotationAngle(bone19, 0.2618F, 0.0F, 0.0F);
		bone19.cubeList.add(new ModelBox(bone19, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring8 = new RendererModel(this);
		ring8.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front3.addChild(ring8);
		ring8.cubeList.add(new ModelBox(ring8, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring8.addChild(bone20);
		setRotationAngle(bone20, -0.2618F, 0.0F, 0.0F);
		bone20.cubeList.add(new ModelBox(bone20, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring8.addChild(bone21);
		setRotationAngle(bone21, 0.2618F, 0.0F, 0.0F);
		bone21.cubeList.add(new ModelBox(bone21, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards3 = new RendererModel(this);
		glow__innards3.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring8.addChild(glow__innards3);
		

		undercurve3 = new RendererModel(this);
		undercurve3.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine3.addChild(undercurve3);
		setRotationAngle(undercurve3, 1.309F, 0.0F, 0.0F);
		undercurve3.cubeList.add(new ModelBox(undercurve3, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve3.cubeList.add(new ModelBox(undercurve3, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve3.cubeList.add(new ModelBox(undercurve3, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve3.cubeList.add(new ModelBox(undercurve3, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve3.cubeList.add(new ModelBox(undercurve3, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve3.cubeList.add(new ModelBox(undercurve3, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend3 = new RendererModel(this);
		backbend3.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine3.addChild(backbend3);
		setRotationAngle(backbend3, 0.3491F, 0.0F, 0.0F);
		backbend3.cubeList.add(new ModelBox(backbend3, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend3.cubeList.add(new ModelBox(backbend3, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend3.cubeList.add(new ModelBox(backbend3, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend3.cubeList.add(new ModelBox(backbend3, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend3.cubeList.add(new ModelBox(backbend3, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin3 = new RendererModel(this);
		shin3.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine3.addChild(shin3);
		setRotationAngle(shin3, -0.2618F, 0.0F, 0.0F);
		

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin3.addChild(bone22);
		

		ribfoot3 = new RendererModel(this);
		ribfoot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine3.addChild(ribfoot3);
		ribfoot3.cubeList.add(new ModelBox(ribfoot3, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		silverib4 = new RendererModel(this);
		silverib4.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib4);
		setRotationAngle(silverib4, 0.0F, 2.0944F, 0.0F);
		

		spine4 = new RendererModel(this);
		spine4.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib4.addChild(spine4);
		setRotationAngle(spine4, 0.0F, -0.5236F, 0.0F);
		

		plain4 = new RendererModel(this);
		plain4.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine4.addChild(plain4);
		setRotationAngle(plain4, -1.309F, 0.0F, 0.0F);
		plain4.cubeList.add(new ModelBox(plain4, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 24, 2, 0.0F, false));
		plain4.cubeList.add(new ModelBox(plain4, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain4.cubeList.add(new ModelBox(plain4, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain4.cubeList.add(new ModelBox(plain4, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain4.cubeList.add(new ModelBox(plain4, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel4 = new RendererModel(this);
		bevel4.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine4.addChild(bevel4);
		setRotationAngle(bevel4, -0.6981F, 0.0F, 0.0F);
		bevel4.cubeList.add(new ModelBox(bevel4, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel4.cubeList.add(new ModelBox(bevel4, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel4.cubeList.add(new ModelBox(bevel4, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel4.cubeList.add(new ModelBox(bevel4, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front4 = new RendererModel(this);
		front4.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine4.addChild(front4);
		front4.cubeList.add(new ModelBox(front4, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front4.cubeList.add(new ModelBox(front4, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front4.cubeList.add(new ModelBox(front4, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front4.addChild(bone23);
		setRotationAngle(bone23, -0.2618F, 0.0F, 0.0F);
		bone23.cubeList.add(new ModelBox(bone23, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone23.cubeList.add(new ModelBox(bone23, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring9 = new RendererModel(this);
		ring9.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front4.addChild(ring9);
		ring9.cubeList.add(new ModelBox(ring9, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring9.addChild(bone24);
		setRotationAngle(bone24, -0.2618F, 0.0F, 0.0F);
		bone24.cubeList.add(new ModelBox(bone24, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring9.addChild(bone25);
		setRotationAngle(bone25, 0.2618F, 0.0F, 0.0F);
		bone25.cubeList.add(new ModelBox(bone25, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring10 = new RendererModel(this);
		ring10.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front4.addChild(ring10);
		ring10.cubeList.add(new ModelBox(ring10, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring10.addChild(bone26);
		setRotationAngle(bone26, -0.2618F, 0.0F, 0.0F);
		bone26.cubeList.add(new ModelBox(bone26, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring10.addChild(bone27);
		setRotationAngle(bone27, 0.2618F, 0.0F, 0.0F);
		bone27.cubeList.add(new ModelBox(bone27, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards4 = new RendererModel(this);
		glow__innards4.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring10.addChild(glow__innards4);
		

		undercurve4 = new RendererModel(this);
		undercurve4.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine4.addChild(undercurve4);
		setRotationAngle(undercurve4, 1.309F, 0.0F, 0.0F);
		undercurve4.cubeList.add(new ModelBox(undercurve4, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve4.cubeList.add(new ModelBox(undercurve4, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve4.cubeList.add(new ModelBox(undercurve4, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve4.cubeList.add(new ModelBox(undercurve4, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve4.cubeList.add(new ModelBox(undercurve4, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve4.cubeList.add(new ModelBox(undercurve4, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend4 = new RendererModel(this);
		backbend4.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine4.addChild(backbend4);
		setRotationAngle(backbend4, 0.3491F, 0.0F, 0.0F);
		backbend4.cubeList.add(new ModelBox(backbend4, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend4.cubeList.add(new ModelBox(backbend4, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend4.cubeList.add(new ModelBox(backbend4, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend4.cubeList.add(new ModelBox(backbend4, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend4.cubeList.add(new ModelBox(backbend4, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin4 = new RendererModel(this);
		shin4.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine4.addChild(shin4);
		setRotationAngle(shin4, -0.2618F, 0.0F, 0.0F);
		

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin4.addChild(bone28);
		

		ribfoot4 = new RendererModel(this);
		ribfoot4.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine4.addChild(ribfoot4);
		ribfoot4.cubeList.add(new ModelBox(ribfoot4, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		silverib5 = new RendererModel(this);
		silverib5.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib5);
		setRotationAngle(silverib5, 0.0F, 3.1416F, 0.0F);
		

		spine5 = new RendererModel(this);
		spine5.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib5.addChild(spine5);
		setRotationAngle(spine5, 0.0F, -0.5236F, 0.0F);
		

		plain5 = new RendererModel(this);
		plain5.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine5.addChild(plain5);
		setRotationAngle(plain5, -1.309F, 0.0F, 0.0F);
		plain5.cubeList.add(new ModelBox(plain5, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 24, 2, 0.0F, false));
		plain5.cubeList.add(new ModelBox(plain5, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain5.cubeList.add(new ModelBox(plain5, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain5.cubeList.add(new ModelBox(plain5, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain5.cubeList.add(new ModelBox(plain5, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel5 = new RendererModel(this);
		bevel5.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine5.addChild(bevel5);
		setRotationAngle(bevel5, -0.6981F, 0.0F, 0.0F);
		bevel5.cubeList.add(new ModelBox(bevel5, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel5.cubeList.add(new ModelBox(bevel5, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel5.cubeList.add(new ModelBox(bevel5, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel5.cubeList.add(new ModelBox(bevel5, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front5 = new RendererModel(this);
		front5.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine5.addChild(front5);
		front5.cubeList.add(new ModelBox(front5, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front5.cubeList.add(new ModelBox(front5, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front5.cubeList.add(new ModelBox(front5, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front5.addChild(bone29);
		setRotationAngle(bone29, -0.2618F, 0.0F, 0.0F);
		bone29.cubeList.add(new ModelBox(bone29, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone29.cubeList.add(new ModelBox(bone29, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring11 = new RendererModel(this);
		ring11.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front5.addChild(ring11);
		ring11.cubeList.add(new ModelBox(ring11, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring11.addChild(bone30);
		setRotationAngle(bone30, -0.2618F, 0.0F, 0.0F);
		bone30.cubeList.add(new ModelBox(bone30, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring11.addChild(bone31);
		setRotationAngle(bone31, 0.2618F, 0.0F, 0.0F);
		bone31.cubeList.add(new ModelBox(bone31, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring12 = new RendererModel(this);
		ring12.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front5.addChild(ring12);
		ring12.cubeList.add(new ModelBox(ring12, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring12.addChild(bone32);
		setRotationAngle(bone32, -0.2618F, 0.0F, 0.0F);
		bone32.cubeList.add(new ModelBox(bone32, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring12.addChild(bone33);
		setRotationAngle(bone33, 0.2618F, 0.0F, 0.0F);
		bone33.cubeList.add(new ModelBox(bone33, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards5 = new RendererModel(this);
		glow__innards5.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring12.addChild(glow__innards5);
		

		undercurve5 = new RendererModel(this);
		undercurve5.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine5.addChild(undercurve5);
		setRotationAngle(undercurve5, 1.309F, 0.0F, 0.0F);
		undercurve5.cubeList.add(new ModelBox(undercurve5, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve5.cubeList.add(new ModelBox(undercurve5, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve5.cubeList.add(new ModelBox(undercurve5, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve5.cubeList.add(new ModelBox(undercurve5, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve5.cubeList.add(new ModelBox(undercurve5, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve5.cubeList.add(new ModelBox(undercurve5, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend5 = new RendererModel(this);
		backbend5.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine5.addChild(backbend5);
		setRotationAngle(backbend5, 0.3491F, 0.0F, 0.0F);
		backbend5.cubeList.add(new ModelBox(backbend5, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend5.cubeList.add(new ModelBox(backbend5, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend5.cubeList.add(new ModelBox(backbend5, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend5.cubeList.add(new ModelBox(backbend5, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend5.cubeList.add(new ModelBox(backbend5, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin5 = new RendererModel(this);
		shin5.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine5.addChild(shin5);
		setRotationAngle(shin5, -0.2618F, 0.0F, 0.0F);
		

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin5.addChild(bone34);
		

		ribfoot5 = new RendererModel(this);
		ribfoot5.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine5.addChild(ribfoot5);
		ribfoot5.cubeList.add(new ModelBox(ribfoot5, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		silverib6 = new RendererModel(this);
		silverib6.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib6);
		setRotationAngle(silverib6, 0.0F, -2.0944F, 0.0F);
		

		spine6 = new RendererModel(this);
		spine6.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib6.addChild(spine6);
		setRotationAngle(spine6, 0.0F, -0.5236F, 0.0F);
		

		plain6 = new RendererModel(this);
		plain6.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine6.addChild(plain6);
		setRotationAngle(plain6, -1.309F, 0.0F, 0.0F);
		plain6.cubeList.add(new ModelBox(plain6, 1, 134, -10.0F, -7.0751F, -0.5863F, 20, 24, 2, 0.0F, false));
		plain6.cubeList.add(new ModelBox(plain6, 1, 133, -10.0F, -24.0751F, -0.5863F, 20, 17, 2, 0.0F, false));
		plain6.cubeList.add(new ModelBox(plain6, 67, 135, -4.5F, -24.0751F, -2.5863F, 9, 14, 2, 0.0F, false));
		plain6.cubeList.add(new ModelBox(plain6, 66, 85, 7.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));
		plain6.cubeList.add(new ModelBox(plain6, 73, 81, -8.0F, -24.0751F, 1.4137F, 1, 41, 4, 0.0F, false));

		bevel6 = new RendererModel(this);
		bevel6.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine6.addChild(bevel6);
		setRotationAngle(bevel6, -0.6981F, 0.0F, 0.0F);
		bevel6.cubeList.add(new ModelBox(bevel6, 1, 158, -10.0F, -2.6563F, -1.5652F, 20, 12, 2, 0.0F, false));
		bevel6.cubeList.add(new ModelBox(bevel6, 67, 155, -4.5F, -25.9206F, 12.283F, 9, 4, 2, 0.0F, false));
		bevel6.cubeList.add(new ModelBox(bevel6, 69, 102, 7.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));
		bevel6.cubeList.add(new ModelBox(bevel6, 40, 109, -8.0F, -1.6563F, 0.4348F, 1, 10, 4, 0.0F, false));

		front6 = new RendererModel(this);
		front6.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine6.addChild(front6);
		front6.cubeList.add(new ModelBox(front6, 45, 136, -10.0F, 3.5169F, -2.9925F, 20, 8, 2, 0.0F, false));
		front6.cubeList.add(new ModelBox(front6, 68, 108, 7.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));
		front6.cubeList.add(new ModelBox(front6, 44, 103, -8.0F, 3.5169F, -0.9925F, 1, 8, 4, 0.0F, false));

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front6.addChild(bone35);
		setRotationAngle(bone35, -0.2618F, 0.0F, 0.0F);
		bone35.cubeList.add(new ModelBox(bone35, 49, 169, -10.0F, 10.5419F, 4.3666F, 20, 2, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 49, 169, -7.0F, 8.0504F, 4.3019F, 14, 1, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 49, 169, -7.0F, 5.2834F, 4.4313F, 14, 1, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 49, 169, -7.0F, 2.5419F, 4.3666F, 14, 1, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 49, 169, -11.0F, 0.5419F, 4.3666F, 22, 2, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 50, 162, -10.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 49, 159, 7.0F, 2.5419F, 4.3666F, 3, 8, 4, 0.0F, false));

		ring13 = new RendererModel(this);
		ring13.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front6.addChild(ring13);
		ring13.cubeList.add(new ModelBox(ring13, 14, 195, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring13.addChild(bone36);
		setRotationAngle(bone36, -0.2618F, 0.0F, 0.0F);
		bone36.cubeList.add(new ModelBox(bone36, 14, 195, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring13.addChild(bone37);
		setRotationAngle(bone37, 0.2618F, 0.0F, 0.0F);
		bone37.cubeList.add(new ModelBox(bone37, 14, 195, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		ring14 = new RendererModel(this);
		ring14.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front6.addChild(ring14);
		ring14.cubeList.add(new ModelBox(ring14, 19, 179, -10.6667F, -58.8488F, -21.9092F, 20, 4, 7, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring14.addChild(bone38);
		setRotationAngle(bone38, -0.2618F, 0.0F, 0.0F);
		bone38.cubeList.add(new ModelBox(bone38, 19, 179, -11.0F, -1.5765F, -5.3126F, 20, 3, 7, 0.0F, false));

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring14.addChild(bone39);
		setRotationAngle(bone39, 0.2618F, 0.0F, 0.0F);
		bone39.cubeList.add(new ModelBox(bone39, 19, 179, -11.0F, -1.4235F, -5.3126F, 20, 3, 7, 0.0F, false));

		glow__innards6 = new RendererModel(this);
		glow__innards6.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring14.addChild(glow__innards6);
		

		undercurve6 = new RendererModel(this);
		undercurve6.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine6.addChild(undercurve6);
		setRotationAngle(undercurve6, 1.309F, 0.0F, 0.0F);
		undercurve6.cubeList.add(new ModelBox(undercurve6, 32, 142, 0.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve6.cubeList.add(new ModelBox(undercurve6, 32, 142, -10.0F, 0.901F, -3.3518F, 10, 22, 2, 0.0F, false));
		undercurve6.cubeList.add(new ModelBox(undercurve6, 65, 154, -10.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve6.cubeList.add(new ModelBox(undercurve6, 65, 154, 0.0F, -17.099F, -3.3518F, 10, 18, 2, 0.0F, false));
		undercurve6.cubeList.add(new ModelBox(undercurve6, 53, 86, 7.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));
		undercurve6.cubeList.add(new ModelBox(undercurve6, 38, 91, -8.0F, -13.099F, -1.3518F, 1, 36, 4, 0.0F, false));

		backbend6 = new RendererModel(this);
		backbend6.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine6.addChild(backbend6);
		setRotationAngle(backbend6, 0.3491F, 0.0F, 0.0F);
		backbend6.cubeList.add(new ModelBox(backbend6, 23, 147, -9.0F, -3.8396F, 13.8373F, 18, 26, 3, 0.0F, false));
		backbend6.cubeList.add(new ModelBox(backbend6, 111, 8, -4.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend6.cubeList.add(new ModelBox(backbend6, 111, 8, 3.0F, -5.8656F, 11.0182F, 1, 28, 3, 0.0F, false));
		backbend6.cubeList.add(new ModelBox(backbend6, 53, 90, 7.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));
		backbend6.cubeList.add(new ModelBox(backbend6, 27, 87, -8.0F, -6.8396F, 16.8373F, 1, 30, 10, 0.0F, false));

		shin6 = new RendererModel(this);
		shin6.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine6.addChild(shin6);
		setRotationAngle(shin6, -0.2618F, 0.0F, 0.0F);
		

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin6.addChild(bone40);
		

		ribfoot6 = new RendererModel(this);
		ribfoot6.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine6.addChild(ribfoot6);
		ribfoot6.cubeList.add(new ModelBox(ribfoot6, 50, 67, -9.5F, -5.0F, -39.0F, 19, 5, 10, 0.0F, false));

		stations = new RendererModel(this);
		stations.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(stations, 0.0F, -0.5236F, 0.0F);
		

		station1 = new RendererModel(this);
		station1.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station1);
		setRotationAngle(station1, 0.0F, -0.5236F, 0.0F);
		

		st_plain1 = new RendererModel(this);
		st_plain1.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station1.addChild(st_plain1);
		setRotationAngle(st_plain1, -1.309F, 0.0F, 0.0F);
		st_plain1.cubeList.add(new ModelBox(st_plain1, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain1.cubeList.add(new ModelBox(st_plain1, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain1.cubeList.add(new ModelBox(st_plain1, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel1 = new RendererModel(this);
		st_bevel1.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station1.addChild(st_bevel1);
		setRotationAngle(st_bevel1, -0.6981F, 0.0F, 0.0F);
		st_bevel1.cubeList.add(new ModelBox(st_bevel1, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel1.cubeList.add(new ModelBox(st_bevel1, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel1.cubeList.add(new ModelBox(st_bevel1, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front1 = new RendererModel(this);
		st_front1.setRotationPoint(0.0F, -50.0F, -72.0F);
		station1.addChild(st_front1);
		st_front1.cubeList.add(new ModelBox(st_front1, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel = new RendererModel(this);
		rotorbevel.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front1.addChild(rotorbevel);
		setRotationAngle(rotorbevel, -0.2443F, 0.0F, 0.0F);
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel.cubeList.add(new ModelBox(rotorbevel, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring4 = new RendererModel(this);
		ring4.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front1.addChild(ring4);
		ring4.cubeList.add(new ModelBox(ring4, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring4.addChild(bone11);
		setRotationAngle(bone11, -0.2618F, 0.0F, 0.0F);
		bone11.cubeList.add(new ModelBox(bone11, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring4.addChild(bone12);
		setRotationAngle(bone12, 0.2618F, 0.0F, 0.0F);
		bone12.cubeList.add(new ModelBox(bone12, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring3 = new RendererModel(this);
		ring3.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front1.addChild(ring3);
		ring3.cubeList.add(new ModelBox(ring3, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring3.addChild(bone6);
		setRotationAngle(bone6, -0.2618F, 0.0F, 0.0F);
		bone6.cubeList.add(new ModelBox(bone6, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring3.addChild(bone7);
		setRotationAngle(bone7, 0.2618F, 0.0F, 0.0F);
		bone7.cubeList.add(new ModelBox(bone7, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under1 = new RendererModel(this);
		st_under1.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station1.addChild(st_under1);
		setRotationAngle(st_under1, 1.309F, 0.0F, 0.0F);
		st_under1.cubeList.add(new ModelBox(st_under1, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under1.cubeList.add(new ModelBox(st_under1, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under1.cubeList.add(new ModelBox(st_under1, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under1.cubeList.add(new ModelBox(st_under1, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under1.cubeList.add(new ModelBox(st_under1, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under1.cubeList.add(new ModelBox(st_under1, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back1 = new RendererModel(this);
		back1.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station1.addChild(back1);
		setRotationAngle(back1, 0.3491F, 0.0F, 0.0F);
		back1.cubeList.add(new ModelBox(back1, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back1.cubeList.add(new ModelBox(back1, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box1 = new RendererModel(this);
		box1.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station1.addChild(box1);
		setRotationAngle(box1, -0.0873F, 0.0F, 0.0F);
		box1.cubeList.add(new ModelBox(box1, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box1.cubeList.add(new ModelBox(box1, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim1 = new RendererModel(this);
		trim1.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station1.addChild(trim1);
		setRotationAngle(trim1, -1.5708F, 0.0F, 0.0F);
		trim1.cubeList.add(new ModelBox(trim1, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		station2 = new RendererModel(this);
		station2.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station2);
		setRotationAngle(station2, 0.0F, 0.5236F, 0.0F);
		

		st_plain2 = new RendererModel(this);
		st_plain2.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station2.addChild(st_plain2);
		setRotationAngle(st_plain2, -1.309F, 0.0F, 0.0F);
		st_plain2.cubeList.add(new ModelBox(st_plain2, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain2.cubeList.add(new ModelBox(st_plain2, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain2.cubeList.add(new ModelBox(st_plain2, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel2 = new RendererModel(this);
		st_bevel2.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station2.addChild(st_bevel2);
		setRotationAngle(st_bevel2, -0.6981F, 0.0F, 0.0F);
		st_bevel2.cubeList.add(new ModelBox(st_bevel2, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel2.cubeList.add(new ModelBox(st_bevel2, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel2.cubeList.add(new ModelBox(st_bevel2, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front2 = new RendererModel(this);
		st_front2.setRotationPoint(0.0F, -50.0F, -72.0F);
		station2.addChild(st_front2);
		st_front2.cubeList.add(new ModelBox(st_front2, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel2 = new RendererModel(this);
		rotorbevel2.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front2.addChild(rotorbevel2);
		setRotationAngle(rotorbevel2, -0.2443F, 0.0F, 0.0F);
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel2.cubeList.add(new ModelBox(rotorbevel2, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring15 = new RendererModel(this);
		ring15.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front2.addChild(ring15);
		ring15.cubeList.add(new ModelBox(ring15, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring15.addChild(bone41);
		setRotationAngle(bone41, -0.2618F, 0.0F, 0.0F);
		bone41.cubeList.add(new ModelBox(bone41, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone41.cubeList.add(new ModelBox(bone41, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring15.addChild(bone42);
		setRotationAngle(bone42, 0.2618F, 0.0F, 0.0F);
		bone42.cubeList.add(new ModelBox(bone42, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring16 = new RendererModel(this);
		ring16.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front2.addChild(ring16);
		ring16.cubeList.add(new ModelBox(ring16, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring16.addChild(bone43);
		setRotationAngle(bone43, -0.2618F, 0.0F, 0.0F);
		bone43.cubeList.add(new ModelBox(bone43, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring16.addChild(bone44);
		setRotationAngle(bone44, 0.2618F, 0.0F, 0.0F);
		bone44.cubeList.add(new ModelBox(bone44, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under2 = new RendererModel(this);
		st_under2.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station2.addChild(st_under2);
		setRotationAngle(st_under2, 1.309F, 0.0F, 0.0F);
		st_under2.cubeList.add(new ModelBox(st_under2, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under2.cubeList.add(new ModelBox(st_under2, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under2.cubeList.add(new ModelBox(st_under2, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under2.cubeList.add(new ModelBox(st_under2, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under2.cubeList.add(new ModelBox(st_under2, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under2.cubeList.add(new ModelBox(st_under2, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back2 = new RendererModel(this);
		back2.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station2.addChild(back2);
		setRotationAngle(back2, 0.3491F, 0.0F, 0.0F);
		back2.cubeList.add(new ModelBox(back2, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back2.cubeList.add(new ModelBox(back2, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box2 = new RendererModel(this);
		box2.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station2.addChild(box2);
		setRotationAngle(box2, -0.0873F, 0.0F, 0.0F);
		box2.cubeList.add(new ModelBox(box2, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box2.cubeList.add(new ModelBox(box2, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim2 = new RendererModel(this);
		trim2.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station2.addChild(trim2);
		setRotationAngle(trim2, -1.5708F, 0.0F, 0.0F);
		trim2.cubeList.add(new ModelBox(trim2, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		station3 = new RendererModel(this);
		station3.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station3);
		setRotationAngle(station3, 0.0F, 1.5708F, 0.0F);
		

		st_plain3 = new RendererModel(this);
		st_plain3.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station3.addChild(st_plain3);
		setRotationAngle(st_plain3, -1.309F, 0.0F, 0.0F);
		st_plain3.cubeList.add(new ModelBox(st_plain3, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain3.cubeList.add(new ModelBox(st_plain3, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain3.cubeList.add(new ModelBox(st_plain3, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel3 = new RendererModel(this);
		st_bevel3.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station3.addChild(st_bevel3);
		setRotationAngle(st_bevel3, -0.6981F, 0.0F, 0.0F);
		st_bevel3.cubeList.add(new ModelBox(st_bevel3, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel3.cubeList.add(new ModelBox(st_bevel3, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel3.cubeList.add(new ModelBox(st_bevel3, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front3 = new RendererModel(this);
		st_front3.setRotationPoint(0.0F, -50.0F, -72.0F);
		station3.addChild(st_front3);
		st_front3.cubeList.add(new ModelBox(st_front3, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel3 = new RendererModel(this);
		rotorbevel3.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front3.addChild(rotorbevel3);
		setRotationAngle(rotorbevel3, -0.2443F, 0.0F, 0.0F);
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel3.cubeList.add(new ModelBox(rotorbevel3, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring17 = new RendererModel(this);
		ring17.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front3.addChild(ring17);
		ring17.cubeList.add(new ModelBox(ring17, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring17.addChild(bone45);
		setRotationAngle(bone45, -0.2618F, 0.0F, 0.0F);
		bone45.cubeList.add(new ModelBox(bone45, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring17.addChild(bone46);
		setRotationAngle(bone46, 0.2618F, 0.0F, 0.0F);
		bone46.cubeList.add(new ModelBox(bone46, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring18 = new RendererModel(this);
		ring18.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front3.addChild(ring18);
		ring18.cubeList.add(new ModelBox(ring18, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring18.addChild(bone47);
		setRotationAngle(bone47, -0.2618F, 0.0F, 0.0F);
		bone47.cubeList.add(new ModelBox(bone47, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring18.addChild(bone48);
		setRotationAngle(bone48, 0.2618F, 0.0F, 0.0F);
		bone48.cubeList.add(new ModelBox(bone48, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under3 = new RendererModel(this);
		st_under3.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station3.addChild(st_under3);
		setRotationAngle(st_under3, 1.309F, 0.0F, 0.0F);
		st_under3.cubeList.add(new ModelBox(st_under3, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under3.cubeList.add(new ModelBox(st_under3, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under3.cubeList.add(new ModelBox(st_under3, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under3.cubeList.add(new ModelBox(st_under3, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under3.cubeList.add(new ModelBox(st_under3, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under3.cubeList.add(new ModelBox(st_under3, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back3 = new RendererModel(this);
		back3.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station3.addChild(back3);
		setRotationAngle(back3, 0.3491F, 0.0F, 0.0F);
		back3.cubeList.add(new ModelBox(back3, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back3.cubeList.add(new ModelBox(back3, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box3 = new RendererModel(this);
		box3.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station3.addChild(box3);
		setRotationAngle(box3, -0.0873F, 0.0F, 0.0F);
		box3.cubeList.add(new ModelBox(box3, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box3.cubeList.add(new ModelBox(box3, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim3 = new RendererModel(this);
		trim3.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station3.addChild(trim3);
		setRotationAngle(trim3, -1.5708F, 0.0F, 0.0F);
		trim3.cubeList.add(new ModelBox(trim3, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		station4 = new RendererModel(this);
		station4.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station4);
		setRotationAngle(station4, 0.0F, 2.618F, 0.0F);
		

		st_plain4 = new RendererModel(this);
		st_plain4.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station4.addChild(st_plain4);
		setRotationAngle(st_plain4, -1.309F, 0.0F, 0.0F);
		st_plain4.cubeList.add(new ModelBox(st_plain4, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain4.cubeList.add(new ModelBox(st_plain4, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain4.cubeList.add(new ModelBox(st_plain4, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel4 = new RendererModel(this);
		st_bevel4.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station4.addChild(st_bevel4);
		setRotationAngle(st_bevel4, -0.6981F, 0.0F, 0.0F);
		st_bevel4.cubeList.add(new ModelBox(st_bevel4, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel4.cubeList.add(new ModelBox(st_bevel4, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel4.cubeList.add(new ModelBox(st_bevel4, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front4 = new RendererModel(this);
		st_front4.setRotationPoint(0.0F, -50.0F, -72.0F);
		station4.addChild(st_front4);
		st_front4.cubeList.add(new ModelBox(st_front4, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel4 = new RendererModel(this);
		rotorbevel4.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front4.addChild(rotorbevel4);
		setRotationAngle(rotorbevel4, -0.2443F, 0.0F, 0.0F);
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel4.cubeList.add(new ModelBox(rotorbevel4, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring19 = new RendererModel(this);
		ring19.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front4.addChild(ring19);
		ring19.cubeList.add(new ModelBox(ring19, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring19.addChild(bone49);
		setRotationAngle(bone49, -0.2618F, 0.0F, 0.0F);
		bone49.cubeList.add(new ModelBox(bone49, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring19.addChild(bone50);
		setRotationAngle(bone50, 0.2618F, 0.0F, 0.0F);
		bone50.cubeList.add(new ModelBox(bone50, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone50.cubeList.add(new ModelBox(bone50, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring20 = new RendererModel(this);
		ring20.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front4.addChild(ring20);
		ring20.cubeList.add(new ModelBox(ring20, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring20.addChild(bone51);
		setRotationAngle(bone51, -0.2618F, 0.0F, 0.0F);
		bone51.cubeList.add(new ModelBox(bone51, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring20.addChild(bone52);
		setRotationAngle(bone52, 0.2618F, 0.0F, 0.0F);
		bone52.cubeList.add(new ModelBox(bone52, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone52.cubeList.add(new ModelBox(bone52, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under4 = new RendererModel(this);
		st_under4.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station4.addChild(st_under4);
		setRotationAngle(st_under4, 1.309F, 0.0F, 0.0F);
		st_under4.cubeList.add(new ModelBox(st_under4, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under4.cubeList.add(new ModelBox(st_under4, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under4.cubeList.add(new ModelBox(st_under4, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under4.cubeList.add(new ModelBox(st_under4, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under4.cubeList.add(new ModelBox(st_under4, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under4.cubeList.add(new ModelBox(st_under4, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back4 = new RendererModel(this);
		back4.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station4.addChild(back4);
		setRotationAngle(back4, 0.3491F, 0.0F, 0.0F);
		back4.cubeList.add(new ModelBox(back4, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back4.cubeList.add(new ModelBox(back4, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box4 = new RendererModel(this);
		box4.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station4.addChild(box4);
		setRotationAngle(box4, -0.0873F, 0.0F, 0.0F);
		box4.cubeList.add(new ModelBox(box4, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box4.cubeList.add(new ModelBox(box4, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim4 = new RendererModel(this);
		trim4.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station4.addChild(trim4);
		setRotationAngle(trim4, -1.5708F, 0.0F, 0.0F);
		trim4.cubeList.add(new ModelBox(trim4, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		station5 = new RendererModel(this);
		station5.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station5);
		setRotationAngle(station5, 0.0F, -2.618F, 0.0F);
		

		st_plain5 = new RendererModel(this);
		st_plain5.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station5.addChild(st_plain5);
		setRotationAngle(st_plain5, -1.309F, 0.0F, 0.0F);
		st_plain5.cubeList.add(new ModelBox(st_plain5, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain5.cubeList.add(new ModelBox(st_plain5, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain5.cubeList.add(new ModelBox(st_plain5, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel5 = new RendererModel(this);
		st_bevel5.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station5.addChild(st_bevel5);
		setRotationAngle(st_bevel5, -0.6981F, 0.0F, 0.0F);
		st_bevel5.cubeList.add(new ModelBox(st_bevel5, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel5.cubeList.add(new ModelBox(st_bevel5, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel5.cubeList.add(new ModelBox(st_bevel5, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front5 = new RendererModel(this);
		st_front5.setRotationPoint(0.0F, -50.0F, -72.0F);
		station5.addChild(st_front5);
		st_front5.cubeList.add(new ModelBox(st_front5, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel5 = new RendererModel(this);
		rotorbevel5.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front5.addChild(rotorbevel5);
		setRotationAngle(rotorbevel5, -0.2443F, 0.0F, 0.0F);
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel5.cubeList.add(new ModelBox(rotorbevel5, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring21 = new RendererModel(this);
		ring21.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front5.addChild(ring21);
		ring21.cubeList.add(new ModelBox(ring21, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring21.addChild(bone53);
		setRotationAngle(bone53, -0.2618F, 0.0F, 0.0F);
		bone53.cubeList.add(new ModelBox(bone53, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring21.addChild(bone54);
		setRotationAngle(bone54, 0.2618F, 0.0F, 0.0F);
		bone54.cubeList.add(new ModelBox(bone54, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone54.cubeList.add(new ModelBox(bone54, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring22 = new RendererModel(this);
		ring22.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front5.addChild(ring22);
		ring22.cubeList.add(new ModelBox(ring22, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring22.addChild(bone55);
		setRotationAngle(bone55, -0.2618F, 0.0F, 0.0F);
		bone55.cubeList.add(new ModelBox(bone55, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring22.addChild(bone56);
		setRotationAngle(bone56, 0.2618F, 0.0F, 0.0F);
		bone56.cubeList.add(new ModelBox(bone56, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under5 = new RendererModel(this);
		st_under5.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station5.addChild(st_under5);
		setRotationAngle(st_under5, 1.309F, 0.0F, 0.0F);
		st_under5.cubeList.add(new ModelBox(st_under5, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under5.cubeList.add(new ModelBox(st_under5, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under5.cubeList.add(new ModelBox(st_under5, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under5.cubeList.add(new ModelBox(st_under5, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under5.cubeList.add(new ModelBox(st_under5, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under5.cubeList.add(new ModelBox(st_under5, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back5 = new RendererModel(this);
		back5.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station5.addChild(back5);
		setRotationAngle(back5, 0.3491F, 0.0F, 0.0F);
		back5.cubeList.add(new ModelBox(back5, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back5.cubeList.add(new ModelBox(back5, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box5 = new RendererModel(this);
		box5.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station5.addChild(box5);
		setRotationAngle(box5, -0.0873F, 0.0F, 0.0F);
		box5.cubeList.add(new ModelBox(box5, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box5.cubeList.add(new ModelBox(box5, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim5 = new RendererModel(this);
		trim5.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station5.addChild(trim5);
		setRotationAngle(trim5, -1.5708F, 0.0F, 0.0F);
		trim5.cubeList.add(new ModelBox(trim5, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		station6 = new RendererModel(this);
		station6.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station6);
		setRotationAngle(station6, 0.0F, -1.5708F, 0.0F);
		

		st_plain6 = new RendererModel(this);
		st_plain6.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station6.addChild(st_plain6);
		setRotationAngle(st_plain6, -1.309F, 0.0F, 0.0F);
		st_plain6.cubeList.add(new ModelBox(st_plain6, 4, 63, -26.0F, -6.0751F, 1.1637F, 52, 16, 4, 0.0F, false));
		st_plain6.cubeList.add(new ModelBox(st_plain6, 33, 75, -10.0F, -23.0751F, -2.5863F, 20, 4, 4, 0.0F, false));
		st_plain6.cubeList.add(new ModelBox(st_plain6, 4, 63, -17.0F, -25.0751F, 1.1637F, 34, 19, 4, 0.0F, false));

		st_bevel6 = new RendererModel(this);
		st_bevel6.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station6.addChild(st_bevel6);
		setRotationAngle(st_bevel6, -0.6981F, 0.0F, 0.0F);
		st_bevel6.cubeList.add(new ModelBox(st_bevel6, 2, 31, -25.4375F, -3.4063F, 3.9348F, 55, 4, 4, 0.0F, false));
		st_bevel6.cubeList.add(new ModelBox(st_bevel6, 4, 80, -23.5923F, -5.2695F, 5.4769F, 52, 2, 5, 0.0F, false));
		st_bevel6.cubeList.add(new ModelBox(st_bevel6, 3, 15, -27.4375F, 0.5937F, 4.4348F, 58, 3, 2, 0.0F, false));

		st_front6 = new RendererModel(this);
		st_front6.setRotationPoint(0.0F, -50.0F, -72.0F);
		station6.addChild(st_front6);
		st_front6.cubeList.add(new ModelBox(st_front6, 0, 35, -29.8452F, 4.0727F, 4.9399F, 59, 5, 4, 0.0F, false));

		rotorbevel6 = new RendererModel(this);
		rotorbevel6.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front6.addChild(rotorbevel6);
		setRotationAngle(rotorbevel6, -0.2443F, 0.0F, 0.0F);
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -5.0938F, -9.0F, 0.2862F, 11, 1, 3, 0.0F, false));
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -4.0938F, -11.5F, 0.2862F, 9, 1, 3, 0.0F, false));
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -4.5938F, -16.25F, -0.7138F, 10, 3, 4, 0.0F, false));
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -6.0F, -3.25F, -0.7138F, 13, 4, 6, 0.0F, false));
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -5.125F, -6.25F, 0.5362F, 11, 2, 3, 0.0F, false));
		rotorbevel6.cubeList.add(new ModelBox(rotorbevel6, 54, 168, -5.5625F, -4.25F, 0.2862F, 12, 1, 3, 0.0F, false));

		ring23 = new RendererModel(this);
		ring23.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front6.addChild(ring23);
		ring23.cubeList.add(new ModelBox(ring23, 22, 207, 24.1556F, -120.2731F, -22.0123F, 14, 4, 5, 0.0F, false));

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring23.addChild(bone57);
		setRotationAngle(bone57, -0.2618F, 0.0F, 0.0F);
		bone57.cubeList.add(new ModelBox(bone57, 22, 207, -7.3444F, -2.2145F, -6.6635F, 13, 4, 2, 0.0F, false));
		bone57.cubeList.add(new ModelBox(bone57, 22, 207, -6.3444F, -0.2145F, -4.6635F, 11, 2, 4, 0.0F, false));

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring23.addChild(bone58);
		setRotationAngle(bone58, 0.2618F, 0.0F, 0.0F);
		bone58.cubeList.add(new ModelBox(bone58, 22, 207, -7.3444F, -1.7855F, -6.6635F, 13, 1, 2, 0.0F, false));
		bone58.cubeList.add(new ModelBox(bone58, 22, 207, -6.3444F, -1.7855F, -4.6635F, 11, 2, 4, 0.0F, false));

		ring24 = new RendererModel(this);
		ring24.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front6.addChild(ring24);
		ring24.cubeList.add(new ModelBox(ring24, 29, 205, 25.7091F, -120.2731F, -21.9142F, 11, 4, 4, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring24.addChild(bone59);
		setRotationAngle(bone59, -0.2618F, 0.0F, 0.0F);
		bone59.cubeList.add(new ModelBox(bone59, 29, 205, -5.8221F, -2.2399F, -6.5687F, 10, 4, 2, 0.0F, false));
		bone59.cubeList.add(new ModelBox(bone59, 29, 205, -4.8221F, -0.2399F, -4.5687F, 8, 2, 3, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring24.addChild(bone60);
		setRotationAngle(bone60, 0.2618F, 0.0F, 0.0F);
		bone60.cubeList.add(new ModelBox(bone60, 29, 205, -5.7909F, -1.7601F, -6.5687F, 10, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 29, 205, -4.7909F, -1.7601F, -4.5687F, 8, 2, 3, 0.0F, false));

		st_under6 = new RendererModel(this);
		st_under6.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station6.addChild(st_under6);
		setRotationAngle(st_under6, 1.309F, 0.0F, 0.0F);
		st_under6.cubeList.add(new ModelBox(st_under6, 3, 30, -28.2702F, -11.2279F, 4.1025F, 58, 8, 2, 0.0F, false));
		st_under6.cubeList.add(new ModelBox(st_under6, 11, 63, -24.0F, -4.6316F, 5.2895F, 49, 13, 3, 0.0F, false));
		st_under6.cubeList.add(new ModelBox(st_under6, 0, 103, -18.0F, 8.3684F, 5.2895F, 37, 8, 3, 0.0F, false));
		st_under6.cubeList.add(new ModelBox(st_under6, 77, 54, -6.0F, 18.3684F, 2.2895F, 13, 16, 3, 0.0F, false));
		st_under6.cubeList.add(new ModelBox(st_under6, 80, 70, -4.0F, 26.3684F, -0.7105F, 9, 9, 4, 0.0F, false));
		st_under6.cubeList.add(new ModelBox(st_under6, 9, 99, -14.0F, 16.3684F, 5.2895F, 29, 17, 3, 0.0F, false));

		back6 = new RendererModel(this);
		back6.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station6.addChild(back6);
		setRotationAngle(back6, 0.3491F, 0.0F, 0.0F);
		back6.cubeList.add(new ModelBox(back6, 11, 106, -12.0F, -4.895F, 17.2141F, 24, 8, 3, 0.0F, false));
		back6.cubeList.add(new ModelBox(back6, 12, 113, -11.0F, 3.105F, 17.2141F, 20, 8, 3, 0.0F, false));

		box6 = new RendererModel(this);
		box6.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station6.addChild(box6);
		setRotationAngle(box6, -0.0873F, 0.0F, 0.0F);
		box6.cubeList.add(new ModelBox(box6, 14, 106, -12.0F, -1.9924F, 20.5911F, 22, 10, 3, 0.0F, false));
		box6.cubeList.add(new ModelBox(box6, 42, 69, -7.0F, 8.0076F, 11.5911F, 14, 4, 13, 0.0F, false));

		trim6 = new RendererModel(this);
		trim6.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station6.addChild(trim6);
		setRotationAngle(trim6, -1.5708F, 0.0F, 0.0F);
		trim6.cubeList.add(new ModelBox(trim6, 19, 58, -12.0F, -43.0F, -4.0F, 24, 16, 4, 0.0F, false));

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(controls, 0.0F, -0.5236F, 0.0F);
		

		controls1 = new RendererModel(this);
		controls1.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls1);
		setRotationAngle(controls1, 0.0F, -0.5236F, 0.0F);
		

		base_plate_1 = new RendererModel(this);
		base_plate_1.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls1.addChild(base_plate_1);
		setRotationAngle(base_plate_1, -1.309F, 0.0F, 0.0F);
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 4, 117, -15.0F, -4.0751F, 0.1637F, 4, 8, 1, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 4, 117, 5.75F, -7.0751F, 0.1637F, 8, 11, 1, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 4, 117, -12.0F, -2.0751F, -1.8363F, 1, 4, 2, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 4, 117, -15.0F, -2.0751F, -1.8363F, 1, 4, 2, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 4, 117, -7.0F, -14.8251F, -0.3363F, 14, 5, 1, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 52, 165, -8.0F, -17.0751F, 0.1637F, 16, 8, 1, 0.0F, false));
		base_plate_1.cubeList.add(new ModelBox(base_plate_1, 73, 167, -3.0F, -9.0751F, 0.1637F, 6, 6, 1, 0.0F, false));

		turn_dial = new RendererModel(this);
		turn_dial.setRotationPoint(-39.5F, 86.8792F, 56.5184F);
		base_plate_1.addChild(turn_dial);
		

		guide = new RendererModel(this);
		guide.setRotationPoint(0.0F, 0.0F, 0.0F);
		turn_dial.addChild(guide);
		guide.cubeList.add(new ModelBox(guide, 7, 9, 38.5F, -94.9542F, -56.6047F, 2, 4, 1, 0.0F, false));
		guide.cubeList.add(new ModelBox(guide, 7, 9, 37.5F, -94.4542F, -56.6047F, 1, 3, 1, 0.0F, false));

		knob4 = new RendererModel(this);
		knob4.setRotationPoint(39.75F, -92.8292F, -56.1672F);
		turn_dial.addChild(knob4);
		setRotationAngle(knob4, 0.0F, 0.0F, -0.9599F);
		knob4.cubeList.add(new ModelBox(knob4, 8, 121, -0.75F, -1.4688F, -0.9375F, 2, 3, 2, 0.0F, false));
		knob4.cubeList.add(new ModelBox(knob4, 8, 121, -1.25F, -0.9688F, -0.9375F, 3, 2, 2, 0.0F, false));
		knob4.cubeList.add(new ModelBox(knob4, 77, 93, -0.75F, -0.9688F, -1.4375F, 2, 2, 2, 0.0F, false));
		knob4.cubeList.add(new ModelBox(knob4, 164, 80, -1.75F, -0.5938F, -1.1875F, 2, 1, 3, 0.0F, false));

		buttonstrip = new RendererModel(this);
		buttonstrip.setRotationPoint(-32.0F, 93.3792F, 56.7684F);
		base_plate_1.addChild(buttonstrip);
		buttonstrip.cubeList.add(new ModelBox(buttonstrip, 48, 169, 15.0F, -87.7042F, -56.1047F, 17, 3, 1, 0.0F, false));
		buttonstrip.cubeList.add(new ModelBox(buttonstrip, 51, 170, 32.0F, -87.7042F, -56.1047F, 17, 3, 1, 0.0F, false));

		buttons_lit = new RendererModel(this);
		buttons_lit.setRotationPoint(0.0F, 0.0F, 0.0F);
		buttonstrip.addChild(buttons_lit);
		

		small_screen = new RendererModel(this);
		small_screen.setRotationPoint(9.0F, 0.1749F, -2.3363F);
		base_plate_1.addChild(small_screen);
		setRotationAngle(small_screen, 0.6109F, 0.0F, 0.0F);
		small_screen.cubeList.add(new ModelBox(small_screen, 7, 109, -2.75F, -4.0F, 0.5F, 7, 8, 10, 0.0F, false));

		four_square = new RendererModel(this);
		four_square.setRotationPoint(-32.0F, 87.6292F, 57.0184F);
		base_plate_1.addChild(four_square);
		four_square.cubeList.add(new ModelBox(four_square, 164, 42, 21.75F, -91.9542F, -56.6047F, 3, 3, 1, 0.0F, false));
		four_square.cubeList.add(new ModelBox(four_square, 130, 60, 24.75F, -91.9542F, -56.6047F, 3, 3, 1, 0.0F, false));
		four_square.cubeList.add(new ModelBox(four_square, 149, 75, 24.75F, -94.9542F, -56.6047F, 3, 3, 1, 0.0F, false));
		four_square.cubeList.add(new ModelBox(four_square, 129, 42, 21.75F, -94.9542F, -56.6047F, 3, 3, 1, 0.0F, false));

		indicator = new RendererModel(this);
		indicator.setRotationPoint(-7.25F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator);
		setRotationAngle(indicator, -1.0472F, 0.0F, 0.0F);
		indicator.cubeList.add(new ModelBox(indicator, 34, 111, -1.5F, -1.375F, -1.125F, 3, 2, 3, 0.0F, false));

		indicator2 = new RendererModel(this);
		indicator2.setRotationPoint(-2.25F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator2);
		setRotationAngle(indicator2, -1.0472F, 0.0F, 0.0F);
		indicator2.cubeList.add(new ModelBox(indicator2, 34, 111, -1.5F, -1.375F, -1.125F, 3, 2, 3, 0.0F, false));

		indicator3 = new RendererModel(this);
		indicator3.setRotationPoint(2.75F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator3);
		setRotationAngle(indicator3, -1.0472F, 0.0F, 0.0F);
		indicator3.cubeList.add(new ModelBox(indicator3, 34, 111, -1.5F, -1.375F, -1.125F, 3, 2, 3, 0.0F, false));

		coms = new RendererModel(this);
		coms.setRotationPoint(0.0F, -13.0751F, -0.0863F);
		base_plate_1.addChild(coms);
		setRotationAngle(coms, -0.7854F, 0.0F, 0.0F);
		coms.cubeList.add(new ModelBox(coms, 7, 92, -7.5F, -3.25F, -2.0F, 15, 5, 1, 0.0F, false));

		com_needle_move_x = new RendererModel(this);
		com_needle_move_x.setRotationPoint(-48.0F, 93.7042F, 57.1047F);
		coms.addChild(com_needle_move_x);
		com_needle_move_x.cubeList.add(new ModelBox(com_needle_move_x, 130, 40, 43.0F, -94.9542F, -57.6047F, 1, 3, 2, 0.0F, false));

		fast_return_rotate_x = new RendererModel(this);
		fast_return_rotate_x.setRotationPoint(-12.8438F, 0.0299F, 0.1605F);
		base_plate_1.addChild(fast_return_rotate_x);
		fast_return_rotate_x.cubeList.add(new ModelBox(fast_return_rotate_x, 8, 8, -1.1563F, -1.99F, -2.1998F, 2, 4, 4, 0.0F, false));

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(-0.1563F, 0.01F, -0.1998F);
		fast_return_rotate_x.addChild(bone68);
		setRotationAngle(bone68, -0.7854F, 0.0F, 0.0F);
		bone68.cubeList.add(new ModelBox(bone68, 8, 8, -0.5F, -2.5F, -2.5F, 1, 5, 5, 0.0F, false));

		controls2 = new RendererModel(this);
		controls2.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls2);
		setRotationAngle(controls2, 0.0F, -1.5708F, 0.0F);
		

		base_plate_2 = new RendererModel(this);
		base_plate_2.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls2.addChild(base_plate_2);
		setRotationAngle(base_plate_2, -1.309F, 0.0F, 0.0F);
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 69, 208, -19.0F, 0.9249F, 0.1637F, 8, 8, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 68, 209, 11.0F, 0.9249F, 0.1637F, 8, 8, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 48, 208, -9.0F, 0.9249F, 0.1637F, 18, 8, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 6, 77, -6.0F, 3.9249F, -0.3363F, 12, 4, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 10, 8, -5.5F, 4.4249F, -0.5863F, 11, 3, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 80, 213, -11.0F, 3.4249F, 0.9137F, 2, 3, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 80, 213, 9.0F, 3.4249F, 0.9137F, 2, 3, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 29, 202, -9.0F, -17.0751F, 0.1637F, 18, 12, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 8, 69, -8.0F, -16.0751F, -0.2113F, 4, 10, 1, 0.0F, false));
		base_plate_2.cubeList.add(new ModelBox(base_plate_2, 8, 69, 4.0F, -16.0751F, -0.2113F, 4, 10, 1, 0.0F, false));

		lever = new RendererModel(this);
		lever.setRotationPoint(-61.5F, 91.3792F, 56.2684F);
		base_plate_2.addChild(lever);
		lever.cubeList.add(new ModelBox(lever, 61, 117, 43.5F, -89.4542F, -56.6047F, 6, 6, 1, 0.0F, false));
		lever.cubeList.add(new ModelBox(lever, 10, 10, 44.5F, -88.4542F, -57.1047F, 4, 4, 1, 0.0F, false));

		lever_turn = new RendererModel(this);
		lever_turn.setRotationPoint(46.5F, -86.3292F, -58.6047F);
		lever.addChild(lever_turn);
		setRotationAngle(lever_turn, 0.0F, 0.0F, 0.0F);
		lever_turn.cubeList.add(new ModelBox(lever_turn, 10, 10, -0.5F, -1.375F, -1.0F, 1, 6, 1, 0.0F, false));
		lever_turn.cubeList.add(new ModelBox(lever_turn, 10, 10, -1.0F, -1.125F, -0.5F, 2, 2, 2, 0.0F, false));

		lever2 = new RendererModel(this);
		lever2.setRotationPoint(-31.5F, 91.3792F, 56.2684F);
		base_plate_2.addChild(lever2);
		lever2.cubeList.add(new ModelBox(lever2, 61, 117, 43.5F, -89.4542F, -56.6047F, 6, 6, 1, 0.0F, false));
		lever2.cubeList.add(new ModelBox(lever2, 10, 10, 44.5F, -88.4542F, -57.1047F, 4, 4, 1, 0.0F, false));

		door_turn = new RendererModel(this);
		door_turn.setRotationPoint(46.5F, -86.5792F, -58.6047F);
		lever2.addChild(door_turn);
		setRotationAngle(door_turn, 0.0F, 0.0F, 0.0F);
		door_turn.cubeList.add(new ModelBox(door_turn, 10, 10, -0.5F, -1.125F, -1.0F, 1, 6, 1, 0.0F, false));
		door_turn.cubeList.add(new ModelBox(door_turn, 10, 10, -1.0F, -0.875F, -0.5F, 2, 2, 2, 0.0F, false));

		wide_strip = new RendererModel(this);
		wide_strip.setRotationPoint(-4.5F, -2.5751F, 0.4137F);
		base_plate_2.addChild(wide_strip);
		setRotationAngle(wide_strip, 0.7854F, 0.0F, 0.0F);
		wide_strip.cubeList.add(new ModelBox(wide_strip, 16, 210, -6.5F, -2.25F, -0.5F, 22, 4, 3, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, -5.75F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, -2.75F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, 0.25F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, 3.25F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, 6.25F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, 9.25F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));
		wide_strip.cubeList.add(new ModelBox(wide_strip, 130, 42, 12.25F, -1.75F, -1.0F, 2, 2, 1, 0.0F, false));

		sonic_port = new RendererModel(this);
		sonic_port.setRotationPoint(-32.0F, 91.1292F, 56.7684F);
		base_plate_2.addChild(sonic_port);
		sonic_port.cubeList.add(new ModelBox(sonic_port, 2, 111, 30.0F, -103.4542F, -57.6047F, 4, 4, 1, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 5, 75, 30.0F, -99.4542F, -58.1047F, 4, 1, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 5, 75, 30.0F, -104.4542F, -58.1047F, 4, 1, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 5, 75, 29.0F, -104.4542F, -58.1047F, 1, 6, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 5, 75, 34.0F, -104.4542F, -58.1047F, 1, 6, 2, 0.0F, false));

		controls3 = new RendererModel(this);
		controls3.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls3);
		setRotationAngle(controls3, 0.0F, -2.618F, 0.0F);
		

		base_plate_3 = new RendererModel(this);
		base_plate_3.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls3.addChild(base_plate_3);
		setRotationAngle(base_plate_3, -1.309F, 0.0F, 0.0F);
		

		facing = new RendererModel(this);
		facing.setRotationPoint(15.5F, 6.6749F, -0.0863F);
		base_plate_3.addChild(facing);
		

		joystick = new RendererModel(this);
		joystick.setRotationPoint(0.0F, 0.0F, 0.0F);
		facing.addChild(joystick);
		joystick.cubeList.add(new ModelBox(joystick, 67, 165, -0.5F, -0.5F, -3.375F, 1, 1, 4, 0.0F, false));
		joystick.cubeList.add(new ModelBox(joystick, 9, 124, -1.5F, -1.5F, -5.125F, 3, 3, 2, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(1.1071F, -0.2894F, 0.2976F);
		base_plate_3.addChild(base);
		base.cubeList.add(new ModelBox(base, 75, 165, 11.8929F, 2.2143F, -0.1339F, 5, 7, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 74, 162, -17.1071F, -0.7857F, -0.1339F, 5, 10, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 131, 44, 12.3929F, 4.7143F, -0.5089F, 4, 4, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 82, 157, -12.1071F, -6.7857F, -0.1339F, 2, 16, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 82, 156, 7.8929F, -6.7857F, -0.1339F, 2, 16, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 5, 61, -6.1071F, -11.7857F, -0.8839F, 10, 15, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 5, 203, -5.6071F, -0.0357F, -1.1339F, 9, 2, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 78, 95, -16.1071F, 5.7143F, -0.6339F, 3, 3, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 78, 95, -16.1071F, 1.7143F, -0.6339F, 3, 3, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 78, 95, -10.8571F, -5.2857F, -0.6339F, 3, 3, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 61, 165, -7.1071F, -13.7857F, -0.1339F, 12, 7, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 80, 152, -10.1071F, -11.7857F, -0.1339F, 3, 21, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 78, 151, 4.8929F, -11.7857F, -0.1339F, 3, 21, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 62, 160, -7.1071F, -6.7857F, -0.1339F, 12, 13, 1, 0.0F, false));

		pull_tab = new RendererModel(this);
		pull_tab.setRotationPoint(-14.6071F, 7.2143F, -0.3839F);
		base.addChild(pull_tab);
		pull_tab.cubeList.add(new ModelBox(pull_tab, 42, 154, -0.5F, -0.5F, -1.25F, 1, 1, 6, 0.0F, false));
		pull_tab.cubeList.add(new ModelBox(pull_tab, 110, 5, -1.0F, -1.0F, -2.25F, 2, 2, 1, 0.0F, false));

		pull_tab2 = new RendererModel(this);
		pull_tab2.setRotationPoint(-14.6071F, 3.2143F, -2.3839F);
		base.addChild(pull_tab2);
		pull_tab2.cubeList.add(new ModelBox(pull_tab2, 37, 152, -0.5F, -0.5F, -1.25F, 1, 1, 6, 0.0F, false));
		pull_tab2.cubeList.add(new ModelBox(pull_tab2, 110, 5, -1.0F, -1.0F, -2.25F, 2, 2, 1, 0.0F, false));

		pull_tab3 = new RendererModel(this);
		pull_tab3.setRotationPoint(-9.3571F, -3.5357F, -0.3839F);
		base.addChild(pull_tab3);
		pull_tab3.cubeList.add(new ModelBox(pull_tab3, 35, 151, -0.5F, -0.5F, -1.25F, 1, 1, 6, 0.0F, false));
		pull_tab3.cubeList.add(new ModelBox(pull_tab3, 110, 5, -1.0F, -1.0F, -2.25F, 2, 2, 1, 0.0F, false));

		randomizers = new RendererModel(this);
		randomizers.setRotationPoint(-8.5F, 67.4898F, 55.0291F);
		base_plate_3.addChild(randomizers);
		

		key_white1 = new RendererModel(this);
		key_white1.setRotationPoint(4.25F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white1);
		key_white1.cubeList.add(new ModelBox(key_white1, 3, 8, -1.0F, 0.0F, -0.5F, 2, 5, 1, 0.0F, false));

		key_black1 = new RendererModel(this);
		key_black1.setRotationPoint(5.25F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black1);
		key_black1.cubeList.add(new ModelBox(key_black1, 4, 122, -0.5F, 0.0F, -0.5F, 1, 3, 1, 0.0F, false));

		key_white2 = new RendererModel(this);
		key_white2.setRotationPoint(6.375F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white2);
		key_white2.cubeList.add(new ModelBox(key_white2, 3, 8, -1.0F, 0.0F, -0.5F, 2, 5, 1, 0.0F, false));

		key_black2 = new RendererModel(this);
		key_black2.setRotationPoint(7.5F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black2);
		key_black2.cubeList.add(new ModelBox(key_black2, 4, 122, -0.5F, 0.0F, -0.5F, 1, 3, 1, 0.0F, false));

		key_white3 = new RendererModel(this);
		key_white3.setRotationPoint(8.5F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white3);
		key_white3.cubeList.add(new ModelBox(key_white3, 3, 8, -1.0F, 0.0F, -0.5F, 2, 5, 1, 0.0F, false));

		key_white4 = new RendererModel(this);
		key_white4.setRotationPoint(10.5938F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white4);
		key_white4.cubeList.add(new ModelBox(key_white4, 3, 8, -1.0F, 0.0F, -0.5F, 2, 5, 1, 0.0F, false));

		key_black3 = new RendererModel(this);
		key_black3.setRotationPoint(11.75F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black3);
		key_black3.cubeList.add(new ModelBox(key_black3, 4, 122, -0.5F, 0.0F, -0.5F, 1, 3, 1, 0.0F, false));

		key_white5 = new RendererModel(this);
		key_white5.setRotationPoint(12.75F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white5);
		key_white5.cubeList.add(new ModelBox(key_white5, 3, 8, -1.0F, 0.0F, -0.5F, 2, 5, 1, 0.0F, false));

		controls4 = new RendererModel(this);
		controls4.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls4);
		setRotationAngle(controls4, 0.0F, 2.618F, 0.0F);
		

		base_plate_4 = new RendererModel(this);
		base_plate_4.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls4.addChild(base_plate_4);
		setRotationAngle(base_plate_4, -1.309F, 0.0F, 0.0F);
		base_plate_4.cubeList.add(new ModelBox(base_plate_4, 12, 205, -14.0F, -7.0751F, 0.1637F, 9, 4, 1, 0.0F, false));
		base_plate_4.cubeList.add(new ModelBox(base_plate_4, 12, 205, -14.0F, -1.0751F, 0.1637F, 9, 4, 1, 0.0F, false));
		base_plate_4.cubeList.add(new ModelBox(base_plate_4, 12, 205, -14.0F, 4.9249F, 0.1637F, 9, 4, 1, 0.0F, false));
		base_plate_4.cubeList.add(new ModelBox(base_plate_4, 12, 205, 3.0F, -4.0751F, 0.1637F, 13, 13, 1, 0.0F, false));
		base_plate_4.cubeList.add(new ModelBox(base_plate_4, 14, 193, -3.0F, -8.3251F, 0.7574F, 5, 18, 1, 0.0F, false));

		coords_z = new RendererModel(this);
		coords_z.setRotationPoint(-12.5F, 6.3792F, 0.7684F);
		base_plate_4.addChild(coords_z);
		

		toggle_base = new RendererModel(this);
		toggle_base.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_z.addChild(toggle_base);
		toggle_base.cubeList.add(new ModelBox(toggle_base, 113, 5, 30.0F, -86.4542F, -57.6047F, 2, 3, 1, 0.0F, false));

		toggle1 = new RendererModel(this);
		toggle1.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_z.addChild(toggle1);
		setRotationAngle(toggle1, 0.2182F, 0.0F, 0.0F);
		toggle1.cubeList.add(new ModelBox(toggle1, 7, 96, -0.5F, -0.8618F, -0.9489F, 1, 2, 1, 0.0F, false));

		coords_y = new RendererModel(this);
		coords_y.setRotationPoint(-12.5F, 0.3792F, 0.7684F);
		base_plate_4.addChild(coords_y);
		

		toggle_base2 = new RendererModel(this);
		toggle_base2.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_y.addChild(toggle_base2);
		toggle_base2.cubeList.add(new ModelBox(toggle_base2, 113, 5, 30.0F, -86.4542F, -57.6047F, 2, 3, 1, 0.0F, false));

		toggle2 = new RendererModel(this);
		toggle2.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_y.addChild(toggle2);
		setRotationAngle(toggle2, 0.2182F, 0.0F, 0.0F);
		toggle2.cubeList.add(new ModelBox(toggle2, 7, 96, -0.5F, -0.8618F, -0.9489F, 1, 2, 1, 0.0F, false));

		coords_x = new RendererModel(this);
		coords_x.setRotationPoint(-12.5F, -5.6208F, 0.7684F);
		base_plate_4.addChild(coords_x);
		

		toggle_base3 = new RendererModel(this);
		toggle_base3.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_x.addChild(toggle_base3);
		toggle_base3.cubeList.add(new ModelBox(toggle_base3, 113, 5, 30.0F, -86.4542F, -57.6047F, 2, 3, 1, 0.0F, false));

		toggle3 = new RendererModel(this);
		toggle3.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_x.addChild(toggle3);
		setRotationAngle(toggle3, 0.2182F, 0.0F, 0.0F);
		toggle3.cubeList.add(new ModelBox(toggle3, 7, 96, -0.5F, -0.8618F, -0.9489F, 1, 2, 1, 0.0F, false));

		radar = new RendererModel(this);
		radar.setRotationPoint(9.5F, 2.4249F, 0.2887F);
		base_plate_4.addChild(radar);
		

		bigslider = new RendererModel(this);
		bigslider.setRotationPoint(-0.5F, 0.4898F, -0.2209F);
		base_plate_4.addChild(bigslider);
		

		inc_slider_move_y = new RendererModel(this);
		inc_slider_move_y.setRotationPoint(-31.5F, 89.8894F, 56.9893F);
		bigslider.addChild(inc_slider_move_y);
		

		slider_knob = new RendererModel(this);
		slider_knob.setRotationPoint(0.0F, 1.0F, 0.0F);
		inc_slider_move_y.addChild(slider_knob);
		

		twist = new RendererModel(this);
		twist.setRotationPoint(31.5F, -83.9542F, -57.3547F);
		slider_knob.addChild(twist);
		setRotationAngle(twist, 0.0F, 0.0F, -0.7854F);
		twist.cubeList.add(new ModelBox(twist, 129, 38, -1.5F, -1.5F, -0.25F, 3, 3, 1, 0.0F, false));
		twist.cubeList.add(new ModelBox(twist, 129, 38, -1.0F, -1.0F, -0.75F, 2, 2, 1, 0.0F, false));

		track = new RendererModel(this);
		track.setRotationPoint(-31.5F, 90.8894F, 56.9893F);
		bigslider.addChild(track);
		track.cubeList.add(new ModelBox(track, 32, 145, 30.0F, -97.4542F, -56.6047F, 1, 14, 1, 0.0F, false));
		track.cubeList.add(new ModelBox(track, 32, 145, 30.0F, -83.4542F, -56.6047F, 3, 1, 1, 0.0F, false));
		track.cubeList.add(new ModelBox(track, 32, 145, 32.0F, -97.4542F, -56.6047F, 1, 14, 1, 0.0F, false));
		track.cubeList.add(new ModelBox(track, 32, 145, 30.0F, -98.4542F, -56.6047F, 3, 1, 1, 0.0F, false));
		track.cubeList.add(new ModelBox(track, 22, 101, 30.4688F, -97.4542F, -56.2297F, 2, 14, 1, 0.0F, false));

		button_array = new RendererModel(this);
		button_array.setRotationPoint(-31.75F, 91.3792F, 56.7684F);
		base_plate_4.addChild(button_array);
		

		base_plate = new RendererModel(this);
		base_plate.setRotationPoint(0.0F, 0.0F, 0.75F);
		button_array.addChild(base_plate);
		base_plate.cubeList.add(new ModelBox(base_plate, 17, 196, 23.25F, -108.4542F, -56.6047F, 17, 8, 1, 0.0F, false));

		controls5 = new RendererModel(this);
		controls5.setRotationPoint(0.0F, 0.0F, -0.75F);
		controls.addChild(controls5);
		setRotationAngle(controls5, 0.0F, 1.5708F, 0.0F);
		

		base_plate_5 = new RendererModel(this);
		base_plate_5.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls5.addChild(base_plate_5);
		setRotationAngle(base_plate_5, -1.309F, 0.0F, 0.0F);
		

		landing_type = new RendererModel(this);
		landing_type.setRotationPoint(-21.0F, 97.3792F, 54.7684F);
		base_plate_5.addChild(landing_type);
		landing_type.cubeList.add(new ModelBox(landing_type, 130, 43, 24.5F, -96.7042F, -54.8547F, 4, 1, 1, 0.0F, false));

		knob = new RendererModel(this);
		knob.setRotationPoint(13.5191F, 6.622F, 0.2493F);
		base_plate_5.addChild(knob);
		setRotationAngle(knob, 0.0F, 0.0F, -0.7854F);
		knob.cubeList.add(new ModelBox(knob, 75, 123, -1.5F, -1.5F, -0.75F, 3, 3, 2, 0.0F, false));
		knob.cubeList.add(new ModelBox(knob, 63, 100, -1.0F, -2.0F, -0.5F, 2, 4, 1, 0.0F, false));
		knob.cubeList.add(new ModelBox(knob, 63, 100, -2.0F, -1.0F, -0.5F, 4, 2, 1, 0.0F, false));

		angeled_panel = new RendererModel(this);
		angeled_panel.setRotationPoint(-32.0F, 92.8792F, 58.4871F);
		base_plate_5.addChild(angeled_panel);
		angeled_panel.cubeList.add(new ModelBox(angeled_panel, 74, 163, 18.0F, -99.4542F, -57.5735F, 4, 9, 2, 0.0F, false));
		angeled_panel.cubeList.add(new ModelBox(angeled_panel, 68, 164, 14.0F, -90.4542F, -57.6047F, 8, 8, 2, 0.0F, false));

		dummy_buttons = new RendererModel(this);
		dummy_buttons.setRotationPoint(-4.0F, -2.0F, -1.0F);
		angeled_panel.addChild(dummy_buttons);
		dummy_buttons.cubeList.add(new ModelBox(dummy_buttons, 131, 45, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons.cubeList.add(new ModelBox(dummy_buttons, 9, 94, 21.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons.cubeList.add(new ModelBox(dummy_buttons, 9, 94, 18.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		dummy_buttons2 = new RendererModel(this);
		dummy_buttons2.setRotationPoint(-4.0F, -4.75F, -1.0F);
		angeled_panel.addChild(dummy_buttons2);
		dummy_buttons2.cubeList.add(new ModelBox(dummy_buttons2, 131, 59, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons2.cubeList.add(new ModelBox(dummy_buttons2, 9, 94, 21.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons2.cubeList.add(new ModelBox(dummy_buttons2, 9, 94, 18.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons2.cubeList.add(new ModelBox(dummy_buttons2, 9, 94, 16.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		dummy_buttons7 = new RendererModel(this);
		dummy_buttons7.setRotationPoint(31.0F, -101.4542F, -58.6047F);
		angeled_panel.addChild(dummy_buttons7);
		setRotationAngle(dummy_buttons7, -0.8727F, 0.0F, 0.0F);
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, -5.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, -3.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, -1.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, 0.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, 2.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));
		dummy_buttons7.cubeList.add(new ModelBox(dummy_buttons7, 9, 94, 4.5F, -0.5F, -1.0F, 1, 1, 2, 0.0F, false));

		dummy_buttons8 = new RendererModel(this);
		dummy_buttons8.setRotationPoint(12.5F, -22.75F, -1.25F);
		angeled_panel.addChild(dummy_buttons8);
		dummy_buttons8.cubeList.add(new ModelBox(dummy_buttons8, 131, 61, 15.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));
		dummy_buttons8.cubeList.add(new ModelBox(dummy_buttons8, 165, 43, 21.0F, -82.2042F, -57.6047F, 1, 1, 2, 0.0F, false));

		dummy_buttons3 = new RendererModel(this);
		dummy_buttons3.setRotationPoint(-4.0F, -7.5F, -1.0F);
		angeled_panel.addChild(dummy_buttons3);
		dummy_buttons3.cubeList.add(new ModelBox(dummy_buttons3, 164, 42, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons3.cubeList.add(new ModelBox(dummy_buttons3, 112, 6, 21.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons3.cubeList.add(new ModelBox(dummy_buttons3, 112, 6, 18.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		dummy_buttons4 = new RendererModel(this);
		dummy_buttons4.setRotationPoint(-4.0F, -10.25F, -1.0F);
		angeled_panel.addChild(dummy_buttons4);
		dummy_buttons4.cubeList.add(new ModelBox(dummy_buttons4, 165, 42, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons4.cubeList.add(new ModelBox(dummy_buttons4, 112, 6, 21.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		dummy_buttons5 = new RendererModel(this);
		dummy_buttons5.setRotationPoint(-4.0F, -13.0F, -1.0F);
		angeled_panel.addChild(dummy_buttons5);
		dummy_buttons5.cubeList.add(new ModelBox(dummy_buttons5, 131, 59, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));
		dummy_buttons5.cubeList.add(new ModelBox(dummy_buttons5, 112, 6, 21.0F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		dummy_buttons6 = new RendererModel(this);
		dummy_buttons6.setRotationPoint(-4.0F, -15.75F, -1.0F);
		angeled_panel.addChild(dummy_buttons6);
		dummy_buttons6.cubeList.add(new ModelBox(dummy_buttons6, 112, 6, 23.5F, -83.2042F, -57.6047F, 2, 2, 2, 0.0F, false));

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(18.0F, -93.9542F, -57.8235F);
		angeled_panel.addChild(bone67);
		setRotationAngle(bone67, 0.0F, 0.0F, 0.5061F);
		bone67.cubeList.add(new ModelBox(bone67, 76, 156, -2.75F, -4.75F, 0.25F, 4, 16, 1, 0.0F, false));
		bone67.cubeList.add(new ModelBox(bone67, 6, 150, 1.25F, 2.25F, 0.25F, 1, 9, 1, 0.0F, false));

		panels = new RendererModel(this);
		panels.setRotationPoint(-5.0F, 91.3792F, 56.7684F);
		base_plate_5.addChild(panels);
		panels.cubeList.add(new ModelBox(panels, 15, 160, 8.0F, -96.4542F, -55.9797F, 1, 15, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 15, 160, 9.0F, -96.4542F, -55.9797F, 3, 1, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 80, 163, 9.0F, -89.4542F, -55.9797F, 3, 8, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 15, 160, 10.0F, -95.4542F, -55.9797F, 1, 6, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 6, 121, 9.0F, -95.4542F, -55.8547F, 1, 6, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 6, 121, 11.0F, -95.4542F, -55.8547F, 1, 6, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 15, 160, 12.0F, -96.4542F, -55.9797F, 1, 15, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 57, 165, -3.0F, -105.4542F, -56.6047F, 14, 7, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 77, 155, 13.0F, -99.4542F, -55.9797F, 4, 18, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 130, 39, 13.75F, -93.7042F, -56.2297F, 3, 6, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 110, 7, 12.75F, -86.7042F, -56.2297F, 3, 4, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 110, 7, 8.75F, -86.4542F, -56.2297F, 3, 3, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 15, 65, 13.75F, -98.4542F, -56.2297F, 3, 4, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 80, 162, 17.0F, -91.4542F, -55.9797F, 2, 10, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 78, 165, 19.0F, -88.4542F, -55.9797F, 3, 7, 1, 0.0F, false));

		obtuse_panel = new RendererModel(this);
		obtuse_panel.setRotationPoint(19.25F, -92.4542F, -56.1047F);
		panels.addChild(obtuse_panel);
		setRotationAngle(obtuse_panel, 0.0F, 0.0F, -0.48F);
		obtuse_panel.cubeList.add(new ModelBox(obtuse_panel, 76, 157, -2.7635F, -7.248F, 0.25F, 4, 17, 1, 0.0F, false));

		refuler = new RendererModel(this);
		refuler.setRotationPoint(-44.25F, 92.1292F, 58.3934F);
		base_plate_5.addChild(refuler);
		

		needle = new RendererModel(this);
		needle.setRotationPoint(40.75F, -86.9542F, -57.511F);
		refuler.addChild(needle);
		setRotationAngle(needle, 0.0F, 0.0F, -1.0472F);
		needle.cubeList.add(new ModelBox(needle, 134, 40, -0.5F, -4.5F, -0.5F, 1, 5, 1, 0.0F, false));

		trim = new RendererModel(this);
		trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		refuler.addChild(trim);
		trim.cubeList.add(new ModelBox(trim, 8, 114, 35.25F, -91.4542F, -57.6047F, 1, 5, 1, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 8, 114, 45.25F, -91.4542F, -57.6047F, 1, 5, 1, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 8, 114, 36.25F, -93.4542F, -57.6047F, 2, 9, 1, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 8, 114, 43.25F, -93.4542F, -57.6047F, 2, 9, 1, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 8, 114, 38.25F, -94.4542F, -57.6047F, 5, 11, 1, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 8, 114, 37.25F, -87.4542F, -58.1047F, 7, 2, 1, 0.0F, false));

		glass = new RendererModel(this);
		glass.setRotationPoint(0.0F, 0.0F, -0.25F);
		refuler.addChild(glass);
		

		controls6 = new RendererModel(this);
		controls6.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls6);
		setRotationAngle(controls6, 0.0F, 0.5236F, 0.0F);
		

		base_plate_6 = new RendererModel(this);
		base_plate_6.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls6.addChild(base_plate_6);
		setRotationAngle(base_plate_6, -1.309F, 0.0F, 0.0F);
		base_plate_6.cubeList.add(new ModelBox(base_plate_6, 10, 203, -17.0F, -1.0751F, 0.1637F, 12, 10, 1, 0.0F, false));
		base_plate_6.cubeList.add(new ModelBox(base_plate_6, 4, 190, -5.0F, -16.0751F, 0.1637F, 10, 25, 1, 0.0F, false));
		base_plate_6.cubeList.add(new ModelBox(base_plate_6, 15, 206, -4.5F, -6.3251F, -0.5863F, 9, 4, 1, 0.0F, false));
		base_plate_6.cubeList.add(new ModelBox(base_plate_6, 10, 120, -3.5F, -5.3251F, -0.8363F, 7, 2, 1, 0.0F, false));
		base_plate_6.cubeList.add(new ModelBox(base_plate_6, 7, 200, 5.0F, -1.0751F, 0.1637F, 12, 10, 1, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(-11.0F, -6.0751F, 0.6637F);
		base_plate_6.addChild(bone69);
		setRotationAngle(bone69, 0.0F, 0.0F, 0.7854F);
		bone69.cubeList.add(new ModelBox(bone69, 10, 203, 0.0F, -9.0F, -0.25F, 8, 17, 1, 0.0F, false));
		bone69.cubeList.add(new ModelBox(bone69, 10, 203, 7.0F, -15.0F, -0.25F, 15, 8, 1, 0.0F, false));

		dummy_buttons9 = new RendererModel(this);
		dummy_buttons9.setRotationPoint(-15.0F, 90.5979F, 56.5184F);
		base_plate_6.addChild(dummy_buttons9);
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 129, 40, 16.0F, -98.4542F, -56.6047F, 3, 1, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 16.5F, -100.7042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 11.5F, -100.7042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 16.5F, -102.9542F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 20.5F, -95.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 23.5F, -95.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 7.5F, -98.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 17, 76, 11.5F, -105.2042F, -56.6047F, 2, 2, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 5, 8, 17.0F, -95.4542F, -57.8547F, 1, 1, 1, 0.0F, false));
		dummy_buttons9.cubeList.add(new ModelBox(dummy_buttons9, 5, 8, 12.0F, -95.4542F, -57.8547F, 1, 1, 1, 0.0F, false));

		dial = new RendererModel(this);
		dial.setRotationPoint(-14.0F, 3.4898F, -3.2209F);
		base_plate_6.addChild(dial);
		

		shield_health = new RendererModel(this);
		shield_health.setRotationPoint(3.0F, 2.9351F, 2.8846F);
		dial.addChild(shield_health);
		setRotationAngle(shield_health, 0.0F, 0.0F, -0.6981F);
		shield_health.cubeList.add(new ModelBox(shield_health, 133, 41, -0.5F, -4.25F, -0.5F, 1, 4, 1, 0.0F, false));

		dial_trim = new RendererModel(this);
		dial_trim.setRotationPoint(-20.0F, 87.8894F, 59.4893F);
		dial.addChild(dial_trim);
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 21.0F, -91.4542F, -56.6047F, 4, 8, 1, 0.0F, false));
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 21.0F, -85.9542F, -57.3547F, 4, 2, 1, 0.0F, false));
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 25.0F, -90.4542F, -56.6047F, 1, 6, 1, 0.0F, false));
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 20.0F, -90.4542F, -56.6047F, 1, 6, 1, 0.0F, false));
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 26.0F, -89.4542F, -56.6047F, 1, 4, 1, 0.0F, false));
		dial_trim.cubeList.add(new ModelBox(dial_trim, 5, 113, 19.0F, -89.4542F, -56.6047F, 1, 4, 1, 0.0F, false));

		dial_glass = new RendererModel(this);
		dial_glass.setRotationPoint(3.5F, -0.6106F, 4.1143F);
		dial.addChild(dial_glass);
		

		dial2 = new RendererModel(this);
		dial2.setRotationPoint(11.0F, 4.4898F, -0.2209F);
		base_plate_6.addChild(dial2);
		

		dial_trim2 = new RendererModel(this);
		dial_trim2.setRotationPoint(-23.0F, 86.8894F, 56.4893F);
		dial2.addChild(dial_trim2);
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 21.0F, -85.9542F, -57.3547F, 4, 2, 1, 0.0F, false));
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 21.0F, -91.4542F, -56.6047F, 4, 8, 1, 0.0F, false));
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 25.0F, -90.4542F, -56.6047F, 1, 6, 1, 0.0F, false));
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 20.0F, -90.4542F, -56.6047F, 1, 6, 1, 0.0F, false));
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 26.0F, -89.4542F, -56.6047F, 1, 4, 1, 0.0F, false));
		dial_trim2.cubeList.add(new ModelBox(dial_trim2, 5, 113, 19.0F, -89.4542F, -56.6047F, 1, 4, 1, 0.0F, false));

		dial_glass2 = new RendererModel(this);
		dial_glass2.setRotationPoint(-22.5F, 87.3894F, 56.1143F);
		dial2.addChild(dial_glass2);
		

		flight_time = new RendererModel(this);
		flight_time.setRotationPoint(0.0F, 1.9351F, -0.1154F);
		dial2.addChild(flight_time);
		setRotationAngle(flight_time, 0.0F, 0.0F, -0.6981F);
		flight_time.cubeList.add(new ModelBox(flight_time, 133, 41, -0.5F, -4.25F, -0.5F, 1, 4, 1, 0.0F, false));

		slider_frames = new RendererModel(this);
		slider_frames.setRotationPoint(-2.5F, 4.3792F, -0.2316F);
		base_plate_6.addChild(slider_frames);
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, -1.5F, -5.4542F, -0.6047F, 1, 9, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, 0.5F, -5.4542F, -0.6047F, 1, 9, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 9, 116, -0.5F, -4.4542F, -0.1047F, 1, 7, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, -0.5F, -5.4542F, -0.6047F, 1, 1, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, -0.5F, 2.5458F, -0.6047F, 1, 1, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, 3.5F, -5.4542F, -0.6047F, 1, 9, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, 5.5F, -5.4542F, -0.6047F, 1, 9, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 10, 105, 4.5F, -4.4542F, -0.1047F, 1, 7, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, 4.5F, -5.4542F, -0.6047F, 1, 1, 1, 0.0F, false));
		slider_frames.cubeList.add(new ModelBox(slider_frames, 11, 7, 4.5F, 2.5458F, -0.6047F, 1, 1, 1, 0.0F, false));

		stablizers = new RendererModel(this);
		stablizers.setRotationPoint(2.0F, 4.3792F, 0.7684F);
		base_plate_6.addChild(stablizers);
		

		slider_left_move_y = new RendererModel(this);
		slider_left_move_y.setRotationPoint(-23.0F, 93.0F, 54.0F);
		stablizers.addChild(slider_left_move_y);
		slider_left_move_y.cubeList.add(new ModelBox(slider_left_move_y, 132, 44, 22.0F, -97.4542F, -56.6047F, 3, 1, 1, 0.0F, false));

		slider_right_move_y = new RendererModel(this);
		slider_right_move_y.setRotationPoint(-23.0F, 93.0F, 54.0F);
		stablizers.addChild(slider_right_move_y);
		slider_right_move_y.cubeList.add(new ModelBox(slider_right_move_y, 130, 43, 17.0F, -91.4542F, -56.6047F, 3, 1, 1, 0.0F, false));

		rib_control = new RendererModel(this);
		rib_control.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(rib_control);
		setRotationAngle(rib_control, 0.0F, -0.5236F, 0.0F);
		

		spine_con1 = new RendererModel(this);
		spine_con1.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con1);
		setRotationAngle(spine_con1, 0.0F, -0.5236F, 0.0F);
		

		plain7 = new RendererModel(this);
		plain7.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con1.addChild(plain7);
		setRotationAngle(plain7, -1.309F, 0.0F, 0.0F);
		plain7.cubeList.add(new ModelBox(plain7, 80, 74, -9.0F, 4.9249F, 0.9137F, 18, 9, 2, 0.0F, false));
		plain7.cubeList.add(new ModelBox(plain7, 96, 97, -8.0F, 5.9249F, -0.0863F, 16, 7, 1, 0.0F, false));
		plain7.cubeList.add(new ModelBox(plain7, 96, 97, -3.5F, 8.4249F, -3.0863F, 7, 2, 3, 0.0F, false));
		plain7.cubeList.add(new ModelBox(plain7, 96, 97, -6.75F, 8.4249F, -3.0863F, 2, 2, 3, 0.0F, false));
		plain7.cubeList.add(new ModelBox(plain7, 96, 97, 4.75F, 8.4249F, -3.0863F, 2, 2, 3, 0.0F, false));
		plain7.cubeList.add(new ModelBox(plain7, 98, 51, -3.5F, -20.0751F, -1.3363F, 7, 10, 2, 0.0F, false));

		bar = new RendererModel(this);
		bar.setRotationPoint(0.25F, 9.4249F, -4.0863F);
		plain7.addChild(bar);
		setRotationAngle(bar, -0.7854F, 0.0F, 0.0F);
		bar.cubeList.add(new ModelBox(bar, 96, 97, -7.75F, -2.0F, -2.0F, 3, 4, 4, 0.0F, false));
		bar.cubeList.add(new ModelBox(bar, 97, 140, -8.0F, -1.5F, -1.5F, 1, 3, 3, 0.0F, false));
		bar.cubeList.add(new ModelBox(bar, 97, 140, 6.5F, -1.5F, -1.5F, 1, 3, 3, 0.0F, false));
		bar.cubeList.add(new ModelBox(bar, 96, 97, 4.25F, -2.0F, -2.0F, 3, 4, 4, 0.0F, false));
		bar.cubeList.add(new ModelBox(bar, 96, 97, -3.75F, -2.0F, -2.0F, 7, 4, 4, 0.0F, false));

		handbrake_x = new RendererModel(this);
		handbrake_x.setRotationPoint(0.5F, 9.4249F, -4.0863F);
		plain7.addChild(handbrake_x);
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -5.0F, -2.5915F, -2.4755F, 1, 5, 5, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -5.0F, -3.5915F, -1.9755F, 1, 1, 4, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -5.0F, -2.0915F, -3.4755F, 1, 4, 1, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -5.0F, -2.0915F, 2.5245F, 1, 4, 1, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, 3.0F, -2.5915F, -2.4755F, 1, 5, 5, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, 3.0F, -3.5915F, -1.9755F, 1, 1, 4, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, 3.0F, -2.0915F, -3.4755F, 1, 4, 1, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, 3.0F, -2.0915F, 2.5245F, 1, 4, 1, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -5.0F, 2.4085F, -0.9755F, 1, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, 3.0F, 2.4085F, -0.9755F, 1, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 96, 141, -6.5F, 4.9085F, -0.4755F, 12, 1, 1, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 130, 43, -6.0F, 4.4085F, -0.9755F, 3, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 130, 43, 2.0F, 4.4085F, -0.9755F, 3, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 130, 43, 0.75F, 4.4085F, -0.9755F, 1, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 130, 43, -2.75F, 4.4085F, -0.9755F, 1, 2, 2, 0.0F, false));
		handbrake_x.cubeList.add(new ModelBox(handbrake_x, 130, 43, -1.5F, 4.4085F, -0.9755F, 2, 2, 2, 0.0F, false));

		bevel7 = new RendererModel(this);
		bevel7.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine_con1.addChild(bevel7);
		setRotationAngle(bevel7, -0.6981F, 0.0F, 0.0F);
		

		spine_con2 = new RendererModel(this);
		spine_con2.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con2);
		setRotationAngle(spine_con2, 0.0F, -2.618F, 0.0F);
		

		plain8 = new RendererModel(this);
		plain8.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con2.addChild(plain8);
		setRotationAngle(plain8, -1.309F, 0.0F, 0.0F);
		plain8.cubeList.add(new ModelBox(plain8, 96, 97, -4.0F, 5.9249F, 0.4137F, 8, 8, 1, 0.0F, false));
		plain8.cubeList.add(new ModelBox(plain8, 96, 97, 1.0F, 6.9249F, -2.5863F, 1, 2, 3, 0.0F, false));
		plain8.cubeList.add(new ModelBox(plain8, 96, 97, -2.0F, 6.9249F, -2.5863F, 1, 2, 3, 0.0F, false));
		plain8.cubeList.add(new ModelBox(plain8, 99, 74, -5.0F, 4.9249F, 0.9137F, 10, 10, 1, 0.0F, false));

		winch = new RendererModel(this);
		winch.setRotationPoint(-0.75F, 8.4249F, -2.0863F);
		plain8.addChild(winch);
		setRotationAngle(winch, -0.6109F, 0.0F, 0.0F);
		

		case_model = new RendererModel(this);
		case_model.setRotationPoint(0.3889F, 1.9444F, -1.0F);
		winch.addChild(case_model);
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, -2.6389F, -1.4444F, -3.5F, 1, 3, 6, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, 2.3611F, -1.4444F, -3.5F, 1, 3, 6, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, 2.3611F, -2.4444F, -3.0F, 1, 5, 5, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, 2.3611F, -2.9444F, -2.0F, 1, 6, 3, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, -2.6389F, -2.9444F, -2.0F, 1, 6, 3, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 93, 164, -2.6389F, -2.4444F, -3.0F, 1, 5, 5, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -4.6389F, -1.4444F, -3.5F, 2, 3, 6, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, 3.3611F, -1.4444F, -3.5F, 2, 3, 6, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -1.6389F, -1.4444F, -3.5F, 4, 3, 6, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -4.6389F, -2.9444F, -2.0F, 2, 6, 3, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, 3.3611F, -2.9444F, -2.0F, 2, 6, 3, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -1.6389F, -2.9444F, -2.0F, 4, 6, 3, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -1.6389F, -1.4444F, 0.0F, 1, 2, 8, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, 1.3611F, -1.4444F, 0.0F, 1, 2, 8, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, 3.3611F, -2.4444F, -3.0F, 3, 5, 5, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -1.6389F, -2.4444F, -3.0F, 4, 5, 5, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -5.6389F, -2.4444F, -3.0F, 3, 5, 5, 0.0F, false));
		case_model.cubeList.add(new ModelBox(case_model, 96, 97, -6.6389F, -1.9444F, -2.5F, 1, 4, 4, 0.0F, false));

		throttle_x = new RendererModel(this);
		throttle_x.setRotationPoint(9.3393F, 2.0357F, -1.5F);
		winch.addChild(throttle_x);
		setRotationAngle(throttle_x, 0.8727F, 0.0F, 0.0F);
		throttle_x.cubeList.add(new ModelBox(throttle_x, 5, 160, -2.3393F, -2.0357F, -2.0F, 1, 4, 4, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 8, 165, -2.3393F, 1.9643F, -1.5F, 1, 5, 3, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 92, 135, -3.0893F, -1.0357F, -1.0F, 2, 2, 2, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 92, 135, -2.5893F, 4.9643F, -0.5F, 7, 1, 1, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 132, 42, -1.0268F, 4.4643F, -1.0F, 1, 2, 2, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 132, 42, 0.0982F, 4.4643F, -1.0F, 3, 2, 2, 0.0F, false));
		throttle_x.cubeList.add(new ModelBox(throttle_x, 132, 42, 3.2857F, 4.4643F, -1.0F, 1, 2, 2, 0.0F, false));

		bevel8 = new RendererModel(this);
		bevel8.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine_con2.addChild(bevel8);
		setRotationAngle(bevel8, -0.6981F, 0.0F, 0.0F);
		

		spine_con3 = new RendererModel(this);
		spine_con3.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con3);
		setRotationAngle(spine_con3, 0.0F, 1.5708F, 0.0F);
		

		plain9 = new RendererModel(this);
		plain9.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con3.addChild(plain9);
		setRotationAngle(plain9, -1.309F, 0.0F, 0.0F);
		

		telepathic2 = new RendererModel(this);
		telepathic2.setRotationPoint(-31.5F, 90.6292F, 59.7684F);
		plain9.addChild(telepathic2);
		telepathic2.cubeList.add(new ModelBox(telepathic2, 94, 72, 26.0F, -86.4542F, -58.1047F, 11, 11, 2, 0.0F, false));
		telepathic2.cubeList.add(new ModelBox(telepathic2, 96, 97, 33.0F, -85.4542F, -59.1047F, 2, 7, 2, 0.0F, false));
		telepathic2.cubeList.add(new ModelBox(telepathic2, 96, 97, 28.0F, -85.4542F, -59.1047F, 2, 7, 2, 0.0F, false));

		microscope = new RendererModel(this);
		microscope.setRotationPoint(31.5F, -77.7189F, -56.9429F);
		telepathic2.addChild(microscope);
		setRotationAngle(microscope, 0.5236F, 0.0F, 0.0F);
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, 0.5F, -1.6013F, -9.6618F, 1, 1, 9, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, -1.5F, -1.6013F, -9.6618F, 1, 1, 9, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, -0.5F, -3.6013F, -3.6618F, 1, 3, 1, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, -2.0F, -6.6013F, -15.6618F, 4, 4, 8, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 56, 115, -2.0F, -6.6013F, -20.6618F, 4, 4, 2, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, -1.5F, -6.1013F, -19.6618F, 3, 3, 13, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, -0.5F, -0.6013F, -9.1618F, 1, 1, 9, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, -3.0F, -1.1013F, -10.6618F, 1, 2, 2, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, -3.0F, -5.6013F, -16.6618F, 1, 2, 2, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, 2.0F, -5.6013F, -16.6618F, 1, 2, 2, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, -2.0F, -0.6013F, -10.1618F, 4, 1, 1, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, -3.5F, -5.1013F, -16.1618F, 7, 1, 1, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 93, 136, 2.0F, -1.1013F, -10.6618F, 1, 2, 2, 0.0F, false));
		microscope.cubeList.add(new ModelBox(microscope, 96, 97, -1.0F, -3.6013F, -9.6618F, 2, 2, 1, 0.0F, false));

		center = new RendererModel(this);
		center.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		bottom = new RendererModel(this);
		bottom.setRotationPoint(0.0F, 37.0068F, 27.7761F);
		center.addChild(bottom);
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, -17.0F, -41.0F, -41.0F, 34, 1, 21, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, -30.0F, -41.0F, -41.0F, 13, 1, 21, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, 17.0F, -41.0F, -41.0F, 13, 1, 21, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, 0.0F, -41.0F, -20.0F, 17, 1, 19, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, -21.0F, -41.0F, -20.0F, 21, 1, 19, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, -19.0F, -41.0F, -57.0F, 21, 1, 16, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 5, 56, 2.0F, -41.0F, -57.0F, 21, 1, 16, 0.0F, false));

		innards = new RendererModel(this);
		innards.setRotationPoint(1.0357F, -60.4932F, -2.2954F);
		center.addChild(innards);
		

		top = new RendererModel(this);
		top.setRotationPoint(0.0F, -15.0F, 0.0F);
		innards.addChild(top);
		setRotationAngle(top, 0.0F, -0.5236F, 0.0F);
		top.cubeList.add(new ModelBox(top, 5, 56, -12.2857F, -0.5F, -19.9286F, 25, 1, 19, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -17.2857F, -0.5F, -17.9286F, 5, 1, 17, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 12.7143F, -0.5F, -17.9286F, 5, 1, 17, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -17.2857F, -0.5F, 6.0714F, 5, 1, 17, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 12.7143F, -0.5F, 6.0714F, 5, 1, 17, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -20.2857F, -0.5F, -10.9286F, 3, 1, 10, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 17.7143F, -0.5F, -10.9286F, 3, 1, 10, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 17.7143F, -0.5F, 6.0714F, 3, 1, 10, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -20.2857F, -0.5F, 6.0714F, 3, 1, 10, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -23.2857F, -0.5F, -7.9286F, 3, 1, 7, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 20.7143F, -0.5F, -7.9286F, 3, 1, 7, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, 20.7143F, -0.5F, 6.0714F, 3, 1, 7, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -23.2857F, -0.5F, 6.0714F, 3, 1, 7, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -12.2857F, -0.5F, 6.0714F, 25, 1, 19, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 5, 56, -25.2857F, -0.5F, -0.9286F, 51, 1, 7, 0.0F, false));
	}

	public void render(ConsoleTile console) {
		//Rotor
		this.glow_rotor_bob_y.offsetY = (float)Math.cos(console.flightTicks * 0.1F) * 0.5F + 0.25F;
		this.crystal_spin_y.rotateAngleY = (float)Math.toRadians(console.flightTicks % 360.0F);
		this.outer_rotate_x.rotateAngleX = (float)Math.toRadians(console.flightTicks * 3 % 360.0F);
		this.inner_rotate_x.rotateAngleX = -(float)Math.toRadians(console.flightTicks * 3 % 360.0F);
		
		//Throttle
		this.throttle_x.rotateAngleX = -(float)Math.toRadians(console.getControl(ThrottleControl.class).getAmount() * 180.0);
		
		//Handbrake
		this.handbrake_x.rotateAngleX = (float)Math.toRadians(console.getControl(HandbrakeControl.class).isFree() ? 0 : 180);
		
		//Facing
		Direction dir = console.getDirection();
		if(dir == Direction.NORTH) {
			this.facing.rotateAngleX = -(float) Math.toRadians(45);
			this.facing.rotateAngleZ = 0;
			this.facing.rotateAngleY = 0;
		}
		else if(dir == Direction.SOUTH) {
			this.facing.rotateAngleX = (float) Math.toRadians(45);
			this.facing.rotateAngleZ = 0;
			this.facing.rotateAngleY = 0;
		}
		else if(dir == Direction.EAST) {
			this.facing.rotateAngleY = -(float) Math.toRadians(45);
			this.facing.rotateAngleX = 0;
			this.facing.rotateAngleZ = 0;
		}
		else {
			this.facing.rotateAngleY = (float) Math.toRadians(45);
			this.facing.rotateAngleX = 0;
			this.facing.rotateAngleZ = 0;
		}
		
		console.getDoor().ifPresent(door -> this.door_turn.rotateAngleZ = (float)Math.toRadians(door.getOpenState() != EnumDoorState.CLOSED ? 90 : 0));
		if(console.getControl(DoorControl.class).getAnimationTicks() > 0)
			this.door_turn.rotateAngleZ = (float)Math.toRadians(console.getControl(DoorControl.class).getAnimationProgress() * 360.0F);
		
		this.fast_return_rotate_x.rotateAngleX = (float) Math.toRadians(console.getControl(FastReturnControl.class).getAnimationProgress() * 360.0F);
		
		RefuelerControl refueler = console.getControl(RefuelerControl.class);
		this.needle.rotateAngleZ = (float) Math.toRadians(refueler.isRefueling() ? 60 : -46);
		if(refueler.getAnimationTicks() > 0) {
			this.needle.rotateAngleZ = (float)Math.toRadians(-46 + (refueler.getAnimationProgress() * 106));
		}
		
		this.landing_type.offsetY = console.getControl(LandingTypeControl.class).getLandType() == EnumLandType.UP ? -0.25F : 0F;
		
		this.inc_slider_move_y.offsetY = -(console.getControl(IncModControl.class).index / (float)IncModControl.COORD_MODS.length);
		
		RandomiserControl random = console.getControl(RandomiserControl.class);
		if(random.getAnimationTicks() > 0) {
			this.key_white1.rotateAngleX = (float) Math.toRadians(random.getAnimationProgress() < 0.75 ? 10 : 0);
			if(random.getAnimationProgress() > 0.5F)
				this.key_white1.rotateAngleX = 0;
			this.key_white3.rotateAngleX = (float) Math.toRadians(random.getAnimationProgress() < 0.5 ? 10 : 0);
			this.key_white4.rotateAngleX = (float) Math.toRadians(random.getAnimationProgress() < 0.25 ? 10 : 0);
		}
		else {
			this.key_white1.rotateAngleX = 0;
			this.key_white3.rotateAngleX = 0;
			this.key_white4.rotateAngleX = 0;
		}
		
		boolean stab = console.getControl(StabilizerControl.class).isStabilized();
		if(stab) {
			this.slider_left_move_y.offsetY = 0.4F;
			this.slider_right_move_y.offsetY = -0.35F;
		}
		else {
			this.slider_left_move_y.offsetY = 0F;
			this.slider_right_move_y.offsetY = -0F;
		}
		
		this.com_needle_move_x.offsetX = (float)Math.sin(console.getControl(CommunicatorControl.class).getAnimationProgress()) * 0.6F;
		
		
		spines.render(0.0625F);
		stations.render(0.0625F);
		controls.render(0.0625F);
		center.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1.0F, glow);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}