package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class ToyotaMonitorScreen extends BaseMonitorScreen{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/toyota.png");
	
	@Override
	public void renderMonitor() {
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.renderBackground();
		int texWidth = 256, texHeight = 185;
		this.blit(width / 2 - texWidth / 2 , height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);

	}

	@Override
	public int getMinY() {
		return this.height / 2 + 32;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 115;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 245;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 110;
	}

	@Override
	public int getWidth() {
		return 201;
	}

	@Override
	public int getHeight() {
		return 152;
	}
	

}
