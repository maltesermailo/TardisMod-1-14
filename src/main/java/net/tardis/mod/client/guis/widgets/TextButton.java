package net.tardis.mod.client.guis.widgets;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.tardis.mod.helper.Helper;

public class TextButton extends Button{
	
	public TextButton(int x, int y, String text, IPressable onPress) {
		super(x, y, Minecraft.getInstance().fontRenderer.getStringWidth(text), Minecraft.getInstance().fontRenderer.FONT_HEIGHT, text, onPress);
	}

	@Override
	public void renderButton(int mouseX, int mouseY, float partialTicks) {
		Minecraft.getInstance().fontRenderer.drawString(this.getMessage(), x, y,
				Helper.isInBounds(mouseX, mouseY, x, y, x + width, y + height) ? 0x25dbf0 : 0xFFFFFF);
	}
}
