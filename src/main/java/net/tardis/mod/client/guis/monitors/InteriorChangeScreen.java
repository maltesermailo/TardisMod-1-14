package net.tardis.mod.client.guis.monitors;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeInteriorMessage;

public class InteriorChangeScreen extends MonitorScreen {

	private List<ConsoleRoom> rooms = new ArrayList<>();
	private int index = 1;
	private ConsoleRoom room;
	public boolean hasConfirmed;
	
	public InteriorChangeScreen(IMonitorGui mon, String menu) {
		super(mon, menu);
	}

	@Override
	protected void init() {
		super.init();

		this.addButton(new TextButton(this.parent.getMinX(),
				this.parent.getMinY(),
                backTranslation.getUnformattedComponentText(), (button) -> modIndex(-1)));
		
		this.addButton(new TextButton(this.parent.getMinX(),
				this.parent.getMinY() - (int)(this.minecraft.fontRenderer.FONT_HEIGHT * 1.25),
                selectTranslation.getUnformattedComponentText(), (button) -> {
                	confirmAction();
				}));
		
		this.addButton(new TextButton(this.parent.getMinX(),
				this.parent.getMinY() - (int)((this.minecraft.fontRenderer.FONT_HEIGHT * 1.25) * 2),
                nextTranslation.getUnformattedComponentText(), (button) -> modIndex(1)));
		
		this.rooms.clear();
		
		TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
			rooms.addAll(tile.getUnlockedInteriors());
			
			int temp = 0;
			for(ConsoleRoom r : rooms) {
				if(r == tile.getConsoleRoom()) {
					this.index = temp;
					
					break;
				}
				++temp;
			}
		});
		//Called here to update the room image
		modIndex(0);
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		
		this.drawCenteredString(this.font, this.room.getDisplayName().getFormattedText(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 90, 0xFFFFFF);
		if(room != null) {
			this.minecraft.getTextureManager().bindTexture(room.getTexture());
			
			float asp = 1.77777F;
            int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
            
            int width = 150;
            int height = (int)(width / asp);
            blit(centerX - width / 2, this.parent.getMaxY(), 0, 0, width, height, width, height);
		}
	}
	
	private void modIndex(int i) {
		if(index + i >= this.rooms.size()) {
			index = 0;
		}
		else if(index + i < 0) {
			index = this.rooms.size() - 1;
		}
		else index += i;
		
		int tempIndex = 0;
		Iterator<ConsoleRoom> it = rooms.iterator();
		while(it.hasNext()) {
			ConsoleRoom r = it.next();
			if(tempIndex == index) {
				this.room = r;
				return;
			}
			++tempIndex;
		}
		
	}
	
	private void confirmAction() {
		this.getMinecraft().displayGuiScreen(new MonitorConfirmScreen(parent, menu, (shouldChange) -> {
			if (shouldChange) {
				Network.sendToServer(new ChangeInteriorMessage(room.getRegistryName()));
				this.minecraft.displayGuiScreen(null);
			}
			else {
				this.minecraft.displayGuiScreen(new InteriorChangeScreen(parent, menu));
			}
		},"Any existing bed positons, paintings and placed blocks in the Tardis will be removed!","Are you sure you wish to change interiors?", "Yes", "No"));
	}

}
