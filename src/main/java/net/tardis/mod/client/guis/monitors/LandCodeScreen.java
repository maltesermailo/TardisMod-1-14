package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.LandType;

public class LandCodeScreen extends MonitorScreen {
	
	private TextFieldWidget code;
	private Button setCode;
	public LandCodeScreen(IMonitorGui monitor) {
		super(monitor, "land_code");
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		TranslationTextComponent land_code_set = new TranslationTextComponent("screen.tardis.land_code.set");
		this.setCode = new Button (centerX - 40,this.parent.getMinY() - parent.getHeight() / 2 + 30, this.font.getStringWidth(land_code_set.getFormattedText()) + 10, 20, land_code_set.getFormattedText(), new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				Network.INSTANCE.sendToServer(new ConsoleUpdateMessage(DataTypes.LAND_CODE, new LandType(code.getText()), Minecraft.getInstance().world.getDimension().getType()));
				onClose();
			}
		});
		int height = this.font.FONT_HEIGHT + 10;
		this.addButton(code = new TextFieldWidget(font,
				centerX - 75,
				this.parent.getMinY() - parent.getHeight() / 2,
				parent.getWidth() - 50,
				height, ""));
		this.addButton(this.setCode);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		this.drawCenteredString(this.font,"Timeship Landing Code",centerX, this.parent.getMaxY() + 10, 0xFFFFFF);
		this.drawCenteredString(this.font,"Set Private Landing Code:",centerX, this.parent.getMinY() - (parent.getHeight() / 2) - 15, 0xFFFFFF);
		if (this.code.isFocused()) {
			this.code.setSuggestion("");
		}
		else if (this.code.getText().isEmpty()){
			this.code.setSuggestion("Enter Landing Code");
		}
	}

	@Override
	public void onClose() {
		super.onClose();
	}

}
