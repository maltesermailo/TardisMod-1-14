package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointSaveMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointNewMonitorScreen extends MonitorScreen {

	private TextFieldWidget waypointName;
	public WaypointNewMonitorScreen(IMonitorGui mon) {
		super(mon, "waypoint_new");
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), new TranslationTextComponent("> Cancel"), 
				but -> Minecraft.getInstance().displayGuiScreen(new WaypointMonitorScreen(parent, menu))));
		
		this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), new TranslationTextComponent("> Save"), but -> {
			Network.sendToServer(new WaypointSaveMessage(this.waypointName.getText()));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		int width = 150;
		this.addButton(this.waypointName = new TextFieldWidget(this.font, centerX - width / 2, this.parent.getMaxY() + 40, width, 20, "Test"));
		this.waypointName.setSuggestion("Enter Waypoint Name");
		
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		this.drawCenteredString(this.font,"Waypoint Creation",centerX + ((width / 2) - 210), this.parent.getMaxY() + 10, 0xFFFFFF);
		this.drawCenteredString(this.font,"Set New Waypoint Name:",centerX + ((width / 2) - 215), this.parent.getMaxY() + 30, 0xFFFFFF);
		if (this.waypointName.isFocused()) {
			this.waypointName.setSuggestion("");
		}
		else if (this.waypointName.getText().isEmpty()){
			this.waypointName.setSuggestion("Enter Waypoint Name");
		}
		TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile && te !=null) {
			ConsoleTile con = (ConsoleTile)te;
			int width = 150;
			this.font.drawString("Location: " + Helper.formatBlockPos(con.getLocation()),
					 centerX - width / 2, this.parent.getMaxY() + 70, 0xFFFFFF);
			
			this.font.drawString("Dimension: " + Helper.formatDimName(con.getDimension()),
					 centerX - width / 2, this.parent.getMaxY() + 80, 0xFFFFFF);
		}
		
	}
}
