package net.tardis.mod.client.guis.monitors;

public interface IMonitorGui {

	void renderMonitor();
	
	int getMinY();
	int getMinX();
	
	int getMaxX();
	int getMaxY();
	
	int getWidth();
	int getHeight();
}
