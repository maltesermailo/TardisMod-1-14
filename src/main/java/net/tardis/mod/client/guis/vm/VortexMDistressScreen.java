package net.tardis.mod.client.guis.vm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.lwjgl.glfw.GLFW;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.UsernameCache;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMDistressMessage;

public class VortexMDistressScreen extends VortexMFunctionScreen{

	private TextFieldWidget userInput;
	private Button send;
	private Button back;
	private Button increment;
	private Button decrement;
	private Button checkName;
	private int index = 0;
	private Collection<NetworkPlayerInfo> players;
	private ArrayList<UUID> UUIDs;
	private UUID selectedUUID; //UUID of player via username input
	private boolean isValid;
	private boolean hasCheckedName; //Second flag to check if we should render error message

	public VortexMDistressScreen(ITextComponent title) {
		super(title);
	}

	public VortexMDistressScreen() {
	}

	@Override
	public void init() {
		super.init();
		this.createPlayerList();
		String next = ">";
		String previous = "<";
		String sendButton = new TranslationTextComponent("button.vm.send").getFormattedText();
		String backButton = new TranslationTextComponent("button.vm.back").getFormattedText();
		String checkNames = "Check Name";
		final int btnH = 20;

		userInput = new TextFieldWidget(this.font, this.getMinX() + 42, this.getMaxY() + 95, 60, this.font.FONT_HEIGHT + 2, "");
		send = new Button(this.getMinX() + 40,this.getMaxY() + 115, this.font.getStringWidth(sendButton) + 10,btnH, sendButton, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				checkInputName();
				if (isValid) {
					Network.sendToServer(new VMDistressMessage(selectedUUID));
					onClose();
				}
			}
		});
		back = new Button(this.getMinX(),this.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		increment = new Button(this.getMinX() + 110, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, next, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				incrementList();
			}
		});
        decrement = new Button(this.getMinX() + 15, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, previous, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				decrementList();
			}
		});
        checkName = new Button(this.getMinX() + 165, this.getMaxY() + 50, 65, this.font.FONT_HEIGHT + 11, checkNames, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				checkInputName();
			}
		});

		this.buttons.clear();
		this.addButton(userInput);
		this.addButton(send);
		this.addButton(increment);
		this.addButton(decrement);
		this.addButton(back);
		this.addButton(checkName);
		userInput.setFocused2(true);
	}

	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        super.render(mouseX, mouseY, partialTicks);
        drawCenteredString(this.font, "Distress Signal", this.getMinX() + 73, this.getMaxY() + 40, 0xFFFFFF);
		drawCenteredString(this.font, "Select Timeship Owner:", this.getMinX() + 75, this.getMaxY() + 76, 0xFFFFFF);
		if (this.hasCheckedName) {
			if (!this.isValid && !this.userInput.isFocused()) {
				drawCenteredString(this.font,"Invalid user:", this.getMinX() + 197, this.getMaxY() + 75, 0xFF0000);
				String user = userInput.getText();
				drawCenteredString(this.font, userInput.getText().isEmpty() || userInput.getText() == null ? "Null" : user, this.getMinX() + 196, this.getMaxY() + 85, 0xffcc00);
			}
			else if (this.isValid && !this.userInput.getText().isEmpty() && !this.userInput.isFocused()){
				drawCenteredString(this.font,"User Found!", this.getMinX() + 197, this.getMaxY() + 75, 0x00FF00);
			}
		}
    }

	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}

	@Override
	public void onClose() {
        super.onClose();
	}

	@Override
	public boolean isPauseScreen() {
	   return true;
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
		if (this.userInput.isFocused()) {
			if (keyCode == GLFW.GLFW_KEY_TAB) {
				incrementList();
			}
			super.keyPressed(keyCode, scanCode, bitmaskModifier);
			return true;
		}
		return super.keyPressed(keyCode, scanCode, bitmaskModifier);
	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		if (!this.checkName.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_))
			this.hasCheckedName = false;
		return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
	}

	@Override
	public void renderBackground() {
		super.renderBackground();
	}

	@Override
	public int texWidth() {
		return super.texWidth();
	}

	@Override
	public int texHeight() {
		return super.texHeight();
	}

	@Override
	public int getMinY() {
		return super.getMinY();
	}

	@Override
	public int getMinX() {
		return super.getMinX();
	}

	@Override
	public int getMaxX() {
		return super.getMaxX();
	}

	@Override
	public int getMaxY() {
		return super.getMaxY();
	}

	public void incrementList() {
		if (index + 1 > UUIDs.size() - 1) {
			index = 0;
        } else {
            ++index;
        }
		this.selectedUUID = UUIDs.get(index);
		this.userInput.setText(UsernameCache.getLastKnownUsername(selectedUUID));
		this.userInput.setCursorPositionZero();
		this.isValid = true;
	}

	public void decrementList() {
		--index;
		if (index - 1 < 0) {
			index = this.UUIDs.size() - 1;
		}
		this.selectedUUID = UUIDs.get(index);
		this.userInput.setText(UsernameCache.getLastKnownUsername(selectedUUID));
		this.userInput.setCursorPositionZero();
		this.isValid = true;
	}

	public void createPlayerList() {
		this.players = Minecraft.getInstance().getConnection().getPlayerInfoMap();
		this.UUIDs = new ArrayList<>();
		this.players.forEach(player -> this.UUIDs.add(player.getGameProfile().getId()));
	}

	public void checkInputName() {
		NetworkPlayerInfo info = Minecraft.getInstance().getConnection().getPlayerInfo(this.userInput.getText());
		if (info != null && selectedUUID == info.getGameProfile().getId())
			isValid = true;
		else isValid = false;
		this.hasCheckedName = true;
	}

	public String getStringWrapped(String s) {
		int size = 14;
		String temp = s;
		if (!(temp = this.splitString(temp, size)).isEmpty()){
			return s;
		}
		return temp;
	}

	public String splitString(String text, int width) {
		if(font.getStringWidth(text) > width) {
			String line = "";
			for(String word : text.split(" ")) {
				if(font.getStringWidth(word + " ") + font.getStringWidth(line) < width)
					return (line += word + " ");
				else {
					return text.replace(line, "");
				}
			}
		}
		return text;
	}

}
