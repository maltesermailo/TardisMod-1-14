package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TabWidget;
import net.tardis.mod.containers.QuantiscopeSonicContainer;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.items.sonicparts.SonicBasePart.SonicComponentTypes;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.network.packets.SonicPartChangeMessage;
import net.tardis.mod.sonic.ISonicPart.SonicPart;
import net.tardis.mod.sonic.capability.SonicCapability;
import net.tardis.mod.tileentities.QuantiscopeTile.EnumMode;

@SuppressWarnings("deprecation")
public class QuantiscopeSonicScreen extends ContainerScreen<QuantiscopeSonicContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/sonic.png");
	
	private ItemStack emitter = new ItemStack(TItems.SONIC_EMITTER);
	private ItemStack activator = new ItemStack(TItems.SONIC_ACTIVATOR);
	private ItemStack handle = new ItemStack(TItems.SONIC_HANDLE);
	private ItemStack end = new ItemStack(TItems.SONIC_END);
	
	
	public QuantiscopeSonicScreen(QuantiscopeSonicContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void init() {
		super.init();
		
		//Reset parts
		emitter = new ItemStack(TItems.SONIC_EMITTER);
		activator = new ItemStack(TItems.SONIC_ACTIVATOR);
		handle = new ItemStack(TItems.SONIC_HANDLE);
		end = new ItemStack(TItems.SONIC_END);
       
		this.readFromSlot();
		
		this.getContainer().setSlotChangeAction(() -> this.readFromSlot());
		
		//Up Arrow buttons
		this.addSonicButton(63, 71, 214, 1, this.emitter);
		this.addSonicButton(26, 71, 214, 1, this.activator);
		this.addSonicButton(-13, 71, 214, 1, this.handle);
		this.addSonicButton(-52, 71, 214, 1, this.end);
		
		//Down Arrow buttons
		this.addSonicButton(63, 40, 232, -1, this.emitter);
		this.addSonicButton(26, 40, 232, -1, this.activator);
		this.addSonicButton(-13, 40, 232, -1, this.handle);
		this.addSonicButton(-52, 40, 232, -1, this.end);
		
		this.addButton(new TabWidget(guiLeft - 69, guiTop - 20, new ItemStack(TItems.SONIC), but -> {})).isSelected = true;
		this.addButton(new TabWidget(guiLeft - 69, guiTop + 10, new ItemStack(Blocks.CRAFTING_TABLE), but -> {
			Network.sendToServer(new QuantiscopeTabMessage(this.container.pos, EnumMode.WELD));
		}));
		
	}
	
	
	public void readFromSlot() {
		if(this.getContainer().getSlot(0).getStack().getItem() instanceof SonicItem) {
			SonicCapability.getForStack(this.getContainer().getSlot(0).getStack()).ifPresent(cap -> {
				SonicBasePart.setType(this.emitter, cap.getSonicPart(SonicPart.EMITTER));
				SonicBasePart.setType(this.activator, cap.getSonicPart(SonicPart.ACTIVATOR));
				SonicBasePart.setType(this.handle, cap.getSonicPart(SonicPart.HANDLE));
				SonicBasePart.setType(this.end, cap.getSonicPart(SonicPart.END));
			});
		}
	}
	
	public void addSonicButton(int x, int y, int u, int mod, ItemStack stack) {
		this.addButton(new ImageButton(width / 2 - x, height / 2 - y, 14, 7, u, 216, 8, TEXTURE, but -> {
			int id = SonicBasePart.getType(stack) + mod;
			SonicComponentTypes type = SonicComponentTypes.values()[0];
			if(id < SonicComponentTypes.values().length && id >= 0) {
				type = SonicComponentTypes.values()[id];
			}
			else if(id < 0) {
				type = SonicComponentTypes.values()[SonicComponentTypes.values().length - 1];
			}
			update(stack, type);
		}));
	}
	
	
	public void update(ItemStack part, SonicComponentTypes type) {
		SonicBasePart.setType(part, type);
		Network.sendToServer(new SonicPartChangeMessage(part, 0, this.getContainer().pos));
	}
	
	@Override
	public void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(width / 2 - 253 / 2, height / 2 - 212 / 2, 0, 0, 253, 212);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 10, this.guiTop + 24, this.guiLeft + 50, this.guiTop + 39)) {
			this.renderTooltip(this.emitter, mouseX, mouseY);
		}
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 52, this.guiTop + 24, this.guiLeft + 90, this.guiTop + 39)) {
			this.renderTooltip(this.activator, mouseX, mouseY);
		}
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 92, this.guiTop + 24, this.guiLeft + 130, this.guiTop + 39)) {
			this.renderTooltip(this.handle, mouseX, mouseY);
		}
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 130, this.guiTop + 24, this.guiLeft + 170, this.guiTop + 39)) {
			this.renderTooltip(this.end, mouseX, mouseY);
		}
		this.drawCenteredString(Minecraft.getInstance().fontRenderer, "Sonic Customisation", this.guiLeft + 85, this.guiTop - 10, 0xFFFFFF);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.translated(0, 0, 100);
		GlStateManager.rotated(90, 0, 1, 0);
		
		//Emmiter
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 52, 60);
		GlStateManager.scaled(50, 50, 50);
		Minecraft.getInstance().getItemRenderer().renderItem(emitter, TransformType.NONE);
		GlStateManager.popMatrix();
		
		//Acivator
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 52, 82);
		GlStateManager.scaled(50, 50, 50);
		Minecraft.getInstance().getItemRenderer().renderItem(activator, TransformType.NONE);
		GlStateManager.popMatrix();
		
		//Handle
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 52, 100);
		GlStateManager.scaled(50, 50, 50);
		Minecraft.getInstance().getItemRenderer().renderItem(handle, TransformType.NONE);
		GlStateManager.popMatrix();
		
		//End
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 52, 120);
		GlStateManager.scaled(50, 50, 50);
		Minecraft.getInstance().getItemRenderer().renderItem(end, TransformType.NONE);
		GlStateManager.popMatrix();
		
		RenderHelper.enableStandardItemLighting();
		GlStateManager.popMatrix();
		
	}
}
