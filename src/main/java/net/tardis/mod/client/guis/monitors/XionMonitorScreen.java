package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class XionMonitorScreen extends BaseMonitorScreen{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/xion.png");
	
	@Override
	public void renderMonitor() {
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.renderBackground();
		int texWidth = 239, texHeight = 182;
		this.blit(width / 2 - texWidth / 2, height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);
	}

	@Override
	public int getMinY() {
		return this.height / 2 + 60;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 100;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 200;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 140;
	}

	@Override
	public int getWidth() {
		return 201;
	}

	@Override
	public int getHeight() {
		return 152;
	}
	

}
