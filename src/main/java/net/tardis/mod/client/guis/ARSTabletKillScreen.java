package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ARSDeleteMessage;

public class ARSTabletKillScreen extends Screen {

	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
	public static int WIDTH = 256, HEIGHT = 256;
	
	public static final TranslationTextComponent TITLE = new TranslationTextComponent("screen.tardis.ars_tablet.title");
	public static final TranslationTextComponent CONFIRM = new TranslationTextComponent("screen.tardis.ars_tablet.confirm");
	public static final TranslationTextComponent CLOSE = new TranslationTextComponent("screen.tardis.ars_tablet.close");
	
	private BlockPos killPos = BlockPos.ZERO;
	
	public ARSTabletKillScreen(GuiContext context) {
		super(new StringTextComponent(""));
		this.killPos = ((BlockPosGuiContext)context).pos;
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int width = 256, height = 173;
		this.blit(this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);
		
		String title = TITLE.getFormattedText();
		int titleWidth = font.getStringWidth(title);
		font.drawString(title, this.width / 2 - titleWidth / 2, this.height / 2 - 50, 0xFFFFFF);
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

	@Override
	protected void init() {
		super.init();
		
		this.buttons.clear();
		
		int x = width / 2 - 100, y = height / 2 + 45;
		
		this.addButton(new TextButton(x, y, CONFIRM.getFormattedText(), but -> {
			Network.sendToServer(new ARSDeleteMessage(this.killPos));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		this.addButton(new TextButton(x, y + font.FONT_HEIGHT + 2, CLOSE.getFormattedText(), but -> Minecraft.getInstance().displayGuiScreen(null)));
		
	}

}
