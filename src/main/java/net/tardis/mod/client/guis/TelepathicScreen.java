package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.controls.TelepathicControl;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.Partition;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicMessage;

public class TelepathicScreen extends Screen{

	public static final TranslationTextComponent TITLE = new TranslationTextComponent("gui.tardis.telepathic");
	private static final int MAX_ELEM_PER_PAGE = 10;
	private List<Search> biomeNames = new ArrayList<Search>();
    private List<List<Search>> biomePages = Partition.ofSize(biomeNames, MAX_ELEM_PER_PAGE);
    private int page = 0;
    private TextFieldWidget textFieldWidget = null;
    private String searchTerm = "";
	
	public TelepathicScreen() {
		super(TITLE);
	}

	@Override
    public void init() {
        super.init();
        this.textFieldWidget = new TextFieldWidget(this.font, this.width / 2 - 100, 50, 200, 20, this.textFieldWidget, I18n.format("selectWorld.search"));
        this.children.add(this.textFieldWidget);

        for (Widget b : this.buttons)
            b.active = false;

        this.buttons.clear();

        createFilteredList(searchTerm, true);
        textFieldWidget.setEnabled(true);
    }

    @Override
    public void tick() {
        super.tick();
        textFieldWidget.tick();
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        for (Widget w : this.buttons) {
            w.renderButton(mouseX, mouseY, partialTicks);
        }
        textFieldWidget.render(mouseX, mouseY, partialTicks);
        
        if (this.textFieldWidget.isFocused()) {
			this.textFieldWidget.setSuggestion("");
		}
		else if (this.textFieldWidget.getText().isEmpty()){
			this.textFieldWidget.setSuggestion("Search...");
		}
    }


    public void createFilteredList(String searchTerm, boolean addBackButtons) {
    	
    	for(Widget w : this.buttons) {
    		w.active = false;
    	}
    	
        this.buttons.clear();
        this.biomeNames.clear();
        
        //Spawn
        
        this.biomeNames.add(new Search(new StringTextComponent("World Spawn"), "", Type.SPAWN));
        
        //Add all biomes
        for(Biome b : ForgeRegistries.BIOMES.getValues()) {
        	this.biomeNames.add(new Search(b.getDisplayName(), b.getRegistryName().toString(), Type.BIOME));
        }
        
        //Add structures if have upgrade
        TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
        	if(tile.getControl(TelepathicControl.class).canSeeStructures()){
	        	for(Entry<String, Structure<?>> struc : Feature.STRUCTURES.entrySet()) {
	            	this.biomeNames.add(new Search(new StringTextComponent(struc.getValue().getStructureName()), struc.getKey(), Type.STRUCTURE));
	            }
        	}
        });
        
        //Sort
        this.biomeNames.sort((one, two) -> {
        	return one.trans.getFormattedText().compareToIgnoreCase(two.trans.getFormattedText());
        });

        int index = 0;

        if (!searchTerm.isEmpty()) {
        	biomeNames.removeIf(biome -> !biome.trans.getUnformattedComponentText().toLowerCase().contains(searchTerm.toLowerCase()));

        }

        biomePages = Partition.ofSize(biomeNames, MAX_ELEM_PER_PAGE);

        if (!biomeNames.isEmpty()) {
            for (Search biome : biomePages.get(page)) {
                this.addButton(new TextButton(width / 2 - 50, height / 2 + 50 - (index * font.FONT_HEIGHT + 2), biome.trans.getFormattedText(), but -> {
                    Network.sendToServer(new TelepathicMessage(biome.type, biome.key.toString()));
                    Minecraft.getInstance().displayGuiScreen(null);
                }));
                ++index;
            }
        }

        if (addBackButtons) {
            this.addButton(new Button(width / 2 + 50, height / 2 + 75, 20, 20, ">", but -> mod(1)));
            this.addButton(new Button(width / 2 - 80, height / 2 + 75, 20, 20, "<", but -> mod(-1)));
        }

    }

	public void mod(int m) {
        int pages = biomePages.size();
        if (page + m >= 0 && page + m < pages)
            page += m;
        init();
	}

    @Override
    public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
        if (textFieldWidget.isFocused()) {
            this.page = 0;
            searchTerm = textFieldWidget.getText();
            createFilteredList(searchTerm, true);
        }
        return super.keyReleased(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
        textFieldWidget.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
        return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
    }

    @Override
    public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
        return this.textFieldWidget.charTyped(p_charTyped_1_, p_charTyped_2_);
    }
    
    public static class Search{
    	
    	public String key;
    	public ITextComponent trans;
    	public Type type;
    	
    	public Search(ITextComponent trans, String key, Type type) {
    		this.key = key;
    		this.trans = trans;
    		this.type = type;
    	}
    }
    
    
    public static enum Type{
    	BIOME,
    	STRUCTURE,
    	SPAWN
    }
}
