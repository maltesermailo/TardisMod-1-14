package net.tardis.mod.client.guis.monitors;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ExteriorChangeMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorScreen extends MonitorScreen {

	private List<IExterior> unlockedExteriors = new ArrayList<>();
	private IExterior ext = ExteriorRegistry.STEAMPUNK;
	private int index = 0;
	
	public ExteriorScreen(IMonitorGui monitor, String type) {
		super(monitor, type);
	}

	@Override
	protected void init() {
		super.init();
		this.unlockedExteriors.clear();
		ConsoleTile console = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
		if(console != null) {
			this.unlockedExteriors.addAll(console.getExteriors());
		}
		
		this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
				backTranslation, but -> modIndex(-1)));
		
		this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
				selectTranslation, but -> {
					Network.INSTANCE.sendToServer(new ExteriorChangeMessage(ext.getRegistryName()));
					Minecraft.getInstance().displayGuiScreen(null);
				}));
		
		this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
				nextTranslation, but -> modIndex(1)));

		int temp = 0;
		for(IExterior ext : this.unlockedExteriors) {
			if(ext == console.getExterior())
				index = temp;
			++temp;
		}
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		
		if(this.unlockedExteriors.size() > this.index)
			ext = this.unlockedExteriors.get(index);
		
		if(ext != null) {
			this.minecraft.getTextureManager().bindTexture(ext.getBlueprintTexture());
			
			int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
			
			int width = 100, height = 100;
			
			blit(centerX - width / 2, this.parent.getMaxY(), 0, 0, width, height, width, height);
		}
	}
	
	public int modIndex(int mod) {
		if(index + mod >= this.unlockedExteriors.size())
			return index = 0;
		else if(index + mod < 0)
			return index = this.unlockedExteriors.size() - 1;
		return this.index += mod;
	}
}
