package net.tardis.mod.client.guis.containers;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.AlembicContainer;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicScreen extends ContainerScreen<AlembicContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png");
	AlembicTile tile;
	
	public AlembicScreen(AlembicContainer cont, PlayerInventory inv, ITextComponent titleIn) {
		super(cont, inv, titleIn);
		tile = cont.getAlembic();
	}

	@Override
	public void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		this.minecraft.textureManager.bindTexture(TEXTURE);
		this.blit(width / 2 - 252 / 2, height / 2 - 211 / 2, 0, 0, 252, 211);

		//Water
		int waterScaled = (int)(42 * (1 - (tile.getWaterTank().getFluidAmount() / (double)tile.getWaterTank().getCapacity())));
		blit(width / 2 - 70, height / 2 - 20 - (42 - waterScaled), 132, 213, 18, 42 - waterScaled);
		
		int mercuryScaled = (int)(42 * (1 - (tile.getMercury() / 1000.0)));
		blit(width / 2 + 13, height / 2 - 20 - (42 - mercuryScaled), 154, 213, 18, 42 - mercuryScaled);
		
		//Fire
		if(tile.getMaxBurnTime() > 0) {
			int fireScaled = (int)(14 * (1 - (tile.getBurnTime() / (double)tile.getMaxBurnTime())));
			blit(width / 2 - 39, height / 2 - 31 + fireScaled, 186, 219 + fireScaled, 14, 14 - fireScaled);
		}
		blit(width / 2 - 17, height / 2 - 54, 217, 222, (int)(23 * tile.getPercent()), 18);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

}
