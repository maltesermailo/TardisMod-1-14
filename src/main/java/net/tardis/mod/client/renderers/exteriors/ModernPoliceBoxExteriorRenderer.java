package net.tardis.mod.client.renderers.exteriors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.client.models.exteriors.ModernPoliceBoxExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class ModernPoliceBoxExteriorRenderer extends ExteriorRenderer<ModernPoliceBoxExteriorTile>{
	
	//File path to exterior texture, 
	//happens to be same for interior door
	//the texture file name HAS to be the same as the exterior registry name
	
	private static BotiManager boti = new BotiManager();
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
			"textures/exteriors/modern_police_box.png");
	
	private ModernPoliceBoxExteriorModel model = new ModernPoliceBoxExteriorModel();
	
	
	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}
	
	@Override
	public void renderExterior(ModernPoliceBoxExteriorTile tile) {
		GlStateManager.pushMatrix();
		/*We want to shift the render up in this case 
		so that when we place the exterior two blocks up from the ground,
		the base of the exterior will be rendered as standing on the ground
		*/
		GlStateManager.enableRescaleNormal(); //Ensures model isn't rendered fullbright all the time
		GlStateManager.translated(0, -0.05, 0); // Translation must be negative as models are loaded in upside down.
		GlStateManager.scalef(0.37f, 0.36f, 0.37f); //Scales the model down by 4
		
		this.bindTexture(TEXTURE);
		this.model.render(tile);
		GlStateManager.disableRescaleNormal();
		
		/*Minecraft.getInstance().getFramebuffer().unbindFramebuffer();
		
		PlayerEntity player = Minecraft.getInstance().player;
		
		boti.setupFramebuffer();
		GlStateManager.pushMatrix();
		WorldShell shell = tile.getBotiWorld();
		GlStateManager.loadIdentity();
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(player.rotationYawHead, 0, 1, 0);
		GlStateManager.rotated(-player.rotationPitch, 1, 0, 0);
		GlStateManager.translated(-shell.getOffset().getX(), -shell.getOffset().getY(), -shell.getOffset().getZ());
		GlStateManager.translated(tile.getPos().getX() - player.posX, tile.getPos().getY() - player.posY, tile.getPos().getZ() - player.posZ);
		GlStateManager.translated(0, -0.8, 0);
		boti.renderWorld(tile.getBotiWorld());
		GlStateManager.popMatrix();
		boti.endFBO();
		
		Minecraft.getInstance().getFramebuffer().bindFramebuffer(true);
		
		boti.fbo.bindFramebufferTexture();
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		bb.pos(0, -8, 0).tex(0, 0).endVertex();
		bb.pos(0, -4, 0).tex(0, 1).endVertex();
		bb.pos(4, -4, 0).tex(1, 1).endVertex();
		bb.pos(4, -8, 0).tex(1, 0).endVertex();
		Tessellator.getInstance().draw();
		boti.fbo.unbindFramebufferTexture();
		*/
		GlStateManager.popMatrix();
		
	}

}
