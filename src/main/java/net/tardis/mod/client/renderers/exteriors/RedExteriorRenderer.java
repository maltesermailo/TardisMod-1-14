package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.RedExteriorModel;
import net.tardis.mod.tileentities.exteriors.RedExteriorTile;

public class RedExteriorRenderer extends ExteriorRenderer<RedExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/red.png");
	public static RedExteriorModel MODEL = new RedExteriorModel();
	
	@Override
	public void renderExterior(RedExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -1, 0);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}

}
