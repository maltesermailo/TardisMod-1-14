package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.XionConsoleModel;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class XionConsoleRenderer extends TileEntityRenderer<XionConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/xion.png");
	public static final XionConsoleModel MODEL = new XionConsoleModel();
	
	@Override
	public void render(XionConsoleTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1); //Rotate 180 degrees so model isn't upside down
		if(tileEntityIn.getVariant() != null) //U
			Minecraft.getInstance().getTextureManager().bindTexture(tileEntityIn.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		double scale = 0.225;
		GlStateManager.scaled(scale, scale, scale); //Use a local variable to scale console easier
		GlStateManager.enableRescaleNormal();
		MODEL.render(tileEntityIn, ModelHelper.RENDER_SCALE); //Scale down by 4
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
}
