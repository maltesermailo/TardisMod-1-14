package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ModelSteamConsole;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;

public class SteamConsoleRenderer extends TileEntityRenderer<SteamConsoleTile> {

	public static ModelSteamConsole steam = new ModelSteamConsole();
	public static final ResourceLocation TEXURE = new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png");
	
	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}
	
	@Override
	public void render(SteamConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.scaled(0.3, 0.3, 0.3);
		GlStateManager.enableRescaleNormal();
		if(console.getVariant() != null)
			Minecraft.getInstance().getTextureManager().bindTexture(console.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXURE);
		steam.render(console, 0.0625F);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

}
