package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.ToyotaSpinnyThingModel;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;

public class ToyotaSpinnyTileRenderer extends TileEntityRenderer<ToyotaSpinnyTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/spinny_thing.png");
	public static final ToyotaSpinnyThingModel MODEL = new ToyotaSpinnyThingModel();
	
	@Override
	public void render(ToyotaSpinnyTile tile, double x, double y, double z, float partialTicks,	int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y - 0.25, z + 0.5); //Note: This actually cuts into the top of the Toyota console rotor 
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		this.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
