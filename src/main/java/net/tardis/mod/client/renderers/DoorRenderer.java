package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.interiordoors.IInteriorDoorRenderer;
import net.tardis.mod.client.models.interiordoors.InteriorTrunkModel;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class DoorRenderer extends EntityRenderer<DoorEntity> {

	public static IInteriorDoorRenderer MODEL = new InteriorTrunkModel();
	
	public DoorRenderer(EntityRendererManager manager) {
		super(manager);
	}

	@Override
	public void doRender(DoorEntity entity, double x, double y, double z,float p_76986_8_, float p_76986_9_) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 1.5, z);
		GlStateManager.rotated(entity.rotationYaw - 180, 0, 1, 0);
		if(entity.getHorizontalFacing() == Direction.EAST || entity.getHorizontalFacing() == Direction.WEST)
			GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.translated(0, 0, 0.1);
		
		IDoorType type = EnumDoorType.TRUNK;
		if(entity.doorType != null)
			type = entity.doorType;
		
		Minecraft.getInstance().getTextureManager().bindTexture(type.getInteriorDoorRenderer().getTexture());
		type.getInteriorDoorRenderer().render(entity);
		
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(DoorEntity arg0) {
		return null;
	}
}
