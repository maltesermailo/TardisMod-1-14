package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.TT2020ExteriorModel;
import net.tardis.mod.tileentities.exteriors.TT2020ExteriorTile;

public class TT2020CapsuleExteriorRenderer extends ExteriorRenderer<TT2020ExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/tt2020.png");
	public static final TT2020ExteriorModel MODEL = new TT2020ExteriorModel();
	
	@Override
	public void renderExterior(TT2020ExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.5, 0);
		GlStateManager.translated(0, Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.25, 0);
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}
	
	@Override
	public boolean floatInAir() {
		return true;
	}

}
