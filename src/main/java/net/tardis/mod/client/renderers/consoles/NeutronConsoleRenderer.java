package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.NeutronConsoleModel;
import net.tardis.mod.tileentities.console.NeutronConsoleTile;

public class NeutronConsoleRenderer extends TileEntityRenderer<NeutronConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/neutron_thaumic.png");
	public static final NeutronConsoleModel MODEL = new NeutronConsoleModel();
	
	@Override
	public void render(NeutronConsoleTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translated(x + 0.5, y + 0.375, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.scaled(0.35, 0.35, 0.35);
		this.bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().textureManager.bindTexture(location);
	}

}
