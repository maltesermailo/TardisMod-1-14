package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.PoliceBoxExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class PoliceBoxExteriorRenderer extends ExteriorRenderer<PoliceBoxExteriorTile>{
	
	//File path to exterior texture, 
	//happens to be same for interior door
	//the texture file name HAS to be the same as the exterior registry name
	
	private static BotiManager boti = new BotiManager();
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
			"textures/exteriors/police_box.png"); 
	
	private PoliceBoxExteriorModel model = new PoliceBoxExteriorModel();
	
	
	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}
	
	@Override
	public void renderExterior(PoliceBoxExteriorTile tile) {
		GlStateManager.pushMatrix();
		/*We want to shift the render up in this case 
		so that when we place the exterior two blocks up from the ground,
		the base of the exterior will be rendered as standing on the ground
		*/
		GlStateManager.enableRescaleNormal(); //Ensures model isn't rendered fullbright all the time
		GlStateManager.translated(0, -0.25, 0); //This renders the box upwards on the y axis by 0.25
		GlStateManager.scalef(0.5f, 0.5f, 0.5f); //Scales the model down by 2
		
		this.bindTexture(TEXTURE);
		this.model.render(tile);
		GlStateManager.disableRescaleNormal();
		
		GlStateManager.popMatrix();
		
	}

}
