package net.tardis.mod.client.renderers.entity.transport;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.TardisEntity;

public class TardisEntityRenderer extends EntityRenderer<TardisEntity>{
	
	public TardisEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public void doRender(TardisEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		GlStateManager.rotated(-entity.renderYaw - 180, 0, 1, 0);
		
		if(entity.hasNoGravity())
			GlStateManager.translated(0, Math.sin(entity.ticksExisted * 0.05) * 0.1, 0);
		
		if(entity.getExteriorTile() != null)
			TileEntityRendererDispatcher.instance.
				render(entity.getExteriorTile(), -0.5, 1, -0.5, partialTicks, 0, true);
		
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(TardisEntity entity) {
		return null;
	}

}
