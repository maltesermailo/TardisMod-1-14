package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ClockExteriorModel;
import net.tardis.mod.tileentities.exteriors.ClockExteriorTile;

public class ClockExteriorRenderer extends ExteriorRenderer<ClockExteriorTile> {

	public static ClockExteriorModel model = new ClockExteriorModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/clock.png");
	
	@Override
	public void renderExterior(ClockExteriorTile tile) {
		GlStateManager.pushMatrix();
		this.bindTexture(TEXTURE);
		model.render(tile);
		GlStateManager.popMatrix();
	}
	

}
