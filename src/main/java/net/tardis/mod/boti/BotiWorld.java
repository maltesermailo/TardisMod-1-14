package net.tardis.mod.boti;

import java.util.ArrayList;
import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BotiWorld extends ClientWorld{

	private WorldShell shell;
	private DimensionType type;
	
	public BotiWorld(DimensionType dimType, WorldShell shell, WorldRenderer worldRendererIn) {
		super(Minecraft.getInstance().player.connection, new WorldSettings(Minecraft.getInstance().world.getWorldInfo()), dimType, 10, BotiWorld.PROFILER, worldRendererIn);
		this.shell = shell;
		this.type = dimType;
	}
	
	@Override
	public int getLight(BlockPos pos) {
		return shell.getLightValue(pos);
	}

	@Override
	public Iterable<Entity> getAllEntities() {
		return new ArrayList<Entity>();
	}

	@Override
	public Entity getEntityByID(int id) {
		return null;
	}

	@Override
	public BlockState getBlockState(BlockPos pos) {
		return shell.getBlockState(pos);
	}

	@Override
	public TileEntity getTileEntity(BlockPos pos) {
		return shell.getTileEntity(pos);
	}
	
	public DimensionType getDimensionType() {
		return this.type;
	}

	public static IProfiler PROFILER = new IProfiler() {

		@Override
		public void startTick() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void endTick() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void startSection(String name) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void startSection(Supplier<String> nameSupplier) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void endSection() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void endStartSection(String name) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void endStartSection(Supplier<String> nameSupplier) {
			// TODO Auto-generated method stub
			
		}};

}
