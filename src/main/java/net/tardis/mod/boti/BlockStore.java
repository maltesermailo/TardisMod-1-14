package net.tardis.mod.boti;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockStore {

	private BlockState state = Blocks.AIR.getDefaultState();
	private int lightVal = 0;
	private CompoundNBT tileTag = null;
	
	public BlockStore(BlockState state, int lightValue, CompoundNBT te) {
		this.state = state;
		this.lightVal = lightValue;
		if(te != null)
			this.tileTag = te;
	}
	
	@OnlyIn(Dist.CLIENT)
	public TileEntity getTile() {
		return this.tileTag == null ? null : TileEntity.create(this.tileTag);
	}
	
	public BlockState getState() {
		return this.state;
	}
	
	public int getLight() {
		return this.lightVal;
	}
	
	public CompoundNBT serialize() {
		CompoundNBT nbt = new CompoundNBT();
		nbt.put("state", NBTUtil.writeBlockState(state));
		nbt.putInt("light", lightVal);
		if(this.tileTag != null)
			nbt.put("tile", this.tileTag);
		return nbt;
	}
	
	public static BlockStore deserialize(CompoundNBT tag) {
		CompoundNBT tile = tag.getCompound("tile");
		return new BlockStore(
				NBTUtil.readBlockState(tag.getCompound("state")),
				tag.getInt("light"),
				tile.isEmpty() ? null : tile);
	}
}
