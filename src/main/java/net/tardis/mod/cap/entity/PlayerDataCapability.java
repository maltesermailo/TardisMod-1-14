package net.tardis.mod.cap.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SyncPlayerMessage;

public class PlayerDataCapability implements IPlayerData {

    private PlayerEntity player;
    private SpaceTimeCoord coord = SpaceTimeCoord.UNIVERAL_CENTER;
    private boolean usedTelepathics = false;
    private double displacement;
    private int shakingTicks = 0;
    private float shakeMagnitude = 1F;
    private int countdownTicks = 0;
    private boolean hasFlyedInTardis;
    private DimensionType interiorDimension;

    public PlayerDataCapability(PlayerEntity ent) {
        this.player = ent;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        tag.putBoolean("telepath", this.usedTelepathics);
        tag.putDouble("displacement", this.displacement);
        tag.putInt("shaking_ticks", this.shakingTicks);
        tag.putFloat("shake_magnitude", this.shakeMagnitude);
        tag.putInt("countdown_ticks", this.countdownTicks);
        tag.putBoolean("hasFlyedInTardis", this.hasFlyedInTardis);
        if(this.hasFlyedInTardis && this.interiorDimension != null) {
        	tag.putString("flyingTardisInterior", this.getInteriorDimension().getRegistryName().toString());
        }
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (nbt.contains("coord"))
            this.coord = SpaceTimeCoord.deserialize(nbt.getCompound("coord"));
    	this.usedTelepathics = nbt.getBoolean("telepath");
    	this.displacement = nbt.getDouble("displacement");
    	this.shakingTicks = nbt.getInt("shaking_ticks");
    	this.hasFlyedInTardis = nbt.getBoolean("hasFlyedInTardis");
    	if(this.hasFlyedInTardis && nbt.contains("flyingTardisInterior") && !nbt.getString("flyingTardisInterior").isEmpty()) {
    		this.interiorDimension = DimensionType.byName(new ResourceLocation(nbt.getString("flyingTardisInterior")));
    	}
    	if(nbt.contains("shake_magnitude"))
    		this.shakeMagnitude = nbt.getFloat("shake_magnitude");
    	this.countdownTicks = nbt.getInt("countdown_ticks");

    }

    @Override
    public SpaceTimeCoord getDestination() {
        return coord;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        this.coord = coord;
    }

    @Override
	public void setHasUsedTelepathics(boolean used) {
		this.usedTelepathics = used;
	}

	@Override
	public boolean hasUsedTelepathics() {
		return this.usedTelepathics;
	}
	
    @Override
    public void tick() {
        if (player instanceof ServerPlayerEntity) {
        	ServerPlayerEntity serverPlayer = (ServerPlayerEntity)player;
            if (serverPlayer.dimension != null && serverPlayer.dimension.getModType() == TDimensions.VORTEX) {
                if (serverPlayer.posY < 0) {
                    ServerWorld world = player.world.getServer().getWorld(DimensionType.byName(coord.getDimType()));
                    if (world != null) {
                	  if (world.getDimension().getType() == TDimensions.VORTEX_TYPE) {
                		  ServerWorld worldDest = player.world.getServer().getWorld(DimensionType.OVERWORLD);
                		  world = worldDest; //Set dimension to Overworld if they try to teleport whilst in the vortex
                      }
                    BlockPos pos = coord.getPos();
                    serverPlayer.fallDistance = 0;
                    serverPlayer.teleport(world, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0);
                    }
                }
            }
        }
        else {//Client- only stuff
        	//Shake dat ass
        	if(this.shakingTicks > 0  && TConfig.CLIENT.interiorShake.get()) {
        		float amt = this.shakeMagnitude / 2.0F;
        		player.rotationYaw += ((player.world.rand.nextFloat() * this.shakeMagnitude) - amt);
        		player.rotationPitch += ((player.world.rand.nextFloat() * this.shakeMagnitude) - amt);
        		--shakingTicks;
        	}
        }
        if(this.countdownTicks > 0)//Decrease countdown ticks on common to allow for sanity checks to be made in other objects
    		--this.countdownTicks;
    }

	@Override
	public double getDisplacement() {
		return this.displacement;
	}

	@Override
	public void calcDisplacement(BlockPos start, BlockPos finish) {
		this.displacement = Math.sqrt(start.distanceSq(finish));
	}

	@Override
	public void setShaking(int shakingTicks) {
		this.setShaking(shakingTicks, 1F);
	}
	
	@Override
	public void setShaking(int shakingTicks, float magnitude) {
		this.shakingTicks = shakingTicks;
		this.shakeMagnitude = magnitude;
	}

	@Override
	public int getShaking() {
		return this.shakingTicks;
	}
	
	@Override
	public void update() {
		if(!player.world.isRemote)
			Network.sendTo(new SyncPlayerMessage(player.getEntityId(), this.serializeNBT()), (ServerPlayerEntity)player);
	}

	@Override
	public void startCountdown(int time) {
		this.countdownTicks = time;
	}

	@Override
	public int getCountdown() {
		return this.countdownTicks;
	}

	@Override
	public void setFlyingInTardis(boolean flying) {
		this.hasFlyedInTardis = flying;
	}

	@Override
	public boolean hasFlyedInTardis() {
		return this.hasFlyedInTardis;
	}

	@Override
	public void setInteriorDimension(DimensionType type) {
		this.interiorDimension = type;
	}

	@Override
	public DimensionType getInteriorDimension() {
		return this.interiorDimension;
	}
}
