package net.tardis.mod.cap;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisWorldCapability implements ITardisWorldData{

	private World world;
	
	public PanelInventory northInv;
	public PanelInventory eastInv;
	public PanelInventory southInv;
	public PanelInventory westInv;
	
	private TardisEnergy power;
	
	public TardisWorldCapability(World world) {
		this.northInv = new PanelInventory(Direction.NORTH);
        this.eastInv = new PanelInventory(Direction.EAST);
        this.southInv = new PanelInventory(Direction.SOUTH);
        this.westInv = new PanelInventory(Direction.WEST);
        this.power = new TardisEnergy(2147483646);
		this.world = world;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("north_inv", northInv.serializeNBT());
		tag.put("east_inv", eastInv.serializeNBT());
		tag.put("south_inv", southInv.serializeNBT());
		tag.put("west_inv", westInv.serializeNBT());
		tag.put("power", this.power.serialize());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		northInv.deserializeNBT(tag.getList("north_inv", Constants.NBT.TAG_COMPOUND));
		eastInv.deserializeNBT(tag.getList("east_inv", Constants.NBT.TAG_COMPOUND));
		southInv.deserializeNBT(tag.getList("south_inv", Constants.NBT.TAG_COMPOUND));
		westInv.deserializeNBT(tag.getList("west_inv", Constants.NBT.TAG_COMPOUND));
		power.deserialize((IntNBT)tag.get("power"));
	}

	@Override
	public PanelInventory getEngineInventoryForSide(Direction dir) {
		switch(dir) {
			case EAST: return eastInv;
			case SOUTH: return southInv;
			case WEST: return westInv;
			default: return northInv;
		}
	}
	
	@Override
	public void tick() {
		
		//Artron charging
		if(!world.isRemote && world.getGameTime() % 200 == 0) {
			for(int index = 0; index < eastInv.getSizeInventory(); ++index) {
				ItemStack slotStack = eastInv.getStackInSlot(index);
				if(slotStack.getItem() instanceof IArtronBattery) {
					((IArtronBattery)slotStack.getItem())
						.charge(slotStack, 10);
				}
			}
		}
	}

	@Override
	public TardisEnergy getEnergy() {
		return this.power;
	}

}
