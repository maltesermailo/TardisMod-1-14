package net.tardis.mod.events;

import net.minecraft.entity.LivingEntity;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.Cancelable;

public class LivingEvents {

	
	@Cancelable
	public static class SpaceAir extends LivingEvent{

		public SpaceAir(LivingEntity entity) {
			super(entity);
		}
		
	}
}
