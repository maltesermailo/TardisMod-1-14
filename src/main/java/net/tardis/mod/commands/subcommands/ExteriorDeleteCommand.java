package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.BlockPosArgument;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ExteriorDeleteCommand extends TCommand{

	public static ExteriorDeleteCommand INSTANCE = new ExteriorDeleteCommand(PermissionEnum.EXTERIOR);
	
	public ExteriorDeleteCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		try {
			BlockPos pos = BlockPosArgument.getLoadedBlockPos(context, "position");
			PlayerEntity player = context.getSource().asPlayer();
			ExteriorTile tile = ((ExteriorTile)player.world.getTileEntity(pos));
			if (tile != null) {
				tile.demat();
				context.getSource().sendFeedback(new TranslationTextComponent("message.tardis.remove_exterior_command", Helper.formatBlockPos(pos)), false);
			}
			else {
				context.getSource().sendErrorMessage(new TranslationTextComponent("message.tardis.remove_exterior_command.error",Helper.formatBlockPos(pos)));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return Command.SINGLE_SUCCESS;
	}
	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("remove_exterior")
				.then(Commands.argument("position", BlockPosArgument.blockPos())
				.executes(INSTANCE));
	}

}
