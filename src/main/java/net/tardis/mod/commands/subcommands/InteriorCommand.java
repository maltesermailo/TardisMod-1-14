package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exceptions.NoDimensionFoundException;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;

public class InteriorCommand extends TCommand {
    private static final InteriorCommand CMD = new InteriorCommand();

    public InteriorCommand() {
        super(PermissionEnum.INTERIOR);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("interior").executes(CMD)
                .then(Commands.argument("username", StringArgumentType.string())
                        .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .executes(CMD));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        CommandSource source = context.getSource();

        if (canExecute(source)) {
            ServerPlayerEntity sourcePlayer;
            
            try {
            	sourcePlayer = source.getServer().getPlayerList().getPlayerByUsername(context.getArgument("username", String.class));
            }
            catch(Exception e) {
            	sourcePlayer = source.asPlayer();
            }
            
            DimensionType consoleDimension = TardisHelper.getTardisByUUID(sourcePlayer.getUniqueID());
            TeleportUtil.teleportEntity(sourcePlayer, consoleDimension, TardisHelper.TARDIS_POS.getX() + 2, TardisHelper.TARDIS_POS.getY(), TardisHelper.TARDIS_POS.getZ());
            source.sendFeedback(new TranslationTextComponent("message.tardis.interior_command", sourcePlayer.getDisplayName()), true);
        }
        else {
            source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        }

        return Command.SINGLE_SUCCESS;
    }

}
