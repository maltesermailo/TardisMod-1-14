package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraftforge.server.permission.PermissionAPI;
import net.tardis.mod.commands.permissions.PermissionEnum;

public abstract class TCommand implements Command<CommandSource> {

    private PermissionEnum permission;

    public TCommand(PermissionEnum permission) {
        this.permission = permission;
    }

    public boolean canExecute(CommandSource source) throws CommandSyntaxException {
        return PermissionAPI.hasPermission(source.asPlayer(), permission.getNode());
    }
}
